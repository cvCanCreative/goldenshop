package id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public class AppDbKrenjangByToko_Impl extends AppDbKrenjangByToko {
  private volatile DAOKranjangByToko _dAOKranjangByToko;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `produkByToko` (`ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idProduk` INTEGER NOT NULL, `idToko` INTEGER NOT NULL, `jumlahProduk` INTEGER NOT NULL, `namaToko` TEXT, `pilih` INTEGER)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"71362cbdc2376a9572c3645640c4d95b\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `produkByToko`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsProdukByToko = new HashMap<String, TableInfo.Column>(6);
        _columnsProdukByToko.put("ID", new TableInfo.Column("ID", "INTEGER", true, 1));
        _columnsProdukByToko.put("idProduk", new TableInfo.Column("idProduk", "INTEGER", true, 0));
        _columnsProdukByToko.put("idToko", new TableInfo.Column("idToko", "INTEGER", true, 0));
        _columnsProdukByToko.put("jumlahProduk", new TableInfo.Column("jumlahProduk", "INTEGER", true, 0));
        _columnsProdukByToko.put("namaToko", new TableInfo.Column("namaToko", "TEXT", false, 0));
        _columnsProdukByToko.put("pilih", new TableInfo.Column("pilih", "INTEGER", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysProdukByToko = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesProdukByToko = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoProdukByToko = new TableInfo("produkByToko", _columnsProdukByToko, _foreignKeysProdukByToko, _indicesProdukByToko);
        final TableInfo _existingProdukByToko = TableInfo.read(_db, "produkByToko");
        if (! _infoProdukByToko.equals(_existingProdukByToko)) {
          throw new IllegalStateException("Migration didn't properly handle produkByToko(id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.TbKranjangByToko).\n"
                  + " Expected:\n" + _infoProdukByToko + "\n"
                  + " Found:\n" + _existingProdukByToko);
        }
      }
    }, "71362cbdc2376a9572c3645640c4d95b", "545ababc233d8f04b224e883409c7c0d");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "produkByToko");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `produkByToko`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public DAOKranjangByToko daoKranjang() {
    if (_dAOKranjangByToko != null) {
      return _dAOKranjangByToko;
    } else {
      synchronized(this) {
        if(_dAOKranjangByToko == null) {
          _dAOKranjangByToko = new DAOKranjangByToko_Impl(this);
        }
        return _dAOKranjangByToko;
      }
    }
  }
}
