package id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;

@SuppressWarnings("unchecked")
public class DAOKranjangByToko_Impl implements DAOKranjangByToko {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfTbKranjangByToko;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfTbKranjangByToko;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfTbKranjangByToko;

  private final SharedSQLiteStatement __preparedStmtOfUpdateJumlah;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAll;

  private final SharedSQLiteStatement __preparedStmtOfDeleteById;

  public DAOKranjangByToko_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfTbKranjangByToko = new EntityInsertionAdapter<TbKranjangByToko>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `produkByToko`(`ID`,`idProduk`,`idToko`,`jumlahProduk`,`namaToko`,`pilih`) VALUES (nullif(?, 0),?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, TbKranjangByToko value) {
        stmt.bindLong(1, value.ID);
        stmt.bindLong(2, value.idProduk);
        stmt.bindLong(3, value.idToko);
        stmt.bindLong(4, value.jumlahProduk);
        if (value.namaToko == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.namaToko);
        }
        final Integer _tmp;
        _tmp = value.pilih == null ? null : (value.pilih ? 1 : 0);
        if (_tmp == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindLong(6, _tmp);
        }
      }
    };
    this.__deletionAdapterOfTbKranjangByToko = new EntityDeletionOrUpdateAdapter<TbKranjangByToko>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `produkByToko` WHERE `ID` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, TbKranjangByToko value) {
        stmt.bindLong(1, value.ID);
      }
    };
    this.__updateAdapterOfTbKranjangByToko = new EntityDeletionOrUpdateAdapter<TbKranjangByToko>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `produkByToko` SET `ID` = ?,`idProduk` = ?,`idToko` = ?,`jumlahProduk` = ?,`namaToko` = ?,`pilih` = ? WHERE `ID` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, TbKranjangByToko value) {
        stmt.bindLong(1, value.ID);
        stmt.bindLong(2, value.idProduk);
        stmt.bindLong(3, value.idToko);
        stmt.bindLong(4, value.jumlahProduk);
        if (value.namaToko == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.namaToko);
        }
        final Integer _tmp;
        _tmp = value.pilih == null ? null : (value.pilih ? 1 : 0);
        if (_tmp == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindLong(6, _tmp);
        }
        stmt.bindLong(7, value.ID);
      }
    };
    this.__preparedStmtOfUpdateJumlah = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE produkByToko SET jumlahProduk =? WHERE idToko = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM produkByToko";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteById = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM produkByToko WHERE idToko = ?";
        return _query;
      }
    };
  }

  @Override
  public long insertBarang(TbKranjangByToko barang) {
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfTbKranjangByToko.insertAndReturnId(barang);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int deleteBarang(TbKranjangByToko barang) {
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__deletionAdapterOfTbKranjangByToko.handle(barang);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int updateBarang(TbKranjangByToko barang) {
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__updateAdapterOfTbKranjangByToko.handle(barang);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int updateJumlah(int id, int jumlahBarangs) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateJumlah.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, jumlahBarangs);
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      final int _result = _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateJumlah.release(_stmt);
    }
  }

  @Override
  public int deleteAll() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAll.acquire();
    __db.beginTransaction();
    try {
      final int _result = _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAll.release(_stmt);
    }
  }

  @Override
  public int deleteById(int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteById.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, id);
      final int _result = _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteById.release(_stmt);
    }
  }

  @Override
  public TbKranjangByToko[] selectAllBarangs() {
    final String _sql = "SELECT * FROM produkByToko";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfID = _cursor.getColumnIndexOrThrow("ID");
      final int _cursorIndexOfIdProduk = _cursor.getColumnIndexOrThrow("idProduk");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("idToko");
      final int _cursorIndexOfJumlahProduk = _cursor.getColumnIndexOrThrow("jumlahProduk");
      final int _cursorIndexOfNamaToko = _cursor.getColumnIndexOrThrow("namaToko");
      final int _cursorIndexOfPilih = _cursor.getColumnIndexOrThrow("pilih");
      final TbKranjangByToko[] _result = new TbKranjangByToko[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final TbKranjangByToko _item;
        _item = new TbKranjangByToko();
        _item.ID = _cursor.getInt(_cursorIndexOfID);
        _item.idProduk = _cursor.getInt(_cursorIndexOfIdProduk);
        _item.idToko = _cursor.getInt(_cursorIndexOfIdToko);
        _item.jumlahProduk = _cursor.getInt(_cursorIndexOfJumlahProduk);
        _item.namaToko = _cursor.getString(_cursorIndexOfNamaToko);
        final Integer _tmp;
        if (_cursor.isNull(_cursorIndexOfPilih)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(_cursorIndexOfPilih);
        }
        _item.pilih = _tmp == null ? null : _tmp != 0;
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public TbKranjangByToko selectProdukId(int id) {
    final String _sql = "SELECT * FROM produkByToko WHERE idToko = ? LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfID = _cursor.getColumnIndexOrThrow("ID");
      final int _cursorIndexOfIdProduk = _cursor.getColumnIndexOrThrow("idProduk");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("idToko");
      final int _cursorIndexOfJumlahProduk = _cursor.getColumnIndexOrThrow("jumlahProduk");
      final int _cursorIndexOfNamaToko = _cursor.getColumnIndexOrThrow("namaToko");
      final int _cursorIndexOfPilih = _cursor.getColumnIndexOrThrow("pilih");
      final TbKranjangByToko _result;
      if(_cursor.moveToFirst()) {
        _result = new TbKranjangByToko();
        _result.ID = _cursor.getInt(_cursorIndexOfID);
        _result.idProduk = _cursor.getInt(_cursorIndexOfIdProduk);
        _result.idToko = _cursor.getInt(_cursorIndexOfIdToko);
        _result.jumlahProduk = _cursor.getInt(_cursorIndexOfJumlahProduk);
        _result.namaToko = _cursor.getString(_cursorIndexOfNamaToko);
        final Integer _tmp;
        if (_cursor.isNull(_cursorIndexOfPilih)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(_cursorIndexOfPilih);
        }
        _result.pilih = _tmp == null ? null : _tmp != 0;
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public TbKranjangByToko selectProdukIdList(int id) {
    final String _sql = "SELECT * FROM produkByToko WHERE idToko = ? LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfID = _cursor.getColumnIndexOrThrow("ID");
      final int _cursorIndexOfIdProduk = _cursor.getColumnIndexOrThrow("idProduk");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("idToko");
      final int _cursorIndexOfJumlahProduk = _cursor.getColumnIndexOrThrow("jumlahProduk");
      final int _cursorIndexOfNamaToko = _cursor.getColumnIndexOrThrow("namaToko");
      final int _cursorIndexOfPilih = _cursor.getColumnIndexOrThrow("pilih");
      final TbKranjangByToko _result;
      if(_cursor.moveToFirst()) {
        _result = new TbKranjangByToko();
        _result.ID = _cursor.getInt(_cursorIndexOfID);
        _result.idProduk = _cursor.getInt(_cursorIndexOfIdProduk);
        _result.idToko = _cursor.getInt(_cursorIndexOfIdToko);
        _result.jumlahProduk = _cursor.getInt(_cursorIndexOfJumlahProduk);
        _result.namaToko = _cursor.getString(_cursorIndexOfNamaToko);
        final Integer _tmp;
        if (_cursor.isNull(_cursorIndexOfPilih)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(_cursorIndexOfPilih);
        }
        _result.pilih = _tmp == null ? null : _tmp != 0;
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
