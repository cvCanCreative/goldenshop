package id.co.cancreative.goldenshop.SQLiteTable.keranjang;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public class AppDbKrenjang_Impl extends AppDbKrenjang {
  private volatile DAOKranjang _dAOKranjang;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `produk` (`barangId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idProduk` INTEGER NOT NULL, `idToko` INTEGER NOT NULL, `nama` TEXT, `catatan` TEXT, `kodePromo` TEXT, `promoPotongan` TEXT, `promoMin` TEXT, `promoStatus` TEXT, `promoExp` TEXT, `id_kategori` TEXT, `nama_kategori` TEXT, `harga` INTEGER NOT NULL, `jumlah_pesan` INTEGER NOT NULL, `stok` INTEGER NOT NULL, `berat` INTEGER NOT NULL, `ongkir` INTEGER NOT NULL, `kurir` TEXT, `durasiPengiriman` TEXT, `jenisKurir` TEXT, `sku` TEXT, `kondisi` TEXT, `gambar` TEXT, `penjual` TEXT, `pilih` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"514f3712c4ce2bc0b6a5360e5b712abc\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `produk`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsProduk = new HashMap<String, TableInfo.Column>(25);
        _columnsProduk.put("barangId", new TableInfo.Column("barangId", "INTEGER", true, 1));
        _columnsProduk.put("idProduk", new TableInfo.Column("idProduk", "INTEGER", true, 0));
        _columnsProduk.put("idToko", new TableInfo.Column("idToko", "INTEGER", true, 0));
        _columnsProduk.put("nama", new TableInfo.Column("nama", "TEXT", false, 0));
        _columnsProduk.put("catatan", new TableInfo.Column("catatan", "TEXT", false, 0));
        _columnsProduk.put("kodePromo", new TableInfo.Column("kodePromo", "TEXT", false, 0));
        _columnsProduk.put("promoPotongan", new TableInfo.Column("promoPotongan", "TEXT", false, 0));
        _columnsProduk.put("promoMin", new TableInfo.Column("promoMin", "TEXT", false, 0));
        _columnsProduk.put("promoStatus", new TableInfo.Column("promoStatus", "TEXT", false, 0));
        _columnsProduk.put("promoExp", new TableInfo.Column("promoExp", "TEXT", false, 0));
        _columnsProduk.put("id_kategori", new TableInfo.Column("id_kategori", "TEXT", false, 0));
        _columnsProduk.put("nama_kategori", new TableInfo.Column("nama_kategori", "TEXT", false, 0));
        _columnsProduk.put("harga", new TableInfo.Column("harga", "INTEGER", true, 0));
        _columnsProduk.put("jumlah_pesan", new TableInfo.Column("jumlah_pesan", "INTEGER", true, 0));
        _columnsProduk.put("stok", new TableInfo.Column("stok", "INTEGER", true, 0));
        _columnsProduk.put("berat", new TableInfo.Column("berat", "INTEGER", true, 0));
        _columnsProduk.put("ongkir", new TableInfo.Column("ongkir", "INTEGER", true, 0));
        _columnsProduk.put("kurir", new TableInfo.Column("kurir", "TEXT", false, 0));
        _columnsProduk.put("durasiPengiriman", new TableInfo.Column("durasiPengiriman", "TEXT", false, 0));
        _columnsProduk.put("jenisKurir", new TableInfo.Column("jenisKurir", "TEXT", false, 0));
        _columnsProduk.put("sku", new TableInfo.Column("sku", "TEXT", false, 0));
        _columnsProduk.put("kondisi", new TableInfo.Column("kondisi", "TEXT", false, 0));
        _columnsProduk.put("gambar", new TableInfo.Column("gambar", "TEXT", false, 0));
        _columnsProduk.put("penjual", new TableInfo.Column("penjual", "TEXT", false, 0));
        _columnsProduk.put("pilih", new TableInfo.Column("pilih", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysProduk = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesProduk = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoProduk = new TableInfo("produk", _columnsProduk, _foreignKeysProduk, _indicesProduk);
        final TableInfo _existingProduk = TableInfo.read(_db, "produk");
        if (! _infoProduk.equals(_existingProduk)) {
          throw new IllegalStateException("Migration didn't properly handle produk(id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang).\n"
                  + " Expected:\n" + _infoProduk + "\n"
                  + " Found:\n" + _existingProduk);
        }
      }
    }, "514f3712c4ce2bc0b6a5360e5b712abc", "752a45c9b9acd047f6b76a140e007af4");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "produk");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `produk`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public DAOKranjang daoKranjang() {
    if (_dAOKranjang != null) {
      return _dAOKranjang;
    } else {
      synchronized(this) {
        if(_dAOKranjang == null) {
          _dAOKranjang = new DAOKranjang_Impl(this);
        }
        return _dAOKranjang;
      }
    }
  }
}
