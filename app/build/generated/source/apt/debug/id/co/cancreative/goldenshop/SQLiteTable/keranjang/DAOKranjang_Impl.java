package id.co.cancreative.goldenshop.SQLiteTable.keranjang;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;

@SuppressWarnings("unchecked")
public class DAOKranjang_Impl implements DAOKranjang {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfTbKranjang;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfTbKranjang;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfTbKranjang;

  private final SharedSQLiteStatement __preparedStmtOfUpdateJumlah;

  private final SharedSQLiteStatement __preparedStmtOfUpdateStatus;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAll;

  private final SharedSQLiteStatement __preparedStmtOfDeleteById;

  public DAOKranjang_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfTbKranjang = new EntityInsertionAdapter<TbKranjang>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `produk`(`barangId`,`idProduk`,`idToko`,`nama`,`catatan`,`kodePromo`,`promoPotongan`,`promoMin`,`promoStatus`,`promoExp`,`id_kategori`,`nama_kategori`,`harga`,`jumlah_pesan`,`stok`,`berat`,`ongkir`,`kurir`,`durasiPengiriman`,`jenisKurir`,`sku`,`kondisi`,`gambar`,`penjual`,`pilih`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, TbKranjang value) {
        stmt.bindLong(1, value.barangId);
        stmt.bindLong(2, value.idProduk);
        stmt.bindLong(3, value.idToko);
        if (value.nama == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.nama);
        }
        if (value.catatan == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.catatan);
        }
        if (value.kodePromo == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.kodePromo);
        }
        if (value.promoPotongan == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.promoPotongan);
        }
        if (value.promoMin == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.promoMin);
        }
        if (value.promoStatus == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.promoStatus);
        }
        if (value.kodeExp == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.kodeExp);
        }
        if (value.id_kategori == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.id_kategori);
        }
        if (value.nama_kategori == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.nama_kategori);
        }
        stmt.bindLong(13, value.harga);
        stmt.bindLong(14, value.jumlahPesan);
        stmt.bindLong(15, value.stok);
        stmt.bindLong(16, value.berat);
        stmt.bindLong(17, value.ongkir);
        if (value.kurir == null) {
          stmt.bindNull(18);
        } else {
          stmt.bindString(18, value.kurir);
        }
        if (value.durasiPengiriman == null) {
          stmt.bindNull(19);
        } else {
          stmt.bindString(19, value.durasiPengiriman);
        }
        if (value.jenisKurir == null) {
          stmt.bindNull(20);
        } else {
          stmt.bindString(20, value.jenisKurir);
        }
        if (value.sku == null) {
          stmt.bindNull(21);
        } else {
          stmt.bindString(21, value.sku);
        }
        if (value.kondisi == null) {
          stmt.bindNull(22);
        } else {
          stmt.bindString(22, value.kondisi);
        }
        if (value.gambar == null) {
          stmt.bindNull(23);
        } else {
          stmt.bindString(23, value.gambar);
        }
        if (value.penjual == null) {
          stmt.bindNull(24);
        } else {
          stmt.bindString(24, value.penjual);
        }
        final int _tmp;
        _tmp = value.pilih ? 1 : 0;
        stmt.bindLong(25, _tmp);
      }
    };
    this.__deletionAdapterOfTbKranjang = new EntityDeletionOrUpdateAdapter<TbKranjang>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `produk` WHERE `barangId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, TbKranjang value) {
        stmt.bindLong(1, value.barangId);
      }
    };
    this.__updateAdapterOfTbKranjang = new EntityDeletionOrUpdateAdapter<TbKranjang>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `produk` SET `barangId` = ?,`idProduk` = ?,`idToko` = ?,`nama` = ?,`catatan` = ?,`kodePromo` = ?,`promoPotongan` = ?,`promoMin` = ?,`promoStatus` = ?,`promoExp` = ?,`id_kategori` = ?,`nama_kategori` = ?,`harga` = ?,`jumlah_pesan` = ?,`stok` = ?,`berat` = ?,`ongkir` = ?,`kurir` = ?,`durasiPengiriman` = ?,`jenisKurir` = ?,`sku` = ?,`kondisi` = ?,`gambar` = ?,`penjual` = ?,`pilih` = ? WHERE `barangId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, TbKranjang value) {
        stmt.bindLong(1, value.barangId);
        stmt.bindLong(2, value.idProduk);
        stmt.bindLong(3, value.idToko);
        if (value.nama == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.nama);
        }
        if (value.catatan == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.catatan);
        }
        if (value.kodePromo == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.kodePromo);
        }
        if (value.promoPotongan == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.promoPotongan);
        }
        if (value.promoMin == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.promoMin);
        }
        if (value.promoStatus == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.promoStatus);
        }
        if (value.kodeExp == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.kodeExp);
        }
        if (value.id_kategori == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.id_kategori);
        }
        if (value.nama_kategori == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.nama_kategori);
        }
        stmt.bindLong(13, value.harga);
        stmt.bindLong(14, value.jumlahPesan);
        stmt.bindLong(15, value.stok);
        stmt.bindLong(16, value.berat);
        stmt.bindLong(17, value.ongkir);
        if (value.kurir == null) {
          stmt.bindNull(18);
        } else {
          stmt.bindString(18, value.kurir);
        }
        if (value.durasiPengiriman == null) {
          stmt.bindNull(19);
        } else {
          stmt.bindString(19, value.durasiPengiriman);
        }
        if (value.jenisKurir == null) {
          stmt.bindNull(20);
        } else {
          stmt.bindString(20, value.jenisKurir);
        }
        if (value.sku == null) {
          stmt.bindNull(21);
        } else {
          stmt.bindString(21, value.sku);
        }
        if (value.kondisi == null) {
          stmt.bindNull(22);
        } else {
          stmt.bindString(22, value.kondisi);
        }
        if (value.gambar == null) {
          stmt.bindNull(23);
        } else {
          stmt.bindString(23, value.gambar);
        }
        if (value.penjual == null) {
          stmt.bindNull(24);
        } else {
          stmt.bindString(24, value.penjual);
        }
        final int _tmp;
        _tmp = value.pilih ? 1 : 0;
        stmt.bindLong(25, _tmp);
        stmt.bindLong(26, value.barangId);
      }
    };
    this.__preparedStmtOfUpdateJumlah = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE produk SET jumlah_pesan =? WHERE idProduk = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdateStatus = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE produk SET pilih =? WHERE idProduk = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM produk";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteById = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM produk WHERE idProduk = ?";
        return _query;
      }
    };
  }

  @Override
  public long insertBarang(TbKranjang barang) {
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfTbKranjang.insertAndReturnId(barang);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int deleteBarang(TbKranjang barang) {
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__deletionAdapterOfTbKranjang.handle(barang);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int updateBarang(TbKranjang barang) {
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__updateAdapterOfTbKranjang.handle(barang);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int updateJumlah(int id, int jumlahBarangs) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateJumlah.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, jumlahBarangs);
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      final int _result = _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateJumlah.release(_stmt);
    }
  }

  @Override
  public int updateStatus(int id, boolean status) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateStatus.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      final int _tmp;
      _tmp = status ? 1 : 0;
      _stmt.bindLong(_argIndex, _tmp);
      _argIndex = 2;
      _stmt.bindLong(_argIndex, id);
      final int _result = _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateStatus.release(_stmt);
    }
  }

  @Override
  public int deleteAll() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAll.acquire();
    __db.beginTransaction();
    try {
      final int _result = _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAll.release(_stmt);
    }
  }

  @Override
  public int deleteById(int id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteById.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, id);
      final int _result = _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteById.release(_stmt);
    }
  }

  @Override
  public TbKranjang[] selectAllBarangs() {
    final String _sql = "SELECT * FROM produk";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfBarangId = _cursor.getColumnIndexOrThrow("barangId");
      final int _cursorIndexOfIdProduk = _cursor.getColumnIndexOrThrow("idProduk");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("idToko");
      final int _cursorIndexOfNama = _cursor.getColumnIndexOrThrow("nama");
      final int _cursorIndexOfCatatan = _cursor.getColumnIndexOrThrow("catatan");
      final int _cursorIndexOfKodePromo = _cursor.getColumnIndexOrThrow("kodePromo");
      final int _cursorIndexOfPromoPotongan = _cursor.getColumnIndexOrThrow("promoPotongan");
      final int _cursorIndexOfPromoMin = _cursor.getColumnIndexOrThrow("promoMin");
      final int _cursorIndexOfPromoStatus = _cursor.getColumnIndexOrThrow("promoStatus");
      final int _cursorIndexOfKodeExp = _cursor.getColumnIndexOrThrow("promoExp");
      final int _cursorIndexOfIdKategori = _cursor.getColumnIndexOrThrow("id_kategori");
      final int _cursorIndexOfNamaKategori = _cursor.getColumnIndexOrThrow("nama_kategori");
      final int _cursorIndexOfHarga = _cursor.getColumnIndexOrThrow("harga");
      final int _cursorIndexOfJumlahPesan = _cursor.getColumnIndexOrThrow("jumlah_pesan");
      final int _cursorIndexOfStok = _cursor.getColumnIndexOrThrow("stok");
      final int _cursorIndexOfBerat = _cursor.getColumnIndexOrThrow("berat");
      final int _cursorIndexOfOngkir = _cursor.getColumnIndexOrThrow("ongkir");
      final int _cursorIndexOfKurir = _cursor.getColumnIndexOrThrow("kurir");
      final int _cursorIndexOfDurasiPengiriman = _cursor.getColumnIndexOrThrow("durasiPengiriman");
      final int _cursorIndexOfJenisKurir = _cursor.getColumnIndexOrThrow("jenisKurir");
      final int _cursorIndexOfSku = _cursor.getColumnIndexOrThrow("sku");
      final int _cursorIndexOfKondisi = _cursor.getColumnIndexOrThrow("kondisi");
      final int _cursorIndexOfGambar = _cursor.getColumnIndexOrThrow("gambar");
      final int _cursorIndexOfPenjual = _cursor.getColumnIndexOrThrow("penjual");
      final int _cursorIndexOfPilih = _cursor.getColumnIndexOrThrow("pilih");
      final TbKranjang[] _result = new TbKranjang[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final TbKranjang _item;
        _item = new TbKranjang();
        _item.barangId = _cursor.getInt(_cursorIndexOfBarangId);
        _item.idProduk = _cursor.getInt(_cursorIndexOfIdProduk);
        _item.idToko = _cursor.getInt(_cursorIndexOfIdToko);
        _item.nama = _cursor.getString(_cursorIndexOfNama);
        _item.catatan = _cursor.getString(_cursorIndexOfCatatan);
        _item.kodePromo = _cursor.getString(_cursorIndexOfKodePromo);
        _item.promoPotongan = _cursor.getString(_cursorIndexOfPromoPotongan);
        _item.promoMin = _cursor.getString(_cursorIndexOfPromoMin);
        _item.promoStatus = _cursor.getString(_cursorIndexOfPromoStatus);
        _item.kodeExp = _cursor.getString(_cursorIndexOfKodeExp);
        _item.id_kategori = _cursor.getString(_cursorIndexOfIdKategori);
        _item.nama_kategori = _cursor.getString(_cursorIndexOfNamaKategori);
        _item.harga = _cursor.getInt(_cursorIndexOfHarga);
        _item.jumlahPesan = _cursor.getInt(_cursorIndexOfJumlahPesan);
        _item.stok = _cursor.getInt(_cursorIndexOfStok);
        _item.berat = _cursor.getInt(_cursorIndexOfBerat);
        _item.ongkir = _cursor.getInt(_cursorIndexOfOngkir);
        _item.kurir = _cursor.getString(_cursorIndexOfKurir);
        _item.durasiPengiriman = _cursor.getString(_cursorIndexOfDurasiPengiriman);
        _item.jenisKurir = _cursor.getString(_cursorIndexOfJenisKurir);
        _item.sku = _cursor.getString(_cursorIndexOfSku);
        _item.kondisi = _cursor.getString(_cursorIndexOfKondisi);
        _item.gambar = _cursor.getString(_cursorIndexOfGambar);
        _item.penjual = _cursor.getString(_cursorIndexOfPenjual);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfPilih);
        _item.pilih = _tmp != 0;
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public TbKranjang[] selectBarangsTerpilih(boolean status) {
    final String _sql = "SELECT * FROM produk WHERE pilih = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    final int _tmp;
    _tmp = status ? 1 : 0;
    _statement.bindLong(_argIndex, _tmp);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfBarangId = _cursor.getColumnIndexOrThrow("barangId");
      final int _cursorIndexOfIdProduk = _cursor.getColumnIndexOrThrow("idProduk");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("idToko");
      final int _cursorIndexOfNama = _cursor.getColumnIndexOrThrow("nama");
      final int _cursorIndexOfCatatan = _cursor.getColumnIndexOrThrow("catatan");
      final int _cursorIndexOfKodePromo = _cursor.getColumnIndexOrThrow("kodePromo");
      final int _cursorIndexOfPromoPotongan = _cursor.getColumnIndexOrThrow("promoPotongan");
      final int _cursorIndexOfPromoMin = _cursor.getColumnIndexOrThrow("promoMin");
      final int _cursorIndexOfPromoStatus = _cursor.getColumnIndexOrThrow("promoStatus");
      final int _cursorIndexOfKodeExp = _cursor.getColumnIndexOrThrow("promoExp");
      final int _cursorIndexOfIdKategori = _cursor.getColumnIndexOrThrow("id_kategori");
      final int _cursorIndexOfNamaKategori = _cursor.getColumnIndexOrThrow("nama_kategori");
      final int _cursorIndexOfHarga = _cursor.getColumnIndexOrThrow("harga");
      final int _cursorIndexOfJumlahPesan = _cursor.getColumnIndexOrThrow("jumlah_pesan");
      final int _cursorIndexOfStok = _cursor.getColumnIndexOrThrow("stok");
      final int _cursorIndexOfBerat = _cursor.getColumnIndexOrThrow("berat");
      final int _cursorIndexOfOngkir = _cursor.getColumnIndexOrThrow("ongkir");
      final int _cursorIndexOfKurir = _cursor.getColumnIndexOrThrow("kurir");
      final int _cursorIndexOfDurasiPengiriman = _cursor.getColumnIndexOrThrow("durasiPengiriman");
      final int _cursorIndexOfJenisKurir = _cursor.getColumnIndexOrThrow("jenisKurir");
      final int _cursorIndexOfSku = _cursor.getColumnIndexOrThrow("sku");
      final int _cursorIndexOfKondisi = _cursor.getColumnIndexOrThrow("kondisi");
      final int _cursorIndexOfGambar = _cursor.getColumnIndexOrThrow("gambar");
      final int _cursorIndexOfPenjual = _cursor.getColumnIndexOrThrow("penjual");
      final int _cursorIndexOfPilih = _cursor.getColumnIndexOrThrow("pilih");
      final TbKranjang[] _result = new TbKranjang[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final TbKranjang _item;
        _item = new TbKranjang();
        _item.barangId = _cursor.getInt(_cursorIndexOfBarangId);
        _item.idProduk = _cursor.getInt(_cursorIndexOfIdProduk);
        _item.idToko = _cursor.getInt(_cursorIndexOfIdToko);
        _item.nama = _cursor.getString(_cursorIndexOfNama);
        _item.catatan = _cursor.getString(_cursorIndexOfCatatan);
        _item.kodePromo = _cursor.getString(_cursorIndexOfKodePromo);
        _item.promoPotongan = _cursor.getString(_cursorIndexOfPromoPotongan);
        _item.promoMin = _cursor.getString(_cursorIndexOfPromoMin);
        _item.promoStatus = _cursor.getString(_cursorIndexOfPromoStatus);
        _item.kodeExp = _cursor.getString(_cursorIndexOfKodeExp);
        _item.id_kategori = _cursor.getString(_cursorIndexOfIdKategori);
        _item.nama_kategori = _cursor.getString(_cursorIndexOfNamaKategori);
        _item.harga = _cursor.getInt(_cursorIndexOfHarga);
        _item.jumlahPesan = _cursor.getInt(_cursorIndexOfJumlahPesan);
        _item.stok = _cursor.getInt(_cursorIndexOfStok);
        _item.berat = _cursor.getInt(_cursorIndexOfBerat);
        _item.ongkir = _cursor.getInt(_cursorIndexOfOngkir);
        _item.kurir = _cursor.getString(_cursorIndexOfKurir);
        _item.durasiPengiriman = _cursor.getString(_cursorIndexOfDurasiPengiriman);
        _item.jenisKurir = _cursor.getString(_cursorIndexOfJenisKurir);
        _item.sku = _cursor.getString(_cursorIndexOfSku);
        _item.kondisi = _cursor.getString(_cursorIndexOfKondisi);
        _item.gambar = _cursor.getString(_cursorIndexOfGambar);
        _item.penjual = _cursor.getString(_cursorIndexOfPenjual);
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfPilih);
        _item.pilih = _tmp_1 != 0;
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public TbKranjang[] selectByIdToko(int id) {
    final String _sql = "SELECT * FROM produk WHERE idToko = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfBarangId = _cursor.getColumnIndexOrThrow("barangId");
      final int _cursorIndexOfIdProduk = _cursor.getColumnIndexOrThrow("idProduk");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("idToko");
      final int _cursorIndexOfNama = _cursor.getColumnIndexOrThrow("nama");
      final int _cursorIndexOfCatatan = _cursor.getColumnIndexOrThrow("catatan");
      final int _cursorIndexOfKodePromo = _cursor.getColumnIndexOrThrow("kodePromo");
      final int _cursorIndexOfPromoPotongan = _cursor.getColumnIndexOrThrow("promoPotongan");
      final int _cursorIndexOfPromoMin = _cursor.getColumnIndexOrThrow("promoMin");
      final int _cursorIndexOfPromoStatus = _cursor.getColumnIndexOrThrow("promoStatus");
      final int _cursorIndexOfKodeExp = _cursor.getColumnIndexOrThrow("promoExp");
      final int _cursorIndexOfIdKategori = _cursor.getColumnIndexOrThrow("id_kategori");
      final int _cursorIndexOfNamaKategori = _cursor.getColumnIndexOrThrow("nama_kategori");
      final int _cursorIndexOfHarga = _cursor.getColumnIndexOrThrow("harga");
      final int _cursorIndexOfJumlahPesan = _cursor.getColumnIndexOrThrow("jumlah_pesan");
      final int _cursorIndexOfStok = _cursor.getColumnIndexOrThrow("stok");
      final int _cursorIndexOfBerat = _cursor.getColumnIndexOrThrow("berat");
      final int _cursorIndexOfOngkir = _cursor.getColumnIndexOrThrow("ongkir");
      final int _cursorIndexOfKurir = _cursor.getColumnIndexOrThrow("kurir");
      final int _cursorIndexOfDurasiPengiriman = _cursor.getColumnIndexOrThrow("durasiPengiriman");
      final int _cursorIndexOfJenisKurir = _cursor.getColumnIndexOrThrow("jenisKurir");
      final int _cursorIndexOfSku = _cursor.getColumnIndexOrThrow("sku");
      final int _cursorIndexOfKondisi = _cursor.getColumnIndexOrThrow("kondisi");
      final int _cursorIndexOfGambar = _cursor.getColumnIndexOrThrow("gambar");
      final int _cursorIndexOfPenjual = _cursor.getColumnIndexOrThrow("penjual");
      final int _cursorIndexOfPilih = _cursor.getColumnIndexOrThrow("pilih");
      final TbKranjang[] _result = new TbKranjang[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final TbKranjang _item;
        _item = new TbKranjang();
        _item.barangId = _cursor.getInt(_cursorIndexOfBarangId);
        _item.idProduk = _cursor.getInt(_cursorIndexOfIdProduk);
        _item.idToko = _cursor.getInt(_cursorIndexOfIdToko);
        _item.nama = _cursor.getString(_cursorIndexOfNama);
        _item.catatan = _cursor.getString(_cursorIndexOfCatatan);
        _item.kodePromo = _cursor.getString(_cursorIndexOfKodePromo);
        _item.promoPotongan = _cursor.getString(_cursorIndexOfPromoPotongan);
        _item.promoMin = _cursor.getString(_cursorIndexOfPromoMin);
        _item.promoStatus = _cursor.getString(_cursorIndexOfPromoStatus);
        _item.kodeExp = _cursor.getString(_cursorIndexOfKodeExp);
        _item.id_kategori = _cursor.getString(_cursorIndexOfIdKategori);
        _item.nama_kategori = _cursor.getString(_cursorIndexOfNamaKategori);
        _item.harga = _cursor.getInt(_cursorIndexOfHarga);
        _item.jumlahPesan = _cursor.getInt(_cursorIndexOfJumlahPesan);
        _item.stok = _cursor.getInt(_cursorIndexOfStok);
        _item.berat = _cursor.getInt(_cursorIndexOfBerat);
        _item.ongkir = _cursor.getInt(_cursorIndexOfOngkir);
        _item.kurir = _cursor.getString(_cursorIndexOfKurir);
        _item.durasiPengiriman = _cursor.getString(_cursorIndexOfDurasiPengiriman);
        _item.jenisKurir = _cursor.getString(_cursorIndexOfJenisKurir);
        _item.sku = _cursor.getString(_cursorIndexOfSku);
        _item.kondisi = _cursor.getString(_cursorIndexOfKondisi);
        _item.gambar = _cursor.getString(_cursorIndexOfGambar);
        _item.penjual = _cursor.getString(_cursorIndexOfPenjual);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfPilih);
        _item.pilih = _tmp != 0;
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public TbKranjang[] selectByIdTokoTerpilih(int id, boolean status) {
    final String _sql = "SELECT * FROM produk WHERE idToko = ? AND pilih = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 2);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    _argIndex = 2;
    final int _tmp;
    _tmp = status ? 1 : 0;
    _statement.bindLong(_argIndex, _tmp);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfBarangId = _cursor.getColumnIndexOrThrow("barangId");
      final int _cursorIndexOfIdProduk = _cursor.getColumnIndexOrThrow("idProduk");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("idToko");
      final int _cursorIndexOfNama = _cursor.getColumnIndexOrThrow("nama");
      final int _cursorIndexOfCatatan = _cursor.getColumnIndexOrThrow("catatan");
      final int _cursorIndexOfKodePromo = _cursor.getColumnIndexOrThrow("kodePromo");
      final int _cursorIndexOfPromoPotongan = _cursor.getColumnIndexOrThrow("promoPotongan");
      final int _cursorIndexOfPromoMin = _cursor.getColumnIndexOrThrow("promoMin");
      final int _cursorIndexOfPromoStatus = _cursor.getColumnIndexOrThrow("promoStatus");
      final int _cursorIndexOfKodeExp = _cursor.getColumnIndexOrThrow("promoExp");
      final int _cursorIndexOfIdKategori = _cursor.getColumnIndexOrThrow("id_kategori");
      final int _cursorIndexOfNamaKategori = _cursor.getColumnIndexOrThrow("nama_kategori");
      final int _cursorIndexOfHarga = _cursor.getColumnIndexOrThrow("harga");
      final int _cursorIndexOfJumlahPesan = _cursor.getColumnIndexOrThrow("jumlah_pesan");
      final int _cursorIndexOfStok = _cursor.getColumnIndexOrThrow("stok");
      final int _cursorIndexOfBerat = _cursor.getColumnIndexOrThrow("berat");
      final int _cursorIndexOfOngkir = _cursor.getColumnIndexOrThrow("ongkir");
      final int _cursorIndexOfKurir = _cursor.getColumnIndexOrThrow("kurir");
      final int _cursorIndexOfDurasiPengiriman = _cursor.getColumnIndexOrThrow("durasiPengiriman");
      final int _cursorIndexOfJenisKurir = _cursor.getColumnIndexOrThrow("jenisKurir");
      final int _cursorIndexOfSku = _cursor.getColumnIndexOrThrow("sku");
      final int _cursorIndexOfKondisi = _cursor.getColumnIndexOrThrow("kondisi");
      final int _cursorIndexOfGambar = _cursor.getColumnIndexOrThrow("gambar");
      final int _cursorIndexOfPenjual = _cursor.getColumnIndexOrThrow("penjual");
      final int _cursorIndexOfPilih = _cursor.getColumnIndexOrThrow("pilih");
      final TbKranjang[] _result = new TbKranjang[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final TbKranjang _item;
        _item = new TbKranjang();
        _item.barangId = _cursor.getInt(_cursorIndexOfBarangId);
        _item.idProduk = _cursor.getInt(_cursorIndexOfIdProduk);
        _item.idToko = _cursor.getInt(_cursorIndexOfIdToko);
        _item.nama = _cursor.getString(_cursorIndexOfNama);
        _item.catatan = _cursor.getString(_cursorIndexOfCatatan);
        _item.kodePromo = _cursor.getString(_cursorIndexOfKodePromo);
        _item.promoPotongan = _cursor.getString(_cursorIndexOfPromoPotongan);
        _item.promoMin = _cursor.getString(_cursorIndexOfPromoMin);
        _item.promoStatus = _cursor.getString(_cursorIndexOfPromoStatus);
        _item.kodeExp = _cursor.getString(_cursorIndexOfKodeExp);
        _item.id_kategori = _cursor.getString(_cursorIndexOfIdKategori);
        _item.nama_kategori = _cursor.getString(_cursorIndexOfNamaKategori);
        _item.harga = _cursor.getInt(_cursorIndexOfHarga);
        _item.jumlahPesan = _cursor.getInt(_cursorIndexOfJumlahPesan);
        _item.stok = _cursor.getInt(_cursorIndexOfStok);
        _item.berat = _cursor.getInt(_cursorIndexOfBerat);
        _item.ongkir = _cursor.getInt(_cursorIndexOfOngkir);
        _item.kurir = _cursor.getString(_cursorIndexOfKurir);
        _item.durasiPengiriman = _cursor.getString(_cursorIndexOfDurasiPengiriman);
        _item.jenisKurir = _cursor.getString(_cursorIndexOfJenisKurir);
        _item.sku = _cursor.getString(_cursorIndexOfSku);
        _item.kondisi = _cursor.getString(_cursorIndexOfKondisi);
        _item.gambar = _cursor.getString(_cursorIndexOfGambar);
        _item.penjual = _cursor.getString(_cursorIndexOfPenjual);
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfPilih);
        _item.pilih = _tmp_1 != 0;
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public TbKranjang selectBarangDetail(int id) {
    final String _sql = "SELECT * FROM produk WHERE barangId = ? LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfBarangId = _cursor.getColumnIndexOrThrow("barangId");
      final int _cursorIndexOfIdProduk = _cursor.getColumnIndexOrThrow("idProduk");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("idToko");
      final int _cursorIndexOfNama = _cursor.getColumnIndexOrThrow("nama");
      final int _cursorIndexOfCatatan = _cursor.getColumnIndexOrThrow("catatan");
      final int _cursorIndexOfKodePromo = _cursor.getColumnIndexOrThrow("kodePromo");
      final int _cursorIndexOfPromoPotongan = _cursor.getColumnIndexOrThrow("promoPotongan");
      final int _cursorIndexOfPromoMin = _cursor.getColumnIndexOrThrow("promoMin");
      final int _cursorIndexOfPromoStatus = _cursor.getColumnIndexOrThrow("promoStatus");
      final int _cursorIndexOfKodeExp = _cursor.getColumnIndexOrThrow("promoExp");
      final int _cursorIndexOfIdKategori = _cursor.getColumnIndexOrThrow("id_kategori");
      final int _cursorIndexOfNamaKategori = _cursor.getColumnIndexOrThrow("nama_kategori");
      final int _cursorIndexOfHarga = _cursor.getColumnIndexOrThrow("harga");
      final int _cursorIndexOfJumlahPesan = _cursor.getColumnIndexOrThrow("jumlah_pesan");
      final int _cursorIndexOfStok = _cursor.getColumnIndexOrThrow("stok");
      final int _cursorIndexOfBerat = _cursor.getColumnIndexOrThrow("berat");
      final int _cursorIndexOfOngkir = _cursor.getColumnIndexOrThrow("ongkir");
      final int _cursorIndexOfKurir = _cursor.getColumnIndexOrThrow("kurir");
      final int _cursorIndexOfDurasiPengiriman = _cursor.getColumnIndexOrThrow("durasiPengiriman");
      final int _cursorIndexOfJenisKurir = _cursor.getColumnIndexOrThrow("jenisKurir");
      final int _cursorIndexOfSku = _cursor.getColumnIndexOrThrow("sku");
      final int _cursorIndexOfKondisi = _cursor.getColumnIndexOrThrow("kondisi");
      final int _cursorIndexOfGambar = _cursor.getColumnIndexOrThrow("gambar");
      final int _cursorIndexOfPenjual = _cursor.getColumnIndexOrThrow("penjual");
      final int _cursorIndexOfPilih = _cursor.getColumnIndexOrThrow("pilih");
      final TbKranjang _result;
      if(_cursor.moveToFirst()) {
        _result = new TbKranjang();
        _result.barangId = _cursor.getInt(_cursorIndexOfBarangId);
        _result.idProduk = _cursor.getInt(_cursorIndexOfIdProduk);
        _result.idToko = _cursor.getInt(_cursorIndexOfIdToko);
        _result.nama = _cursor.getString(_cursorIndexOfNama);
        _result.catatan = _cursor.getString(_cursorIndexOfCatatan);
        _result.kodePromo = _cursor.getString(_cursorIndexOfKodePromo);
        _result.promoPotongan = _cursor.getString(_cursorIndexOfPromoPotongan);
        _result.promoMin = _cursor.getString(_cursorIndexOfPromoMin);
        _result.promoStatus = _cursor.getString(_cursorIndexOfPromoStatus);
        _result.kodeExp = _cursor.getString(_cursorIndexOfKodeExp);
        _result.id_kategori = _cursor.getString(_cursorIndexOfIdKategori);
        _result.nama_kategori = _cursor.getString(_cursorIndexOfNamaKategori);
        _result.harga = _cursor.getInt(_cursorIndexOfHarga);
        _result.jumlahPesan = _cursor.getInt(_cursorIndexOfJumlahPesan);
        _result.stok = _cursor.getInt(_cursorIndexOfStok);
        _result.berat = _cursor.getInt(_cursorIndexOfBerat);
        _result.ongkir = _cursor.getInt(_cursorIndexOfOngkir);
        _result.kurir = _cursor.getString(_cursorIndexOfKurir);
        _result.durasiPengiriman = _cursor.getString(_cursorIndexOfDurasiPengiriman);
        _result.jenisKurir = _cursor.getString(_cursorIndexOfJenisKurir);
        _result.sku = _cursor.getString(_cursorIndexOfSku);
        _result.kondisi = _cursor.getString(_cursorIndexOfKondisi);
        _result.gambar = _cursor.getString(_cursorIndexOfGambar);
        _result.penjual = _cursor.getString(_cursorIndexOfPenjual);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfPilih);
        _result.pilih = _tmp != 0;
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public TbKranjang selectProdukId(int id) {
    final String _sql = "SELECT * FROM produk WHERE idProduk = ? LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfBarangId = _cursor.getColumnIndexOrThrow("barangId");
      final int _cursorIndexOfIdProduk = _cursor.getColumnIndexOrThrow("idProduk");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("idToko");
      final int _cursorIndexOfNama = _cursor.getColumnIndexOrThrow("nama");
      final int _cursorIndexOfCatatan = _cursor.getColumnIndexOrThrow("catatan");
      final int _cursorIndexOfKodePromo = _cursor.getColumnIndexOrThrow("kodePromo");
      final int _cursorIndexOfPromoPotongan = _cursor.getColumnIndexOrThrow("promoPotongan");
      final int _cursorIndexOfPromoMin = _cursor.getColumnIndexOrThrow("promoMin");
      final int _cursorIndexOfPromoStatus = _cursor.getColumnIndexOrThrow("promoStatus");
      final int _cursorIndexOfKodeExp = _cursor.getColumnIndexOrThrow("promoExp");
      final int _cursorIndexOfIdKategori = _cursor.getColumnIndexOrThrow("id_kategori");
      final int _cursorIndexOfNamaKategori = _cursor.getColumnIndexOrThrow("nama_kategori");
      final int _cursorIndexOfHarga = _cursor.getColumnIndexOrThrow("harga");
      final int _cursorIndexOfJumlahPesan = _cursor.getColumnIndexOrThrow("jumlah_pesan");
      final int _cursorIndexOfStok = _cursor.getColumnIndexOrThrow("stok");
      final int _cursorIndexOfBerat = _cursor.getColumnIndexOrThrow("berat");
      final int _cursorIndexOfOngkir = _cursor.getColumnIndexOrThrow("ongkir");
      final int _cursorIndexOfKurir = _cursor.getColumnIndexOrThrow("kurir");
      final int _cursorIndexOfDurasiPengiriman = _cursor.getColumnIndexOrThrow("durasiPengiriman");
      final int _cursorIndexOfJenisKurir = _cursor.getColumnIndexOrThrow("jenisKurir");
      final int _cursorIndexOfSku = _cursor.getColumnIndexOrThrow("sku");
      final int _cursorIndexOfKondisi = _cursor.getColumnIndexOrThrow("kondisi");
      final int _cursorIndexOfGambar = _cursor.getColumnIndexOrThrow("gambar");
      final int _cursorIndexOfPenjual = _cursor.getColumnIndexOrThrow("penjual");
      final int _cursorIndexOfPilih = _cursor.getColumnIndexOrThrow("pilih");
      final TbKranjang _result;
      if(_cursor.moveToFirst()) {
        _result = new TbKranjang();
        _result.barangId = _cursor.getInt(_cursorIndexOfBarangId);
        _result.idProduk = _cursor.getInt(_cursorIndexOfIdProduk);
        _result.idToko = _cursor.getInt(_cursorIndexOfIdToko);
        _result.nama = _cursor.getString(_cursorIndexOfNama);
        _result.catatan = _cursor.getString(_cursorIndexOfCatatan);
        _result.kodePromo = _cursor.getString(_cursorIndexOfKodePromo);
        _result.promoPotongan = _cursor.getString(_cursorIndexOfPromoPotongan);
        _result.promoMin = _cursor.getString(_cursorIndexOfPromoMin);
        _result.promoStatus = _cursor.getString(_cursorIndexOfPromoStatus);
        _result.kodeExp = _cursor.getString(_cursorIndexOfKodeExp);
        _result.id_kategori = _cursor.getString(_cursorIndexOfIdKategori);
        _result.nama_kategori = _cursor.getString(_cursorIndexOfNamaKategori);
        _result.harga = _cursor.getInt(_cursorIndexOfHarga);
        _result.jumlahPesan = _cursor.getInt(_cursorIndexOfJumlahPesan);
        _result.stok = _cursor.getInt(_cursorIndexOfStok);
        _result.berat = _cursor.getInt(_cursorIndexOfBerat);
        _result.ongkir = _cursor.getInt(_cursorIndexOfOngkir);
        _result.kurir = _cursor.getString(_cursorIndexOfKurir);
        _result.durasiPengiriman = _cursor.getString(_cursorIndexOfDurasiPengiriman);
        _result.jenisKurir = _cursor.getString(_cursorIndexOfJenisKurir);
        _result.sku = _cursor.getString(_cursorIndexOfSku);
        _result.kondisi = _cursor.getString(_cursorIndexOfKondisi);
        _result.gambar = _cursor.getString(_cursorIndexOfGambar);
        _result.penjual = _cursor.getString(_cursorIndexOfPenjual);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfPilih);
        _result.pilih = _tmp != 0;
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
