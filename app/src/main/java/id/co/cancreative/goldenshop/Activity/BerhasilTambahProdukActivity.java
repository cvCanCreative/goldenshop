package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.R;

public class BerhasilTambahProdukActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Produk produk;

    private Toolbar toolbar;
    private ImageView imgProduk;
    private TextView tvNamaProduk;
    private TextView tvHarga;
    private Button btnTambahProduk;
    private TextView btnLihatBarang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berhasil_tambah_p);
        initView();

        s = new SharedPref(this);
        produk = s.getProduk();

        setValue();
        mainButton();
        setToolbar();
    }

    private void setValue() {
        tvNamaProduk.setText(produk.BA_NAME);
        tvHarga.setText(new Helper().convertRupiah(Integer.valueOf(produk.BA_PRICE)));

        String image = new Helper().splitText(produk.BA_IMAGE);
        Log.d("URL", "" + Config.URL_produkGolden + image);

        Glide.with(this).load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .into(imgProduk);

        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(imgProduk);
    }

    private void mainButton() {

        btnLihatBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DetailProdukTokoActivity.class);
                intent.putExtra("dataInten", "aaaa");
                startActivity(intent);
                finish();
            }
        });

        btnTambahProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TambahProdukActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Barang Berhasil Ditambah");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgProduk = (ImageView) findViewById(R.id.img_produk);
        tvNamaProduk = (TextView) findViewById(R.id.tv_namaProduk);
        tvHarga = (TextView) findViewById(R.id.tv_harga);
        btnTambahProduk = (Button) findViewById(R.id.btn_tambahProduk);
        btnLihatBarang = (TextView) findViewById(R.id.btn_lihatBarang);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
