package id.co.cancreative.goldenshop.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.Kota;
import id.co.cancreative.goldenshop.Model.Provinsi;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelProvinsi;
import id.co.cancreative.goldenshop.Model.ResultProvinsi;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BukaTokoActivity extends AppCompatActivity {

    private SharedPref s;
    private ApiService api = ApiConfig.getInstanceRetrofit();

    private boolean infoToko = false, uploadKtp = false, keluarForm = true, addBerhasil = false;
    private String namaToko, deskripsi, prof, kota, kecamatan, noKtp, alamat;
    private String icon, ktp, selfi;
    private int pos;
    private String kodeposs;
    private String idProv, idKota, idOrigin, agenKurir;
    private File imageFile, fileKtp, fileSelfi;
    private File _imageFile, _fileKtp, _fileSelfi;

    private ResultProvinsi rajaOangkir;
    private User user;

    private List<String> idProvinsi = new ArrayList<>();
    private List<String> provinsi = new ArrayList<>();
    private List<String> idCity = new ArrayList<>();
    private List<String> namaKota = new ArrayList<>();
    private List<String> kodePos = new ArrayList<>();
    private List<String> idKec = new ArrayList<>();
    private List<String> namaKec = new ArrayList<>();

    private List<String> provinsi_list = new ArrayList<>();
    private List<String> list_kota = new ArrayList<>();
    private List<String> list_kec = new ArrayList<>();

    private Toolbar toolbar;
    private RelativeLayout divNamaToko;
    private Button btnBukaToko;
    private RelativeLayout divLangkah1;
    private Button btnSelanjutanya;
    private EditText edtName;
    private EditText edtDeskripsi;
    private ImageView btnAddFoto;
    private RelativeLayout divTokoSiap;
    private ImageView imgToko;
    private TextView tvNamaToko;
    private Button btnHalamanToko;
    private Button btnAddProduk;
    private LinearLayout lyKurir;
    private Spinner spnProvinsi;
    private Spinner spnKota;
    private TextView tvProvinsi;
    private TextView tvKota;
    private Spinner spnKec;
    private TextView tvKec;
    private EditText edtAlamat;
    private Button btnSelanjutanya2;
    private ImageView btnFotoKtp;
    private ImageView btnFotoSelfi;
    private RelativeLayout divUploadData;
    private EditText edtNoKtp;
    private ImageView imgFotoKtp;
    private ImageView imgFotoSelfi;
    private TextView tvNamaUser;
    private Button btnPilihAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buka_toko);
        initView();

        s = new SharedPref(this);
        rajaOangkir = s.getProvinsi();
        user = s.getUser();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (rajaOangkir != null) {
            setAlamat();
        } else {
            getListProv();
        }

        setValue();
        mainButton();
        setToolbar();
    }

    private void setValue() {
        String sPesan = "Hallo, " + "<b>" + user.fullname + "</b>";
        tvNamaUser.setText(Html.fromHtml(sPesan));
    }

    private void getListProv() {
        SweetAlert.onLoading(BukaTokoActivity.this);
        api.getListProv().enqueue(new Callback<ResponModelProvinsi>() {
            @Override
            public void onResponse(Call<ResponModelProvinsi> call, Response<ResponModelProvinsi> response) {
                SweetAlert.dismis();
                ResponModelProvinsi prov = response.body();
                s.setProvinsi(prov.rajaongkir);

                setAlamat();
            }

            @Override
            public void onFailure(Call<ResponModelProvinsi> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                SweetAlert.dismis();
                SweetAlert.onFailure(BukaTokoActivity.this, t.getMessage());
            }
        });
    }

    private void setAlamat() {
        rajaOangkir = s.getProvinsi();
        idProvinsi.add("0");
        List<Provinsi> provinsis = rajaOangkir.results;
        for (Provinsi p : provinsis) {
            idProvinsi.add(p.province_id);
            provinsi.add(p.province);
        }

        Log.d("Size", ": " + provinsis.size());

        provinsi_list.add("Pilih Provinsi");
        provinsi_list.addAll(provinsi);

//        spn_ongkir = findViewById(R.id.spn_ongkir);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, provinsi_list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        spnProvinsi.setAdapter(adapter);
        spnProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                tvProvinsi.setError(null);
                tvKec.setText(null);

                list_kota.removeAll(list_kota);
                list_kec.removeAll(list_kec);
                namaKec.removeAll(namaKec);
                namaKota.removeAll(namaKota);
                idCity.removeAll(idCity);
                idKec.removeAll(idKec);

                if (spnProvinsi.getSelectedItemPosition() != 0) {

                    SweetAlert.onLoading(BukaTokoActivity.this);

                    pos = spnProvinsi.getSelectedItemPosition();
                    idProv = idProvinsi.get(pos);
                    tvProvinsi.setText(spnProvinsi.getSelectedItem().toString());

                    final ApiService api = ApiConfig.getInstanceRetrofit();
                    api.getListoKota(idProv).enqueue(new Callback<ResponModel>() {
                        @Override
                        public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                            SweetAlert.dismis();
                            if (response.isSuccessful()) {
                                List<Kota> kota = response.body().getRajaongkir().results;

                                idCity.add("0");
                                kodePos.add("0");
                                for (Kota k : kota) {
                                    idCity.add(k.getCityId());
                                    namaKota.add(k.getCityName() + " " + k.getType());
                                    kodePos.add(k.getPostalCode());
                                }

                                list_kota.add("Pilih kota");
                                list_kota.addAll(namaKota);

                                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(BukaTokoActivity.this,
                                        android.R.layout.simple_spinner_item, list_kota);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                adapter.notifyDataSetChanged();
                                spnKota.setAdapter(adapter);
                                spnKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        int pos = spnKota.getSelectedItemPosition();
                                        idKota = idCity.get(pos);
                                        kodeposs = kodePos.get(pos);
                                        Log.d("idKota", ": " + idKota + " kode Pos: " + kodeposs);

                                        namaKec.removeAll(namaKec);
                                        idKec.removeAll(idKec);

                                        tvKota.setText(spnKota.getSelectedItem().toString());
                                        if (kodeposs.equals("0")) {
//                                            edtKodePos.setText(null);
                                        } else {
//                                            edtKodePos.setText(kodeposs);
                                        }

                                        if (pos != 0) {

                                            tvKota.setError(null);

                                            SweetAlert.onLoading(BukaTokoActivity.this);
                                            ApiService api = ApiConfig.getInstanceRetrofit();
                                            api.getListoKec(idKota).enqueue(new Callback<ResponModel>() {
                                                @Override
                                                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                                    SweetAlert.dismis();
                                                    if (response.isSuccessful()) {
                                                        List<Kota> kota = response.body().getRajaongkir().results;

                                                        idKec.add("0");

                                                        for (Kota k : kota) {
                                                            idKec.add(k.getSubdistrict_id());
                                                            namaKec.add(k.getSubdistrict_name());
                                                        }

                                                        list_kec.add("Pilih kecamatan");
                                                        list_kec.addAll(namaKec);

                                                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(BukaTokoActivity.this,
                                                                android.R.layout.simple_spinner_item, list_kec);
                                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                        adapter.notifyDataSetChanged();
                                                        spnKec.setAdapter(adapter);
                                                        spnKec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                            @Override
                                                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                                                tvKec.setError(null);
                                                                int pos = spnKec.getSelectedItemPosition();
                                                                Log.d("IdKecamatan", "" + String.valueOf(idKec.get(pos)));

                                                                idOrigin = idKec.get(pos);
                                                                tvKec.setText(spnKec.getSelectedItem().toString());
                                                            }

                                                            @Override
                                                            public void onNothingSelected(AdapterView<?> adapterView) {

                                                            }
                                                        });

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ResponModel> call, Throwable t) {
                                                    SweetAlert.dismis();
                                                    SweetAlert.onFailure(BukaTokoActivity.this, t.getMessage());
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponModel> call, Throwable t) {
                            SweetAlert.dismis();
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void mainButton() {

        btnPilihAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnHalamanToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("extra", "akun");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
            }
        });

        btnAddFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openGallery(BukaTokoActivity.this, 1);
            }
        });

        btnFotoKtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openCamera(BukaTokoActivity.this, 2);
            }
        });

        btnFotoSelfi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openCamera(BukaTokoActivity.this, 3);
            }
        });

        btnSelanjutanya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                namaToko = edtName.getText().toString();
                deskripsi = edtDeskripsi.getText().toString();
                if (namaToko.isEmpty()) {
                    edtName.setError("Kolom tidak boleh kosong");
                } else if (deskripsi.isEmpty()) {
                    edtDeskripsi.setError("Kolom tidak boleh kosong");
                } else if (_imageFile == null) {
                    Toasti.error(BukaTokoActivity.this, "Tambahkan Icon Toko Anda");
                } else {
                    divNamaToko.animate().translationY(divNamaToko.getHeight())
                            .alpha(0.0f)
                            .setDuration(300)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    divNamaToko.setVisibility(View.GONE);
                                    infoToko = true;
                                    keluarForm = false;
                                }
                            });
                }
            }
        });

        btnSelanjutanya2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                uploadFoto();
                s.setString(SharedPref.TOK_NAME, edtName.getText().toString());
                divUploadData.animate().translationY(divUploadData.getHeight())
                        .alpha(0.0f)
                        .setDuration(300)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                divUploadData.setVisibility(View.GONE);
                                uploadKtp = true;
                                infoToko = false;
                            }
                        });
            }
        });
        btnBukaToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bukaToko();
            }
        });

    }

    private void uploadFoto() {

        String toko = edtName.getText().toString();
        String sNoKtp = edtNoKtp.getText().toString();

        RequestBody sName = RequestBody.create(MediaType.parse("text/plain"), toko);

        if (sNoKtp.isEmpty()) {
            edtNoKtp.setError("No KTP tidak boleh kosong");
        } else if (_fileKtp == null) {
            Toasti.error(BukaTokoActivity.this, "Lampirkan Bukti Foto KTP");
        } else if (_fileSelfi == null) {
            Toasti.error(BukaTokoActivity.this, "Lampirkan Foto diri bersama KTP");
        } else {

            try {
                imageFile = new Compressor(this).compressToFile(_imageFile);
                fileKtp = new Compressor(this).compressToFile(_fileKtp);
                fileSelfi = new Compressor(this).compressToFile(_fileSelfi);
            } catch (IOException e) {
                e.printStackTrace();
            }

            SweetAlert.onLoadingCustom(this, "Uploading File...");

            // Icon
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
            MultipartBody.Part fIcon = MultipartBody.Part.createFormData("file", imageFile.getName(), requestFile);
            // KTP
            RequestBody sFileKtp = RequestBody.create(MediaType.parse("multipart/form-data"), fileKtp);
            MultipartBody.Part fKtp = MultipartBody.Part.createFormData("file", fileKtp.getName(), sFileKtp);
            // Selfi
            RequestBody sFileSelfi = RequestBody.create(MediaType.parse("multipart/form-data"), fileSelfi);
            MultipartBody.Part fSelfi = MultipartBody.Part.createFormData("file", fileSelfi.getName(), sFileSelfi);

            api.uploadFotoToko(s.getApi(), s.getIdUser(), s.getSession(), sName, fIcon, fKtp, fSelfi).enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    SweetAlert.dismis();
                    if (response.isSuccessful()) {
                        ResponModel rm = response.body();
                        if (rm.getSuccess() == 1) {
                            icon = rm.getResult().getIcon();
                            ktp = rm.getResult().getKtp();
                            selfi = rm.getResult().getSelfie();
                            s.setString(SharedPref.TOK_NAME, edtName.getText().toString());

                            divUploadData.animate().translationY(divUploadData.getHeight())
                                    .alpha(0.0f)
                                    .setDuration(300)
                                    .setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            super.onAnimationEnd(animation);
                                            divUploadData.setVisibility(View.GONE);
                                            uploadKtp = true;
                                            infoToko = false;
                                        }
                                    });

                        } else {
                            Toasti.error(BukaTokoActivity.this, "" + rm.getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    SweetAlert.dismis();
                    SweetAlert.onFailure(BukaTokoActivity.this, t.getMessage());
                }
            });

        }
    }

    private void bukaToko() {

        SweetAlert.sendingData(this);

        String sNama = edtName.getText().toString();
        String sDeskripsi = edtDeskripsi.getText().toString();
        String sIdProf = idProv;
        String sIdKota = idKota;
        String sIdKec = idOrigin;
        String sKota = spnKota.getSelectedItem().toString();
        String sKec = spnKec.getSelectedItem().toString();
        String sNoKtp = edtNoKtp.getText().toString();
        String sAlamat = edtAlamat.getText().toString();

        api.addToko(s.getApi(), s.getIdUser(), s.getSession(), sNama, sDeskripsi, sIdProf, sIdKota,
                sIdKec, sKota, sKec, sNoKtp, sAlamat, icon, selfi, ktp).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    ResponModel rm = response.body();
                    if (rm.getSuccess() == 1) {

                        tvNamaToko.setText(edtName.getText().toString());
                        addBerhasil = true;
                        SweetAlert.basicMessage(BukaTokoActivity.this, "Data berhasil dikirim");
                        divLangkah1.animate().translationY(divLangkah1.getHeight())
                                .alpha(0.0f)
                                .setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        divLangkah1.setVisibility(View.GONE);
                                    }
                                });

                        s.setBoolean(SharedPref.TOK_STATUS, true);
                    } else if (rm.getSuccess() == 0) {
                        addBerhasil = true;
                        SweetAlert.waringWithIntent(BukaTokoActivity.this, "Pengajuan Toko Sudah ada", "Anda sudah mengajukan toko, Mohon Tunggu konfirmasi dari kami", MainActivity.class);
//                        Toasti.info(BukaTokoActivity.this, "Anda sudah mengajukan toko, Mohon Tunggu konfirmasi dari kami");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(BukaTokoActivity.this, t.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        EasyImage.handleActivityResult(requestCode, resultCode, data, BukaTokoActivity.this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFiles, EasyImage.ImageSource source, int type) {

                if (type == 1) {
                    _imageFile = imageFiles;
                    upload(1, _imageFile);
                }

                if (type == 2) {
                    _fileKtp = imageFiles;
                    upload(2, _fileKtp);
                }

                if (type == 3) {
                    _fileSelfi = imageFiles;
                    upload(3, _fileSelfi);
                }
            }
        });
    }

    private String upload(final int type, final File file) {
        final String[] image = {""};
        String toko = edtName.getText().toString();
        if (edtName.getText().toString().isEmpty()) {
            final int min = 100;
            final int max = 999;
            final int random = new Random().nextInt((max - min) + 1) + min;
            toko = "namaToko" + random;
        }
        RequestBody sName = RequestBody.create(MediaType.parse("text/plain"), toko);

        try {
            imageFile = new Compressor(this).compressToFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        SweetAlert.onLoadingCustom(this, "Uploading File...");

        // Icon
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part fIcon = MultipartBody.Part.createFormData("file", imageFile.getName(), requestFile);

        String status = "";
        if (type == 1) {
            status = "img_toko";
        }
        if (type == 2) {
            status = "toko_ktp";
        }

        if (type == 3) {
            status = "toko_selfie";
        }

        api.uploadImgToko(status, s.getApi(), s.getIdUser(), s.getSession(), sName, fIcon).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    ResponModel rm = response.body();
                    if (rm.getSuccess() == 1) {
                        switch (type) {
                            case 1:
                                icon = rm.getResult().getIcon();
                                image[0] = icon;
                                Picasso.with(getApplicationContext())
                                        .load(Config.url_toko + icon)
                                        .placeholder(R.drawable.image_loading)
                                        .error(R.drawable.image_error)
                                        .noFade()
                                        .into(imgToko);
                                Picasso.with(getApplicationContext())
                                        .load(Config.url_toko + icon)
                                        .placeholder(R.drawable.image_loading)
                                        .error(R.drawable.image_error)
                                        .noFade()
                                        .into(btnAddFoto);
                                break;
                            case 2:
                                ktp = rm.getResult().getKtp();
                                image[0] = ktp;
                                Picasso.with(getApplicationContext())
                                        .load(Config.url_ktp + ktp)
                                        .placeholder(R.drawable.image_loading)
                                        .error(R.drawable.image_error)
                                        .noFade()
                                        .into(btnFotoKtp);

                                break;
                            case 3:
                                selfi = rm.getResult().getSelfie();
                                image[0] = selfi;
                                Picasso.with(getApplicationContext())
                                        .load(Config.url_selfie + selfi)
                                        .placeholder(R.drawable.image_loading)
                                        .error(R.drawable.image_error)
                                        .noFade()
                                        .into(btnFotoSelfi);
                                break;
                        }
                    } else {
                        Toasti.error(BukaTokoActivity.this, "" + rm.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(BukaTokoActivity.this, t.getMessage());
            }
        });

        return image[0];
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Buka Toko");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        divNamaToko = (RelativeLayout) findViewById(R.id.div_namaToko);
        btnBukaToko = (Button) findViewById(R.id.btn_bukaToko);
        divLangkah1 = (RelativeLayout) findViewById(R.id.div_langkah1);
        btnSelanjutanya = (Button) findViewById(R.id.btn_selanjutanya);
        edtName = (EditText) findViewById(R.id.edt_name);
        edtDeskripsi = (EditText) findViewById(R.id.edt_deskripsi);
        btnAddFoto = (ImageView) findViewById(R.id.btn_addFoto);
        divTokoSiap = (RelativeLayout) findViewById(R.id.div_tokoSiap);
        imgToko = (ImageView) findViewById(R.id.img_toko);
        tvNamaToko = (TextView) findViewById(R.id.tv_namaToko);
        btnHalamanToko = (Button) findViewById(R.id.btn_halamanToko);
        lyKurir = (LinearLayout) findViewById(R.id.ly_kurir);
        spnProvinsi = (Spinner) findViewById(R.id.spn_provinsi);
        spnKota = (Spinner) findViewById(R.id.spn_kota);
        tvProvinsi = (TextView) findViewById(R.id.tv_provinsi);
        tvKota = (TextView) findViewById(R.id.tv_kota);
        spnKec = (Spinner) findViewById(R.id.spn_kec);
        tvKec = (TextView) findViewById(R.id.tv_kec);
        edtAlamat = (EditText) findViewById(R.id.edt_alamat);
        btnSelanjutanya2 = (Button) findViewById(R.id.btn_selanjutanya2);
        btnFotoKtp = (ImageView) findViewById(R.id.btn_fotoKtp);
        btnFotoSelfi = (ImageView) findViewById(R.id.btn_fotoSelfi);
        divUploadData = (RelativeLayout) findViewById(R.id.div_uploadData);
        edtNoKtp = (EditText) findViewById(R.id.edt_noKtp);
        imgFotoKtp = (ImageView) findViewById(R.id.img_fotoKtp);
        imgFotoSelfi = (ImageView) findViewById(R.id.img_fotoSelfi);
        tvNamaUser = (TextView) findViewById(R.id.tv_namaUser);
        btnPilihAlamat = (Button) findViewById(R.id.btn_pilihAlamat);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {

        if (addBerhasil) {
            finish();
//            super.onBackPressed();
        }

        if (keluarForm) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(BukaTokoActivity.this);
            dialog.setCancelable(true);
            dialog.setMessage("Apakah anda yakin ingin keluar ?");
            dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    BukaTokoActivity.this.finish();

                }
            });

            dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();
            return;
        }

        if (infoToko) {
            divNamaToko.setVisibility(View.VISIBLE);
            divNamaToko.animate().translationY(0)
                    .alpha(1.0f)
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation, boolean isReverse) {
                            divNamaToko.setVisibility(View.VISIBLE);
                            infoToko = false;
                            keluarForm = true;
                        }
                    });
            return;
        }

        if (uploadKtp) {
            infoToko = true;
            divUploadData.setVisibility(View.VISIBLE);
            divUploadData.animate().translationY(0)
                    .alpha(1.0f)
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation, boolean isReverse) {
                            divUploadData.setVisibility(View.VISIBLE);
                            uploadKtp = false;
                            infoToko = true;
                        }
                    });
            return;
        }
    }
}
