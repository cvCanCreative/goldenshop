package id.co.cancreative.goldenshop.Activity;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.Adapter.AdapterPilihProduk;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukCari;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponseCari;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CariBarangActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private AppDbTerakhirDilihat db;
    private BottomSheetBehavior bottomSheetBehavior;

    private String s_filter = null;
    private String s_query = "";
    private Integer id_toko;
    private boolean by_toko = false;

    private ArrayList<Produk> arrayproduk = new ArrayList<>();

    private Toolbar toolbar;
    private LinearLayout bottomSheet;
    private TextView bsTvFilterbaru;
    private TextView bsTvFilterbekas;
    private ImageView acImgBack;
    private SearchView acScCari;
    private ImageView acImgFilter;
    private LinearLayout acLinProgress;
    private ProgressBar acPbLoading;
    private TextView acTvPesan;
    private Button acBtnUlangi;
    private RecyclerView acRecyData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caribarang);
        initView();
        acImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        acScCari.isIconfiedByDefault();
        s = new SharedPref(this);
        db = Room.databaseBuilder(this, AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();

        //cek data intent
        by_toko = getIntent().getBooleanExtra("by_toko", false);
        id_toko = getIntent().getIntExtra("id_toko", 0);

        acBtnUlangi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getdata();
            }
        });

        //MULAI BOTTOMSHEET SETUP
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }


            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        //SELESAI BOTTOM SHEET

        //------------------------------------------------------
        //MULAI FILTER SETUP
        /*
         * s_filter adalah parameter untuk wadah kategori
         * NEW untuk kateori barang baru
         * SECOND untuk kategori barang bekas
         * */
        acImgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cek_kategori();
                if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

                }
            }
        });

        bsTvFilterbaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s_filter = "NEW";
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                cek_kategori();
                getdata();
            }
        });

        bsTvFilterbekas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s_filter = "SECOND";
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                cek_kategori();
                getdata();
            }
        });

        //SELESAI FILTER SETUP


        //MULAI SETUP PENCARIAN (SEARCHVIEW)
        if (getIntent().getStringExtra("query") != null) {
            s_query = getIntent().getStringExtra("query");
            acScCari.setQuery(getIntent().getStringExtra("query"), false);
            getdata();
        }

        acScCari.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d("onQueryTextSubmit", "Do This " +s);
                getdata();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d("onQueryTextChange", "Do This " +s);
                return false;
            }
        });
        //SELESAI SETUP PENCARIAN (SEARCHVIEW)

    }

    /*
     * Pameter deskripsi :
     * by_toko adalah paramater boolean bila
     *       [true] pencarian dilakukan untuk 1 toko (DetailTokoActivity)
     *       [false] pencarian dilakukan dari semua toko (HomeFragment)
     * -------------------------------------
     * s_filter adalah paramer string untuk menampung filter yang di pilih user dengan value (NEW/SECOND)
     * s_query adalah parameter string untuk menampung kata yang di cari oleh user
     * id_toko adalah parameter Integer untuk menampung id_toko yang di parsing dari [getIntent().getInt("id_toko",0)]
     * */
    private void getdata() {
        loading(null, true);

        if (by_toko) {
            //CARI SEMUA BARANG BILA ADA FILTERNYA
            if (s_filter != null) {
                api.cari_barang_berdasarkan_status_toko(s.getApi(), s_query, s_filter, id_toko).enqueue(new Callback<ResponseCari>() {
                    @Override
                    public void onResponse(Call<ResponseCari> call, Response<ResponseCari> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getSuccess() == 1) {
                                loading(null, false);
                                arrayproduk = (ArrayList<Produk>) response.body().getProducts();
                                acRecyData.setLayoutManager(new GridLayoutManager(CariBarangActivity.this, 2));
                                acRecyData.setAdapter(new AdapterProdukTrading(arrayproduk, CariBarangActivity.this, db));

                                if (arrayproduk.size() > 0) {
                                    loading(null, false);
                                } else {
                                    loading(response.body().getMessage(), false);
                                }
                            } else {
                                loading(response.body().getMessage(), false);
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseCari> call, Throwable t) {
                        loading(getString(R.string.error_internet), false);
                    }
                });

                //CARI SEMUA BARANG BILA TIDAK ADA FILTERNYA
            } else {

                api.cari_barang_toko(s.getApi(), s_query, id_toko).enqueue(new Callback<ResponseCari>() {
                    @Override
                    public void onResponse(Call<ResponseCari> call, Response<ResponseCari> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getSuccess() == 1) {
                                loading(null, false);
                                arrayproduk = (ArrayList<Produk>) response.body().getProducts();
                                acRecyData.setLayoutManager(new GridLayoutManager(CariBarangActivity.this, 2));
                                acRecyData.setAdapter(new AdapterProdukCari(arrayproduk, CariBarangActivity.this));

                                if (arrayproduk.size() > 0) {
                                    loading(null, false);
                                } else {
                                    loading(response.body().getMessage(), false);
                                }
                            } else {
                                loading(response.body().getMessage(), false);
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseCari> call, Throwable t) {
                        loading(getString(R.string.error_internet), false);
                    }
                });
            }
        } else {

            //CARI SEMUA BARANG BILA ADA FILTERNYA
            if (s_filter != null) {
                api.cari_barang_berdasarkan_status(s.getApi(), s_query, s_filter).enqueue(new Callback<ResponseCari>() {
                    @Override
                    public void onResponse(Call<ResponseCari> call, Response<ResponseCari> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getSuccess() == 1) {
                                loading(null, false);
                                arrayproduk = (ArrayList<Produk>) response.body().getProducts();
                                acRecyData.setLayoutManager(new GridLayoutManager(CariBarangActivity.this, 2));
                                acRecyData.setAdapter(new AdapterProdukCari(arrayproduk, CariBarangActivity.this));

                                if (arrayproduk.size() > 0) {
                                    loading(null, false);
                                } else {
                                    loading(response.body().getMessage(), false);
                                }
                            } else {
                                loading(response.body().getMessage(), false);
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseCari> call, Throwable t) {
                        loading(getString(R.string.error_internet), false);
                    }
                });

                //CARI SEMUA BARANG BILA TIDAK ADA FILTERNYA
            } else {
                api.cari_barang(s.getApi(), s_query, 1).enqueue(new Callback<ResponModel>() {
                    @Override
                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getSuccess() == 1) {
                                loading(null, false);
                                arrayproduk = response.body().products;
                                acRecyData.setLayoutManager(new GridLayoutManager(CariBarangActivity.this, 2));
                                acRecyData.setAdapter(new AdapterProdukTrading(arrayproduk, CariBarangActivity.this, db));

                                if (arrayproduk.size() > 0) {
                                    loading(null, false);
                                } else {
                                    loading(response.body().getMessage(), false);
                                }
                            } else {
                                loading(response.body().getMessage(), false);
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponModel> call, Throwable t) {
                        loading(getString(R.string.error_internet), false);
                    }
                });
            }
        }
    }

    private void cek_kategori() {
        if (s_filter != null) {
            if (s_filter.equals("NEW")) {
                bsTvFilterbaru.setBackgroundResource(R.color.grey_10);
                bsTvFilterbekas.setBackgroundResource(R.color.white);
            } else if (s_filter.equals("SECOND")) {
                bsTvFilterbaru.setBackgroundResource(R.color.white);
                bsTvFilterbekas.setBackgroundResource(R.color.grey_10);
            } else {
                bsTvFilterbaru.setBackgroundResource(R.color.white);
                bsTvFilterbekas.setBackgroundResource(R.color.white);
            }
        }

    }

    private void initView() {
        bottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
        bsTvFilterbaru = (TextView) findViewById(R.id.bs_tv_filterbaru);
        bsTvFilterbekas = (TextView) findViewById(R.id.bs_tv_filterbekas);
        acImgBack = (ImageView) findViewById(R.id.ac_img_back);
        acScCari = (SearchView) findViewById(R.id.ac_sc_cari);
        acImgFilter = (ImageView) findViewById(R.id.ac_img_filter);
        acLinProgress = (LinearLayout) findViewById(R.id.ac_lin_progress);
        acPbLoading = (ProgressBar) findViewById(R.id.ac_pb_loading);
        acTvPesan = (TextView) findViewById(R.id.ac_tv_pesan);
        acBtnUlangi = (Button) findViewById(R.id.ac_btn_ulangi);
        acRecyData = (RecyclerView) findViewById(R.id.ac_recy_data);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void loading(String pesan, boolean tampil) {
        if (tampil == true) {
            acLinProgress.setVisibility(View.VISIBLE);
            acPbLoading.setVisibility(View.VISIBLE);
            acBtnUlangi.setVisibility(View.GONE);
            acRecyData.setVisibility(View.GONE);
            acTvPesan.setVisibility(View.VISIBLE);
            acTvPesan.setText(getString(R.string.mohon_tunggu));
        } else {
            if (pesan != null) {
                acLinProgress.setVisibility(View.VISIBLE);
                acPbLoading.setVisibility(View.GONE);
                acBtnUlangi.setVisibility(View.VISIBLE);
                acRecyData.setVisibility(View.GONE);
                acTvPesan.setVisibility(View.VISIBLE);
                acTvPesan.setText(pesan);
            } else {
                acLinProgress.setVisibility(View.GONE);
                acPbLoading.setVisibility(View.GONE);
                acBtnUlangi.setVisibility(View.GONE);
                acRecyData.setVisibility(View.VISIBLE);
                acTvPesan.setVisibility(View.GONE);
            }
        }
    }

}
