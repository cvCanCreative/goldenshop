package id.co.cancreative.goldenshop.Activity;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CariProductActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private AppDbTerakhirDilihat db;

    private String s_filter = null;
    private String s_query = "";
    private String cari = "";
    private Integer id_toko;
    private boolean by_toko = false;

    private String pilih = "1000";
    private String pilihan;
    private String min, max;
    private int indexLoad = 1;
    private int viewThreshold = 8;
    private int pastVisibleitem, visibleItemCount, totalItemCount, previousTotal = 0;
    private boolean isLoading = true;

    private ArrayList<Produk> mProduk = new ArrayList<>();
    private AdapterProdukTrading mAdapter;
    private GridLayoutManager layoutManager;

    private BottomSheetBehavior behavior;
    private BottomSheetDialog mBottomSheetDialog;

    private Toolbar toolbar;
    private ImageView acImgBack;
    private SearchView acScCari;
    private LinearLayout ly;
    private LinearLayout lyStatToko;
    private LinearLayout lyFilter;
    private LinearLayout lyUrutkan;
    private RecyclerView rv;
    private ProgressBar pb;
    private LinearLayout divNoProduk;
    private TextView tvPesan;
    private ProgressBar pd;
    private LinearLayout bottomSheet;
    private ImageView btnCari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_produk);
        initView();

        acScCari.isIconfiedByDefault();
        s = new SharedPref(this);
        db = Room.databaseBuilder(this, AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();

        setlayoutManager();
        mainButton();
        getIntentExtra();
    }

    private void getIntentExtra() {
        by_toko = getIntent().getBooleanExtra("by_toko", false);
        id_toko = getIntent().getIntExtra("id_toko", 0);
        if (getIntent().getStringExtra("query") != null) {
            s_query = getIntent().getStringExtra("query");
            acScCari.setQuery(s_query, false);
            searchProduct(s_query);
        }
    }

    private void searchProduct(final String produk) {
        api.cari_barang(s.getApi(), produk, 1).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                s_query = produk;
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = response.body().products;
                    if (mProduk.size() >= 1) {
                        mAdapter = new AdapterProdukTrading(mProduk, CariProductActivity.this, db);
                        rv.setAdapter(mAdapter);
                    } else {
                        tvPesan.setText("Produk " + s_query + " Tidak Tersedia");
                        divNoProduk.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(CariProductActivity.this, t.getMessage());
            }
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
                        indexLoad = indexLoad + 1;
                        loadSearchProduct(indexLoad, produk);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadSearchProduct(int indexLoad, String produk) {
        pb.setVisibility(View.VISIBLE);
        api.cari_barang(s.getApi(), produk, indexLoad).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ArrayList<Produk> produks = response.body().products;
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(CariProductActivity.this, t.getMessage());
            }
        });
    }

    private void filterHarga(final String min, final String max, final String produk) {
        api.cari_barangByHarga(s.getApi(), produk, min, max, 1).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = response.body().products;
                    if (mProduk.size() >= 1) {
                        mAdapter = new AdapterProdukTrading(mProduk, CariProductActivity.this, db);
                        rv.setAdapter(mAdapter);
                    } else {
                        tvPesan.setText("Produk " + produk + " Tidak Tersedia");
                        divNoProduk.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(CariProductActivity.this, t.getMessage());
            }
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
                        indexLoad = indexLoad + 1;
                        loadFilterHarga(indexLoad, min, max, produk);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadFilterHarga(int indexLoad, String min, String max, String produk) {
        pb.setVisibility(View.VISIBLE);
        api.cari_barangByHarga(s.getApi(), produk, min, max, indexLoad).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ArrayList<Produk> produks = response.body().products;
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(CariProductActivity.this, t.getMessage());
            }
        });
    }

    private void filterKondisi(final String produk, final String kondisi) {
        api.cari_ByStatus(s.getApi(), produk, kondisi, 1).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = response.body().products;
                    if (mProduk.size() >= 1) {
                        mAdapter = new AdapterProdukTrading(mProduk, CariProductActivity.this, db);
                        rv.setAdapter(mAdapter);
                    } else {
                        tvPesan.setText("Produk " + produk + " Tidak Tersedia");
                        divNoProduk.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(CariProductActivity.this, t.getMessage());
            }
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
                        indexLoad = indexLoad + 1;
                        loadFilterKondisi(indexLoad, produk, kondisi);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadFilterKondisi(int indexLoad, String produk, String kondisi) {
        pb.setVisibility(View.VISIBLE);
        api.cari_ByStatus(s.getApi(), produk, kondisi, indexLoad).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ArrayList<Produk> produks = response.body().products;
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(CariProductActivity.this, t.getMessage());
            }
        });
    }

    private void filterStatusToko(final String produk, final String kondisi) {
        api.cari_ByStoko(s.getApi(), produk, kondisi, 1).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = response.body().products;
                    if (mProduk.size() >= 1) {
                        mAdapter = new AdapterProdukTrading(mProduk, CariProductActivity.this, db);
                        rv.setAdapter(mAdapter);
                    } else {
                        tvPesan.setText("Produk " + produk + " Tidak Tersedia");
                        divNoProduk.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(CariProductActivity.this, t.getMessage());
            }
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
                        indexLoad = indexLoad + 1;
                        loadFilterStatusToko(indexLoad, produk, kondisi);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadFilterStatusToko(int indexLoad, String produk, String kondisi) {
        pb.setVisibility(View.VISIBLE);
        api.cari_ByStoko(s.getApi(), produk, kondisi, indexLoad).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ArrayList<Produk> produks = response.body().products;
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(CariProductActivity.this, t.getMessage());
            }
        });
    }

    private void mainButton() {
        acImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        lyStatToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetStatToko();
            }
        });
        lyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetHarga();
            }
        });
        lyUrutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetKondisi();
            }
        });

        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        acScCari.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d("onQueryTextSubmit", "Do This " + s);
                pd.setVisibility(View.VISIBLE);
                searchProduct(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                cari = s;
                return false;
            }
        });

        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setVisibility(View.VISIBLE);
                searchProduct(cari);
            }
        });
    }

    private void setlayoutManager() {
        layoutManager = new GridLayoutManager(this, 2);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(layoutManager);
    }

    private void resetIndex() {
        pastVisibleitem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        previousTotal = 0;
        indexLoad = 1;
    }

    private void sheetStatToko() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_stat_toko, null);
        final LinearLayout lyAktif = view.findViewById(R.id.ly_aktif);
        final LinearLayout lyBelum = view.findViewById(R.id.ly_belum);
        final LinearLayout lyNormal = view.findViewById(R.id.ly_normal);
        final ImageView imgClose = view.findViewById(R.id.img_close);

        TextView tvNoAktif = view.findViewById(R.id.tv_no_aktif);
        TextView tvNoBelum = view.findViewById(R.id.tv_no_belum);
        TextView tvNoAll = view.findViewById(R.id.tv_no_semua);

        ImageView imgAktif = view.findViewById(R.id.img_aktif);
        ImageView imgBelum = view.findViewById(R.id.img_belum);
        ImageView imgAll = view.findViewById(R.id.img_semua);

        tvNoAktif.setText("5");
        tvNoBelum.setText("6");
        tvNoAll.setText("7");

        if (pilih.equals(tvNoAktif.getText().toString())) {
            imgAktif.setVisibility(View.VISIBLE);
        } else if (pilih.equals(tvNoBelum.getText().toString())) {
            imgBelum.setVisibility(View.VISIBLE);
        } else if (pilih.equals(tvNoAll.getText().toString())) {
            imgAll.setVisibility(View.VISIBLE);
        } else {
            imgAll.setVisibility(View.GONE);
            imgAktif.setVisibility(View.GONE);
            imgBelum.setVisibility(View.GONE);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        lyAktif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "TK_AKTIF";
                pilih = "5";
                resetIndex();
                filterStatusToko(s_query, pilihan);
                mBottomSheetDialog.dismiss();
            }
        });

        lyBelum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "TK_BLM_AKTIF";
                pilih = "6";
                resetIndex();
                filterStatusToko(s_query, pilihan);
                mBottomSheetDialog.dismiss();
            }
        });

        lyNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "ALL";
                pilih = "7";
                resetIndex();
                searchProduct(s_query);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void sheetHarga() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_filter, null);
        final ImageView imgClose = view.findViewById(R.id.img_close);
        final EditText edtMin = view.findViewById(R.id.edt_min);
        final EditText edtMax = view.findViewById(R.id.edt_max);
        final Button btnAktifkan = view.findViewById(R.id.btn_aktifkan);

        edtFilter(edtMin, edtMax);

        btnAktifkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "1000";
                mBottomSheetDialog.dismiss();
                resetIndex();
                filterHarga(min, max, s_query);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void sheetKondisi() {
        //untuk hide
//        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_urutkan, null);
        final LinearLayout lyBaru = view.findViewById(R.id.ly_baru);
        final LinearLayout lyLama = view.findViewById(R.id.ly_bekas);
        final LinearLayout lyLaris = view.findViewById(R.id.ly_laris);
        final LinearLayout lyNormal = view.findViewById(R.id.ly_normal);
        final ImageView imgClose = view.findViewById(R.id.img_close);

        TextView tvNoBaru = view.findViewById(R.id.tv_no_baru);
        TextView tvNoBekas = view.findViewById(R.id.tv_no_bekas);
        TextView tvNoAll = view.findViewById(R.id.tv_no_semua);

        ImageView imgBaru = view.findViewById(R.id.img_baru);
        ImageView imgBekas = view.findViewById(R.id.img_bekas);
        ImageView imgAll = view.findViewById(R.id.img_semua);

        tvNoBaru.setText("1");
        tvNoBekas.setText("2");
        tvNoAll.setText("3");

        if (pilih.equals(tvNoBaru.getText().toString())) {
            imgBaru.setVisibility(View.VISIBLE);
        } else if (pilih.equals(tvNoBekas.getText().toString())) {
            imgBekas.setVisibility(View.VISIBLE);
        } else if (pilih.equals(tvNoAll.getText().toString())) {
            imgAll.setVisibility(View.VISIBLE);
        } else {
            imgAll.setVisibility(View.GONE);
            imgBaru.setVisibility(View.GONE);
            imgBekas.setVisibility(View.GONE);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        lyBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "NEW";
                pilih = "1";
                resetIndex();
                filterKondisi(s_query, pilihan);
                mBottomSheetDialog.dismiss();
            }
        });

        lyLama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "SECOND";
                pilih = "2";
                resetIndex();
                filterKondisi(s_query, pilihan);
                mBottomSheetDialog.dismiss();
            }
        });

        lyNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "ALL";
                pilih = "3";
                searchProduct(s_query);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void edtFilter(final EditText edtMin, final EditText edtMax) {

        edtMin.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtMin.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        min = cleanString;
                        edtMin.setText(formatted);
                        edtMin.setSelection(formatted.length());

                        edtMin.addTextChangedListener(this);
                    } else {
                        edtMin.setText(current);
                        edtMin.setSelection(current.length());

                        edtMin.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });


        edtMax.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtMax.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        max = cleanString;
                        edtMax.setText(formatted);
                        edtMax.setSelection(formatted.length());

                        edtMax.addTextChangedListener(this);
                    } else {
                        edtMax.setText(current);
                        edtMax.setSelection(current.length());

                        edtMax.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        acImgBack = (ImageView) findViewById(R.id.ac_img_back);
        acScCari = (SearchView) findViewById(R.id.ac_sc_cari);
        ly = (LinearLayout) findViewById(R.id.ly);
        lyStatToko = (LinearLayout) findViewById(R.id.ly_stat_toko);
        lyFilter = (LinearLayout) findViewById(R.id.ly_filter);
        lyUrutkan = (LinearLayout) findViewById(R.id.ly_urutkan);
        rv = (RecyclerView) findViewById(R.id.rv);
        pb = (ProgressBar) findViewById(R.id.progressBar);
        divNoProduk = (LinearLayout) findViewById(R.id.div_noProduk);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        pd = (ProgressBar) findViewById(R.id.pd);
        bottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
        btnCari = (ImageView) findViewById(R.id.btn_cari);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
