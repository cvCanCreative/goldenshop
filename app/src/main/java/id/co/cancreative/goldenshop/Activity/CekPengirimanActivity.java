package id.co.cancreative.goldenshop.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.Adapter.AdapterPengiriman;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.Pengiriman;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelList;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CekPengirimanActivity extends AppCompatActivity {

    List<Pengiriman> list = new ArrayList<>();
    RecyclerView.LayoutManager layoutManager;
    AdapterPengiriman mAdapter;
    String kurir,resi;

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;
    private TextView tvKurir;
    private TextView tvResi;
    private TextView tvKet;
    private RecyclerView rvPengiriman;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cek_pengiriman);
        initView();

        s = new SharedPref(this);

        setToolbar();

        setData();
    }

    private void setData(){
        resi = getIntent().getStringExtra("resi");
        kurir = getIntent().getStringExtra("kurir");

        tvKurir.setText("Kurir: "+kurir.toUpperCase());
        tvResi.setText(""+resi);

        getData(kurir,resi);
    }

    private void getData(final String kurir, final String resi){
        SweetAlert.sendingData(this);
        api.getPengiriman(""+resi,
                ""+kurir).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                if (response.isSuccessful()){
                    SweetAlert.dismis();
                    if (response.body().getSuccess() == 1){
                        tvKet.setVisibility(View.GONE);
                        rvPengiriman.setVisibility(View.VISIBLE);

                        list = response.body().manifest;
                        layoutManager = new LinearLayoutManager(CekPengirimanActivity.this,LinearLayoutManager.VERTICAL,false);
                        rvPengiriman.setLayoutManager(layoutManager);
                        mAdapter = new AdapterPengiriman(CekPengirimanActivity.this,list);
                        rvPengiriman.setAdapter(mAdapter);

                    }else {
                        tvKet.setVisibility(View.VISIBLE);
                        rvPengiriman.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                final SweetAlertDialog dialogGagal = new SweetAlertDialog(CekPengirimanActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Ulang");

                dialogGagal.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        getData(kurir,resi);
                    }
                });
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Status Pengiriman");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvKurir = (TextView) findViewById(R.id.tv_kurir);
        tvResi = (TextView) findViewById(R.id.tv_resi);
        tvKet = (TextView) findViewById(R.id.tv_ket);
        rvPengiriman = (RecyclerView) findViewById(R.id.rv_pengiriman);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
