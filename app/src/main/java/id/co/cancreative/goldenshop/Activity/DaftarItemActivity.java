package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Adapter.AdapterAlamat;
import id.co.cancreative.goldenshop.Adapter.AdapterPilihAlamat;
import id.co.cancreative.goldenshop.Adapter.AdapterRekening;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarItemActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;

    private ArrayList<ModelAlamat> mItem = new ArrayList<>();
    private ArrayList<Rekening> rekening = new ArrayList<>();

    private Toolbar toolbar;
    private ImageView btnAddAlamat;
    private TextView toolbarTitle;
    private RecyclerView rv;
    private LinearLayout divNoAlamat;
    private Button btnAddRekening;
    private TextView tvToolbarTitle;
    private LinearLayout divNoRekening;
    private ImageView btnAddRekeningToolbar;
    private ImageView btnAdd1;
    private ImageView btnAdd2;
    private ProgressBar pd;
    private SwipeRefreshLayout swipeRefrash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_item);
        initView();

        s = new SharedPref(this);

        callFungtion();
        getExtraIntent();
        mainButton();
        setToolbar();
    }

    private void callFungtion() {
        CallFunction.setRefreshListAlamat(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                String extra = getIntent().getStringExtra("extra");
                if (extra != null) {
                    if (extra.equals("alamat")) {
                        getListAlamat();
                    } else if (extra.equals("pilihAlamat")) {
                        pilihAlamat();
                    }
                }
                pd.setVisibility(View.VISIBLE);
                return null;
            }
        });

        CallFunction.setRefreshListRekening(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                getListRekening();
                pd.setVisibility(View.VISIBLE);
                return null;
            }
        });
    }

    private void getExtraIntent() {
        String extra = getIntent().getStringExtra("extra");
        if (extra != null) {
            if (extra.equals("alamat")) {
                getListAlamat();
                pullRefrashAlamat();
            } else if (extra.equals("rekening")) {
                getListRekening();
                pullRefrashRekening();
            } else if (extra.equals("pilihAlamat")) {
                pilihAlamat();
                pullRefrashPilihAlamat();
            }
        }
    }

    private void getListRekening() {
        btnAddRekeningToolbar.setVisibility(View.VISIBLE);
        btnAddAlamat.setVisibility(View.GONE);
        toolbarTitle.setText("Pengaturan Rekening");

        api.getRekening(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                if (response.isSuccessful()) {

                    if (response.body().getSuccess() == 1) {
                        rekening = response.body().rekening;
                        s.setListRekUser(response.body());
                        layoutManager = new LinearLayoutManager(DaftarItemActivity.this, LinearLayoutManager.VERTICAL, false);
                        mAdapter = new AdapterRekening(rekening, DaftarItemActivity.this);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(mAdapter);
                    } else if (response.body().getSuccess() == 0) {
                        divNoRekening.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                SweetAlert.onFailure(DaftarItemActivity.this, t.getMessage());
            }
        });
    }

    private void getListAlamat() {

        toolbarTitle.setText("Pengaturan Alamat");
        api.getAlamat(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                if (response.isSuccessful()) {

                    if (response.body().getSuccess() == 1) {
//                        ResponModel respon = response.body();
                        mItem = (ArrayList<ModelAlamat>) response.body().alamat;

                        layoutManager = new LinearLayoutManager(DaftarItemActivity.this, LinearLayoutManager.VERTICAL, false);
                        mAdapter = new AdapterAlamat(mItem, DaftarItemActivity.this);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(mAdapter);
                    } else if (response.body().getSuccess() == 0) {
                        divNoAlamat.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                SweetAlert.onFailure(DaftarItemActivity.this, t.getMessage());
            }
        });
    }

    private void pilihAlamat() {

        toolbarTitle.setText("Pengaturan Alamat");

        api.getAlamat(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                if (response.isSuccessful()) {

                    if (response.body().getSuccess() == 1) {
//                        ResponModel respon = response.body();
                        mItem = (ArrayList<ModelAlamat>) response.body().alamat;

                        layoutManager = new LinearLayoutManager(DaftarItemActivity.this, LinearLayoutManager.VERTICAL, false);
                        mAdapter = new AdapterPilihAlamat(mItem, DaftarItemActivity.this);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(mAdapter);
                    } else if (response.body().getSuccess() == 0) {
                        divNoAlamat.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                SweetAlert.onFailure(DaftarItemActivity.this, t.getMessage());
            }
        });
    }

    private void pullRefrashAlamat() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListAlamat();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void pullRefrashRekening() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListRekening();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void pullRefrashPilihAlamat() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pilihAlamat();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void mainButton() {
        btnAddAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String extra = getIntent().getStringExtra("extra");
                if (extra != null) {
                    if (extra.equals("pilihAlamat")) {
                        Intent i = new Intent(getApplicationContext(), TambahAlamatActivity.class);
                        i.putExtra("extra", "pilihAlamat");
                        startActivity(i);
                        finish();
                    } else {
                        startActivity(new Intent(getApplicationContext(), TambahAlamatActivity.class));
                        finish();
                    }
                }
            }
        });

        btnAddRekening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TambahRekeningActivity.class));
                finish();
            }
        });

        btnAddRekeningToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TambahRekeningActivity.class));
                finish();
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pengaturan Alamat");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnAddAlamat = (ImageView) findViewById(R.id.btn_add1);
        toolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        rv = (RecyclerView) findViewById(R.id.rv);
        divNoAlamat = (LinearLayout) findViewById(R.id.div_noAlamat);
        btnAddRekening = (Button) findViewById(R.id.btn_addRekening);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        divNoRekening = (LinearLayout) findViewById(R.id.div_noRekening);
        btnAddRekeningToolbar = (ImageView) findViewById(R.id.btn_add2);
        btnAdd1 = (ImageView) findViewById(R.id.btn_add1);
        btnAdd2 = (ImageView) findViewById(R.id.btn_add2);
        pd = (ProgressBar) findViewById(R.id.pd);
        swipeRefrash = (SwipeRefreshLayout) findViewById(R.id.swipeRefrash);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
