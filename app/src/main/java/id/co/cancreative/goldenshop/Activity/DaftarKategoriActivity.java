package id.co.cancreative.goldenshop.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.AdapterCategoryAll;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelCategory;
import id.co.cancreative.goldenshop.Model.ModelSubKat;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarKategoriActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;

    private ArrayList<ModelCategory> mKategori = new ArrayList<>();
    private ArrayList<ModelSubKat> mSubKategori = new ArrayList<>();
    private ModelCategory data;

    private Toolbar toolbar;
    private RecyclerView rv;
    private ProgressBar pd;
    private String extra;
    private LinearLayout divOnfilure;
    private TextView btnRefrash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_kategori);
        initView();

        s = new SharedPref(this);
        data = new ModelCategory();

        getIntentExtra();
        getKategori();
        setToolbar();
    }

    private void getIntentExtra() {
        extra = getIntent().getStringExtra("extra");
        data = getIntent().getParcelableExtra("data");
    }

    private void getKategori() {
        api.getKategori(s.getApi()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                if (response.body().success == 1) {
                    pd.setVisibility(View.GONE);
                    mKategori = response.body().kategori;
                    mAdapter = new AdapterCategoryAll(mKategori, DaftarKategoriActivity.this);
                    rv.setLayoutManager(new GridLayoutManager(DaftarKategoriActivity.this, 3));
                    rv.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.onFailure(DaftarKategoriActivity.this, t.getMessage());
                divOnfilure.setVisibility(View.VISIBLE);
                pd.setVisibility(View.GONE);
            }
        });
    }


    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pilih Kategori");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        rv = (RecyclerView) findViewById(R.id.rv);
        pd = (ProgressBar) findViewById(R.id.pd);
        divOnfilure = (LinearLayout) findViewById(R.id.div_onfilure);
        btnRefrash = (TextView) findViewById(R.id.btn_refrash);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
