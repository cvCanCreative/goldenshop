package id.co.cancreative.goldenshop.Activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.AdapterKomplain;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Komplain;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarKomplainActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;
    private ProgressBar pd;
    private RecyclerView rv;
    private LinearLayout lyKosong;
    private SwipeRefreshLayout swipeRefrash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_komplain);
        initView();

        s = new SharedPref(this);

        setToolbar();
        mainButton();
        getHistoryKomplain();
        pullrefrash();
    }

    private void mainButton() {
    }

    private void getHistoryKomplain() {
        api.getHistoryKomplain(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                swipeRefrash.setRefreshing(false);
                pd.setVisibility(View.GONE);
                lyKosong.setVisibility(View.GONE);

                if (response.body().success == 1) {
                    ArrayList<Komplain> mItem = response.body().komplain;
                    RecyclerView.Adapter mAdapter = new AdapterKomplain(mItem, DaftarKomplainActivity.this);
                    rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);
                } else {
                    lyKosong.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                swipeRefrash.setRefreshing(false);
                pd.setVisibility(View.GONE);
                lyKosong.setVisibility(View.VISIBLE);
                Toasti.onFailure(DaftarKomplainActivity.this, t.getMessage());
            }
        });
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHistoryKomplain();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Riwayat Komplain");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        pd = (ProgressBar) findViewById(R.id.pd);
        rv = (RecyclerView) findViewById(R.id.rv);
        lyKosong = (LinearLayout) findViewById(R.id.ly_kosong);
        swipeRefrash = (SwipeRefreshLayout) findViewById(R.id.swipeRefrash);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
