package id.co.cancreative.goldenshop.Activity;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukTradingPage;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiConfigDemo;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelCategory;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.ModelSubKat;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelSerializ;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarProdukActivity extends AppCompatActivity {

    private ApiService apiDemo = ApiConfigDemo.getInstanceRetrofit();
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private AppDbTerakhirDilihat db;

    private String pilih = "1000";
    private String pilihan;
    private String min, max;

    private int indexLoad = 1;
    private int viewThreshold = 8;
    private int pastVisibleitem, visibleItemCount, totalItemCount, previousTotal = 0;
    private boolean isLoading = true;

    private GridLayoutManager layoutManager;
    private AdapterProdukTrading mAdapter;
    private AdapterProdukTradingPage mAdapterPage;

    private ArrayList<ModelProduk> mItem = new ArrayList<>();
    private ArrayList<Produk> mProduk = new ArrayList<>();
    private ArrayList<ModelCategory> mKategori = new ArrayList<>();

    private ModelSubKat data;
    private BottomSheetBehavior behavior;
    private BottomSheetDialog mBottomSheetDialog;

    private Toolbar toolbar;
    private RecyclerView rv;
    private ProgressBar pd;
    private LinearLayout divNoProduk;
    private TextView tvPesan;
    private LinearLayout bottomSheet;
    private LinearLayout lyFilter;
    private LinearLayout lyUrutkan;
    private LinearLayout ly;
    private LinearLayout lyStatToko;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_produk);
        initView();

        data = new ModelSubKat();
        s = new SharedPref(this);
        db = Room.databaseBuilder(this, AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();

        resetIndex();
        setlayoutManager();

        getIntentExtra();
        setToolbar();
        mainButton();
    }

    private void resetIndex() {
        pastVisibleitem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        previousTotal = 0;
        indexLoad = 1;
    }

    private void setlayoutManager() {
        layoutManager = new GridLayoutManager(this, 2);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(layoutManager);
    }

    private void getIntentExtra() {
        data = getIntent().getParcelableExtra("data");

        String extra = getIntent().getStringExtra("extra");
        if (extra != null) {
            if (extra.equals("gsproduk")) {
                getGSProduk();
                ly.setVisibility(View.GONE);
            }
        } else {
            getProduk();
        }
    }

    private void getGSProduk() {
        api.getProdukGS(s.getApi(), 1).enqueue(new Callback<ResponModelSerializ>() {
            @Override
            public void onResponse(Call<ResponModelSerializ> call, Response<ResponModelSerializ> response) {
//                swipeRefrash.setRefreshing(false);
                pd.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ResponModelSerializ produk = response.body();

                    mProduk = (ArrayList<Produk>) produk.products;
                    RecyclerView.Adapter mAdapter = new AdapterProdukTrading(mProduk, DaftarProdukActivity.this, db);
                    rv.setLayoutManager(new GridLayoutManager(DaftarProdukActivity.this, 2));
                    rv.setAdapter(mAdapter);
                }

            }

            @Override
            public void onFailure(Call<ResponModelSerializ> call, Throwable t) {
                if (DaftarProdukActivity.this != null) {
                    SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
                }
            }
        });
    }

    private void getProduk() {

        int page = 1;

        api.getProdukByK(s.getApi(), data.getiDSUBKATEGORI(), page).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = response.body().products;
                    if (mProduk.size() >= 1) {
                        mAdapter = new AdapterProdukTrading(mProduk, DaftarProdukActivity.this, db);
                        rv.setAdapter(mAdapter);
                    } else {
                        tvPesan.setText("Produk pada kategori " + data.getsKATNAME() + " kosong");
                        divNoProduk.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
            }
        });

        setupPageination();
    }

    private void setupPageination() {
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
                        indexLoad = indexLoad + 1;
                        loadProductTrading(indexLoad);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadProductTrading(int index) {
        pb.setVisibility(View.VISIBLE);
        api.getProdukByK(s.getApi(), data.getiDSUBKATEGORI(), index).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ArrayList<Produk> produks = response.body().products;
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
            }
        });
    }

    private void filterKondisi(final String kondisi) {
        api.getProdukByKFilter(s.getApi(), data.getiDSUBKATEGORI(),
                "" + kondisi, 1).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = response.body().products;
                    if (mProduk.size() >= 1) {
                        mAdapter = new AdapterProdukTrading(mProduk, DaftarProdukActivity.this, db);
                        rv.setAdapter(mAdapter);
                    } else {
                        tvPesan.setText("Produk pada kategori " + data.getsKATNAME() + " kosong");
                        divNoProduk.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
            }
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
                        indexLoad = indexLoad + 1;
                        loadFilterKondisi(indexLoad, kondisi);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadFilterKondisi(int page, String kondisi) {
        api.getProdukByKFilter(s.getApi(), data.getiDSUBKATEGORI(),
                "" + kondisi, page).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ArrayList<Produk> produks = response.body().products;
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
            }
        });
    }

    private void filterStatToko(final String kondisi) {
        api.getProdukByKAndStatToko(s.getApi(), data.getiDSUBKATEGORI(),
                "" + kondisi, 1).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = response.body().products;
                    if (mProduk.size() >= 1) {
                        mAdapter = new AdapterProdukTrading(mProduk, DaftarProdukActivity.this, db);
                        rv.setAdapter(mAdapter);
                    } else {
                        rv.setVisibility(View.GONE);
                        tvPesan.setText("Produk pada kategori " + data.getsKATNAME() + " kosong");
                        divNoProduk.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
            }
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
                        indexLoad = indexLoad + 1;
                        loadFilterToko(indexLoad, kondisi);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadFilterToko(int page, String kondisi) {
        api.getProdukByKAndStatToko(s.getApi(), data.getiDSUBKATEGORI(),
                "" + kondisi, page).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ArrayList<Produk> produks = response.body().products;
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
            }
        });
    }

    private void filterHarga(final String min, final String max) {
        api.getProdukByKbyHarga(s.getApi(), data.getiDSUBKATEGORI(),
                "" + min,
                "" + max,
                1 ).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = response.body().products;
                    if (mProduk.size() >= 1) {
                        mAdapter = new AdapterProdukTrading(mProduk, DaftarProdukActivity.this, db);
                        rv.setAdapter(mAdapter);
                    } else {
                        rv.setVisibility(View.GONE);
                        tvPesan.setText("Produk pada kategori " + data.getsKATNAME() + " kosong");
                        divNoProduk.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                divNoProduk.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
            }
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
                        indexLoad = indexLoad + 1;
                        loadFilterHarga(indexLoad, min, max);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadFilterHarga(int page, String min, String max) {
        api.getProdukByKbyHarga(s.getApi(), data.getiDSUBKATEGORI(),
                "" + min,
                "" + max, page).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ArrayList<Produk> produks = response.body().products;
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
            }
        });
    }

    private void sheetHarga() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_filter, null);
        final ImageView imgClose = view.findViewById(R.id.img_close);
        final EditText edtMin = view.findViewById(R.id.edt_min);
        final EditText edtMax = view.findViewById(R.id.edt_max);
        final Button btnAktifkan = view.findViewById(R.id.btn_aktifkan);

        edtFilter(edtMin, edtMax);

        btnAktifkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "1000";
                mBottomSheetDialog.dismiss();
                resetIndex();
                filterHarga(min, max);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void edtFilter(final EditText edtMin, final EditText edtMax) {

        edtMin.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtMin.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        min = cleanString;
                        edtMin.setText(formatted);
                        edtMin.setSelection(formatted.length());

                        edtMin.addTextChangedListener(this);
                    } else {
                        edtMin.setText(current);
                        edtMin.setSelection(current.length());

                        edtMin.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });


        edtMax.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtMax.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        max = cleanString;
                        edtMax.setText(formatted);
                        edtMax.setSelection(formatted.length());

                        edtMax.addTextChangedListener(this);
                    } else {
                        edtMax.setText(current);
                        edtMax.setSelection(current.length());

                        edtMax.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }

    private void sheetKondisi() {
        //untuk hide
//        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_urutkan, null);
        final LinearLayout lyBaru = view.findViewById(R.id.ly_baru);
        final LinearLayout lyLama = view.findViewById(R.id.ly_bekas);
        final LinearLayout lyLaris = view.findViewById(R.id.ly_laris);
        final LinearLayout lyNormal = view.findViewById(R.id.ly_normal);
        final ImageView imgClose = view.findViewById(R.id.img_close);

        TextView tvNoBaru = view.findViewById(R.id.tv_no_baru);
        TextView tvNoBekas = view.findViewById(R.id.tv_no_bekas);
        TextView tvNoAll = view.findViewById(R.id.tv_no_semua);

        ImageView imgBaru = view.findViewById(R.id.img_baru);
        ImageView imgBekas = view.findViewById(R.id.img_bekas);
        ImageView imgAll = view.findViewById(R.id.img_semua);

        tvNoBaru.setText("1");
        tvNoBekas.setText("2");
        tvNoAll.setText("3");

        if (pilih.equals(tvNoBaru.getText().toString())) {
            imgBaru.setVisibility(View.VISIBLE);
        } else if (pilih.equals(tvNoBekas.getText().toString())) {
            imgBekas.setVisibility(View.VISIBLE);
        } else if (pilih.equals(tvNoAll.getText().toString())) {
            imgAll.setVisibility(View.VISIBLE);
        } else {
            imgAll.setVisibility(View.GONE);
            imgBaru.setVisibility(View.GONE);
            imgBekas.setVisibility(View.GONE);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        lyBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "NEW";
                pilih = "1";
                resetIndex();
                filterKondisi(pilihan);
                mBottomSheetDialog.dismiss();
            }
        });

        lyLama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "SECOND";
                pilih = "2";
                resetIndex();
                filterKondisi(pilihan);
                mBottomSheetDialog.dismiss();
            }
        });

        lyNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "ALL";
                pilih = "3";
                getProduk();
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void sheetStatToko() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_stat_toko, null);
        final LinearLayout lyAktif = view.findViewById(R.id.ly_aktif);
        final LinearLayout lyBelum = view.findViewById(R.id.ly_belum);
        final LinearLayout lyNormal = view.findViewById(R.id.ly_normal);
        final ImageView imgClose = view.findViewById(R.id.img_close);

        TextView tvNoAktif = view.findViewById(R.id.tv_no_aktif);
        TextView tvNoBelum = view.findViewById(R.id.tv_no_belum);
        TextView tvNoAll = view.findViewById(R.id.tv_no_semua);

        ImageView imgAktif = view.findViewById(R.id.img_aktif);
        ImageView imgBelum = view.findViewById(R.id.img_belum);
        ImageView imgAll = view.findViewById(R.id.img_semua);

        tvNoAktif.setText("5");
        tvNoBelum.setText("6");
        tvNoAll.setText("7");

        if (pilih.equals(tvNoAktif.getText().toString())) {
            imgAktif.setVisibility(View.VISIBLE);
        } else if (pilih.equals(tvNoBelum.getText().toString())) {
            imgBelum.setVisibility(View.VISIBLE);
        } else if (pilih.equals(tvNoAll.getText().toString())) {
            imgAll.setVisibility(View.VISIBLE);
        } else {
            imgAll.setVisibility(View.GONE);
            imgAktif.setVisibility(View.GONE);
            imgBelum.setVisibility(View.GONE);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        lyAktif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "TK_AKTIF";
                pilih = "5";
                resetIndex();
                filterStatToko(pilihan);
                mBottomSheetDialog.dismiss();
            }
        });

        lyBelum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "TK_BLM_AKTIF";
                pilih = "6";
                resetIndex();
                filterStatToko(pilihan);
                mBottomSheetDialog.dismiss();
            }
        });

        lyNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "ALL";
                pilih = "7";
                resetIndex();
                getProduk();
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }


    private void mainButton() {
        lyUrutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetKondisi();
            }
        });

        lyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetHarga();
            }
        });

        lyStatToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetStatToko();
            }
        });


        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        if (data != null) {
            getSupportActionBar().setTitle(data.getsKATNAME());
        } else {
            getSupportActionBar().setTitle(getString(R.string.produk_txt));
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        rv = (RecyclerView) findViewById(R.id.rv);
        pd = (ProgressBar) findViewById(R.id.pd);
        divNoProduk = (LinearLayout) findViewById(R.id.div_noProduk);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        lyFilter = (LinearLayout) findViewById(R.id.ly_filter);
        lyUrutkan = (LinearLayout) findViewById(R.id.ly_urutkan);
        ly = (LinearLayout) findViewById(R.id.ly);
        lyStatToko = (LinearLayout) findViewById(R.id.ly_stat_toko);
        pb = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
