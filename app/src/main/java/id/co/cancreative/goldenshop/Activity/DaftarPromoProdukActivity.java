package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Adapter.AdapterPromo;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.ItemClickSupport;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.Promo;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarPromoProdukActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;

    private RecyclerView.Adapter mAdapter;
    private Produk produk;

    private String extra, idSub, toolbarText, namaProduk, promoProduk, idProduk;
    
    private Toolbar toolbar;
    private SwipeRefreshLayout swipeRefrash;
    private TextView tvToolbarTitle;
    private LinearLayout divProdukKosong;
    private TextView tvPesan;
    private RecyclerView rv;
    private ProgressBar pd;
    private TextView tvTitle;
    private LinearLayout btnReload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_promo_produk);
        initView();

        s = new SharedPref(this);
        produk =s.getProduk();
        extra = getIntent().getStringExtra("extra");
        idProduk = getIntent().getStringExtra("idProduk");
        promoProduk = getIntent().getStringExtra("produk");

        getPromoById();

        setToolbar();
        mainButton();
        pullrefrash();

        CallFunction.setRefrashPromo(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                getPromoById();
                return null;
            }
        });
    }

    private void mainButton() {
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPromoById();
            }
        });
    }

    private void getPromoById(){
        api.getPromoByProduk(s.getApi(), s.getIdUser(), s.getSession(), idProduk).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                if (response.body().success == 1) {
                    divProdukKosong.setVisibility(View.GONE);
                    final ArrayList<Promo> mItem = response.body().promo;
                    mAdapter = new AdapterPromo(mItem, DaftarPromoProdukActivity.this);
                    rv.setLayoutManager(new LinearLayoutManager(DaftarPromoProdukActivity.this, LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);

                    ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                        @Override
                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                            onItemSelectedPromo(mItem.get(position));
                        }
                    });

                } else {
                    divProdukKosong.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                tvPesan.setText("Gagal memuat data");
                tvTitle.setText("Tab untuk memuat ulang");
                divProdukKosong.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(DaftarPromoProdukActivity.this, t.getMessage());
            }
        });
    }

    private void onItemSelectedPromo(final Promo p){
        Intent i = new Intent(this, DetailPromoActivity.class);
        s.setPromo(p);
        startActivity(i);
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPromoById();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GoldenShop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        swipeRefrash = (SwipeRefreshLayout) findViewById(R.id.swipeRefrash);
        divProdukKosong = (LinearLayout) findViewById(R.id.div_produkKosong);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        rv = (RecyclerView) findViewById(R.id.rv);
        pd = (ProgressBar) findViewById(R.id.pd);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        btnReload = (LinearLayout) findViewById(R.id.btn_reload);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
