package id.co.cancreative.goldenshop.Activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Fragment.DaftarTransaksi.PesananDikirimFragment;
import id.co.cancreative.goldenshop.Fragment.DaftarTransaksi.PesananDiprosesFragment;
import id.co.cancreative.goldenshop.Fragment.DaftarTransaksi.PesananKomplainFragment;
import id.co.cancreative.goldenshop.Fragment.DaftarTransaksi.PesananSelesaiFragment;
import id.co.cancreative.goldenshop.Fragment.DaftarTransaksi.PesananTibaFragment;
import id.co.cancreative.goldenshop.Fragment.DaftarTransaksi.WaitingConfFragment;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.R;

public class DaftarTransaksiActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private FragmentPagerItemAdapter adapter;
    private ViewPagerAdapter lyAdapter;

    private boolean menungguKonfirmasi = false;
    private String posisi;

    private Toolbar toolbar;
    private Button btnMeungguKonfirmasi;
    private Button btnPesananDiproses;
    private SmartTabLayout viewpagertab;
    private ViewPager viewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_transaksi);
        initView();

        s = new SharedPref(this);
        posisi = getIntent().getStringExtra("FRAGMENT_ID");


        setToolbar();
        mainButton();

        setupViewPager(viewpager); //wait
        viewpagertab.setViewPager(viewpager);
        if (posisi != null){
            //select tab
            viewpagertab.getTabAt(Integer.valueOf(posisi/*posisi tab terpilih*/)).setSelected(true);
            //select viewpager
            viewpager.setCurrentItem(Integer.valueOf(posisi)/*posisi tab terpilih*/);
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        lyAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        lyAdapter.addFragment(new WaitingConfFragment(), "Menunggu Konfirmasi");
        lyAdapter.addFragment(new PesananDiprosesFragment(), "Pesanan Diproses");
        lyAdapter.addFragment(new PesananDikirimFragment(), "Pesanan Dikirim");
        lyAdapter.addFragment(new PesananTibaFragment(), "Pesanan Tiba");
        lyAdapter.addFragment(new PesananKomplainFragment(), "Pesanan Dikomplain");
        lyAdapter.addFragment(new PesananSelesaiFragment(), "Pesanan Selesai");
        viewPager.setAdapter(lyAdapter);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragmentList.get(i);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String detail) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(detail);
        }
    }

    private void mainButton() {
        btnMeungguKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.button_daftar_trans_white);
                ((Button) v).setTextColor(R.color.black);
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Daftar Transaksi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnMeungguKonfirmasi = (Button) findViewById(R.id.btn_meungguKonfirmasi);
        btnPesananDiproses = (Button) findViewById(R.id.btn_pesananDiproses);
        viewpagertab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
