package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.DaftarTrans.AdapterMenungguPembayaran;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarTransaksiMenunguPembayaran extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;
    private ArrayList<Transaksi> mItem = new ArrayList<>();
    private ArrayList<Transaksi> listTrans = new ArrayList<>();

    private RecyclerView rv;
    private LinearLayout lyKosong;
    private TextView tvPesan;
    private ProgressBar pd;
    private SwipeRefreshLayout swipeRefrash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_transaksi);
        initView();

        s = new SharedPref(this);

        pullrefrash();
        getHistory("WAITING_TF");
        setToolbar();
    }

    private void getHistory(String status) {

        api.getHistoryTransaksi(s.getApi(), s.getIdUser(), s.getSession(), status).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                if (response.body().success == 1) {
                    lyKosong.setVisibility(View.GONE);
                    mItem = response.body().transaksi;
                    listTrans = new ArrayList<>();
                    String a = "";

                    for (Transaksi d : mItem) {
                        if (!a.equals(d.TS_KODE_PAYMENT)) {
                            a = d.TS_KODE_PAYMENT;
                            listTrans.add(d);
                        }
                    }

                    RecyclerView.Adapter mAdapter = new AdapterMenungguPembayaran(listTrans, DaftarTransaksiMenunguPembayaran.this);
                    rv.setLayoutManager(new LinearLayoutManager(DaftarTransaksiMenunguPembayaran.this, LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);
                } else {
                    lyKosong.setVisibility(View.VISIBLE);
                    tvPesan.setText("Tidak Ada Data");

                    listTrans = new ArrayList<>();
                    RecyclerView.Adapter mAdapter = new AdapterMenungguPembayaran(listTrans, DaftarTransaksiMenunguPembayaran.this);
                    rv.setLayoutManager(new LinearLayoutManager(DaftarTransaksiMenunguPembayaran.this, LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                SweetAlert.onFailure(DaftarTransaksiMenunguPembayaran.this, t.getMessage());
                lyKosong.setVisibility(View.VISIBLE);
                tvPesan.setText("Gagal mengambil data, Tab untuk reload");
                lyKosong.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lyKosong.setVisibility(View.GONE);
                        pd.setVisibility(View.VISIBLE);
                        getHistory("WAITING_TF");
                    }
                });
            }
        });
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHistory("WAITING_TF");
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Menunggu Pembayaran");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        rv = (RecyclerView) findViewById(R.id.rv);
        lyKosong = (LinearLayout) findViewById(R.id.ly_kosong);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        pd = (ProgressBar) findViewById(R.id.pd);
        swipeRefrash = (SwipeRefreshLayout) findViewById(R.id.swipeRefrash);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(DaftarTransaksiMenunguPembayaran.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("extra", "akun");
        startActivity(i);
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        getHistory("WAITING_TF");
        super.onResume();
    }
}
