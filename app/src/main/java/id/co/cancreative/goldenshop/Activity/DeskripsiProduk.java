package id.co.cancreative.goldenshop.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.TbTerakhirDilihat;

public class DeskripsiProduk extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Produk produk;
    private TbTerakhirDilihat dilihat;
    private String dataInten;


    private Toolbar toolbar;
    private ImageView imgBarang;
    private TextView tvNamabarang;
    private TextView tvHargaBarang;
    private TextView tvKeterangan;
    private TextView tvNamaToko;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deskripsi_produk);
        initView();

        s = new SharedPref(this);
        produk = s.getProduk();
        dilihat = s.getProdukT();

        dataInten = getIntent().getStringExtra("dataInten");
        if (dataInten != null){
            if (dataInten.equals("terakhir")) {
                setValueD();
            } else {
                setValue();
            }
        } else {
            setValue();
        }

        setToolbar();
    }

    private void setValue() {
        tvNamabarang.setText(produk.BA_NAME);
        tvHargaBarang.setText(new Helper().convertRupiah(Integer.valueOf(produk.BA_PRICE)));
        Spanned string = Html.fromHtml(produk.BA_DESCRIPTION);
        tvKeterangan.setText(string);
        tvNamaToko.setText(produk.TOK_NAME);

        String image = new Helper().splitText(produk.BA_IMAGE);
        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.img_kosong)
                .error(R.drawable.img_kosong)
                .noFade()
                .into(imgBarang);
    }

    private void setValueD() {
        tvNamabarang.setText(dilihat.nama);
        tvHargaBarang.setText(new Helper().convertRupiah(Integer.valueOf(dilihat.harga)));
        Spanned string = Html.fromHtml(dilihat.deskripsi);
        tvKeterangan.setText(string);
        tvNamaToko.setText(dilihat.penjual);

        String image = new Helper().splitText(dilihat.gambar);
        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.img_kosong)
                .error(R.drawable.img_kosong)
                .noFade()
                .into(imgBarang);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Deskripsi Produk");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgBarang = (ImageView) findViewById(R.id.img_barang);
        tvNamabarang = (TextView) findViewById(R.id.tv_namabarang);
        tvHargaBarang = (TextView) findViewById(R.id.tv_hargaBarang);
        tvKeterangan = (TextView) findViewById(R.id.tv_keterangan);
        tvNamaToko = (TextView) findViewById(R.id.tv_namaToko);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
