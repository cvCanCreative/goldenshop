package id.co.cancreative.goldenshop.Activity;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.github.chrisbanes.photoview.PhotoView;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.ChildAnimation;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.Feedback;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.TbTerakhirDilihat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProdukActivity extends AppCompatActivity implements ViewPagerEx.OnPageChangeListener {

    private ArrayList<Produk> mProduk = new ArrayList<>();
    private ArrayList<Produk> mProduk2 = new ArrayList<>();
    private List<Feedback> listFeedback = new ArrayList<>();
    private Produk produk;
    private TbTerakhirDilihat dilihat;
    private SharedPref s;
    private AppDbTerakhirDilihat db;

    private String dataInten;
    private String idBarang;

    private View cart_badge;
    private MenuItem wishlist_menu;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterProdukTrading mAdapter;
    private Toolbar toolbar;
    private ImageView tvImgProduk, btnBack, imgBintang;
    private TextView tvNamaProduk;
    private CardView card;
    private RelativeLayout layout;
    private TextView tvDiscount, tvHargaAsli, tvHargaProduk;
    private ImageView image, image2, image3, image4, image5;
    private TextView tvStok;
    private TextView tvKeterangan;
    private CircleImageView imgUser;
    private RecyclerView rv;
    private LinearLayout divFooter;
    private LinearLayout btnBeli;
    private TextView tvTerjual, tvKondisi, tvNamaToko, tvAsalToko;
    private LinearLayout btnAddToCart;
    private CircleImageView imgPenjual;
    private SliderLayout sliderHome;
    private RelativeLayout divPopUpImage;
    private ImageView btnClose;
    private PhotoView imgView;
    private RelativeLayout divPromo;
    private TextView tvLihatUlasan;
    private AppCompatRatingBar rtBarKomen;
    private TextView tvPembuatKomen;
    private TextView tvKomen;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private LinearLayout lyKomen;
    private LinearLayout btnToko;
    private TextView tvInfoDiscount;
    private TextView tvStatusToko;
    private TextView btnSelengkapnya;
    private RelativeLayout divHargaDiscount;
    private AppCompatRatingBar rtBarToko;
    private TextView tvRetingToko;
    private RelativeLayout divLayer;
    private ProgressBar pd;
    private LinearLayout divKosong;
    private TextView tvKosong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);
        initView();

        s = new SharedPref(this);
        db = Room.databaseBuilder(this, AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();

        dataInten = getIntent().getStringExtra("dataInten");
        if (dataInten.equals("terakhir")) {
            dilihat = s.getProdukT();
            getDetailBarang(dilihat.idProduk);
            setupSliderD();
            setValueD();
        } else {
            produk = s.getProduk();
            setValue();
            setupSlider();
        }

        mainButton();
        setToolbar();
        setlayoutManager();
        getProduk();
//        setValue();
//        setupSlider();
    }

    private void getDetailBarang(int id) {
        divLayer.setVisibility(View.VISIBLE);
        api.getDetailBarang(s.getApi(), id).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().success == 1) {
                        divLayer.setVisibility(View.GONE);
                        s.setProduk(response.body().product);
                        produk = s.getProduk();
                        setValue();
                    } else {
                        Toasti.error(DetailProdukActivity.this, response.body().message);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                SweetAlert.onFailure(DetailProdukActivity.this, t.getMessage());
            }
        });
    }

    private void setupSlider() {

        String sliderList = produk.BA_IMAGE.replace("|", " ");
        final String strArray[] = sliderList.split(" ");

        for (final String d : strArray) {
            TextSliderView textSliderView = new TextSliderView(getApplicationContext());
            textSliderView
                    .image(Config.URL_produkGolden + d)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            divPopUpImage.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(Config.URL_produkGolden + d)
                                    .error(R.drawable.ic_cat1)
                                    .placeholder(R.drawable.image_loading)
                                    .into(imgView);
                        }
                    });

            sliderHome.addSlider(textSliderView);
        }

        sliderHome.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderHome.setCustomAnimation(new ChildAnimation());
//        sliderHome.setCustomIndicator(indicator);
        sliderHome.setDuration(8000);
        sliderHome.addOnPageChangeListener(this);
    }

    private void setupSliderD() {

        String sliderList = dilihat.gambar.replace("|", " ");
        final String strArray[] = sliderList.split(" ");

        for (final String d : strArray) {
            TextSliderView textSliderView = new TextSliderView(getApplicationContext());
            textSliderView
                    .image(Config.URL_produkGolden + d)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            divPopUpImage.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(Config.URL_produkGolden + d)
                                    .error(R.drawable.ic_cat1)
                                    .placeholder(R.drawable.image_loading)
                                    .into(imgView);
                        }
                    });

            sliderHome.addSlider(textSliderView);
        }

        sliderHome.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderHome.setCustomAnimation(new ChildAnimation());
//        sliderHome.setCustomIndicator(indicator);
        sliderHome.setDuration(8000);
        sliderHome.addOnPageChangeListener(this);
    }

    private void setValue() {

        rtBarToko.setRating(produk.rating_toko);
        tvRetingToko.setText("" + produk.rating_toko + " / ");
        if (produk.rating_toko == 0.0) tvRetingToko.setText("Belum Ada Ulasan" + " / ");

        if (produk.TOK_STATUS != null) {
            if (produk.TOK_STATUS.equals("TK_AKTIF")) {
                tvStatusToko.setText("Verified Account");
            } else {
                tvStatusToko.setText("Toko Belum Terverifikasi");
                Picasso.with(this)
                        .load(R.drawable.ic_reting_juragan)
                        .placeholder(R.drawable.ic_reting_rekomeded)
                        .error(R.drawable.ic_reting_rekomeded)
                        .noFade()
                        .into(image3);
            }
        }


        final int min1 = 10;
        final int max1 = 35;
        final int randomTerjaul = new Random().nextInt((max1 - min1) + 1) + min1;

        final int min = 10;
        final int max = 30;
        final int random = new Random().nextInt((max - min) + 1) + min;
        int hargaAsli = Integer.valueOf(produk.BA_PRICE);

        idBarang = "" + produk.ID_BARANG;
        tvNamaProduk.setText(produk.BA_NAME);
        tvHargaProduk.setText(new Helper().convertRupiah(hargaAsli));
        tvStok.setText(produk.BA_STOCK);
        tvTerjual.setText(String.valueOf(produk.TERJUAL));
        tvKondisi.setText(produk.BA_CONDITION);
        Spanned string = Html.fromHtml(produk.BA_DESCRIPTION);
        tvKeterangan.setText(string);
        tvNamaToko.setText(produk.TOK_NAME);
        tvAsalToko.setText(produk.TOK_KOTA);

        if (produk.promo_status == 1) {

            divPromo.setVisibility(View.VISIBLE);
            divHargaDiscount.setVisibility(View.VISIBLE);

            tvHargaAsli.setPaintFlags(tvHargaAsli.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            int potongan = 10000;
            if (produk.PR_POTONGAN != null && produk.PR_STATUS.equals("ACTIVE")) {
                potongan = Integer.parseInt(produk.PR_POTONGAN);
            } else {
                potongan = Integer.parseInt(produk.promo_admin.get(0).PR_POTONGAN);
            }

            int discount = Integer.valueOf(produk.BA_PRICE) - Integer.valueOf(potongan);

            if (discount < 0) discount = 0;
            tvInfoDiscount.setText("Diskon " + new Helper().convertRupiah(potongan) + " dengan Promo Toko");
            tvHargaAsli.setText(new Helper().convertRupiah(hargaAsli));
            tvHargaProduk.setText(new Helper().convertRupiah(discount));
        }

        String image = new Helper().splitText(produk.BA_IMAGE);

        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.img_kosong)
                .error(R.drawable.img_kosong)
                .noFade()
                .into(tvImgProduk);

        Picasso.with(this)
                .load(Config.url_toko + produk.TOK_FOTO)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(imgPenjual);

        getData(idBarang);

    }

    private void setValueD() {

        if (dilihat.kodePromo != null) {
            divPromo.setVisibility(View.VISIBLE);
        }

        final int min1 = 10;
        final int max1 = 35;
        final int randomTerjaul = new Random().nextInt((max1 - min1) + 1) + min1;

        final int min = 10;
        final int max = 30;
        final int random = new Random().nextInt((max - min) + 1) + min;
        int hargaAsli = Integer.valueOf(dilihat.harga);
        int diskon = (int) (hargaAsli * ((double) random / 100));
        int hargaAkhir = hargaAsli - diskon;

        idBarang = "" + dilihat.idProduk;
        tvNamaProduk.setText(dilihat.nama);
        tvHargaAsli.setText(new Helper().convertRupiah(hargaAsli));
        tvHargaAsli.setPaintFlags(tvHargaAsli.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvHargaProduk.setText(new Helper().convertRupiah(hargaAkhir));
        tvDiscount.setText(String.valueOf(random) + "%");
        tvStok.setText("" + dilihat.stok);
        tvTerjual.setText(String.valueOf(randomTerjaul));
        tvKondisi.setText(dilihat.kondisi);
        Spanned string = Html.fromHtml(dilihat.deskripsi);
        tvKeterangan.setText(string);
        tvNamaToko.setText(dilihat.penjual);
        tvAsalToko.setText(dilihat.kotaPenjual);

        String image = new Helper().splitText(dilihat.gambar);

        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.img_kosong)
                .error(R.drawable.img_kosong)
                .noFade()
                .into(tvImgProduk);

        Picasso.with(this)
                .load(Config.url_toko + dilihat.fotoPenjual)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(imgPenjual);

        getData(idBarang);

    }

    private void mainButton() {

        btnSelengkapnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DeskripsiProduk.class);
                if (dataInten.equals("terakhir")) {
                    intent.putExtra("dataInten", "terakhir");
                }
                startActivity(intent);
            }
        });

        btnToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DetailTokoActivity.class);
                startActivity(intent);
            }
        });

        divPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), DetailPromoActivity.class);
//                Promo p = new Promo();
//                p.PR_CODE = produk.PR_CODE;
//                p.PR_MIN = produk.PR_MIN;
//                p.PR_STATUS = produk.PR_STATUS;
//                p.PR_POTONGAN = produk.PR_POTONGAN;
//                p.ID_PROMO = produk.ID_PROMO;
//                p.PR_EXPIRED = produk.PR_EXPIRED;
//                p.PR_NAME = produk.PR_NAME;
//                s.setPromo(p);
//                startActivity(i);

                Intent i = new Intent(getApplicationContext(), DaftarPromoProdukActivity.class);
                i.putExtra("idProduk", produk.ID_BARANG);
                startActivity(i);
            }
        });

        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), TambahKeranjangActivity.class);
                startActivity(i);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvLihatUlasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LihatUlasanActivity.class);
                intent.putExtra("id_barang", "" + idBarang);
                startActivity(intent);
            }
        });

        btnBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (s.getStatusLogin()) {
                    Intent i = new Intent(getApplicationContext(), PesananAturActivity.class);
                    startActivity(i);
                    return;
                }

                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                divPopUpImage.setVisibility(View.GONE);
            }
        });


    }

    private void getData(final String id) {
        SweetAlert.sendingData(this);
        api.getFeedback(s.getApi(), "" + id).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                if (response.isSuccessful()) {
                    SweetAlert.dismis();
                    if (response.body().getSuccess() == 1) {
                        lyKomen.setVisibility(View.VISIBLE);
                        listFeedback = response.body().feedback;
                        rtBarKomen.setRating(Float.parseFloat(listFeedback.get(0).FE_RATING));
                        tvPembuatKomen.setText("Oleh " + listFeedback.get(0).US_USERNAME + ", " + new Helper().convertDateTime(listFeedback.get(0).CREATED_AT, "yyyy-MM-dd hh:mm:ss") + " WIB");
                        tvKomen.setText(listFeedback.get(0).FE_COMMENT);

                        tvLihatUlasan.setText("Lihat Semua Ulasan (" + listFeedback.size() + ")");
                    } else {
                        lyKomen.setVisibility(View.GONE);
                        tvLihatUlasan.setText("Belum ada ulasan");
                    }
                } else {
                    SweetAlert.dismis();
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                final SweetAlertDialog dialogGagal = new SweetAlertDialog(DetailProdukActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Ulangi");

                dialogGagal.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        getData(id);
                    }
                });
            }
        });
    }

//    private void getProduk() {
//        mProduk.addAll(DataProduct.getListData());
//        layoutManager = new GridLayoutManager(this, 2);
//        mAdapter = new AdapterProdukRecomed(mProduk, this);
//        rv.setLayoutManager(layoutManager);
//        rv.setAdapter(mAdapter);
//    }

    private void setlayoutManager() {
        layoutManager = new GridLayoutManager(this, 2);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(layoutManager);
    }

    private void getProduk() {

        int page = 1;

        api.getProdukByK(s.getApi(), produk.ID_SUB_KATEGORI, page).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                divKosong.setVisibility(View.GONE);

                if (response.isSuccessful()) {

                    if (response.body().success == 1) {
                        mProduk = response.body().products;
                        for (Produk p : mProduk){
                            if (!idBarang.equals(p.ID_BARANG)){
                                Produk produk = new Produk();
                                produk.ID_BARANG = p.ID_BARANG;
                                produk.ID_TOKO = p.ID_TOKO;
                                produk.ID_PROMO = p.ID_PROMO;
                                produk.ID_SUB_KATEGORI = p.ID_SUB_KATEGORI;
                                produk.BA_IMAGE = p.BA_IMAGE;
                                produk.BA_NAME = p.BA_NAME;
                                produk.BA_PSHOP = p.BA_PSHOP;
                                produk.BA_PGLD = p.BA_PGLD;
                                produk.BA_PRICE = p.BA_PRICE;
                                produk.BA_SKU = p.BA_SKU;
                                produk.BA_DESCRIPTION = p.BA_DESCRIPTION;
                                produk.BA_STOCK = p.BA_STOCK;
                                produk.BA_WEIGHT = p.BA_WEIGHT;
                                produk.BA_CONDITION = p.BA_CONDITION;
                                produk.CREATED_AT = p.CREATED_AT;
                                produk.UPDATED_AT = p.UPDATED_AT;
                                produk.TOK_NAME = p.TOK_NAME;
                                produk.TOK_FOTO = p.TOK_FOTO;
                                produk.TOK_DETAIL = p.TOK_DETAIL;
                                produk.TOK_NO_KTP = p.TOK_NO_KTP;
                                produk.TOK_FOTO_KTP = p.TOK_FOTO_KTP;
                                produk.TOK_SELFIE = p.TOK_SELFIE;
                                produk.TOK_ADDRESS = p.TOK_ADDRESS;
                                produk.TOK_ID_PROV = p.TOK_ID_PROV;
                                produk.TOK_ID_KOTA = p.TOK_ID_KOTA;
                                produk.TOK_KOTA = p.TOK_KOTA;
                                produk.TOK_ID_KECAMATAN = p.TOK_ID_KECAMATAN;
                                produk.TOK_KECAMATAN = p.TOK_KECAMATAN;
                                produk.TOK_STATUS = p.TOK_STATUS;
                                produk.PR_JENIS = p.PR_JENIS;
                                produk.PR_NAME = p.PR_NAME;
                                produk.PR_DESKRIPSI = p.PR_DESKRIPSI;
                                produk.PR_CODE = p.PR_CODE;
                                produk.PR_POTONGAN = p.PR_POTONGAN;
                                produk.PR_MIN = p.PR_MIN;
                                produk.PR_STATUS = p.PR_STATUS;
                                produk.PR_MULAI = p.PR_MULAI;
                                produk.PR_EXPIRED = p.PR_EXPIRED;
                                produk.ID_KATEGORI = p.ID_KATEGORI;
                                produk.SKAT_NAME = p.SKAT_NAME;
                                produk.SKAT_IMAGE = p.SKAT_IMAGE;
                                produk.TERJUAL = p.TERJUAL;
                                produk.promo_status = p.promo_status;
                                produk.rating_toko = p.rating_toko;
                                produk.promo_admin = p.promo_admin;
                                mProduk2.add(produk);
                            }
                        }

                        if (mProduk2.size() != 0) {
                            mAdapter = new AdapterProdukTrading(mProduk, DetailProdukActivity.this, db);
                            rv.setAdapter(mAdapter);
                        } else {
                            divKosong.setVisibility(View.VISIBLE);
                        }
                    } else {
                        divKosong.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                divKosong.setVisibility(View.VISIBLE);
                SweetAlert.onFailure(DetailProdukActivity.this, t.getMessage());
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle("Toolbar");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_produk, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == R.id.item_cart) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("extra", "keranjang");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else if (item_id == R.id.item_menu) {

        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        tvImgProduk = (ImageView) findViewById(R.id.tv_imgProduk);
        btnBack = (ImageView) findViewById(R.id.btn_back);
        imgBintang = (ImageView) findViewById(R.id.img_bintang);
        tvNamaProduk = (TextView) findViewById(R.id.tv_namaProduk);
        card = (CardView) findViewById(R.id.card);
        layout = (RelativeLayout) findViewById(R.id.layout);
        tvDiscount = (TextView) findViewById(R.id.tv_discount);
        tvHargaAsli = (TextView) findViewById(R.id.tv_hargaAsli);
        tvHargaProduk = (TextView) findViewById(R.id.tv_hargaProduk);
        image = (ImageView) findViewById(R.id.image);
        tvStok = (TextView) findViewById(R.id.tv_stok);
        image2 = (ImageView) findViewById(R.id.image2);
        tvKeterangan = (TextView) findViewById(R.id.tv_keterangan);
        imgUser = (CircleImageView) findViewById(R.id.img_penjual);
        image3 = (ImageView) findViewById(R.id.image3);
        image4 = (ImageView) findViewById(R.id.image4);
        image5 = (ImageView) findViewById(R.id.image5);
        rv = (RecyclerView) findViewById(R.id.rv);
        divFooter = (LinearLayout) findViewById(R.id.div_footer);
        tvTerjual = (TextView) findViewById(R.id.tv_terjual);
        tvKondisi = (TextView) findViewById(R.id.tv_kondisi);
        btnBeli = (LinearLayout) findViewById(R.id.btn_beli);
        tvNamaToko = (TextView) findViewById(R.id.tv_namaToko);
        tvAsalToko = (TextView) findViewById(R.id.tv_asalToko);
        btnAddToCart = (LinearLayout) findViewById(R.id.btn_addToCart);
        imgPenjual = (CircleImageView) findViewById(R.id.img_penjual);
        sliderHome = (SliderLayout) findViewById(R.id.slider_home);
        divPopUpImage = (RelativeLayout) findViewById(R.id.div_popUpImage);
        btnClose = (ImageView) findViewById(R.id.btn_close);
        imgView = (PhotoView) findViewById(R.id.img_view);
        divPromo = (RelativeLayout) findViewById(R.id.div_promo);
        tvLihatUlasan = (TextView) findViewById(R.id.tv_lihat_ulasan);
        rtBarKomen = (AppCompatRatingBar) findViewById(R.id.rt_bar_komen);
        tvPembuatKomen = (TextView) findViewById(R.id.tv_pembuat_komen);
        tvKomen = (TextView) findViewById(R.id.tv_komen);
        lyKomen = (LinearLayout) findViewById(R.id.ly_komen);
        btnToko = (LinearLayout) findViewById(R.id.btn_toko);
        tvInfoDiscount = (TextView) findViewById(R.id.tv_infoDiscount);
        tvStatusToko = (TextView) findViewById(R.id.tv_statusToko);
        btnSelengkapnya = (TextView) findViewById(R.id.btn_selengkapnya);
        divHargaDiscount = (RelativeLayout) findViewById(R.id.div_hargaDiscount);
        rtBarToko = (AppCompatRatingBar) findViewById(R.id.rt_barToko);
        tvRetingToko = (TextView) findViewById(R.id.tv_retingToko);
        divLayer = (RelativeLayout) findViewById(R.id.div_layer);
        pd = (ProgressBar) findViewById(R.id.pd);
        divKosong = (LinearLayout) findViewById(R.id.div_kosong);
        tvKosong = (TextView) findViewById(R.id.tv_kosong);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
