package id.co.cancreative.goldenshop.Activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Promo;
import id.co.cancreative.goldenshop.R;

public class DetailPromoActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private String getKodePromo;
    private Promo promo;

    private Toolbar toolbar;
    private CardView cvMain;
    private TextView tvNama;
    private ImageView imgBarang;
    private TextView tvBeratBarang;
    private TextView tvTgLExp;
    private TextView tvPotongan;
    private TextView tvMinTrans;
    private TextView tvKodePromo;
    private TextView btnCopy;
    private TextView tvKategory;
    private TextView tvStatusPromo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_promo);
        initView();

        s = new SharedPref(this);
        promo = s.getPromo();
        setValue();
        mainButton();
        setToolbar();
    }

    private void setValue() {

        tvKodePromo.setText(promo.PR_CODE);
        tvMinTrans.setText("" + new Helper().convertRupiah(Integer.valueOf(promo.PR_MIN)));
        tvPotongan.setText("" + new Helper().convertRupiah(Integer.valueOf(promo.PR_POTONGAN)));
        tvTgLExp.setText("" + new Helper().convertDateFormat(promo.PR_EXPIRED));
        tvNama.setText(promo.PR_NAME);

        String status = "Promo Aktif";
        if (promo.PR_STATUS.equals("NON_ACTIVE")){
            status = "Promo Tidak Aktif";
        }

        if (promo.PR_STATUS.equals("ACTIVE")){
            status = "Promo Aktif";
        }

        tvStatusPromo.setText(status);

        String kategory = "Berlaku Untuk Kategori : ";
        if (getIntent().getStringExtra("extra") != null){
            if (getIntent().getStringExtra("extra").equals("true"));

        }
        kategory = "Berlaku Untuk Kategori : "+ promo.SKAT_NAME;

        tvKategory.setText(kategory);
    }

    private void mainButton() {
        btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Label", tvKodePromo.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toasti.success(getApplicationContext(), "Kode Copied");
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Promo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cvMain = (CardView) findViewById(R.id.cv_main);
        tvNama = (TextView) findViewById(R.id.tv_nama);
        imgBarang = (ImageView) findViewById(R.id.img_barang);
        tvBeratBarang = (TextView) findViewById(R.id.tv_beratBarang);
        tvTgLExp = (TextView) findViewById(R.id.tv_tgLExp);
        tvPotongan = (TextView) findViewById(R.id.tv_potongan);
        tvMinTrans = (TextView) findViewById(R.id.tv_minTrans);
        tvKodePromo = (TextView) findViewById(R.id.tv_kodePromo);
        btnCopy = (TextView) findViewById(R.id.btn_copy);
        tvKategory = (TextView) findViewById(R.id.tv_kategory);
        tvStatusPromo = (TextView) findViewById(R.id.tv_statusPromo);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
