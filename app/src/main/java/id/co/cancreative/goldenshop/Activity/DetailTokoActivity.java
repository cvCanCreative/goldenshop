package id.co.cancreative.goldenshop.Activity;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailTokoActivity extends AppCompatActivity {

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Produk> mProduk = new ArrayList<>();
    private AppDbTerakhirDilihat db;

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;
    private ImageView btnSearch;
    private EditText edtSearch;
    private CircleImageView dtImgPenjual;
    private TextView dtTvNamaToko;
    private TextView dtTvAsalToko;
    private ImageView image3;
    private ImageView image4;
    private ImageView image5;
    private Produk produk;
    private LinearLayout divProdukKosong;
    private TextView tvPesan;
    private Button btnTambahProduk;
    private RecyclerView rv;
    private ProgressBar pd;
    private SwipeRefreshLayout swipeRefrash;
    private TextView tvRetingToko;
    private AppCompatRatingBar rtBarToko;
    private TextView tvStatusToko;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailtoko);
        initView();

        s = new SharedPref(this);
        produk = s.getProduk();
        db = Room.databaseBuilder(this, AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();

        setToolbar();

        getProduk();
        setValue();
        mainButton();
    }

    private void mainButton() {

        edtSearch.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                actionId == EditorInfo.IME_ACTION_DONE ||
                                event != null &&
                                        event.getAction() == KeyEvent.ACTION_DOWN &&
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            if (event == null || !event.isShiftPressed()) {
                                if (!edtSearch.getText().toString().isEmpty()) {
                                    Intent intent = new Intent(getApplicationContext(), CariBarangActivity.class);
                                    intent.putExtra("query", edtSearch.getText().toString());
                                    intent.putExtra("by_toko", true);
                                    intent.putExtra("id_toko", Integer.valueOf(produk.ID_TOKO));
                                    startActivity(intent);
                                }

                                return true; // consume.
                            }
                        }
                        return false; // pass on to other listeners.
                    }
                }
        );

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CariBarangActivity.class);
                intent.putExtra("query", edtSearch.getText().toString());
                intent.putExtra("by_toko", true);
                intent.putExtra("id_toko", Integer.valueOf(produk.ID_TOKO));
                startActivity(intent);
            }
        });

    }

    private void setValue() {
        dtTvNamaToko.setText(produk.TOK_NAME);
        dtTvAsalToko.setText(produk.TOK_KOTA);

        Picasso.with(this)
                .load(Config.url_toko + produk.TOK_FOTO)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(dtImgPenjual);

        rtBarToko.setRating(produk.rating_toko);
        tvRetingToko.setText("" + produk.rating_toko + " / ");
        if (produk.rating_toko == 0.0) tvRetingToko.setText("Belum Ada Ulasan" + " / ");

        if (produk.TOK_STATUS != null) {
            if (produk.TOK_STATUS.equals("TK_AKTIF")) {
                tvStatusToko.setText("Verified Account");
            } else {
                tvStatusToko.setText("Toko Belum Terverifikasi");
                Picasso.with(this)
                        .load(R.drawable.ic_reting_juragan)
                        .placeholder(R.drawable.ic_reting_rekomeded)
                        .error(R.drawable.ic_reting_rekomeded)
                        .noFade()
                        .into(image3);
            }
        }
    }


    private void getProduk() {
        api.getProdukByT(s.getApi(), produk.ID_TOKO).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                swipeRefrash.setRefreshing(false);
                pd.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    mProduk = response.body().products;
                    RecyclerView.Adapter mAdapter = new AdapterProdukTrading(mProduk, DetailTokoActivity.this, db);
                    rv.setLayoutManager(new GridLayoutManager(DetailTokoActivity.this, 2));
                    rv.setAdapter(mAdapter);

                    if (response.body().success == 0 || mProduk.size() == 0) {
                        divProdukKosong.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                SweetAlert.onFailure(DetailTokoActivity.this, t.getMessage());
            }
        });
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProduk();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(produk.TOK_NAME);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnSearch = (ImageView) findViewById(R.id.btn_search);
        edtSearch = (EditText) findViewById(R.id.edt_search);
        dtImgPenjual = (CircleImageView) findViewById(R.id.dt_img_penjual);
        dtTvNamaToko = (TextView) findViewById(R.id.dt_tv_namaToko);
        dtTvAsalToko = (TextView) findViewById(R.id.dt_tv_asalToko);
        image3 = (ImageView) findViewById(R.id.image3);
        image4 = (ImageView) findViewById(R.id.image4);
        image5 = (ImageView) findViewById(R.id.image5);
        divProdukKosong = (LinearLayout) findViewById(R.id.div_produkKosong);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        btnTambahProduk = (Button) findViewById(R.id.btn_tambahProduk);
        rv = (RecyclerView) findViewById(R.id.rv);
        pd = (ProgressBar) findViewById(R.id.pd);
        swipeRefrash = (SwipeRefreshLayout) findViewById(R.id.swipeRefrash);
        tvRetingToko = (TextView) findViewById(R.id.tv_retingToko);
        rtBarToko = (AppCompatRatingBar) findViewById(R.id.rt_barToko);
        tvStatusToko = (TextView) findViewById(R.id.tv_statusToko);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
