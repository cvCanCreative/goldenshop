package id.co.cancreative.goldenshop.Activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.DaftarTrans.AdapterListProdukTrans;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Alamat;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.Model.TransaksiDetail;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailTransaksiTokoActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;

    private String status;

    private Transaksi trans;
    private Toolbar toolbar;
    private ImageView img1;
    private TextView tvStatus;
    private Button btnBayar;
    private TextView tvTgl;
    private TextView tvPembeli;
    private TextView tvKodeTrans;
    private RecyclerView rvProduk;
    private TextView tvJmlBarang;
    private TextView tvTtlBelenja;
    private TextView tvOngkir;
    private RelativeLayout lyDiscount;
    private TextView tvPotongan;
    private TextView tvKodeUnik;
    private TextView tvTtlBayar;
    private Button btnProses;
    private EditText edtResi;
    private TextView tvJenisKurir;
    private LinearLayout divResi;
    private TextView tvNoResi;
    private TextView tvCopy;
    private TextView tvPenerima;
    private TextView tvPhone;
    private TextView tvAlamat;
    private Button btnBatal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaksi_toko);
        initView();

        s = new SharedPref(this);
        trans = s.getDataTransaksi();
        setToolbar();
        setValue();
        listProduk();
        mainButton();
    }

    private void mainButton() {

        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtResi.getText().toString().isEmpty()) {
                    Toasti.error(getApplicationContext(), "Harap Masukkan No Resi");
                } else {
                    accShop();
                }
            }
        });

        tvCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Label", tvNoResi.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toasti.success(getApplicationContext(), "Kode Copied");
            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                batalTransaksi();
            }
        });
    }

    private void setValue() {

        if (!trans.TS_POT_PROMO.equals("0")) {
            lyDiscount.setVisibility(View.VISIBLE);
            tvPotongan.setText("" + new Helper().convertRupiah(Integer.valueOf(trans.TS_POT_PROMO)));
        }

        tvKodeTrans.setText(trans.TS_KODE_PAYMENT);
        tvTgl.setText("" + new Helper().convertDateTimeToDate(trans.CREATED_AT, "yyyy-MM-dd hh:mm:s"));
        if (trans.TS_STATUS.equals("PROSES_ADMIN")) {
            status = "Menunggu Konfirmasi";
        }

        if (trans.TS_STATUS.equals("PROSES_VENDOR")) {
            status = "Pesanan Diproses";
        }

        if (trans.TS_STATUS.equals("PROSES_KIRIM")) {
            status = "Pesanan Dikirim";
        }

        if (trans.TS_STATUS.equals("DITERIMA_USER")) {
            status = "Pesanan Tiba";
        }

        if (trans.TS_STATUS.equals("TERIMA")) {
            status = "Pesanan Selesai";
        }
        tvStatus.setText(status);

        int totalBayar = 0;
        int qt = 0;
        int berat = 0;
        int ttlBerat = 0;

        for (TransaksiDetail t : trans.transaksi_detail) {
            qt = qt + Integer.parseInt(t.TSD_QTY);

            totalBayar = totalBayar + Integer.valueOf(t.TSD_HARGA_TOTAL);
            berat = berat + Integer.parseInt(t.barang.BA_WEIGHT);
            ttlBerat = ttlBerat + (Integer.parseInt(t.barang.BA_WEIGHT) * qt);
        }
        float kg = ttlBerat / 1000;
        NumberFormat nf = new DecimalFormat("#.#");
        String weight = nf.format(kg).replace(".0", "");

        tvPembeli.setText("" + trans.pembeli.US_USERNAME);
        tvTtlBayar.setText("" + new Helper().convertRupiah(Integer.valueOf(trans.TS_TF_GOLD)));
        tvTtlBelenja.setText("" + new Helper().convertRupiah(totalBayar));
        tvOngkir.setText("" + new Helper().convertRupiah(Integer.parseInt(trans.TS_BAYAR_ONGKIR)));
        tvJmlBarang.setText("" + qt + " barang - " + weight + " kg");
        tvKodeUnik.setText("" + new Helper().convertRupiah(Integer.parseInt(trans.TS_KODE_UNIK)));

        Alamat alamat = trans.transaksi_detail.get(0).alamat_kirim;
        tvJenisKurir.setText(trans.transaksi_detail.get(0).TSD_JENIS_KURIR);
        if (trans.TS_RESI.isEmpty()) {
            divResi.setVisibility(View.GONE);
        }
        tvNoResi.setText(trans.TS_RESI);
        tvPenerima.setText(alamat.PE_NAMA);
        tvPhone.setText(alamat.PE_TELP);
        tvAlamat.setText(alamat.PE_ALAMAT + ", " + alamat.PE_KOTA + ", " + alamat.PE_PROVINSI + " " + alamat.PE_KODE_POS);
    }

    private void listProduk() {
        ArrayList<TransaksiDetail> mItem = trans.transaksi_detail;
        RecyclerView.Adapter mAdapter = new AdapterListProdukTrans(mItem, this);
        rvProduk.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvProduk.setAdapter(mAdapter);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GoldenShop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void accShop() {
        SweetAlert.onLoading(DetailTransaksiTokoActivity.this);
        api.accShop(s.getApi(),
                "" + s.getIdUser(),
                "" + s.getSession(),
                "" + trans.ID_TRANSAKSI,
                "PROSES_KIRIM",
                "" + edtResi.getText().toString()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    if (response.body().success == 1) {
                        Toasti.success(DetailTransaksiTokoActivity.this, "" + response.body().message);
                        onBackPressed();
                        finish();
                    } else {
                        Toasti.error(DetailTransaksiTokoActivity.this, "" + response.body().message);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                final SweetAlertDialog dialogGagal = new SweetAlertDialog(DetailTransaksiTokoActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Ulang");

                dialogGagal.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        accShop();
                    }
                });
            }
        });
    }

    private void batalTransaksi(){
        final ApiService api = ApiConfig.getInstanceRetrofit();

        String sPesan = "Apakah Anda yakin ingin Membatalkan Transaksi Ini" +"?";
        final SweetAlertDialog pDialog = new SweetAlertDialog(DetailTransaksiTokoActivity.this, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Hapus")
                .setContentText(String.valueOf(Html.fromHtml(sPesan)))
                .setCancelText("Tidak")
                .setConfirmText("Iya,Batalkan")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        pDialog.cancel();
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(final SweetAlertDialog sweetAlertDialog) {
                        SweetAlert.onLoading(DetailTransaksiTokoActivity.this);
                        api.getBatalTransaksi(s.getApi(), s.getIdUser(), s.getSession(), trans.TS_KODE_PAYMENT).enqueue(new Callback<ResponModel>() {
                            @Override
                            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                SweetAlert.dismis();
                                if (response.body().success == 1){
                                    sweetAlertDialog
                                            .setTitleText("Transaksi Dibatalkan")
                                            .setContentText("Transaksi anda telah kami Batalkan")
                                            .setConfirmText("OK")
                                            .showCancelButton(false)
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweet) {
                                                    onBackPressed();
                                                    sweetAlertDialog.dismiss();
                                                }
                                            })
                                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                } else {
                                    Toasti.error(DetailTransaksiTokoActivity.this, response.body().message);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponModel> call, Throwable t) {
                                pDialog.dismiss();
                                SweetAlert.dismis();
                                SweetAlert.onFailure(DetailTransaksiTokoActivity.this, t.getMessage());
                            }
                        });
                    }
                })
                .show();
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        img1 = (ImageView) findViewById(R.id.img1);
        tvStatus = (TextView) findViewById(R.id.tv_status);
        btnBayar = (Button) findViewById(R.id.btn_bayar);
        tvTgl = (TextView) findViewById(R.id.tv_tgl);
        tvPembeli = (TextView) findViewById(R.id.tv_pembeli);
        tvKodeTrans = (TextView) findViewById(R.id.tv_kodeTrans);
        rvProduk = (RecyclerView) findViewById(R.id.rv_produk);
        tvJmlBarang = (TextView) findViewById(R.id.tv_jmlBarang);
        tvTtlBelenja = (TextView) findViewById(R.id.tv_ttlBelenja);
        tvOngkir = (TextView) findViewById(R.id.tv_ongkir);
        lyDiscount = (RelativeLayout) findViewById(R.id.ly_discount);
        tvPotongan = (TextView) findViewById(R.id.tv_potongan);
        tvKodeUnik = (TextView) findViewById(R.id.tv_kodeUnik);
        tvTtlBayar = (TextView) findViewById(R.id.tv_ttlBayar);
        btnProses = (Button) findViewById(R.id.btn_proses);
        edtResi = (EditText) findViewById(R.id.edt_resi);
        tvJenisKurir = (TextView) findViewById(R.id.tv_jenisKurir);
        divResi = (LinearLayout) findViewById(R.id.div_resi);
        tvNoResi = (TextView) findViewById(R.id.tv_noResi);
        tvCopy = (TextView) findViewById(R.id.tv_copy);
        tvPenerima = (TextView) findViewById(R.id.tv_penerima);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
        tvAlamat = (TextView) findViewById(R.id.tv_alamat);
        btnBatal = (Button) findViewById(R.id.btn_batal);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
