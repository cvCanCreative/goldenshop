package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Kota;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.Model.Provinsi;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResultProvinsi;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAlamatActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private ResultProvinsi rajaOangkir;
    private ModelAlamat alamat;

    private String id_pengiriman;
    private String kodeposs;
    private String idKota;
    private int pos;
    private String sIdProv, sIdKota, sIdKec, agenKurir;
    private String provinsi, kota, kecamatan;

    private List<String> idProv = new ArrayList<>();
    private List<String> idCity = new ArrayList<>();
    private List<String> idKec = new ArrayList<>();
    private List<String> namaProv = new ArrayList<>();
    private List<String> namaKota = new ArrayList<>();
    private List<String> namaKec = new ArrayList<>();
    private List<String> kodePos = new ArrayList<>();

    private List<String> list_prov = new ArrayList<>();
    private List<String> list_kota = new ArrayList<>();
    private List<String> list_kec = new ArrayList<>();

    private Toolbar toolbar;
    private TextView tvProvinsi;
    private TextView tvKota;
    private TextView tvKecamatan;
    private Button btnSimpan;
    private EditText edtLabelAlamat;
    private EditText edtPenerima;
    private EditText edtKodePos;
    private EditText edtAlamat;
    private EditText edtPhone;
    private TextView tvProv;
    private RelativeLayout btnProvinsi;
    private RelativeLayout btnKota;
    private RelativeLayout btnKecamatan;
    private Spinner spnProv;
    private Spinner spnKota;
    private Spinner spnKec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_alamat);
        initView();

        s = new SharedPref(this);
        rajaOangkir = s.getProvinsi();
        alamat = s.getShipping();

        mainButton();
        setValue();
//        getKota();
        setAlamat();
        setToolbar();
    }

    private void setValue() {

        id_pengiriman = alamat.ID_PENGIRIMAN;
        edtLabelAlamat.setText(alamat.PE_JUDUL);
        edtPenerima.setText(alamat.PE_NAMA);
        edtAlamat.setText(alamat.PE_ALAMAT);
        edtPhone.setText(alamat.PE_TELP);
        edtKodePos.setText(alamat.PE_KODE_POS);
        tvProvinsi.setText(alamat.PE_PROVINSI);
        tvKota.setText(alamat.PE_KOTA);
        tvKecamatan.setText(alamat.PE_KECAMATAN);
        sIdKota = alamat.PE_ID_KOTA;
        sIdKec = alamat.PE_ID_KOTA;
        kodeposs = alamat.PE_KODE_POS;

        List<Provinsi> listProv = rajaOangkir.results;
        for (Provinsi p : listProv) {

            if (p.province.equals(alamat.PE_PROVINSI)) {
                sIdProv = p.province_id;
                Log.d("idProv", "! "+alamat.PE_PROVINSI +" : "+sIdProv);
            }
        }

    }

    private void mainButton() {
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendAlamat();
            }
        });
    }

    private void sendAlamat() {

        String sJudul = edtLabelAlamat.getText().toString();
        String sNama = edtPenerima.getText().toString();
        String sAlamat = edtAlamat.getText().toString();
        String sProvinsi = tvProvinsi.getText().toString();
        String sKota = tvKota.getText().toString();
        String sKecamatan = tvKecamatan.getText().toString();
        String sKode_pos = edtKodePos.getText().toString();
        String sTelp = edtPhone.getText().toString();

        if (sJudul.isEmpty()) {
            edtLabelAlamat.setError("kolom Tidak boleh Kosong");
        } else if (sNama.isEmpty()) {
            edtPenerima.setError("kolom Tidak boleh Kosong");
        } else if (sProvinsi.isEmpty()) {
            tvProvinsi.setError("kolom Tidak boleh Kosong");
        } else if (sKota.isEmpty()) {
            tvKota.setError("kolom Tidak boleh Kosong");
        } else if (sKecamatan.isEmpty()) {
            tvKecamatan.setError("kolom Tidak boleh Kosong");
        } else if (sKode_pos.isEmpty()) {
            edtKodePos.setError("kolom Tidak boleh Kosong");
        } else if (sAlamat.isEmpty()) {
            edtAlamat.setError("kolom Tidak boleh Kosong");
        } else if (sTelp.isEmpty()) {
            edtPhone.setError("kolom Tidak boleh Kosong");
        } else {

            SweetAlert.sendingData(this);
            api.editAlamat(s.getApi(), s.getIdUser(), s.getSession(), id_pengiriman, sJudul, sNama, sAlamat, sProvinsi,
                    sKota, sKecamatan, sIdKota, sIdKec, sKode_pos, sTelp).enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    SweetAlert.dismis();
                    ResponModel respon = response.body();
                    if (response.isSuccessful()) {
                        int kode = respon.getSuccess();
                        if (kode == 1) {

                            CallFunction.getRefreshListAlamat();
                            onBackPressed();
                            finish();

                        } else {
                            Toasti.error(getApplicationContext(), respon.getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    SweetAlert.dismis();
                    SweetAlert.onFailure(EditAlamatActivity.this, t.getMessage());
                }
            });
        }
    }

    private void setAlamat() {

        idProv.add("0");
        List<Provinsi> provinsis = rajaOangkir.results;
        for (Provinsi p : provinsis) {
            idProv.add(p.province_id);
            namaProv.add(p.province);
        }

        list_prov.add("Pilih Provinsi");
        list_prov.addAll(namaProv);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list_prov);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        spnProv.setAdapter(adapter);
        spnProv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                list_kota.removeAll(list_kota);
                list_kec.removeAll(list_kec);
                namaKec.removeAll(namaKec);
                namaKota.removeAll(namaKota);
                idCity.removeAll(idCity);
                idKec.removeAll(idKec);

                if (spnProv.getSelectedItemPosition() != 0) {

                    tvProvinsi.setError(null);
                    tvKecamatan.setText(null);

                    pos = spnProv.getSelectedItemPosition();
                    //ID Provinsi
                    sIdProv = idProv.get(pos);
                    provinsi = spnProv.getSelectedItem().toString();
                    tvProvinsi.setText(provinsi);

                    SweetAlert.onLoading(EditAlamatActivity.this);
                    final ApiService api = ApiConfig.getInstanceRetrofit();
                    api.getListoKota(sIdProv).enqueue(new Callback<ResponModel>() {
                        @Override
                        public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                            SweetAlert.dismis();
                            if (response.isSuccessful()) {
                                List<Kota> listKota = response.body().getRajaongkir().results;

                                idCity.add("0");
                                kodePos.add("0");
                                for (Kota k : listKota) {
                                    idCity.add(k.getCityId());
                                    namaKota.add(k.getCityName() + " " + k.getType());
                                    kodePos.add(k.getPostalCode());
                                }

                                list_kota.add("Pilih kota");
                                list_kota.addAll(namaKota);

                                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditAlamatActivity.this,
                                        android.R.layout.simple_spinner_item, list_kota);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                adapter.notifyDataSetChanged();
                                spnKota.setAdapter(adapter);
                                spnKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        int pos = spnKota.getSelectedItemPosition();

                                        //ID Kecamatan
                                        sIdKec = idCity.get(pos);

                                        kodeposs = kodePos.get(pos);
                                        namaKec.removeAll(namaKec);
                                        idKec.removeAll(idKec);
                                        kecamatan = spnKota.getSelectedItem().toString();
                                        tvKota.setText(kecamatan);
                                        if (kodeposs.equals("0")) {
                                            edtKodePos.setText(null);
                                        } else {
                                            edtKodePos.setText(kodeposs);
                                        }

                                        if (pos != 0) {

                                            tvKota.setError(null);
                                            SweetAlert.onLoading(EditAlamatActivity.this);
                                            ApiService api = ApiConfig.getInstanceRetrofit();
                                            api.getListoKec(sIdKec).enqueue(new Callback<ResponModel>() {
                                                @Override
                                                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                                    SweetAlert.dismis();
                                                    if (response.isSuccessful()) {
                                                        List<Kota> listKota = response.body().getRajaongkir().results;
                                                        idKec.add("0");
                                                        for (Kota k : listKota) {
                                                            idKec.add(k.getSubdistrict_id());
                                                            namaKec.add(k.getSubdistrict_name());
                                                        }

                                                        list_kec.add("Pilih kecamatan");
                                                        list_kec.addAll(namaKec);

                                                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditAlamatActivity.this,
                                                                android.R.layout.simple_spinner_item, list_kec);
                                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                        adapter.notifyDataSetChanged();
                                                        spnKec.setAdapter(adapter);
                                                        spnKec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                            @Override
                                                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                                int pos = spnKec.getSelectedItemPosition();
                                                                tvKecamatan.setError(null);
                                                                sIdKec = idKec.get(pos);
                                                                kecamatan = spnKec.getSelectedItem().toString();
                                                                tvKecamatan.setText(kecamatan);
                                                            }

                                                            @Override
                                                            public void onNothingSelected(AdapterView<?> adapterView) {

                                                            }
                                                        });

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ResponModel> call, Throwable t) {
                                                    SweetAlert.dismis();
                                                    SweetAlert.onFailure(EditAlamatActivity.this, t.getMessage());
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponModel> call, Throwable t) {
                            SweetAlert.dismis();
                            SweetAlert.onFailure(EditAlamatActivity.this, t.getMessage());
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tambah Alamat");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvProvinsi = (TextView) findViewById(R.id.tv_provinsi);
        tvKota = (TextView) findViewById(R.id.tv_kota);
        tvKecamatan = (TextView) findViewById(R.id.tv_kecamatan);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        edtLabelAlamat = (EditText) findViewById(R.id.edt_labelAlamat);
        edtPenerima = (EditText) findViewById(R.id.edt_penerima);
        edtKodePos = (EditText) findViewById(R.id.edt_kodePos);
        edtAlamat = (EditText) findViewById(R.id.edt_alamat);
        edtPhone = (EditText) findViewById(R.id.edt_phone);
        tvProv = (TextView) findViewById(R.id.tv_prov);
        btnProvinsi = (RelativeLayout) findViewById(R.id.btn_provinsi);
        btnKota = (RelativeLayout) findViewById(R.id.btn_kota);
        btnKecamatan = (RelativeLayout) findViewById(R.id.btn_kecamatan);
        spnProv = (Spinner) findViewById(R.id.spn_prov);
        spnKota = (Spinner) findViewById(R.id.spn_kota);
        spnKec = (Spinner) findViewById(R.id.spn_kec);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
