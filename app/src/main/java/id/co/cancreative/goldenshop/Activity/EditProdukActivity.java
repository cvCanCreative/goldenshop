package id.co.cancreative.goldenshop.Activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.co.cancreative.goldenshop.Adapter.AdapterGambarP;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiConfigDemo;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProdukActivity extends AppCompatActivity {

    private View parent_view;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private ApiService api2 = ApiConfigDemo.getInstanceRetrofit();
    private SharedPref s;

    RecyclerView.Adapter mAdapter;

    private Produk produk;
    private User user;

    private File _fileProduk, fileProduk;
    private List<File> imgProduct = new ArrayList<>();
    private ArrayList<String> files = new ArrayList<>();

    private String harga = "";
    private String current = "";
    private String kondisi;
    private String allGambar = "";

    private Toolbar toolbar;
    private EditText edtHarga;
    private EditText edtStok;
    private EditText edtBerat;
    private TextView tvKondisi;
    private TextView tvSatuanBerat;
    private TextView tvToolbarTitle;
    private ImageView btnSimpan;
    private EditText edtNama;
    private EditText edtDeskripsi;
    private EditText edtSku;
    private LinearLayout btnKondisi;
    private LinearLayout btnKategori;
    private TextView tvKategori;
    private RecyclerView rv;
    private LinearLayout btnTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_produk);
        initView();

        s = new SharedPref(this);
        produk = s.getProduk();
        user = s.getUser();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setValue();
        editText();
        mainButton();
        setToolbar();
    }


    private void setValue() {
        edtNama.setText(produk.BA_NAME);
        Spanned string = Html.fromHtml(produk.BA_DESCRIPTION);
        edtDeskripsi.setText(string);
        tvKategori.setText(produk.SKAT_NAME);
        harga = produk.BA_PRICE;
        edtHarga.setText(new Helper().convertRupiah(Integer.valueOf(harga)));
        edtStok.setText(produk.BA_STOCK);
        edtBerat.setText(produk.BA_WEIGHT);
        tvSatuanBerat.setVisibility(View.VISIBLE);
        edtSku.setText(produk.BA_SKU);
        kondisi = produk.BA_CONDITION;
        s.setString(SharedPref.ID_SubKategori, produk.ID_SUB_KATEGORI);
        if (produk.equals("Baru")) {
            kondisi = "NEW";
        } else if (produk.equals("Bekas")) {
            kondisi = "SECOND";
        }
        tvKondisi.setText(kondisi);

        String gb = produk.BA_IMAGE.replace("|", " ");
        String strArray[] = gb.split(" ");

        for (String d : strArray) {
            Log.d("Gambar", "d :" + d);
            files.add(d);
        }
        files.add("");

        mAdapter = new AdapterGambarP(files, this);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv.setAdapter(mAdapter);

    }

    private void mainButton() {

        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                files.add("");
                mAdapter.notifyDataSetChanged();
            }
        });
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (fileProduk == null) {
//                    Snackbar.make(parent_view, "Pilih Provinsi", Snackbar.LENGTH_SHORT).show();
//                } else
                if (edtNama.getText().toString().isEmpty()) {
                    edtNama.setError("Kolom Tidak Boleh Kosong");
                } else if (edtDeskripsi.getText().toString().isEmpty()) {
                    edtDeskripsi.setError("Kolom Tidak Boleh Kosong");
                } else if (edtHarga.getText().toString().isEmpty()) {
                    edtHarga.setError("Kolom Tidak Boleh Kosong");
                } else if (edtStok.getText().toString().isEmpty()) {
                    edtStok.setError("Kolom Tidak Boleh Kosong");
                } else if (edtBerat.getText().toString().isEmpty()) {
                    edtBerat.setError("Kolom Tidak Boleh Kosong");
                } else if (edtSku.getText().toString().isEmpty()) {
                    edtSku.setError("Kolom Tidak Boleh Kosong");
                } else {

                    for (String d : files) {
                        if (!d.isEmpty()) {
                            Log.d("Gambar", d);
                            allGambar = allGambar + d + "|";
                        }
                    }
                    Log.d("Allgambar", "Size: " + " : " + allGambar);
                    editProduct();
                }
            }
        });

        btnKondisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(EditProdukActivity.this, btnKondisi);
                popupMenu.getMenuInflater().inflate(R.menu.menu_kondisi, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("Baru")) {
                            kondisi = "NEW";
                        } else if (item.getTitle().equals("Bekas")) {
                            kondisi = "SECOND";
                        }
                        tvKondisi.setText(item.getTitle());
                        return true;
                    }
                });

                popupMenu.show();
            }
        });

        btnKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditProdukActivity.this, PilihKategoriActivity.class);
                intent.putExtra("extra", "addProduk");
                startActivity(intent);
            }
        });
    }

    private void editText() {

        // Edit Harga
        edtHarga.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtHarga.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        harga = cleanString;
                        edtHarga.setText(formatted);
                        edtHarga.setSelection(formatted.length());

                        edtHarga.addTextChangedListener(this);
                    } else {
                        edtHarga.setText(current);
                        edtHarga.setSelection(current.length());
                        edtHarga.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        // Edit Berat
        edtBerat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String currentBerat = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(currentBerat)) {

                    if (s.toString().isEmpty()) {
                        tvSatuanBerat.setVisibility(View.GONE);
                    }
                    currentBerat = s.toString();
                    edtHarga.removeTextChangedListener(this);
                    tvSatuanBerat.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void editProduct() {

        String Id = s.getString(SharedPref.ID_SubKategori);
        String Id_promo = "1";
        String Name = edtNama.getText().toString();
        String Price = harga;
        String Sku = edtSku.getText().toString();
        String Deskripsi = edtDeskripsi.getText().toString();
        String Stock = edtStok.getText().toString();
        String Weight = edtBerat.getText().toString();
        String Condition = tvKondisi.getText().toString();

        SweetAlert.sendingData(this);
        api.editProduk(s.getApi(), s.getIdUser(), s.getSession(), produk.ID_BARANG, Id, Id_promo, Name, Price, Sku, Deskripsi, Stock, Weight, Condition, allGambar).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    Produk p = response.body().product;
                    CallFunction.getRefreshListProdukTK();

                    onBackPressed();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(EditProdukActivity.this, t.getMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        } else if (requestCode != Activity.RESULT_CANCELED) {
            files.add("");
            mAdapter = new AdapterGambarP(files, EditProdukActivity.this);
            rv.setAdapter(mAdapter);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            fileProduk = new File(Crop.getOutput(result).getPath());
            uploadGmabar();
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadGmabar() {

        try {
            _fileProduk = new Compressor(this).compressToFile(fileProduk);
        } catch (IOException e) {
            e.printStackTrace();
        }

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), _fileProduk);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", _fileProduk.getName(), requestFile);

        Log.d("gambar golden", "" + image);

        SweetAlert.sendingData(this);
        api.uploadFoto(s.getApi(), s.getIdUser(), s.getSession(), image).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, final Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {

                    files.add(response.body().image_name);
                    if (files.size() <= 5) {
                        files.add("");
                    }
                    mAdapter = new AdapterGambarP(files, EditProdukActivity.this);
                    rv.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(EditProdukActivity.this, t.getMessage());
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        tvToolbarTitle.setText("Tambah Produk");
        getSupportActionBar().setTitle("Tambah Produk");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        parent_view = findViewById(android.R.id.content);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        edtHarga = (EditText) findViewById(R.id.edt_harga);
        edtStok = (EditText) findViewById(R.id.edt_stok);
        edtBerat = (EditText) findViewById(R.id.edt_berat);
        tvKondisi = (TextView) findViewById(R.id.tv_kondisi);
        tvSatuanBerat = (TextView) findViewById(R.id.tv_satuanBerat);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        btnSimpan = (ImageView) findViewById(R.id.btn_simpan);
        edtNama = (EditText) findViewById(R.id.edt_nama);
        edtDeskripsi = (EditText) findViewById(R.id.edt_deskripsi);
        edtSku = (EditText) findViewById(R.id.edt_sku);
        btnKondisi = (LinearLayout) findViewById(R.id.btn_kondisi);
        btnKategori = (LinearLayout) findViewById(R.id.btn_kategori);
        tvKategori = (TextView) findViewById(R.id.tv_kategori);
        rv = (RecyclerView) findViewById(R.id.rv);
        btnTest = (LinearLayout) findViewById(R.id.btn_test);
    }

    @Override
    protected void onResume() {
        if (s.getString(SharedPref.NAMA_SubKategori) != null) {
            tvKategori.setText(s.getString(SharedPref.NAMA_SubKategori));
        }
        super.onResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
