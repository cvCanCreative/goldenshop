package id.co.cancreative.goldenshop.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileUserActivity extends AppCompatActivity {

    String TAG = "Respon";

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private User user;

    private String tgl_lahir;
    private File imageFile, _imageFile;
    private DatePickerDialog.OnDateSetListener setCalender;

    private Toolbar toolbar;
    private ImageView imgProfile;
    private EditText edtUbahNama;
    private EditText edtUbahAlamat;
    private RelativeLayout btnUbahTgl;
    private TextView tvTanggal;
    private RadioGroup rgJenisKelamin;
    private RadioButton rdPria;
    private RadioButton rdWanita;
    private TextView edtEmail;
    private TextView btnUbahEmail;
    private TextView edtPhone;
    private TextView btnPhone;
    private TextView btnUbahPassword;
    private AppBarLayout appbarLayout;
    private CardView aepCardGantipassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initView();

        s = new SharedPref(this);
        user = s.getUser();

        mainButton();
        setValue();
        setToolbar();
    }

    private void mainButton() {

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Crop.pickImage(EditProfileUserActivity.this);

//                EasyImage.openGallery(EditProfileUserActivity.this, 1);
            }
        });

        btnPhone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final AlertDialog alertDialogInput = new AlertDialog.Builder(EditProfileUserActivity.this).create();
                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.dialog_input, null);
                Button btn_no = (Button) view.findViewById(R.id.btn_no);
                Button btn_ok = (Button) view.findViewById(R.id.btn_ok);
                TextView title = view.findViewById(R.id.tv_title);
                TextView pesan = view.findViewById(R.id.tv_pesan);
                final EditText editText1 = (EditText) view.findViewById(R.id.edt_dialog_input1);
                final EditText editText2 = (EditText) view.findViewById(R.id.edt_dialog_input2);
                alertDialogInput.setView(view);
                alertDialogInput.setCancelable(false);
                alertDialogInput.show();

                title.setText(user.telp);
                pesan.setText("Nomor Saat ini");
                editText1.setHint("Nomor Baru");

                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogInput.dismiss();
                        if (!editText1.getText().toString().isEmpty()) {
                            edtPhone.setText(editText1.getText().toString());
                        }
                    }
                });

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogInput.dismiss();
                    }
                });

                editText2.setVisibility(View.GONE);
            }
        });

        btnUbahTgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(EditProfileUserActivity.this, setCalender, year, month, day);
                dialog.show();
            }
        });

        setCalender = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int thun, int month, int day) {

                tgl_lahir = thun + "-" + (month + 1) + "-" + day;
                Log.d(TAG, "onDateSet: " + tgl_lahir);
                String bulan;
                switch (month + 1) {
                    case 1:
                        bulan = "January";
                        break;
                    case 2:
                        bulan = "February";
                        break;
                    case 3:
                        bulan = "March";
                        break;
                    case 4:
                        bulan = "April";
                        break;
                    case 5:
                        bulan = "Mey";
                        break;
                    case 6:
                        bulan = "June";
                        break;
                    case 7:
                        bulan = "July";
                        break;
                    case 8:
                        bulan = "August";
                        break;
                    case 9:
                        bulan = "September";
                        break;
                    case 10:
                        bulan = "October";
                        break;
                    case 11:
                        bulan = "November";
                        break;
                    default:
                        bulan = "December";
                }

                String tanggal = day + " " + bulan + " " + thun;
                tvTanggal.setText(tanggal);
            }
        };

        btnUbahPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), GantiPasswordActivity.class);
                startActivity(i);
            }
        });
    }

    private void update() {

        String sNama = edtUbahNama.getText().toString();
        String sEmail = user.email;
        // Set Jenis Kelmin
        String sGender;
        final int radioId = rgJenisKelamin.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(radioId);
        if (radioButton.getText().toString().equals("Pria")) {
            sGender = "LK";
        } else {
            sGender = "PR";
        }

        String sAlamat = edtUbahAlamat.getText().toString();
        String sTlp = edtPhone.getText().toString();

        SweetAlert.sendingData(this);
        api.editProfil(s.getApi(), s.getIdUser(), s.getSession(), sNama, sEmail, sGender, sAlamat, sTlp, tgl_lahir).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {

                    s.setUser(response.body().user);

                    final SweetAlertDialog pDialog = new SweetAlertDialog(EditProfileUserActivity.this);
                    pDialog.setTitleText("Profile Berhasil di Update")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    onBackPressed();
                                    pDialog.dismiss();
                                }
                            });
                    pDialog.show();
                    return;
                }
                Toasti.error(EditProfileUserActivity.this, response.body().message);

            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(EditProfileUserActivity.this, t.getMessage());
            }
        });
    }

    private void uploadFile() {
        try {
            _imageFile = new Compressor(this).compressToFile(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), _imageFile);
        MultipartBody.Part image = MultipartBody.Part.createFormData("picture", _imageFile.getName(), requestFile);

        SweetAlert.onLoadingCustom(this, "Uploading File...");
        api.uploadFotoProfile(s.getApi(), s.getIdUser(), s.getSession(), image).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {
                    user.foto = response.body().picture;
                    s.setUser(user);
                    user = s.getUser();

                    Picasso.with(EditProfileUserActivity.this)
                            .load(Config.url_user + user.foto)
                            .placeholder(R.drawable.image_loading)
                            .error(R.drawable.image_error)
                            .resize(200, 200)
                            .noFade()
                            .into(imgProfile);
                    return;
                }

                Toasti.error(EditProfileUserActivity.this, response.body().message);
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(EditProfileUserActivity.this, t.getMessage());
            }
        });
    }

    private void setValue() {

        tgl_lahir = user.birth;

        if (tgl_lahir.equals("0000-00-00")) {
            tgl_lahir = "1990-10-10";
        }

        String name = user.fullname;
        if (name.isEmpty() || name == null) {
            name = user.username;
        }
        edtUbahNama.setText(name);
        edtUbahAlamat.setText(user.alamat);
        edtEmail.setText(user.email);
        edtPhone.setText(user.telp);
        tvTanggal.setText(new Helper().convertDateFormat(tgl_lahir));

        String jenis = user.gender;
        if (jenis.equals("LK") || jenis.equals("lk")) {
            rdPria.setChecked(true);
        } else if (jenis.equals("PR") || jenis.equals("pr")) {
            rdWanita.setChecked(true);
        } else {
            rdPria.setChecked(true);
        }

        Picasso.with(this)
                .load(Config.url_user + user.foto)
//                .load(R.drawable.image_loading)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(imgProfile);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_simpan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        } else if (item_id == R.id.action_checkout) {
            update();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        } else if (requestCode != Activity.RESULT_CANCELED){
            Log.d(TAG, "onActivityResult: Cenceled");
        }

//        EasyImage.handleActivityResult(requestCode, resultCode, data, EditProfileUserActivity.this, new DefaultCallback() {
//            @Override
//            public void onImagePicked(File imageFiles, EasyImage.ImageSource source, int type) {
//                imageFile = imageFiles;
//                uploadFile();
//            }
//        });
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            imageFile = new File(Crop.getOutput(result).getPath());
            uploadFile();
//            imgProfile.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgProfile = (ImageView) findViewById(R.id.img_profile);
        edtUbahNama = (EditText) findViewById(R.id.edt_ubah_nama);
        edtUbahAlamat = (EditText) findViewById(R.id.edt_ubah_alamat);
        btnUbahTgl = (RelativeLayout) findViewById(R.id.btn_ubah_tgl);
        tvTanggal = (TextView) findViewById(R.id.tv_tanggal);
        rgJenisKelamin = (RadioGroup) findViewById(R.id.rg_jenisKelamin);
        rdPria = (RadioButton) findViewById(R.id.rd_pria);
        rdWanita = (RadioButton) findViewById(R.id.rd_wanita);
        edtEmail = (TextView) findViewById(R.id.edt_email);
        btnUbahEmail = (TextView) findViewById(R.id.btn_ubah_email);
        edtPhone = (TextView) findViewById(R.id.edt_phone);
        btnPhone = (TextView) findViewById(R.id.btn_phone);
        btnUbahPassword = (TextView) findViewById(R.id.btn_ubah_password);
        appbarLayout = (AppBarLayout) findViewById(R.id.appbar_layout);
        aepCardGantipassword = (CardView) findViewById(R.id.aep_card_gantipassword);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
