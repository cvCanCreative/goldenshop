package id.co.cancreative.goldenshop.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SaveData;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Helper.Value;
import id.co.cancreative.goldenshop.Model.Promo;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPromoActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Promo promo;
    Calendar calendar;

    private String sNama, sDeskripsi, sKode, sPotongan, sMinimal, sIdKat, sTglMulai = "2019-4-1", sTglExp = "2019-4-2";

    private String subKategori;
    private String idPromo;
    private int potongan;

    private Toolbar toolbar;
    private TextView tvToolbarTitle;
    private ImageView btnSimpan;
    private EditText edtNama;
    private EditText edtDeskripsi;
    private RelativeLayout divPilihKetegori;
    private LinearLayout btnTest;
    private LinearLayout btnProduk;
    private TextView tvKategori;
    private EditText edtPotongan;
    private EditText edtMinimumBelanja;
    private EditText edtKodePromo;
    private CardView btnCalenderMulai;
    private TextView tvTglMulai;
    private CardView btnCalenderSampai;
    private TextView tvTglSampai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_promo);
        initView();

        s = new SharedPref(this);
        calendar = Calendar.getInstance();
        promo = s.getPromo();

        setValue();
        setToolbar();
        mainButton();

    }

    private void setValue() {

        sPotongan = promo.PR_POTONGAN;
        sMinimal = promo.PR_MIN;
        idPromo = promo.ID_PROMO;
        edtNama.setText(promo.PR_NAME);
        edtDeskripsi.setText(promo.PR_DESKRIPSI);
        edtPotongan.setText(new Helper().convertRupiah(Integer.valueOf(sPotongan)));
        edtMinimumBelanja.setText(new Helper().convertRupiah(Integer.valueOf(sMinimal)));
        edtKodePromo.setText(promo.PR_CODE);
        tvTglMulai.setText(""+new Helper().convertDateFormat(promo.PR_MULAI));
        tvTglSampai.setText(""+new Helper().convertDateFormat(promo.PR_EXPIRED));

    }

    private void mainButton() {

        edtPotongan.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtPotongan.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");

                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        sPotongan = cleanString;
                        edtPotongan.setText(formatted);
                        edtPotongan.setSelection(formatted.length());

                        edtPotongan.addTextChangedListener(this);
                    } else {
                        edtPotongan.setText(current);
                        edtPotongan.setSelection(current.length());

                        edtPotongan.addTextChangedListener(this);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtMinimumBelanja.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtMinimumBelanja.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");

                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        sMinimal = cleanString;
                        edtMinimumBelanja.setText(formatted);
                        edtMinimumBelanja.setSelection(formatted.length());

                        edtMinimumBelanja.addTextChangedListener(this);
                    } else {
                        edtMinimumBelanja.setText(current);
                        edtMinimumBelanja.setSelection(current.length());

                        edtMinimumBelanja.addTextChangedListener(this);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnCalenderMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tanggalMulai();
            }
        });

        btnCalenderSampai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tanggalSelesai();
            }
        });

        btnProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditPromoActivity.this, PilihKategoriActivity.class);
                intent.putExtra("extra", "addProduk");
                startActivity(intent);
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPromo();
            }
        });

    }

    private void editPromo() {
        SweetAlert.sendingData(this);
        ApiService apiService = api;
        apiService.editPromo(
                Value.Api,
                s.getIdUser(),
                s.getSession(),
                idPromo,
                edtNama.getText().toString(),
                edtDeskripsi.getText().toString(),
                edtKodePromo.getText().toString(),
                sPotongan,
                sMinimal,
                sTglMulai,
                sTglExp).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {

                    new SweetAlertDialog(EditPromoActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Berhasil Mengubah Promo")
                            .setContentText("" + response.body().message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                    CallFunction.getrefrashPromo();
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();

                } else {
                    Toasti.error(EditPromoActivity.this, response.body().message);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(EditPromoActivity.this, t.getMessage());
            }
        });

    }

    private void tanggalSelesai() {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                calendar.set(Calendar.YEAR, i);
                calendar.set(Calendar.MONTH, i1);
                calendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                tvTglSampai.setText(""+new Helper().convertDateFormat(dateFormat.format(calendar.getTime())));

                sTglExp = dateFormat.format(calendar.getTime());
            }
        },
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void tanggalMulai() {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                calendar.set(Calendar.YEAR, i);
                calendar.set(Calendar.MONTH, i1);
                calendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                tvTglMulai.setText(""+new Helper().convertDateFormat(dateFormat.format(calendar.getTime())));

                sTglMulai = dateFormat.format(calendar.getTime());
            }
        },
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        tvToolbarTitle.setText("Edit Promo");
        getSupportActionBar().setTitle("GoldenShop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        tvToolbarTitle = findViewById(R.id.tv_toolbarTitle);
        btnSimpan = findViewById(R.id.btn_simpan);
        edtNama = findViewById(R.id.edt_nama);
        edtDeskripsi = findViewById(R.id.edt_deskripsi);
        divPilihKetegori = findViewById(R.id.div_pilihKetegori);
        btnTest = findViewById(R.id.btn_test);
        btnProduk = findViewById(R.id.btn_produk);
        tvKategori = findViewById(R.id.tv_kategori);
        edtPotongan = findViewById(R.id.edt_potongan);
        edtMinimumBelanja = findViewById(R.id.edt_minimumBelanja);
        edtKodePromo = findViewById(R.id.edt_kodePromo);
        btnCalenderMulai = findViewById(R.id.btn_calenderMulai);
        tvTglMulai = findViewById(R.id.tv_tglMulai);
        btnCalenderSampai = findViewById(R.id.btn_calenderSampai);
        tvTglSampai = findViewById(R.id.tv_tglSampai);
    }

    @Override
    protected void onResume() {

        if (SaveData.getNameSubKat() != null) {
            subKategori = SaveData.getNameSubKat();
            tvKategori.setText(subKategori);
        }
        super.onResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
