package id.co.cancreative.goldenshop.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.AdapterBank;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.ItemClickSupport;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditRekeningActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;
    private String namaBank, idBank, idRekening, imgBank;

    private ArrayList<Rekening> mItem = new ArrayList<>();
    private Rekening rekening;

    private Toolbar toolbar;
    private RecyclerView rvBank;
    private RelativeLayout btnBank;
    private TextView edtBank;
    private ImageView imgKecamatan;
    private EditText edtNoRekening;
    private EditText edtNamaPemilik;
    private Button btnSimpan;
    private LinearLayout divListBank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_rekening);
        initView();

        s = new SharedPref(this);
        rekening = s.getRekening();

        divListBank.animate().translationY(divListBank.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divListBank.setVisibility(View.GONE);
                    }
                });

        setValue();
        getBank();
        mainButton();
        setToolbar();
    }

    private void setValue() {
        idRekening = rekening.ID_REKENING;
        idBank = rekening.ID_BANK;
        edtBank.setText(rekening.BK_NAME);
        edtNamaPemilik.setText(rekening.RK_NAME);
        edtNoRekening.setText(rekening.RK_NOMOR);
    }

    private void getBank() {
        api.getBank(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {

                if (response.isSuccessful()) {
                    mItem = response.body().bank;

                    layoutManager = new LinearLayoutManager(EditRekeningActivity.this, LinearLayoutManager.VERTICAL, false);
                    mAdapter = new AdapterBank(mItem, EditRekeningActivity.this);
                    rvBank.setLayoutManager(layoutManager);
                    rvBank.setAdapter(mAdapter);

                    ItemClickSupport.addTo(rvBank).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                        @Override
                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                            itemSelected(mItem.get(position));
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {

            }
        });
    }

    private void itemSelected(Rekening mb) {

        namaBank = mb.BK_NAME;
        idBank = mb.ID_BANK;
        imgBank = mb.BK_IMAGE;

        edtBank.setText(namaBank);
        divListBank.animate().translationY(divListBank.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
//                        divListBank.setVisibility(View.GONE);
                    }
                });
    }

    private void mainButton() {
        btnBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("test", "work");
                slideUp();
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBank();
            }
        });
    }

    private void sendBank() {
        final String sNoRek = edtNoRekening.getText().toString();
        final String sNamaPemilik = edtNamaPemilik.getText().toString();

        if (sNoRek.isEmpty()) {
            edtNoRekening.setError("Kolom Tidak Boleh Kosong");
        } else if (sNamaPemilik.isEmpty()) {
            edtNamaPemilik.setError("Kolom Tidak Boleh Kosong");
        } else {
            SweetAlert.sendingData(this);
            api.editRekening(s.getApi(), s.getIdUser(), s.getSession(), idRekening, idBank, sNoRek, sNamaPemilik).enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    SweetAlert.dismis();
                    if (response.isSuccessful()) {

                        CallFunction.getRefreshListRekening();
                        onBackPressed();
                        finish();
                    }

                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    SweetAlert.dismis();
                    SweetAlert.onFailure(EditRekeningActivity.this, t.getMessage());
                }
            });
        }
    }

    private void slideUp() {
        divListBank.setVisibility(View.VISIBLE);
        divListBank.animate().translationY(0)
                .alpha(1.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divListBank.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Rekening");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        rvBank = (RecyclerView) findViewById(R.id.rv_bank);
        btnBank = (RelativeLayout) findViewById(R.id.btn_bank);
        edtBank = (TextView) findViewById(R.id.edt_bank);
        imgKecamatan = (ImageView) findViewById(R.id.img_kecamatan);
        edtNoRekening = (EditText) findViewById(R.id.edt_noRekening);
        edtNamaPemilik = (EditText) findViewById(R.id.edt_namaPemilik);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        divListBank = (LinearLayout) findViewById(R.id.div_listBank);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
