package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.AdapterFAQ;
import id.co.cancreative.goldenshop.Helper.IjinAplikasi;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Faq;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;

public class FAQActivity extends AppCompatActivity {

    private SharedPref s;

    private RecyclerView afRecyMain;
    private ArrayList<Faq> datafaq = new ArrayList<>();
    private ResponModel responModel;

    private Toolbar toolbar;
    private LinearLayout afLnEmail;
    private LinearLayout afLnTelepon;
    private TextView tvEmail;
    private TextView tvPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        initView();
        setToolbar();
        IjinAplikasi.mintsijintelepon(FAQActivity.this);

        s = new SharedPref(this);

        mainButton();
        setValue();
    }

    private void setValue() {
        responModel = s.getAbout();
        datafaq = responModel.faq;
        afRecyMain.setAdapter(new AdapterFAQ(datafaq, FAQActivity.this));
        afRecyMain.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        tvEmail.setText(responModel.kontak.KON_EMAIL);
        tvPhone.setText(responModel.kontak.KON_TELP);
    }

    private void mainButton() {

        afLnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, responModel.kontak.KON_EMAIL);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Keluhan");
                emailIntent.setType("plain/text");
                startActivity(emailIntent);
            }
        });

        afLnTelepon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IjinAplikasi.mintsijintelepon(FAQActivity.this)) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + responModel.kontak.KON_TELP));
                    startActivity(intent);
                }

            }
        });

    }

    private void initView() {
        afRecyMain = (RecyclerView) findViewById(R.id.af_recy_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        afLnEmail = (LinearLayout) findViewById(R.id.af_ln_email);
        afLnTelepon = (LinearLayout) findViewById(R.id.af_ln_telepon);
        tvEmail = (TextView) findViewById(R.id.tv_email);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.pusatbantuan));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
}
