package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.DescendantOffsetUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GantiPasswordActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;
    private EditText agpEdtPasswordlama;
    private EditText agpEdtPasswordbaru;
    private EditText agpEdtPasswordbarukonfirmasi;
    private Button agpBtnKirim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_password);
        initView();

        s = new SharedPref(this);

        setToolbar();

        agpBtnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // cek bila kosong
                if (agpEdtPasswordlama.getText().toString().isEmpty()){
                    agpEdtPasswordlama.setError(getString(R.string.edt_kosong));

                }else if (agpEdtPasswordbaru.getText().toString().isEmpty()){
                    agpEdtPasswordbaru.setError(getString(R.string.edt_kosong));

                }else if (agpEdtPasswordbarukonfirmasi.getText().toString().isEmpty()){
                    agpEdtPasswordbarukonfirmasi.setError(getString(R.string.edt_kosong));

                }else if (!agpEdtPasswordbarukonfirmasi.getText().toString().equals(agpEdtPasswordbaru.getText().toString())){
                    Toasti.error(GantiPasswordActivity.this, getString(R.string.konfirmasipassword_gagal));
                    agpEdtPasswordbarukonfirmasi.setText("");

                }else {
                    String password_lama = agpEdtPasswordlama.getText().toString();
                    String password_baru = agpEdtPasswordbaru.getText().toString();
                    kirim_ganti_password(password_lama,password_baru);
                }
            }
        });
    }

    private void kirim_ganti_password(String password_lama, String password_baru) {
        SweetAlert.sendingData(GantiPasswordActivity.this);

        api.gantiPassword(s.getApi(), s.getIdUser(), s.getSession(), password_lama, password_baru).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()){

                    try {
                        JSONObject jsonresponse = new JSONObject(response.body().string());
                        String pesan  = jsonresponse.optString("message","");
                        if (jsonresponse.optInt("success", 0)==1){
                            Toasti.success(GantiPasswordActivity.this,
                                    pesan);

                            Intent intent = new Intent(GantiPasswordActivity.this, MainActivity.class);
                            intent.putExtra("intent", "login");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                            startActivity(intent);

                            agpEdtPasswordbaru.setText("");
                            agpEdtPasswordbarukonfirmasi.setText("");
                            agpEdtPasswordlama.setText("");

                        }else {
                            Toasti.error(GantiPasswordActivity.this,
                                    pesan);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(GantiPasswordActivity.this, t.getMessage());
            }
        });
    }


    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.ganti_password));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        agpEdtPasswordlama = (EditText) findViewById(R.id.agp_edt_passwordlama);
        agpEdtPasswordbaru = (EditText) findViewById(R.id.agp_edt_passwordbaru);
        agpEdtPasswordbarukonfirmasi = (EditText) findViewById(R.id.agp_edt_passwordbarukonfirmasi);
        agpBtnKirim = (Button) findViewById(R.id.agp_btn_kirim);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
