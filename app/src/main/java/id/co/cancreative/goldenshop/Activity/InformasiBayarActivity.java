package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.R;

public class InformasiBayarActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;

    private Rekening rekGold;

    private String tglexp, kodeUnik = "0";
    private int ttlBayar = 0;

    private ImageView imgBank;
    private TextView tvNoRek;
    private TextView tvTgl;
    private ImageView imgUpload;
    private TextView tvNamaPemilik;
    private Button btnHistory;
    private TextView tvTotalBayar;
    private TextView tvKodeUnik;
    private TextView tvPesanBayar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi_bayar);
        initView();

        s = new SharedPref(this);
        rekGold = s.getRekeningGolden();

        getIntentExtra();
        mainButton();
        setValue();
        setToolbar();
    }

    private void getIntentExtra() {
        tglexp = getIntent().getStringExtra("extra");
        if (getIntent().getStringExtra("ttlBayar") != null) {
            ttlBayar = Integer.parseInt(getIntent().getStringExtra("ttlBayar"));
        }

    }

    private void mainButton() {
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(InformasiBayarActivity.this, DaftarTransaksiMenunguPembayaran.class);
                startActivity(i);
            }
        });
    }

    private void setValue() {

        if (tglexp != null) {
            tvTgl.setText("" + new Helper().convertDateTime(tglexp, "yyyy-MM-dd hh:mm:s"));
        } else {
            tvTgl.setText("" + new Helper().convertDateTime("2019-04-11 09:41:34", "yyyy-MM-dd hh:mm:s"));
        }

        tvTotalBayar.setText("" + new Helper().convertRupiah(ttlBayar));

        String gb = new Helper().convertRupiah(ttlBayar).replace(".", " ");
        String strArray[] = gb.split(" ");

        kodeUnik = strArray[strArray.length - 1];
        tvKodeUnik.setText("." + kodeUnik);

        tvNamaPemilik.setText("a/n " + rekGold.RK_NAME);
        tvNoRek.setText(rekGold.RK_NOMOR);
        Picasso.with(this)
                .load(Config.url_bank + rekGold.BK_IMAGE)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(imgBank);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Golden Shop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgBank = (ImageView) findViewById(R.id.img_bank);
        tvNoRek = (TextView) findViewById(R.id.tv_noRek);
        tvTgl = (TextView) findViewById(R.id.tv_tgl);
        imgUpload = (ImageView) findViewById(R.id.img_upload);
        tvNamaPemilik = (TextView) findViewById(R.id.tv_namaPemilik);
        btnHistory = (Button) findViewById(R.id.btn_history);
        tvTotalBayar = (TextView) findViewById(R.id.tv_totalBayar);
        tvKodeUnik = (TextView) findViewById(R.id.tv_kodeUnik);
        tvPesanBayar = (TextView) findViewById(R.id.tv_pesanBayar);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(InformasiBayarActivity.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        super.onBackPressed();
    }
}

