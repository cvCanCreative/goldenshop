package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.Adapter.AdapterAddGambarP;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiConfigDemo;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KomplainBarangActivity extends AppCompatActivity {

    private View parent_view;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private ApiService api2 = ApiConfigDemo.getInstanceRetrofit();
    private SharedPref s;
    private Transaksi trans;

    RecyclerView.Adapter mAdapter;

    private Produk produk;
    private User user;

    private File compresed;
    private List<File> imgProduct = new ArrayList<>();
    ArrayList<String> files = new ArrayList<>();
    ArrayList<File> _files = new ArrayList<>();
    ArrayList<File> filesCompres = new ArrayList<>();
    private String _status = "";

    private Toolbar toolbar;
    private TextView tvToolbarTitle;
    private ImageView btnSimpan;
    private EditText edtNama;
    private EditText edtDeskripsi;
    private RecyclerView rv;
    private ImageView image;
    private TextView tvKomplainAs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komplain_barang);
        initView();

        s = new SharedPref(this);
        user = s.getUser();
        trans = s.getDataTransaksi();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setValue();
        mainButton();
        setToolbar();

    }

    private void setValue() {

        String status = getIntent().getStringExtra("status");
        if (status != null){
            _status = status +" : ";
            tvKomplainAs.setText(status);
        }

        _files.add(null);
        mAdapter = new AdapterAddGambarP(_files, this);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv.setAdapter(mAdapter);
    }

    private void mainButton() {

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtNama.getText().toString().isEmpty()) {
                    edtNama.setError("Kolom Tidak Boleh Kosong");
                } else if (edtDeskripsi.getText().toString().isEmpty()) {
                    edtDeskripsi.setError("Kolom Tidak Boleh Kosong");
                } else {

                    String extra = getIntent().getStringExtra("extra");
                    if (extra != null) {
                        if (extra.equals("komplainBarang")) {

                            if (_files.size() <= 1) {
                                Snackbar.make(parent_view, "Sisipkan Bukti Foto", Snackbar.LENGTH_SHORT).show();
                            } else {
                                filesCompres.removeAll(filesCompres);
                                for (int i = 0; i < _files.size(); i++) {
                                    if (_files.get(i) != null) {

                                        try {
                                            compresed = new Compressor(KomplainBarangActivity.this).compressToFile(_files.get(i));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        filesCompres.add(compresed);
                                    }
                                }
                                addKomplain();
                            }

                        } else if (extra.equals("komplainAll")) {

                            if (_files.size() <= 1) {
                                addKomplainNoGbr();
                            } else {
                                filesCompres.removeAll(filesCompres);
                                for (int i = 0; i < _files.size(); i++) {
                                    if (_files.get(i) != null) {

                                        try {
                                            compresed = new Compressor(KomplainBarangActivity.this).compressToFile(_files.get(i));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        filesCompres.add(compresed);
                                    }
                                }
                                addKomplainAll();
                            }
                        }
                    }
                }
            }
        });
    }

    private void addKomplainNoGbr() {
        SweetAlert.onLoading(this);
        api.addKomplainAllNoGbr(s.getApi(), s.getIdUser(), s.getSession(), _status + edtNama.getText().toString(), edtDeskripsi.getText().toString()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    if (response.body().success == 1) {
                        new SweetAlertDialog(KomplainBarangActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Berhasil Mengirim Komplain")
                                .setContentText("Info selanjutanya akan kami kirim ke Email: " + user.email)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        finish();
                                    }
                                })
                                .show();
                    } else {
                        Toasti.info(KomplainBarangActivity.this, response.body().message);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(KomplainBarangActivity.this, t.getMessage());
            }
        });
    }

    private void addKomplainAll() {

        RequestBody sJudul = RequestBody.create(MediaType.parse("text/plain"), _status + edtNama.getText().toString());
        RequestBody sDetail = RequestBody.create(MediaType.parse("text/plain"), edtNama.getText().toString());

        MultipartBody.Part[] ImageProduk = new MultipartBody.Part[filesCompres.size()];
        for (int index = 0; index < filesCompres.size(); index++) {
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), filesCompres.get(index));
            ImageProduk[index] = MultipartBody.Part.createFormData("image[]", filesCompres.get(index).getName(), surveyBody);
        }
        SweetAlert.sendingData(this);
        api.addKomplainAll(s.getApi(), s.getIdUser(), s.getSession(), sJudul, sDetail, ImageProduk).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    if (response.body().success == 1) {
                        new SweetAlertDialog(KomplainBarangActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Berhasil Mengirim Komplain")
                                .setContentText("Info selanjutanya akan kami kirim ke Email: " + user.email)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        finish();
                                    }
                                })
                                .show();
                    } else {
                        Toasti.info(KomplainBarangActivity.this, response.body().message);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(KomplainBarangActivity.this, t.getMessage());
            }
        });
    }

    private void addKomplain() {

        RequestBody sId = RequestBody.create(MediaType.parse("text/plain"), trans.ID_TRANSAKSI);
        RequestBody sJudul = RequestBody.create(MediaType.parse("text/plain"), edtNama.getText().toString());
        RequestBody sDetail = RequestBody.create(MediaType.parse("text/plain"), edtNama.getText().toString());

        MultipartBody.Part[] ImageProduk = new MultipartBody.Part[filesCompres.size()];
        for (int index = 0; index < filesCompres.size(); index++) {
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), filesCompres.get(index));
            ImageProduk[index] = MultipartBody.Part.createFormData("image[]", filesCompres.get(index).getName(), surveyBody);
        }
        SweetAlert.sendingData(this);
        api.addKomplain(s.getApi(), s.getIdUser(), s.getSession(), sId, sJudul, sDetail, ImageProduk).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    if (response.body().success == 1) {
                        new SweetAlertDialog(KomplainBarangActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Berhasil Mengirim Komplain")
                                .setContentText("Info selanjutanya akan kami kirim ke Email: " + user.email)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        finish();
                                    }
                                })
                                .show();
                    } else {
                        Toasti.info(KomplainBarangActivity.this, response.body().message);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(KomplainBarangActivity.this, t.getMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, KomplainBarangActivity.this, new DefaultCallback() {
            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
                _files.add(null);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onImagePicked(File imageFiles, EasyImage.ImageSource source, int type) {

                if (type == 1) {
                    _files.add(imageFiles);
                    if (_files.size() <= 5) {
                        _files.add(null);
                    }
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        tvToolbarTitle.setText("Komplain Barang");
        getSupportActionBar().setTitle("Tambah Produk");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        parent_view = findViewById(android.R.id.content);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        btnSimpan = (ImageView) findViewById(R.id.btn_simpan);
        edtNama = (EditText) findViewById(R.id.edt_nama);
        edtDeskripsi = (EditText) findViewById(R.id.edt_deskripsi);
        rv = (RecyclerView) findViewById(R.id.rv);
        image = (ImageView) findViewById(R.id.image);
        tvKomplainAs = (TextView) findViewById(R.id.tv_komplainAs);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
