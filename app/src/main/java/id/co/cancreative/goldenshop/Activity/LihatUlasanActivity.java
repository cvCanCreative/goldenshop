package id.co.cancreative.goldenshop.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.Adapter.AdapterUlasan;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Feedback;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelList;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatUlasanActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;
    private RecyclerView rvUlasan;
    RecyclerView.LayoutManager layoutManager;
    AdapterUlasan mAdapter;
    String idBarang;

    List<Feedback> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_ulasan);
        initView();

        s = new SharedPref(this);

        setToolbar();

        idBarang = getIntent().getStringExtra("id_barang");
        getData(idBarang);
    }

    private void getData(final String id){
        SweetAlert.sendingData(this);
        api.getFeedback(s.getApi(),""+id).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                if (response.isSuccessful()){
                    SweetAlert.dismis();
                    if (response.body().getSuccess() == 1){
                        list = response.body().feedback;

                        rvUlasan.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        mAdapter = new AdapterUlasan(getApplicationContext(), list);
                        rvUlasan.setAdapter(mAdapter);
                    }else {
                        Toasti.info(getApplicationContext(),"Belum Ada Feedback");
                    }
                }else {
                    SweetAlert.dismis();
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                final SweetAlertDialog dialogGagal = new SweetAlertDialog(LihatUlasanActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Ulangi");

                dialogGagal.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        getData(id);
                    }
                });
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Semua Ulasan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        rvUlasan = (RecyclerView) findViewById(R.id.rv_ulasan);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
