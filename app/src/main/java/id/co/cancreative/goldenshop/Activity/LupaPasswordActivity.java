package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LupaPasswordActivity extends AppCompatActivity {
    boolean feedback = false;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;
    private EditText lpEdtEmail;
    private Button lpBtnKirim;
    private TextView lpTvKembali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password);
        initView();

        s = new SharedPref(this);

        setToolbar();

        lpTvKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        lpBtnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emailvalid(lpEdtEmail)){
                    lpBtnKirim.setError(getString(R.string.email_tidak_valid));
                }else {
                    String email = lpEdtEmail.getText().toString();
                    reset_password(email);
                }
            }
        });

    }

    private void reset_password(String email) {
        SweetAlert.sendingData(LupaPasswordActivity.this);
        api.lupaPassowrd(s.getApi(),email).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()){

                    try {
                        JSONObject jsonresponse = new JSONObject(response.body().string());
                        String pesan  = jsonresponse.optString("message","");
                        if (jsonresponse.optInt("success", 0)==1){
                            new SweetAlertDialog(LupaPasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Reset Berhasil")
                                    .setContentText(pesan)
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            onBackPressed();
                                        }
                                    })
                                    .show();
                        }else {
                            SweetAlert.waring(LupaPasswordActivity.this,"Gagal Mengirim Email" ,pesan);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(LupaPasswordActivity.this, t.getMessage());
            }
        });
    }

    private boolean emailvalid(EditText lpEdtEmail) {
        // VALIDASI EMAIL
        final String email = lpEdtEmail.getText().toString().trim();

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        lpEdtEmail.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (email.matches(emailPattern) && s.length() > 0)
                {
                    feedback =  true;
                }
                else
                {
                    feedback = false;

                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });
      return feedback;
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GoldenShop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        lpEdtEmail = (EditText) findViewById(R.id.lp_edt_email);
        lpBtnKirim = (Button) findViewById(R.id.lp_btn_kirim);
        lpTvKembali = (TextView) findViewById(R.id.lp_tv_kembali);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
