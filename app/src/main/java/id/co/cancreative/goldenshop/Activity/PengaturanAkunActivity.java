package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.io.File;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiConfigDemo;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelSerializ;
import id.co.cancreative.goldenshop.Model.ResponseCari;
import id.co.cancreative.goldenshop.R;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PengaturanAkunActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private File imageFile;
    private File fileImage, _fileImage;
    private boolean status;

    private SharedPref s;

    private Toolbar toolbar;
    private LinearLayout btnLogout;
    private RelativeLayout btnSettingToko;
    private RelativeLayout btnSettingAlamat;
    private RelativeLayout btnSettingRekening;
    private LinearLayout btnEditAkun;
    private RelativeLayout btnPusatbantuan;
    private ImageView image;
    private RelativeLayout btnReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan_akun);
        initView();

        s = new SharedPref(this);

        setToolbar();
        mainButton();
        getAbout();
    }

    private void mainButton() {
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s.clearAll();
                Intent intent = new Intent(PengaturanAkunActivity.this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        btnSettingAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DaftarItemActivity.class);
                intent.putExtra("extra", "alamat");
                startActivity(intent);
            }
        });

        btnSettingRekening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DaftarItemActivity.class);
                intent.putExtra("extra", "rekening");
                startActivity(intent);
            }
        });

        btnSettingToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.getStatusToko() == true) {
                    Intent intent = new Intent(getApplicationContext(), PengaturanTokoActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), BukaTokoActivity.class);
                    startActivity(intent);
                }
            }
        });

        btnEditAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                EasyImage.openCamera(PengaturanAkunActivity.this, 1);
                Intent intent = new Intent(getApplicationContext(), EditProfileUserActivity.class);
                startActivity(intent);
            }
        });

        btnPusatbantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), FAQActivity.class));
            }
        });

        btnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://play.google.com/store/apps/dev?id=id.co.cancreative.goldenshop"));
                startActivity(i);
            }
        });
    }

    private void getAbout() {
        api.getAbout(s.getApi()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().success == 1) s.setAbout(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {

            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pengaturan Akun");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, PengaturanAkunActivity.this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFiles, EasyImage.ImageSource source, int type) {

                Log.d("do this", "do");
                if (type == 1) {
                    imageFile = imageFiles;
                    Log.d("do", "do" + imageFiles.getName());
//                    SaveData.setImage(imageFiles);
                    Glide.with(PengaturanAkunActivity.this).load(imageFile)
                            .centerCrop()
                            .into(image);
                    absen();
//                    absenMasuk("9872348", "-29813123", "2019-04-28 18:00:00");
//                    CallFunction.getGambar();
//                    finish();
                }
            }
        });
    }


    private void absen() {
        String token = "s6QwOBzESfiPkhynSAM0fiDZvI9VXil08edTShf7DeriqozAlkJDyoAvwPFsbZFK";
        api.absenMasuk(token, "9872348", "-29813123", "2019-04-28 18:00:00").enqueue(new Callback<ResponseCari>() {
            @Override
            public void onResponse(Call<ResponseCari> call, Response<ResponseCari> response) {

            }

            @Override
            public void onFailure(Call<ResponseCari> call, Throwable t) {

            }
        });
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnLogout = (LinearLayout) findViewById(R.id.btn_logout);
        btnSettingToko = (RelativeLayout) findViewById(R.id.btn_settingToko);
        btnSettingAlamat = (RelativeLayout) findViewById(R.id.btn_settingAlamat);
        btnSettingRekening = (RelativeLayout) findViewById(R.id.btn_settingRekening);
        btnEditAkun = (LinearLayout) findViewById(R.id.btn_editAkun);
        btnPusatbantuan = (RelativeLayout) findViewById(R.id.btn_pusatbantuan);
        image = (ImageView) findViewById(R.id.image);
        btnReview = (RelativeLayout) findViewById(R.id.btn_review);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public boolean setBoolean(boolean _status){
        status = _status;
        return status;
    }

}
