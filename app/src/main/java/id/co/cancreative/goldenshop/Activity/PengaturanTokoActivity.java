package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.EditToko;
import id.co.cancreative.goldenshop.Model.Kota;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.Model.Provinsi;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelProvinsi;
import id.co.cancreative.goldenshop.Model.ResultProvinsi;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PengaturanTokoActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toko toko;
    private ModelAlamat alamat;
    private EditToko editToko;
    private ResultProvinsi mProvinsi;

    private List<Provinsi> listProv;

    private List<String> idProvinsi = new ArrayList<>();
    private List<String> idCity = new ArrayList<>();
    private List<String> idKec = new ArrayList<>();
    private List<String> provinsi = new ArrayList<>();
    private List<String> namaKota = new ArrayList<>();
    private List<String> namaKec = new ArrayList<>();
    private List<String> kodePos = new ArrayList<>();

    private List<String> provinsi_list = new ArrayList<>();
    private List<String> list_kota = new ArrayList<>();
    private List<String> list_kec = new ArrayList<>();

    private int pos;
    private String kodeposs;
    private String namaToko, deskripsi, image;
    private String _provinsi, kota, kecamatan, _alamat;
    private String idProv, idKota, _idKec;
    private boolean alamatEdited = false;

    private Toolbar toolbar;
    private AppBarLayout appbarLayout;
    private TextView tvNamaToko;
    private TextView tvDeskripsi;
    private LinearLayout lyKurir;
    private Spinner spnProvinsi;
    private TextView tvProvinsi;
    private Spinner spnKota;
    private TextView tvKota;
    private Spinner spnKec;
    private TextView tvKec;
    private EditText edtAlamat;
    private ImageView imgToko;
    private Button btnPilihAlamat;
    private LinearLayout btnUbah;
    private Button btnSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan_toko);
        initView();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        s = new SharedPref(this);
        toko = s.getToko();
        editToko = s.getEditToko();
        mProvinsi = s.getProvinsi();
        alamat = s.getShipping();

        setAlamat();
        mainButton();
        setValue();
        callFungtion();
        setToolbar();
    }

    private void callFungtion() {
//        CallFunction.setPengaturanToko(new Callable<Void>() {
//            @Override
//            public Void call() throws Exception {
//                editToko = s.getEditToko();
//                setValue();
//                return null;
//            }
//        });

        CallFunction.setPesananActivity(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                alamat = s.getShipping();
                _alamat = alamat.PE_ALAMAT;
                _provinsi = alamat.PE_PROVINSI;
                kota = alamat.PE_KOTA;
                kecamatan = alamat.PE_KECAMATAN;
                idKota = alamat.PE_ID_KOTA;
                _idKec = alamat.PE_ID_KECAMATAN;

                // get ID Provinsi
                mProvinsi = s.getProvinsi();
                listProv = mProvinsi.results;
                for (Provinsi p : listProv) {

                    if (p.province.equals(_provinsi)) {
                        idProv = p.province_id;
                        Log.d("idProv", "! " + _provinsi + " : " + idProv);
                    }
                }

                tvProvinsi.setText(_provinsi);
                tvKota.setText(kota);
                tvKec.setText(kecamatan);
                edtAlamat.setText(_alamat);
                return null;
            }
        });
    }

    private void mainButton() {
        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), UbahNamaTokoActivity.class);
                startActivity(i);
            }
        });

        btnPilihAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), DaftarItemActivity.class);
                i.putExtra("extra", "pilihAlamat");
                startActivity(i);
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
    }

    private void saveData() {

        _alamat = edtAlamat.getText().toString();
        if (!namaToko.equals(toko.TOK_NAME) || !deskripsi.equals(toko.TOK_DETAIL) || !image.equals(toko.TOK_FOTO) || !_idKec.equals(toko.TOK_ID_KECAMATAN) || !_alamat.equals(toko.TOK_ADDRESS)) {
            SweetAlert.onLoading(this);
            api.editToko(
                    s.getApi(), s.getIdUser(), s.getSession(), toko.ID_TOKO, namaToko,
                    deskripsi, _alamat, idProv, idKota, _idKec, kota, kecamatan, image)
                    .enqueue(new Callback<ResponModelProvinsi>() {
                        @Override
                        public void onResponse(Call<ResponModelProvinsi> call, Response<ResponModelProvinsi> response) {

                            api.getToko(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
                                @Override
                                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                    SweetAlert.dismis();
                                    ResponModel res = response.body();
                                    if (response.isSuccessful()) {
                                        SweetAlert.success(PengaturanTokoActivity.this, "Update berhasil", "Berhasil memperbarui info toko");
                                        toko = new Toko();
                                        toko = res.tokos;
                                        s.setToko(toko);
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponModel> call, Throwable t) {
                                    SweetAlert.dismis();
                                    SweetAlert.onFailure(PengaturanTokoActivity.this, t.getMessage());
                                }
                            });
                        }

                        @Override
                        public void onFailure(Call<ResponModelProvinsi> call, Throwable t) {
                            SweetAlert.dismis();
                            SweetAlert.onFailure(PengaturanTokoActivity.this, t.getMessage());
                        }
                    });
            Log.d("Status", "Edited");
        } else {
            PengaturanTokoActivity.this.finish();
            Log.d("Status", "Tidak di Edit");
        }
    }

    private void setValue() {

        namaToko = toko.TOK_NAME;
        deskripsi = toko.TOK_DETAIL;
        image = toko.TOK_FOTO;

        // Cek Status informasi umum, apakah pernah di edit
        if (editToko != null) {
            if (editToko.status) {
                namaToko = editToko.TOK_NAME;
                deskripsi = editToko.TOK_DETAIL;
                image = editToko.TOK_FOTO;
            }
        }

        tvNamaToko.setText(namaToko);
        tvDeskripsi.setText(deskripsi);

        Picasso.with(this)
                .load(Config.url_toko + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(imgToko);

        if (alamatEdited) {
            return;
        }

        // Set informasi Alamat
        idProv = toko.TOK_ID_PROV;
        idKota = toko.TOK_ID_KOTA;
        _idKec = toko.TOK_ID_KECAMATAN;

        kota = toko.TOK_KOTA;
        kecamatan = toko.TOK_KECAMATAN;
        _alamat = toko.TOK_ADDRESS;

        listProv = mProvinsi.results;
        for (Provinsi p : listProv) {
            if (p.province_id.equals(toko.TOK_ID_PROV)) {
                tvProvinsi.setText(p.province);
            }
        }

        tvKota.setText(kota);
        tvKec.setText(kecamatan);
        edtAlamat.setText(_alamat);

    }

    private void setAlamat() {

        idProvinsi.add("0");
        List<Provinsi> provinsis = mProvinsi.results;
        for (Provinsi p : provinsis) {
            idProvinsi.add(p.province_id);
            provinsi.add(p.province);
        }

        provinsi_list.add("Pilih Provinsi");
        provinsi_list.addAll(provinsi);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, provinsi_list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        spnProvinsi.setAdapter(adapter);
        spnProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                list_kota.removeAll(list_kota);
                list_kec.removeAll(list_kec);
                namaKec.removeAll(namaKec);
                namaKota.removeAll(namaKota);
                idCity.removeAll(idCity);
                idKec.removeAll(idKec);

                if (spnProvinsi.getSelectedItemPosition() != 0) {

                    tvProvinsi.setError(null);
                    tvKec.setText(null);

                    SweetAlert.onLoading(PengaturanTokoActivity.this);

                    pos = spnProvinsi.getSelectedItemPosition();
                    idProv = idProvinsi.get(pos);
                    _provinsi = spnProvinsi.getSelectedItem().toString();
                    tvProvinsi.setText(_provinsi);

                    final ApiService api = ApiConfig.getInstanceRetrofit();
                    api.getListoKota(idProv).enqueue(new Callback<ResponModel>() {
                        @Override
                        public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                            SweetAlert.dismis();
                            if (response.isSuccessful()) {
                                final List<Kota> kotas = response.body().getRajaongkir().results;

                                idCity.add("0");
                                kodePos.add("0");
                                for (Kota k : kotas) {
                                    idCity.add(k.getCityId());
                                    namaKota.add(k.getCityName() + " " + k.getType());
                                    kodePos.add(k.getPostalCode());
                                }

                                list_kota.add("Pilih kota");
                                list_kota.addAll(namaKota);

                                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_spinner_item, list_kota);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                adapter.notifyDataSetChanged();
                                spnKota.setAdapter(adapter);
                                spnKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        int pos = spnKota.getSelectedItemPosition();
                                        namaKec.removeAll(namaKec);
                                        idKec.removeAll(idKec);

                                        idKota = idCity.get(pos);
                                        kodeposs = kodePos.get(pos);
                                        kota = spnKota.getSelectedItem().toString();
                                        tvKota.setText(kota);

                                        if (pos != 0) {

                                            tvKota.setError(null);

                                            SweetAlert.onLoading(PengaturanTokoActivity.this);
                                            ApiService api = ApiConfig.getInstanceRetrofit();
                                            api.getListoKec(idKota).enqueue(new Callback<ResponModel>() {
                                                @Override
                                                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                                    SweetAlert.dismis();
                                                    if (response.isSuccessful()) {
                                                        List<Kota> kota = response.body().getRajaongkir().results;

                                                        idKec.add("0");
                                                        for (Kota k : kota) {
                                                            idKec.add(k.getSubdistrict_id());
                                                            namaKec.add(k.getSubdistrict_name());
                                                        }

                                                        list_kec.add("Pilih kecamatan");
                                                        list_kec.addAll(namaKec);

                                                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                                                                android.R.layout.simple_spinner_item, list_kec);
                                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                        adapter.notifyDataSetChanged();
                                                        spnKec.setAdapter(adapter);
                                                        spnKec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                            @Override
                                                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                                int pos = spnKec.getSelectedItemPosition();
                                                                tvKec.setError(null);
                                                                _idKec = idKec.get(pos);
                                                                kecamatan = spnKec.getSelectedItem().toString();
                                                                tvKec.setText(kecamatan);

                                                                if (!_idKec.equals(toko.TOK_ID_KECAMATAN)) {
                                                                    alamatEdited = true;
                                                                }
                                                            }

                                                            @Override
                                                            public void onNothingSelected(AdapterView<?> adapterView) {

                                                            }
                                                        });

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ResponModel> call, Throwable t) {
                                                    SweetAlert.dismis();
                                                    SweetAlert.onFailure(PengaturanTokoActivity.this, t.getMessage());
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponModel> call, Throwable t) {
                            SweetAlert.dismis();
                            SweetAlert.onFailure(PengaturanTokoActivity.this, t.getMessage());
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Atur Toko");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_simpan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            onBackPressed();
        } else if (item_id == R.id.action_checkout) {
            saveData();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        appbarLayout = (AppBarLayout) findViewById(R.id.appbar_layout);
        tvNamaToko = (TextView) findViewById(R.id.tv_namaToko);
        tvDeskripsi = (TextView) findViewById(R.id.tv_deskripsi);
        lyKurir = (LinearLayout) findViewById(R.id.ly_kurir);
        spnProvinsi = (Spinner) findViewById(R.id.spn_provinsi);
        tvProvinsi = (TextView) findViewById(R.id.tv_provinsi);
        spnKota = (Spinner) findViewById(R.id.spn_kota);
        tvKota = (TextView) findViewById(R.id.tv_kota);
        spnKec = (Spinner) findViewById(R.id.spn_kec);
        tvKec = (TextView) findViewById(R.id.tv_kec);
        edtAlamat = (EditText) findViewById(R.id.edt_alamat);
        imgToko = (ImageView) findViewById(R.id.img_toko);
        btnPilihAlamat = (Button) findViewById(R.id.btn_pilihAlamat);
        btnUbah = (LinearLayout) findViewById(R.id.btn_ubah);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
    }

    @Override
    protected void onResume() {
        editToko = s.getEditToko();
        setValue();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (editToko != null) {
            if (editToko.status) {
                final SweetAlertDialog pDialog = new SweetAlertDialog(PengaturanTokoActivity.this, SweetAlertDialog.WARNING_TYPE);
                pDialog.setTitleText("Profile belum tersimpan")
                        .setContentText("Perubahan akan dihapus")
                        .setConfirmText("Simpan")
                        .setCancelText("Hapus")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                saveData();
                                s.clearEditToko();
                                pDialog.dismiss();
                                finish();
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                s.clearEditToko();
                                pDialog.dismiss();
                                finish();
                            }
                        });
                pDialog.show();
                return;
            }
            return;
        }
        super.onBackPressed();
    }
}
