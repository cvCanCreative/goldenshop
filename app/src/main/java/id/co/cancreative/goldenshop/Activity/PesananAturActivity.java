package id.co.cancreative.goldenshop.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.Model.ModelTransaksi;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PesananAturActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Produk produk;
    private ModelAlamat alamat;
    private ModelTransaksi trans;

    private int totalHarga = 0, totalBayar = 0, ongkir = 0, promo = 0, uPromo = 0;
    private double jumlahItem = 1;
    private double hargaAsli = 0;
    private String agenKurir, idOrigin;
    private String sKurir, jenisKurir;
    private String kodePromo = "null";

    List<String> jasaPengiriman = new ArrayList<>();
    List<String> listJenisKurir = new ArrayList<>();
    List<String> listHarga = new ArrayList<>();
    List<String> listTipe = new ArrayList<>();

    private Toolbar toolbar;
    private TextView tvNamaToko;
    private ImageView btnAddFoto;
    private LinearLayout btnKurang, divLayout, divFooter, btnTambah;
    private ImageView imgProduk;
    private TextView tvNamaProduk;
    private TextView tvHarga;
    private TextView tvJumlahItem;
    private TextView tvTotalHarga;
    private TextView tvTotalBayar;
    private LinearLayout btnAlamat;
    private TextView tvAlamat;
    private ImageView image;
    private Spinner spnKurir;
    private TextView tvKurir;
    private Spinner spnType;
    private TextView tvOngkir;
    private TextView tvTotalOngkir;
    private Button btnBayar;
    private LinearLayout divNoAlamat;
    private LinearLayout divKurir;
    private EditText edtCatatan;
    private TextView tvStatusKurir;
    private LinearLayout divPilihMetodeSlide;
    private ImageView btnClose;
    private EditText edtPromo;
    private TextView tvPesan;
    private Button btnGunakan;
    private RelativeLayout divLayer;
    private RelativeLayout btnMasukkanPromo;
    private RelativeLayout lyPromo;
    private TextView tvPromo;
    private RelativeLayout layer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atur_pesanan);
        initView();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        s = new SharedPref(this);
        produk = s.getProduk();
        alamat = s.getShipping();

        mainButton();
        setValue();
        callFunction();
        setToolbar();

    }

    private void callFunction() {
        CallFunction.setPesananActivity(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                alamat = s.getShipping();
                setValue();
                setOngkir();
                return null;
            }
        });
    }

    private void mainButton() {
        btnKurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jumlahItem != 1) {
                    jumlahItem--;
                    tvJumlahItem.setText(String.valueOf((int) jumlahItem));
                    totalHarga = (int) (hargaAsli * jumlahItem);
                    totalBayar = ongkir + totalHarga;
                    tvTotalHarga.setText(new Helper().convertRupiah(totalHarga));
                    tvTotalBayar.setText(new Helper().convertRupiah(totalBayar));
                }
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int iStok = Integer.valueOf(produk.BA_STOCK);
                if (!(jumlahItem >= iStok)) {
                    jumlahItem++;
                    tvJumlahItem.setText(String.valueOf((int) jumlahItem));
                    totalHarga = (int) (hargaAsli * jumlahItem);
                    totalBayar = ongkir + totalHarga;
                    tvTotalHarga.setText(new Helper().convertRupiah(totalHarga));
                    tvTotalBayar.setText(new Helper().convertRupiah(totalBayar));
                }
            }
        });

        btnAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DaftarItemActivity.class);
                intent.putExtra("extra", "pilihAlamat");
                startActivity(intent);
            }
        });

        btnMasukkanPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideDown();
            }
        });

        divLayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideDown();
            }
        });

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ongkir == 0) {
                    tvStatusKurir.setError("Mohon Pilih Jenis Ongkir");
                } else {

                    final int min = 100;
                    final int max = 999;
                    final int random = new Random().nextInt((max - min) + 1) + min;

                    ModelTransaksi trans = new ModelTransaksi();

                    trans.kode_unik = random;
                    trans.kode_promo = kodePromo;
                    trans.totalBayar = totalBayar;
                    trans.id_penerima.add(alamat.ID_PENGIRIMAN);
                    trans.id_barang.add(Integer.valueOf(produk.ID_BARANG));
                    trans.qty.add((int) jumlahItem);
                    trans.kurir.add(sKurir);
                    trans.jns_kurir.add(jenisKurir);
                    trans.ongkir.add(ongkir);

                    if (edtCatatan.getText().toString().isEmpty()) {
                        trans.catatan.add("null");
                    } else {
                        trans.catatan.add(edtCatatan.getText().toString());
                    }
                    s.setTransaksi(trans);

                    Intent intent = new Intent(getApplicationContext(), PesananBayarSingelActivity.class);
                    intent.putExtra("promo", "" + uPromo);
                    intent.putExtra("kodePromo", "" + kodePromo);
                    startActivity(intent);
                }
            }
        });

        btnGunakan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtPromo.getText().toString().isEmpty()){
                    tvPesan.setVisibility(View.VISIBLE);
                    tvPesan.setText("Kode Promo Kosong");
                }

                SweetAlert.onLoading(PesananAturActivity.this);
                ArrayList<String> idProduk = new ArrayList<>();
                idProduk.add(produk.ID_BARANG);
                api.cekPromo(s.getApi(), s.getIdUser(), s.getSession(), idProduk, String.valueOf(totalBayar), edtPromo.getText().toString()).enqueue(new Callback<ResponModel>() {
                    @Override
                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                        SweetAlert.dismis();
                        if (response.body().success == 1) {
                            promo = Integer.parseInt(response.body().detail_promo.PR_POTONGAN);

                            final SweetAlertDialog pDialog = new SweetAlertDialog(PesananAturActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                            pDialog.setTitleText("Kode Promo Sesuai")
                                    .setContentText("Selamat anda mendapatkan potongan sebesar " + new Helper().convertRupiah(promo))
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            kodePromo = edtPromo.getText().toString();

                                            lyPromo.setVisibility(View.VISIBLE);
                                            edtPromo.setText(null);
                                            tvPesan.setVisibility(View.GONE);
                                            tvTotalBayar.setText(new Helper().convertRupiah(totalBayar - promo));
                                            tvPromo.setText(new Helper().convertRupiah(promo));
                                            uPromo = promo;
                                            slideDown();

                                            sweetAlertDialog.dismiss();
                                        }
                                    });
                            pDialog.show();
                        } else {
                            tvPesan.setVisibility(View.VISIBLE);
                            tvPesan.setText(response.body().message);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponModel> call, Throwable t) {
                        SweetAlert.dismis();
                        SweetAlert.onFailure(PesananAturActivity.this, t.getMessage());
                    }
                });

//                if (produk.PR_CODE != null) {
//                    if (produk.PR_CODE.equals(edtPromo.getText().toString())) {
//                        if (produk.PR_STATUS.equals("TK_AKTIF")) {
//                            tvPesan.setVisibility(View.VISIBLE);
//                            tvPesan.setText("Kode promo sudah tidak berlaku");
//                        } else {
//
//                            promo = Integer.parseInt(produk.PR_POTONGAN);
//
//                            final SweetAlertDialog pDialog = new SweetAlertDialog(PesananAturActivity.this, SweetAlertDialog.SUCCESS_TYPE);
//                            pDialog.setTitleText("Kode Promo Sesuai")
//                                    .setContentText("Selamat anda mendapatkan potongan sebesar "+ new Helper().convertRupiah(promo))
//                                    .setConfirmText("Ok")
//                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                        @Override
//                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                            kodePromo = edtPromo.getText().toString();
//
//                                            lyPromo.setVisibility(View.VISIBLE);
//                                            edtPromo.setText(null);
//                                            tvPesan.setVisibility(View.GONE);
//                                            tvTotalBayar.setText(new Helper().convertRupiah(totalBayar - promo));
//                                            tvPromo.setText(new Helper().convertRupiah(promo));
//                                            uPromo = promo;
//                                            slideDown();
//
//                                            sweetAlertDialog.dismiss();
//                                        }
//                                    });
//                            pDialog.show();
//                        }
//                    } else {
//                        tvPesan.setVisibility(View.VISIBLE);
//                        tvPesan.setText("Kode Promo yang kamu masukkan salah");
//                    }
//                } else {
//                    tvPesan.setVisibility(View.VISIBLE);
//                    tvPesan.setText("Kode Promo yang kamu masukkan salah");
//                }
            }
        });
    }

    private void setValue() {

        divPilihMetodeSlide.animate().translationY(divPilihMetodeSlide.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divPilihMetodeSlide.setVisibility(View.GONE);
                    }
                });

        edtPromo.setHint("Masukkan Kode Promo");

        hargaAsli = Double.valueOf(produk.BA_PRICE);
        totalHarga = Integer.valueOf(produk.BA_PRICE);
        totalBayar = ongkir + totalHarga;

        tvNamaToko.setText(produk.TOK_NAME);
        tvNamaProduk.setText(produk.BA_NAME);
        tvHarga.setText(new Helper().convertRupiah(Integer.valueOf(produk.BA_PRICE)));
        tvTotalHarga.setText(new Helper().convertRupiah(totalHarga));
        tvTotalBayar.setText(new Helper().convertRupiah(totalBayar - promo));
        tvJumlahItem.setText(String.valueOf((int) jumlahItem));
        tvTotalOngkir.setText(new Helper().convertRupiah(ongkir));

        String image = new Helper().splitText(produk.BA_IMAGE);

        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.img_kosong)
                .error(R.drawable.img_kosong)
                // To fit image into imageView
//					.fit()
                // To prevent fade animation
                .noFade()
                .into(imgProduk);

        if (alamat != null) {
            String sAlamat = alamat.PE_JUDUL + " - " + alamat.PE_ALAMAT;
            tvAlamat.setText(sAlamat);
            setOngkir();
            divNoAlamat.setVisibility(View.GONE);
            divKurir.setVisibility(View.VISIBLE);
        }
    }

    private void setOngkir() {
        List<String> shipping_list = new ArrayList<>();
//        shipping_list.add("Pilih Kurir");
        shipping_list.add("JNE");
        shipping_list.add("TIKI");
        shipping_list.add("POS");
        shipping_list.add("J&T");

        // Initialize and set Adapter
        final ArrayAdapter adapter_shipping = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, shipping_list.toArray());
        adapter_shipping.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnKurir.setAdapter(adapter_shipping);
        spnKurir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                listHarga.removeAll(listHarga);
                jasaPengiriman.removeAll(jasaPengiriman);

                tvKurir.setText(spnKurir.getSelectedItem().toString());
                sKurir = spnKurir.getSelectedItem().toString().toLowerCase();

                if (sKurir == "j&t" || sKurir.equals("j&t")) {
                    sKurir = "jnt";
                }

//                alamat.PE_ID_KECAMATAN
                SweetAlert.onLoading(PesananAturActivity.this);
                ApiService api = ApiConfig.getInstanceRetrofit();
                api.getOngkir(Config.origin, "subdistrict", alamat.PE_ID_KECAMATAN, "subdistrict", "1", sKurir).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        SweetAlert.dismis();
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                final JSONObject rajaongkir = jsonObject.optJSONObject("rajaongkir");
                                final JSONObject status = rajaongkir.optJSONObject("status");
                                int code = status.optInt("code");
                                String description = status.optString("description");
                                if (code == 200) {
                                    JSONArray jsonArray = rajaongkir.optJSONArray("results");
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(0);
                                    agenKurir = jsonObject1.optString("code");
                                    JSONArray jsonArray1 = jsonObject1.optJSONArray("costs");
                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        JSONObject jsonObject2 = jsonArray1.optJSONObject(j);
                                        String tipe = jsonObject2.optString("service");
                                        JSONArray jsonArray2 = jsonObject2.optJSONArray("cost");
                                        JSONObject jsonObject3 = jsonArray2.optJSONObject(0);
                                        String harga = jsonObject3.optString("value");
                                        String lama = jsonObject3.optString("etd");

                                        listHarga.add(harga);
                                        listTipe.add(tipe);
                                        if (lama == "" || lama.equals("") || lama == null) {
                                            lama = "3-4";
                                        }

                                        listJenisKurir.add(agenKurir.toUpperCase() + " " + tipe);
                                        jasaPengiriman.add(agenKurir.toUpperCase() + " " + tipe + " (" + lama + " hari kerja) - " + new Helper().convertRupiah(Integer.parseInt(harga)));

                                    }

                                    adapter_shipping.notifyDataSetChanged();
                                    ArrayAdapter<String> adpHarga = new ArrayAdapter<String>(getApplicationContext(),
                                            android.R.layout.simple_spinner_item, jasaPengiriman);
                                    adpHarga.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    spnType.setAdapter(adpHarga);
                                    spnType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                            tvOngkir.setText(spnType.getSelectedItem().toString());
                                            int x = spnType.getSelectedItemPosition();

                                            jenisKurir = listJenisKurir.get(x);
                                            ongkir = Integer.valueOf(listHarga.get(x));
                                            totalBayar = totalHarga + ongkir;

                                            tvTotalOngkir.setText(new Helper().convertRupiah(ongkir));
                                            tvTotalBayar.setText(new Helper().convertRupiah(totalBayar - promo));

                                            tvStatusKurir.setError(null);
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });
                                } else {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        SweetAlert.dismis();
                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void slideDown() {
        divPilihMetodeSlide.animate().translationY(divPilihMetodeSlide.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divPilihMetodeSlide.setVisibility(View.GONE);
                        divLayer.setVisibility(View.GONE);
                        tvPesan.setVisibility(View.GONE);
                    }
                });
    }

    private void slideUp() {
        divLayer.setVisibility(View.VISIBLE);
        divPilihMetodeSlide.setVisibility(View.VISIBLE);
        divPilihMetodeSlide.animate().translationY(0)
                .alpha(1.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation, boolean isReverse) {
                        divPilihMetodeSlide.setVisibility(View.VISIBLE);
                        divLayer.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Atur Pesanan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvNamaToko = (TextView) findViewById(R.id.tv_namaToko);
        btnAddFoto = (ImageView) findViewById(R.id.btn_addFoto);
        btnKurang = (LinearLayout) findViewById(R.id.btn_kurang);
        divLayout = (LinearLayout) findViewById(R.id.div_layout);
        divFooter = (LinearLayout) findViewById(R.id.div_footer);
        imgProduk = (ImageView) findViewById(R.id.img_produk);
        tvNamaProduk = (TextView) findViewById(R.id.tv_namaProduk);
        tvHarga = (TextView) findViewById(R.id.tv_harga);
        btnTambah = (LinearLayout) findViewById(R.id.btn_tambah);
        tvJumlahItem = (TextView) findViewById(R.id.tv_jumlahItem);
        tvTotalHarga = (TextView) findViewById(R.id.tv_totalHarga);
        tvTotalBayar = (TextView) findViewById(R.id.tv_totalBayar);
        btnAlamat = (LinearLayout) findViewById(R.id.btn_alamat);
        tvAlamat = (TextView) findViewById(R.id.tv_alamat);
        image = (ImageView) findViewById(R.id.image);
        spnKurir = (Spinner) findViewById(R.id.spn_kurir);
        tvKurir = (TextView) findViewById(R.id.tv_kurir);
        spnType = (Spinner) findViewById(R.id.spn_type);
        tvOngkir = (TextView) findViewById(R.id.tv_ongkir);
        tvTotalOngkir = (TextView) findViewById(R.id.tv_totalOngkir);
        btnBayar = (Button) findViewById(R.id.btn_bayar);
        divNoAlamat = (LinearLayout) findViewById(R.id.div_noAlamat);
        divKurir = (LinearLayout) findViewById(R.id.div_kurir);
        edtCatatan = (EditText) findViewById(R.id.edt_catatan);
        tvStatusKurir = (TextView) findViewById(R.id.tv_statusKurir);
        divPilihMetodeSlide = (LinearLayout) findViewById(R.id.div_pilihMetodeSlide);
        btnClose = (ImageView) findViewById(R.id.btn_close);
        edtPromo = (EditText) findViewById(R.id.edt_promo);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        btnGunakan = (Button) findViewById(R.id.btn_gunakan);
        divLayer = (RelativeLayout) findViewById(R.id.layer);
        btnMasukkanPromo = (RelativeLayout) findViewById(R.id.btn_masukkanPromo);
        lyPromo = (RelativeLayout) findViewById(R.id.ly_promo);
        tvPromo = (TextView) findViewById(R.id.tv_promo);
        layer = (RelativeLayout) findViewById(R.id.layer);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
