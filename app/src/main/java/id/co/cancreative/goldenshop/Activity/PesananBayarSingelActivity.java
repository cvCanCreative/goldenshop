package id.co.cancreative.goldenshop.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.Adapter.AdapterRekeningGold;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.ItemClickSupport;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.Model.ModelTransaksi;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.AppDbKrenjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.AppDbKrenjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PesananBayarSingelActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;
    private Rekening rekening;
    private Rekening rekGolden;
    private ModelAlamat alamat;
    private ArrayList<Rekening> _rekening = new ArrayList<>();
    private ArrayList<Rekening> _rekeningUser = new ArrayList<>();

    private AppDbKrenjang db;
    private ArrayList<TbKranjang> daftarBarang;

    List<String> id_penerima = new ArrayList<>();
    List<Integer> id_barang = new ArrayList<>();
    List<Integer> qty = new ArrayList<>();
    List<String> catatan = new ArrayList<>();
    List<String> kurir = new ArrayList<>();
    List<String> jns_kurir = new ArrayList<>();
    List<Integer> ongkir = new ArrayList<>();

    private int totalBayar;
    private int totalTransfer;
    private int uPormo = 0;

    private ModelTransaksi trans;
    private TextView tvTotalTagihan;
    private CardView btnPilihRekening;
    private TextView tvSaldo;
    private ImageView asset;
    private ImageView imgBank;
    private TextView tvNamaBank;
    private TextView tvRekeningUser;
    private ImageView btnMenu;
    private TextView tvTotalBayar;
    private Button btnBayar;
    private CardView btnGuankanSaldo;
    private RecyclerView rv;
    private LinearLayout divNoRekening;
    private LinearLayout divPilihMetode;
    private ImageView imgBankGolden;
    private TextView tvBankGolden;
    private TextView tvNoRekGolden;
    private RelativeLayout divInfoBankUser;
    private LinearLayout divInputInfoBank;
    private TextView tvBankTransfer;
    private ImageView imgBankTransfer;
    private EditText edtNoRekening;
    private EditText edtNamaPemilik;
    private LinearLayout divPilihMetodeSlide;
    private RecyclerView rvBank;
    private ImageView btnClose;
    private RelativeLayout divLayer;
    private TextView btnSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bayar_pesanan);
        initView();

        s = new SharedPref(this);
        trans = s.getTransaksi();
        rekening = s.getRekening();
        rekGolden = s.getRekeningGolden();
        alamat = s.getShipping();

        if (getIntent().getStringExtra("promo") != null) {
            uPormo = Integer.valueOf(getIntent().getStringExtra("promo"));
        }

        divPilihMetodeSlide.animate().translationY(divPilihMetodeSlide.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divPilihMetodeSlide.setVisibility(View.GONE);
                    }
                });

        getData();
        getRekeningGolden();
        mainButton();
        setToolbar();
    }

    private void mainButton() {
        btnPilihRekening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResponModel res = s.getListRek();
                _rekening = res.rekening;
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(PesananBayarSingelActivity.this, LinearLayoutManager.VERTICAL, false);
                RecyclerView.Adapter mAdapter = new AdapterRekeningGold(_rekening, PesananBayarSingelActivity.this);
                rvBank.setLayoutManager(layoutManager);
                rvBank.setAdapter(mAdapter);

                ItemClickSupport.addTo(rvBank).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        showItemSelectedGolden(_rekening.get(position));
                    }
                });

                slideUp();
            }
        });

        btnGuankanSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasti.info(getApplicationContext(), "Fungsi dalam pengembangan");
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayMenu();
            }
        });

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bayar();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideDown();
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edtNoRekening.getText().toString().equals(rekening.RK_NOMOR) || !edtNamaPemilik.getText().toString().equals(rekening.RK_NAME)) {
                    updateRekening(edtNoRekening.getText().toString(), edtNamaPemilik.getText().toString());
                } else {
                    divInfoBankUser.setVisibility(View.VISIBLE);
                    divInputInfoBank.setVisibility(View.GONE);
                }
            }
        });

    }

    private void getData() {

        id_penerima.add(alamat.ID_PENGIRIMAN);
        id_barang.addAll(trans.id_barang);
        qty.addAll(trans.qty);
        catatan.addAll(trans.catatan);
        kurir.addAll(trans.kurir);
        jns_kurir.addAll(trans.jns_kurir);
        ongkir.addAll(trans.ongkir);

        totalTransfer = trans.totalBayar;

        tvTotalBayar.setText("" + new Helper().convertRupiah(totalTransfer - uPormo));
        tvTotalTagihan.setText("" + new Helper().convertRupiah(totalTransfer - uPormo));
    }

    private void setValue() {

        rekening = s.getRekening();
        if (rekening != null) {

            divInputInfoBank.setVisibility(View.GONE);
            divInfoBankUser.setVisibility(View.VISIBLE);

            tvNamaBank.setText(rekening.BK_NAME);
            tvRekeningUser.setText(rekening.RK_NOMOR + " - " + rekening.RK_NAME);
            Picasso.with(this)
                    .load(Config.url_bank + rekening.BK_IMAGE)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.image_error)
                    .noFade()
                    .into(imgBank);

            // set informasi rekening Transfer
            tvBankTransfer.setText(rekening.BK_NAME);
            Picasso.with(this)
                    .load(Config.url_bank + rekening.BK_IMAGE)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.image_error)
                    .noFade()
                    .into(imgBankTransfer);
        } else {
            getRekening();
        }


        rekGolden = s.getRekeningGolden();
        // set informasi rekening Golden
        tvBankGolden.setText(rekGolden.BK_NAME);
        tvNoRekGolden.setText(rekGolden.RK_NOMOR + " - " + rekGolden.RK_NAME);
        Picasso.with(this)
                .load(Config.url_bank + rekGolden.BK_IMAGE)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(imgBankGolden);

        tvTotalBayar.setText("" + new Helper().convertRupiah(totalTransfer - uPormo));
        tvTotalTagihan.setText("" + new Helper().convertRupiah(totalTransfer - uPormo));
    }

    private void bayar() {

        if (rekening == null) {

            String sNoRek = edtNoRekening.getText().toString();
            String sNamaPemilik = edtNamaPemilik.getText().toString();

            if (sNoRek.isEmpty()) {
                edtNoRekening.setError("Kolom Tidak Boleh Kosong");
            } else if (sNamaPemilik.isEmpty()) {
                edtNamaPemilik.setError("Kolom Tidak Boleh Kosong");
            } else {
                SweetAlert.onLoading(this);
                api.addRekening(s.getApi(), s.getIdUser(), s.getSession(), rekGolden.ID_BANK, sNoRek, sNamaPemilik).enqueue(new Callback<ResponModel>() {
                    @Override
                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {

                        if (response.body().success == 1) {
                            s.setRekening(response.body().rekenings);
                            rekening = s.getRekening();
                            transaksi();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponModel> call, Throwable t) {
                        SweetAlert.dismis();
                    }
                });
            }
        } else {
            SweetAlert.onLoading(this);
            transaksi();
        }
    }

    private void transaksi() {

        String kodePromo = "null";
        if (getIntent().getStringExtra("kodePromo") != null) {
            kodePromo = getIntent().getStringExtra("kodePromo");
        }

        api.transaksi(s.getApi(), s.getIdUser(), s.getSession(),  rekening.ID_REKENING, rekGolden.ID_REKENING, trans.kode_unik, kodePromo,
                id_penerima, id_barang, qty, kurir, jns_kurir, ongkir, catatan).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {
                    Intent i = new Intent(PesananBayarSingelActivity.this, InformasiBayarActivity.class);
                    i.putExtra("extra", response.body().exp_trx);
                    i.putExtra("ttlBayar", response.body().total_bayar);
                    startActivity(i);
                    finish();
                } else {
                    Toasti.error(PesananBayarSingelActivity.this, "" + response.body().message);
                }

            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(PesananBayarSingelActivity.this, t.getMessage());
            }
        });
    }

    private void getRekening() {
        SweetAlert.onLoading(this);
        api.getRekening(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();

                if (response.body().success == 1) {
                    s.setRekening(response.body().rekening.get(0));
                    s.setListRekUser(response.body());
                    divInfoBankUser.setVisibility(View.VISIBLE);
                    divInputInfoBank.setVisibility(View.GONE);
                    setValue();
                } else {
                    divInfoBankUser.setVisibility(View.GONE);
                    divInputInfoBank.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(PesananBayarSingelActivity.this, t.getMessage());
            }
        });
    }

    private void updateRekening(String noRek, String namaPemilik) {
        SweetAlert.onLoading(this);
        api.editRekening(s.getApi(), s.getIdUser(), s.getSession(), rekening.ID_REKENING, rekening.ID_BANK, noRek, namaPemilik).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {
                    divInfoBankUser.setVisibility(View.VISIBLE);
                    divInputInfoBank.setVisibility(View.GONE);
                    s.setRekening(response.body().rekenings);
                    tvRekeningUser.setText(edtNoRekening.getText().toString() + " - " + edtNamaPemilik.getText().toString());

                } else {
                    Toasti.error(PesananBayarSingelActivity.this, "" + response.body().message);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(PesananBayarSingelActivity.this, "" + t.getMessage());
            }
        });
    }

    private void getRekeningGolden() {

        if (rekGolden == null) {
            divNoRekening.setVisibility(View.GONE);
            SweetAlert.onLoading(this);
            api.getRekeningGolden(s.getApi()).enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    SweetAlert.dismis();
                    if (response.isSuccessful()) {

                        if (response.body().getSuccess() == 1) {
                            divPilihMetode.setVisibility(View.VISIBLE);
                            _rekening = response.body().rekening;
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(PesananBayarSingelActivity.this, LinearLayoutManager.VERTICAL, false);
                            RecyclerView.Adapter mAdapter = new AdapterRekeningGold(_rekening, PesananBayarSingelActivity.this);
                            rv.setLayoutManager(layoutManager);
                            rv.setAdapter(mAdapter);

                            s.setListRek(response.body());
                        }

                        ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                            @Override
                            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                showItemSelectedGolden(_rekening.get(position));
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    SweetAlert.dismis();
                    SweetAlert.onFailure(PesananBayarSingelActivity.this, t.getMessage());
                    onBackPressed();
                }
            });
        } else {
            divPilihMetode.setVisibility(View.GONE);
            setValue();
        }
    }

    private void showItemSelectedGolden(Rekening i) {
        s.setRekeningGolden(i);
        divPilihMetode.setVisibility(View.GONE);
        divNoRekening.setVisibility(View.VISIBLE);

        // set informasi rekening Transfer
        tvBankTransfer.setText(i.BK_NAME);
        Picasso.with(this)
                .load(Config.url_bank + i.BK_IMAGE)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(imgBankTransfer);

        setValue();
        slideDown();
    }

    private void showItemSelectedUser(Rekening i) {
        s.setRekening(i);
        divInfoBankUser.setVisibility(View.VISIBLE);
        divInputInfoBank.setVisibility(View.GONE);
        setValue();
        slideDown();
    }

    private void displayMenu() {
        final PopupMenu popupMenu = new PopupMenu(PesananBayarSingelActivity.this, btnMenu);
        popupMenu.getMenuInflater().inflate(R.menu.menu_rekening, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_edit:
                        divInfoBankUser.setVisibility(View.GONE);
                        divInputInfoBank.setVisibility(View.VISIBLE);
                        btnSimpan.setVisibility(View.VISIBLE);
                        edtNoRekening.setText(rekening.RK_NOMOR);
                        edtNamaPemilik.setText(rekening.RK_NAME);
                        return true;
                    case R.id.item_ganti:
                        ResponModel res = s.getListRekUser();
                        _rekeningUser = res.rekening;
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(PesananBayarSingelActivity.this, LinearLayoutManager.VERTICAL, false);
                        RecyclerView.Adapter mAdapter = new AdapterRekeningGold(_rekeningUser, PesananBayarSingelActivity.this);
                        rvBank.setLayoutManager(layoutManager);
                        rvBank.setAdapter(mAdapter);
                        slideUp();

                        ItemClickSupport.addTo(rvBank).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                            @Override
                            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                showItemSelectedUser(_rekeningUser.get(position));
                            }
                        });

                        return true;
                }
                return false;
            }
        });

        popupMenu.show();
    }

    private void slideDown() {
        divPilihMetodeSlide.animate().translationY(divPilihMetodeSlide.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divPilihMetodeSlide.setVisibility(View.GONE);
                        divLayer.setVisibility(View.GONE);
                    }
                });
    }

    private void slideUp() {
        divLayer.setVisibility(View.VISIBLE);
        divPilihMetodeSlide.setVisibility(View.VISIBLE);
        divPilihMetodeSlide.animate().translationY(0)
                .alpha(1.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation, boolean isReverse) {
                        divPilihMetodeSlide.setVisibility(View.VISIBLE);
                        divLayer.setVisibility(View.VISIBLE);
                    }
                });
    }

    @SuppressLint("StaticFieldLeak")
    private void deleteDataKranjang() {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses Delete data
                AppDbKrenjang db;
                db = Room.databaseBuilder(getApplicationContext(),
                        AppDbKrenjang.class, Config.db_keranjang)
                        .build();

                long status = db.daoKranjang().deleteAll();
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
//                Toast.makeText(getApplicationContext(), "barangByH add " + status, Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void deleteDataKranjangToko() {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses Delete data
                AppDbKrenjangByToko db;
                db = Room.databaseBuilder(getApplicationContext(),
                        AppDbKrenjangByToko.class, Config.db_keranjangToko)
                        .build();

                long status = db.daoKranjang().deleteAll();
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
//                Toast.makeText(getApplicationContext(), "barangByH add " + status, Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bayar");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTotalTagihan = (TextView) findViewById(R.id.tv_totalTagihan);
        btnPilihRekening = (CardView) findViewById(R.id.btn_pilihRekening);
        tvSaldo = (TextView) findViewById(R.id.tv_saldo);
        asset = (ImageView) findViewById(R.id.asset);
        imgBank = (ImageView) findViewById(R.id.img_bank);
        tvNamaBank = (TextView) findViewById(R.id.tv_namaBank);
        tvRekeningUser = (TextView) findViewById(R.id.tv_rekeningUser);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        tvTotalBayar = (TextView) findViewById(R.id.tv_totalBayar);
        btnBayar = (Button) findViewById(R.id.btn_bayar);
        btnGuankanSaldo = (CardView) findViewById(R.id.btn_guankanSaldo);
        rv = (RecyclerView) findViewById(R.id.rv);
        divNoRekening = (LinearLayout) findViewById(R.id.div_noRekening);
        divPilihMetode = (LinearLayout) findViewById(R.id.div_pilihMetode);
        imgBankGolden = (ImageView) findViewById(R.id.img_bankGolden);
        tvBankGolden = (TextView) findViewById(R.id.tv_bankGolden);
        tvNoRekGolden = (TextView) findViewById(R.id.tv_noRekGolden);
        divInfoBankUser = (RelativeLayout) findViewById(R.id.div_infoBankUser);
        divInputInfoBank = (LinearLayout) findViewById(R.id.div_inputInfoBank);
        tvBankTransfer = (TextView) findViewById(R.id.tv_bankTransfer);
        imgBankTransfer = (ImageView) findViewById(R.id.img_bankTransfer);
        edtNoRekening = (EditText) findViewById(R.id.edt_noRekening);
        edtNamaPemilik = (EditText) findViewById(R.id.edt_namaPemilik);
        divPilihMetodeSlide = (LinearLayout) findViewById(R.id.div_pilihMetodeSlide);
        rvBank = (RecyclerView) findViewById(R.id.rv_bank);
        btnClose = (ImageView) findViewById(R.id.btn_close);
        divLayer = (RelativeLayout) findViewById(R.id.div_layer);
        btnSimpan = (TextView) findViewById(R.id.btn_simpan);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
