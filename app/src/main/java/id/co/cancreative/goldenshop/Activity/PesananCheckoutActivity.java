package id.co.cancreative.goldenshop.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Adapter.AdapterChekoutByToko;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.Model.ModelTransaksi;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.AppDbKrenjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.TbKranjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.AppDbKrenjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PesananCheckoutActivity extends AppCompatActivity {

    private View parent_view;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;

    private ModelAlamat alamat;

    //SQLite
    private AppDbKrenjang db;
    private AppDbKrenjangByToko dbtoko;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<TbKranjang> daftarBarang;
    private ArrayList<TbKranjangByToko> listBarang;

    private int sum, hargaAwal, hargaSekarang, ongkir = 0, promo = 0, uPromo = 0;
    private boolean statusOngkir = false;
    private String kodePromo, potongan, minBelanja;

    private CardView btnInputPromo;
    private ImageView img1;
    private TextView tvNama;
    private TextView tvLabel;
    private TextView tvAlamat;
    private LinearLayout btnUbah;
    private RecyclerView rvProduk;
    private TextView tvTotalBarang;
    private TextView tvTotalHarga;
    private TextView tvOngkir;
    private RelativeLayout lyPromo;
    private TextView tvPromo;
    private LinearLayout layout;
    private TextView tvTotalBayar;
    private Button btnBayar;
    private LinearLayout divAlamat;
    private RelativeLayout btnPilihAlamat;
    private LinearLayout btnAlamat;
    private ImageView image;
    private RelativeLayout divLayer;
    private LinearLayout divPilihMetodeSlide;
    private ImageView btnClose;
    private EditText edtPromo;
    private TextView tvPesan;
    private Button btnGunakan;
    private LinearLayout btnPilihAlamat2;
    private RelativeLayout layer;
    private TextView tvTotalBelanja;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_pesanan);
        initView();

        s = new SharedPref(this);
        alamat = s.getShipping();

        callFungtion();
        setValue();
        mainButton();
//        displayKranjangByToko();
        displayKranjang();
        setToolbar();
    }

    private void callFungtion() {
        CallFunction.setrefrashChekout(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                hitungUlang();
                return null;
            }
        });
        CallFunction.setvalueChekout(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                alamat = s.getShipping();
                displayKranjang();
                setValue();
                return null;
            }
        });
    }

    private void setValue() {

        divPilihMetodeSlide.animate().translationY(divPilihMetodeSlide.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divPilihMetodeSlide.setVisibility(View.GONE);
                    }
                });

        edtPromo.setHint("Masukkan Kode Promo");

        if (alamat != null) {
            divAlamat.setVisibility(View.VISIBLE);
            btnPilihAlamat.setVisibility(View.GONE);

            tvNama.setText(alamat.PE_NAMA);
            tvLabel.setText("(" + alamat.PE_JUDUL + ")");
            tvAlamat.setText(alamat.PE_ALAMAT + ", " + alamat.PE_KECAMATAN + ", " + alamat.PE_KOTA + ", " + alamat.PE_TELP);
        }
    }

    private void mainButton() {

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideDown();
            }
        });

        btnGunakan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                daftarBarang = new ArrayList<>();
                daftarBarang.addAll(Arrays.asList(db.daoKranjang().selectBarangsTerpilih(true)));
                final String nilai = edtPromo.getText().toString();

                SweetAlert.onLoading(PesananCheckoutActivity.this);
                ArrayList<String> idProduk = new ArrayList<>();

                for (final TbKranjang d : daftarBarang) {
                    idProduk.add(String.valueOf(d.idProduk));
                }

                api.cekPromo(s.getApi(), s.getIdUser(), s.getSession(), idProduk, String.valueOf(sum), edtPromo.getText().toString()).enqueue(new Callback<ResponModel>() {
                    @Override
                    public void onResponse(Call<ResponModel> call, final Response<ResponModel> response) {
                        SweetAlert.dismis();
                        if (response.body().success == 1) {
                            promo = Integer.parseInt(response.body().detail_promo.PR_POTONGAN);

                            final SweetAlertDialog pDialog = new SweetAlertDialog(PesananCheckoutActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                            pDialog.setTitleText("Kode Promo Sesuai")
                                    .setContentText("Selamat anda mendapatkan potongan sebesar " + new Helper().convertRupiah(promo))
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            uPromo = promo;
                                            kodePromo = nilai;
                                            tvPesan.setVisibility(View.GONE);
                                            edtPromo.setText(null);
                                            tvPromo.setText("" + "-" + new Helper().convertRupiah(promo));
                                            lyPromo.setVisibility(View.VISIBLE);
                                            tvTotalBayar.setText(new Helper().convertRupiah((sum + ongkir) - promo));
                                            tvTotalBelanja.setText("" + new Helper().convertRupiah((sum + ongkir) - promo));
                                            slideDown();

                                            sweetAlertDialog.dismiss();
                                        }
                                    });
                            pDialog.show();
                        } else {
                            tvPesan.setVisibility(View.VISIBLE);
                            tvPesan.setText(response.body().message);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponModel> call, Throwable t) {
                        SweetAlert.dismis();
                        SweetAlert.onFailure(PesananCheckoutActivity.this, t.getMessage());
                    }
                });

//                for (final TbKranjang d : daftarBarang) {
//                    if (d.kodePromo != null) {
//                        if (d.kodePromo.equals(nilai) || d.kodePromo == nilai) {
//                            if (d.promoStatus.equals("ACTIVE")) {
//                                if (!(sum < Integer.valueOf(d.promoMin))) {
//
//                                    promo = Integer.parseInt(d.promoPotongan);
//
//                                    final SweetAlertDialog pDialog = new SweetAlertDialog(PesananCheckoutActivity.this, SweetAlertDialog.SUCCESS_TYPE);
//                                    pDialog.setTitleText("Kode Promo Sesuai")
//                                            .setContentText("Selamat anda mendapatkan potongan sebesar "+ new Helper().convertRupiah(promo))
//                                            .setConfirmText("Ok")
//                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                                @Override
//                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
//
//                                                    uPromo = promo;
//                                                    kodePromo = nilai;
//                                                    tvPesan.setVisibility(View.GONE);
//                                                    edtPromo.setText(null);
//                                                    tvPromo.setText("" + "-" + new Helper().convertRupiah(Integer.valueOf(d.promoPotongan)));
//                                                    lyPromo.setVisibility(View.VISIBLE);
//                                                    tvTotalBayar.setText(new Helper().convertRupiah((sum + ongkir) - promo));
//                                                    tvTotalBelanja.setText("" + new Helper().convertRupiah((sum + ongkir) - promo));
//                                                    slideDown();
//
//                                                    sweetAlertDialog.dismiss();
//                                                }
//                                            });
//                                    pDialog.show();
//
//
//                                } else {
//                                    tvPesan.setVisibility(View.VISIBLE);
//                                    tvPesan.setText("Total belanja tidak mencukupi, belanja sebesar " + new Helper().convertRupiah(Integer.valueOf(d.promoMin)) + " untuk mendapatkan promo terkait");
//                                }
//                                return;
//                            } else {
//                                Log.d("Gagal Tidak Aktif", d.kodePromo + " " + d.promoStatus);
//                                tvPesan.setVisibility(View.VISIBLE);
//                                tvPesan.setText("Kode Promo sudah tidak berlaku");
//                                return;
//                            }
//                        } else {
//                            tvPesan.setVisibility(View.VISIBLE);
//                            tvPesan.setText("Kode Promo yang kamu masukkan salah");
//                        }
//                    }
//                }
            }
        });

        divLayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideDown();
            }
        });

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alamat == null) {
                    Snackbar.make(parent_view, "Pastikan Anda telah memilih alamat tujuan pengiriman", Snackbar.LENGTH_SHORT).show();
                    return;
                } else {
                    if (alamat.PE_ID_KECAMATAN == null) {
                        Snackbar.make(parent_view, "Pastikan Anda telah memilih alamat tujuan pengiriman", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                }
                bayar();
            }
        });

        btnInputPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp();
            }
        });

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DaftarItemActivity.class);
                intent.putExtra("extra", "pilihAlamat");
                startActivity(intent);
            }
        });

        btnPilihAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DaftarItemActivity.class);
                intent.putExtra("extra", "pilihAlamat");
                startActivity(intent);
            }
        });

        btnPilihAlamat2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DaftarItemActivity.class);
                intent.putExtra("extra", "pilihAlamat");
                startActivity(intent);
            }
        });
    }

    private void displayKranjang() {

        db = Room.databaseBuilder(this,
                AppDbKrenjang.class, Config.db_keranjang).allowMainThreadQueries().build();
        dbtoko = Room.databaseBuilder(this,
                AppDbKrenjangByToko.class, Config.db_keranjangToko).allowMainThreadQueries().build();

        daftarBarang = new ArrayList<>();
        listBarang = new ArrayList<>();
        daftarBarang.addAll(Arrays.asList(db.daoKranjang().selectBarangsTerpilih(true)));

        int apa = 0;
        for (TbKranjang d : daftarBarang) {

            if (apa != d.idToko) {
                apa = d.idToko;
                listBarang.addAll(Arrays.asList(dbtoko.daoKranjang().selectProdukIdList(apa)));
            }
        }

        rvProduk.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvProduk.setLayoutManager(layoutManager);
        adapter = new AdapterChekoutByToko(listBarang, this);
        rvProduk.setAdapter(adapter);

//        rvProduk.setHasFixedSize(true);
//        layoutManager = new LinearLayoutManager(this);
//        rvProduk.setLayoutManager(layoutManager);
//        adapter = new AdapterChekout(daftarBarang, this);
//        rvProduk.setAdapter(adapter);

        int nilai = daftarBarang.size();
        tvTotalBarang.setText("Total Harga (" + nilai + " Barang)");

        int a = 0;
        sum = 0;
        for (TbKranjang b : daftarBarang) {

            hargaSekarang = b.jumlahPesan * b.harga;
            sum = sum + hargaSekarang;

            if (a != b.idToko) {
                a = b.idToko;
                ongkir = ongkir + b.ongkir;
            }
//            Log.d("total", "total: "+sum+" jmlPesan: "+b.jumlahPesan+" hatga: "+b.harga+" hargaSekarang: "+hargaSekarang+" ");
//            hargaAwal = hargaAwal + b.harga;
        }
        tvOngkir.setText("" + new Helper().convertRupiah(ongkir));
        tvTotalHarga.setText("" + new Helper().convertRupiah(sum));
        tvTotalBayar.setText("" + new Helper().convertRupiah((sum + ongkir) - promo));
        tvTotalBelanja.setText("" + new Helper().convertRupiah((sum + ongkir) - promo));
    }

    private void hitungUlang() {
        daftarBarang = new ArrayList<>();
        daftarBarang.addAll(Arrays.asList(db.daoKranjang().selectBarangsTerpilih(true)));
        sum = 0;
        ongkir = 0;
        statusOngkir = true;
        int a = 0;
        for (TbKranjang b : daftarBarang) {

            hargaSekarang = b.jumlahPesan * b.harga;
            sum = sum + hargaSekarang;

            if (a != b.idToko) {
                a = b.idToko;
                ongkir = ongkir + b.ongkir;
            }

            if (b.ongkir == 0) {
                statusOngkir = false;
            }

//            Log.d("total", "total: "+sum+" jmlPesan: "+b.jumlahPesan+" hatga: "+b.harga+" Ongkir: "+ongkir+" ");
//            hargaAwal = hargaAwal + b.harga;
        }
        tvOngkir.setText("" + new Helper().convertRupiah(ongkir));
        tvTotalHarga.setText("" + new Helper().convertRupiah(sum));
        tvTotalBayar.setText("" + new Helper().convertRupiah((sum + ongkir) - promo));
        tvTotalBelanja.setText("" + new Helper().convertRupiah((sum + ongkir) - promo));
    }

    private void bayar() {
        statusOngkir = true;

        final int min = 100;
        final int max = 999;
        final int random = new Random().nextInt((max - min) + 1) + min;

        ModelTransaksi trans = new ModelTransaksi();

        trans.kode_unik = random;
        trans.kode_promo = "null";
        trans.totalBayar = sum + ongkir;

        // Set Array transaksi
        daftarBarang = new ArrayList<>();
        daftarBarang.addAll(Arrays.asList(db.daoKranjang().selectBarangsTerpilih(true)));
        sum = 0;
        ongkir = 0;
        for (TbKranjang b : daftarBarang) {

            if (b.ongkir == 0) {
                statusOngkir = false;
            }
            trans.id_penerima.add(alamat.ID_PENGIRIMAN);
            trans.id_barang.add(Integer.valueOf(b.idProduk));
            trans.qty.add(b.jumlahPesan);
            trans.kurir.add(b.kurir);
            trans.jns_kurir.add(b.jenisKurir);
            trans.ongkir.add(b.ongkir);
            trans.catatan.add(b.catatan);
        }

        s.setTransaksi(trans);

        if (statusOngkir) {
            Intent intent = new Intent(getApplicationContext(), PesananBayarActivity.class);
            intent.putExtra("promo", "" + uPromo);
            intent.putExtra("kodePromo", "" + kodePromo);
            startActivity(intent);
        } else {
            Snackbar.make(parent_view, "Pastikan Anda telah memilih kurir pengiriman untuk semua alamat dan melengkapi informasi tambahan.", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void hapusOngkir() {
        daftarBarang.addAll(Arrays.asList(db.daoKranjang().selectAllBarangs()));
        edtPromo.setHint("Masukkan Kode Promo");

        for (TbKranjang d : daftarBarang) {
            d.ongkir = 0;
            updateBarang(d);
        }

    }

    @SuppressLint("StaticFieldLeak")
    private void updateBarang(final TbKranjang barang) {
        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
//                AppDbKrenjang db;
//                db = Room.databaseBuilder(getActivity(),
//                        AppDbKrenjang.class, Config.db_keranjang)
//                        .build();
                long status = db.daoKranjang().updateBarang(barang);
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {

            }
        }.execute();
    }

    private void slideDown() {
        divPilihMetodeSlide.animate().translationY(divPilihMetodeSlide.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divPilihMetodeSlide.setVisibility(View.GONE);
                        divLayer.setVisibility(View.GONE);
                        tvPesan.setVisibility(View.GONE);
                        edtPromo.setText(null);
                    }
                });
    }

    private void slideUp() {
        divLayer.setVisibility(View.VISIBLE);
        divPilihMetodeSlide.setVisibility(View.VISIBLE);
        divPilihMetodeSlide.animate().translationY(0)
                .alpha(1.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation, boolean isReverse) {
                        divPilihMetodeSlide.setVisibility(View.VISIBLE);
                        divLayer.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Chekout");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        parent_view = findViewById(android.R.id.content);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnInputPromo = (CardView) findViewById(R.id.btn_inputPromo);
        img1 = (ImageView) findViewById(R.id.img1);
        tvNama = (TextView) findViewById(R.id.tv_nama);
        tvLabel = (TextView) findViewById(R.id.tv_label);
        tvAlamat = (TextView) findViewById(R.id.tv_alamat);
        btnUbah = (LinearLayout) findViewById(R.id.btn_ubah);
        rvProduk = (RecyclerView) findViewById(R.id.rv_produk);
        tvTotalBarang = (TextView) findViewById(R.id.tv_totalBarang);
        tvTotalHarga = (TextView) findViewById(R.id.tv_totalHarga);
        tvOngkir = (TextView) findViewById(R.id.tv_ongkir);
        lyPromo = (RelativeLayout) findViewById(R.id.ly_promo);
        tvPromo = (TextView) findViewById(R.id.tv_promo);
        layout = (LinearLayout) findViewById(R.id.layout);
        tvTotalBayar = (TextView) findViewById(R.id.tv_totalBayar);
        btnBayar = (Button) findViewById(R.id.btn_bayar);
        divAlamat = (LinearLayout) findViewById(R.id.div_alamat);
        btnPilihAlamat = (RelativeLayout) findViewById(R.id.btn_pilihAlamat);
        image = (ImageView) findViewById(R.id.image);
        divLayer = (RelativeLayout) findViewById(R.id.layer);
        divPilihMetodeSlide = (LinearLayout) findViewById(R.id.div_pilihMetodeSlide);
        btnClose = (ImageView) findViewById(R.id.btn_close);
        edtPromo = (EditText) findViewById(R.id.edt_promo);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        btnGunakan = (Button) findViewById(R.id.btn_gunakan);
        btnPilihAlamat2 = (LinearLayout) findViewById(R.id.btn_pilihAlamat2);
        layer = (RelativeLayout) findViewById(R.id.layer);
        tvTotalBelanja = (TextView) findViewById(R.id.tv_totalBelanja);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        hapusOngkir();
        super.onBackPressed();
    }
}
