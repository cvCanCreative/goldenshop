package id.co.cancreative.goldenshop.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ProgressBar;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.AdapterCategory;
import id.co.cancreative.goldenshop.Adapter.AdapterPilihKategory;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukToko;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelCategory;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PilihKategoriActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private String extra;
    private ArrayList<ModelCategory> mKategori = new ArrayList<>();

    private Toolbar toolbar;
    private RecyclerView rv;
    private ProgressBar pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_kategori);
        initView();

        s = new SharedPref(this);

        getIntentExtra();
        getKategori();
        setToolbar();
    }

    private void getIntentExtra() {
        extra = getIntent().getStringExtra("extra");
    }

    private void getKategori() {

        api.getKategori(s.getApi()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mKategori = (ArrayList<ModelCategory>) response.body().getKategori();
                    RecyclerView.Adapter mAdapter = new AdapterPilihKategory(mKategori, PilihKategoriActivity.this);
                    rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.onFailure(PilihKategoriActivity.this, t.getMessage());
            }
        });
    }


    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pilih Kategori");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        rv = (RecyclerView) findViewById(R.id.rv);
        pd = (ProgressBar) findViewById(R.id.pd);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
