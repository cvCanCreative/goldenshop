package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Adapter.AdapterPilihProduk;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukToko;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PilihProdukActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toolbar toolbar;
    private Toko toko;

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;

    private ArrayList<Produk> mProduk = new ArrayList<>();

    private TextView tvToolbarTitle;
    private RecyclerView rv;
    private ProgressBar pd;
    private ImageView btnAdd1;
    private ImageView btnAdd2;
    private LinearLayout divProdukKosong;
    private TextView tvPesan;
    private Button btnTambahProduk;
    private SwipeRefreshLayout swipeRefrash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_produk);
        initView();

        s = new SharedPref(this);
        toko = s.getToko();

        pullrefrash();
        callFungtion();
        mainButton();
        getProduk();
        setToolbar();
    }

    private void callFungtion() {
        CallFunction.setRefreshListProdukTK(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                getProduk();
                return null;
            }
        });
    }

    private void mainButton() {
        btnAdd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TambahProdukActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnTambahProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TambahProdukActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void getProduk() {
        api.getProdukByT(s.getApi(), toko.ID_TOKO).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                swipeRefrash.setRefreshing(false);
                pd.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    mProduk = response.body().products;
                    layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    mAdapter = new AdapterPilihProduk(mProduk, PilihProdukActivity.this);
                    rv.setLayoutManager(layoutManager);
                    rv.setAdapter(mAdapter);

                    if (response.body().success == 0 || mProduk.size() == 0) {
                        divProdukKosong.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                SweetAlert.onFailure(PilihProdukActivity.this, t.getMessage());
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        tvToolbarTitle.setText("Produk Dijual");
        getSupportActionBar().setTitle("Produk Dijual");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProduk();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        rv = (RecyclerView) findViewById(R.id.rv);
        pd = (ProgressBar) findViewById(R.id.pd);
        btnAdd1 = (ImageView) findViewById(R.id.btn_add1);
        btnAdd2 = (ImageView) findViewById(R.id.btn_add2);
        divProdukKosong = (LinearLayout) findViewById(R.id.div_produkKosong);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        btnTambahProduk = (Button) findViewById(R.id.btn_tambahProduk);
        swipeRefrash = (SwipeRefreshLayout) findViewById(R.id.swipeRefrash);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
