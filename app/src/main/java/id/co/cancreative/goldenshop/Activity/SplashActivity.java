package id.co.cancreative.goldenshop.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelProvinsi;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.Model.TokoStatus;
import id.co.cancreative.goldenshop.NotificationPackage.NotificationUtils;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.utils.CallbackDialog;
import id.co.cancreative.goldenshop.utils.DialogUtils;
import id.co.cancreative.goldenshop.utils.NetworkCheck;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private SharedPref s;
    ApiService api = ApiConfig.getInstanceRetrofit();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        s = new SharedPref(this);

        getInfo();
//        startActivityMainDelay();
    }

    private void startActivityMainDelay() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                if (getIntent().getExtras() != null) {
                    if (getIntent().getStringExtra("data") != null) {

                        //Mengambil data payload intent dari notifikasi
                        try {
                            JSONObject jsonInti = new JSONObject(getIntent().getStringExtra("data"));
                            JSONObject jsonPayload = jsonInti.getJSONObject("payload");
                            String type_data = jsonPayload.getString("type");

                            //Mngarahkan Intent sesui type notifikasi
                            //Mulai  Bagian Pembeli
                            switch (type_data) {
                                case "pembayaran":
                                    intent = new Intent(getApplicationContext(), DaftarTransaksiMenunguPembayaran.class);
                                    break;
                                case "proses_admin":
                                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                                    intent.putExtra("FRAGMENT_ID", "0");
                                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                                        intent.putExtra("status_notif", true);
                                    } else {
                                        intent.putExtra("status_notif", false);
                                    }

                                    break;
                                case "proses_vendor_user":
                                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                                    intent.putExtra("FRAGMENT_ID", "1");
                                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                                        intent.putExtra("status_notif", true);
                                    } else {
                                        intent.putExtra("status_notif", false);
                                    }

                                    break;
                                case "proses_kirim":
                                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                                    intent.putExtra("FRAGMENT_ID", "2");
                                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                                        intent.putExtra("status_notif", true);
                                    } else {
                                        intent.putExtra("status_notif", false);
                                    }

                                    break;
                                case "terima_user":
                                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                                    intent.putExtra("FRAGMENT_ID", "3");
                                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                                        intent.putExtra("status_notif", true);
                                    } else {
                                        intent.putExtra("status_notif", false);
                                    }

                                    break;
                                case "batal":
                                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                                    intent.putExtra("FRAGMENT_ID", "4");
                                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                                        intent.putExtra("status_notif", true);
                                    } else {
                                        intent.putExtra("status_notif", false);
                                    }

                                    break;
                                //Selesai  Bagian Pembeli
                                //Mulai Bagian Penjual
                                case "proses_vendor":
                                    intent = new Intent(getApplicationContext(), DaftarTransaksiTokoActivity.class);
                                    intent.putExtra("FRAGMENT_ID", "0");
                                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                                        intent.putExtra("status_notif", true);
                                    } else {
                                        intent.putExtra("status_notif", false);
                                    }

                                    break;
                                //Selesai  Bagian Penjual
                                default:
                                    intent = new Intent(getApplicationContext(), MainActivity.class);
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            intent = new Intent(getApplicationContext(), MainActivity.class);
                        }
                    }
                }
                startActivity(intent);
                finish();
            }
        }, 2000);
    }

    private void getInfo() {
        api.getListProv().enqueue(new Callback<ResponModelProvinsi>() {
            @Override
            public void onResponse(Call<ResponModelProvinsi> call, Response<ResponModelProvinsi> response) {
                ResponModelProvinsi prov = response.body();
                s.setProvinsi(prov.rajaongkir);
                getData();
//                Log.d("Size" , ": "+s.getProvinsi().results.size());
            }

            @Override
            public void onFailure(Call<ResponModelProvinsi> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                dialogServerNotConnect();
            }
        });
    }

    private void getData() {

        SweetAlert.onLoading(this);
        api.getAccount(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    ResponModel respon = response.body();
                    if (respon.getSuccess() == 1) {

                        Saldo saldo = respon.result.saldo;
                        Toko toko = respon.result.toko;
                        TokoStatus statusToko = respon.result.cek_toko;

                        // Set Saldo
                        s.setSaldo(saldo);
                        s.setToko(toko);
                        s.setString(SharedPref.ID_TOKO, toko.ID_TOKO);
                        s.setMinWd(respon.result.min_wd);
                        Saldo min = s.getMinWd();
                        Log.d("Test", "" + min.minimal_wd);

                        // Set Status Toko
                        if (statusToko.success == 1) {
                            s.setBoolean(SharedPref.TOK_STATUS, true);
                            String status = statusToko.status;
                            if (status.equals("TK_BLM_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, "TK_BLM_AKTIF");
                            } else if (status.equals("TK_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, status);
                            }
                        } else if (statusToko.success == 0) {
                            s.setBoolean(SharedPref.TOK_STATUS, false);
                        }

                        startActivityMainDelay();

                    } else if (respon.getSuccess() == 0) {
                        if (respon.message.contains("Silahkan login kembali")) {
                            s.clearAll();
//                            Intent intent = new Intent(SplashActivity.this, SplashActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(intent);
//                            finish();
                        }
                        startActivityMainDelay();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
            }
        });
    }

    public void dialogServerNotConnect() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_unable_connect, R.string.msg_unable_connect, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_connect, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                retryOpenApplication();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }

    // make a delay to start next activity
    private void retryOpenApplication() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startProcess();
            }
        }, 2000);
    }

    private void startProcess() {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            getInfo();
        }
    }

    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                retryOpenApplication();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }


}
