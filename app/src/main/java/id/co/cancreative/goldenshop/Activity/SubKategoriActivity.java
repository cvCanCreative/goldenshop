package id.co.cancreative.goldenshop.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.AdapterPilihSubCategory;
import id.co.cancreative.goldenshop.Adapter.AdapterSubCategory;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.ModelCategory;
import id.co.cancreative.goldenshop.Model.ModelSubKat;
import id.co.cancreative.goldenshop.R;

public class SubKategoriActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;

    private ArrayList<ModelCategory> mKategori = new ArrayList<>();
    private ArrayList<ModelSubKat> mSubKategori = new ArrayList<>();
    private ModelCategory data;

    private Toolbar toolbar;
    private RecyclerView rv;
    private ProgressBar pd;
    private String extra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_kategori);
        initView();

        s = new SharedPref(this);
        data = new ModelCategory();

        getIntentExtra();
        getKategori();
        setToolbar();
    }

    private void getIntentExtra() {
        extra = getIntent().getStringExtra("extra");
        data = getIntent().getParcelableExtra("data");
    }

    private void getKategori() {
        pd.setVisibility(View.GONE);
        mSubKategori = (ArrayList<ModelSubKat>) data.getSkat();
//        mKategori.addAll(data.getSkat().indexOf(i));

        if (extra != null) {
            if (extra.equals("addProduk")) {
                mAdapter = new AdapterPilihSubCategory(mSubKategori, SubKategoriActivity.this);
                Log.d("do this", "1");
            } else {
                mAdapter = new AdapterSubCategory(mSubKategori, SubKategoriActivity.this);
                Log.d("do this", "2");
            }
        } else {
            mAdapter = new AdapterSubCategory(mSubKategori, SubKategoriActivity.this);
            Log.d("do this", "3");
        }
        rv.setLayoutManager(new GridLayoutManager(SubKategoriActivity.this, 3));
        rv.setAdapter(mAdapter);
    }


    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pilih Kategori");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        rv = (RecyclerView) findViewById(R.id.rv);
        pd = (ProgressBar) findViewById(R.id.pd);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
