package id.co.cancreative.goldenshop.Activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.Promo;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.AppDbKrenjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.TbKranjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.AppDbKrenjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;

public class TambahKeranjangActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Produk produk;
    private Promo mPromo;
    private AppDbKrenjang db;
    private AppDbKrenjangByToko dbtoko;

    private int jumlahPesan = 1, stok = 1;
    private String totalHarga;

    private Toolbar toolbar;
    private ImageView imgProduk;
    private TextView tvNamaproduk;
    private TextView tvHargaProduk;
    private TextView tvKetStok;
    private ImageView btnKurangi;
    private TextView tvJumlah;
    private ImageView btnTambah;
    private EditText edtCatatan;
    private TextView tvTotalHarga;
    private CardView btnTambahKeKeranjang;
    private TextView tvStok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_keranjang);
        initView();

        s = new SharedPref(this);
        produk = s.getProduk();

        setValue();
        mainButton();
        setToolbar();
    }

    private void setValue() {
        stok = Integer.valueOf(produk.BA_STOCK);
        String pesan = "Tersedia: ";

        if (stok >= 10){
            pesan = "Sisa: ";
        }

        totalHarga = new Helper().convertRupiah(Integer.valueOf(produk.BA_PRICE));
        tvTotalHarga.setText(totalHarga);
        tvNamaproduk.setText(produk.BA_NAME);
        tvHargaProduk.setText(totalHarga);
        tvStok.setText(pesan + stok + " Piece");

        String image = new Helper().splitText(produk.BA_IMAGE);
        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.img_kosong)
                .error(R.drawable.img_kosong)
                .noFade()
                .into(imgProduk);
    }

    private void tambahKeranjang() {

        TbKranjang b = new TbKranjang();
        String catatan = edtCatatan.getText().toString();

        db = Room.databaseBuilder(getApplicationContext(),
                AppDbKrenjang.class, Config.db_keranjang).allowMainThreadQueries().build();
        TbKranjang barang = db.daoKranjang().selectProdukId(Integer.valueOf(produk.ID_BARANG));

        if (barang != null) {
            int z = barang.jumlahPesan;
            db.daoKranjang().updateJumlah(Integer.valueOf(produk.ID_BARANG), z + jumlahPesan);
            Log.d("Jumlah Barang", String.valueOf(z));
            alart();
        } else {

            // Informasi Promo
            String promo = null, potongan = null, min = null, status = null;
            if (produk.PR_CODE != null){
                promo = produk.PR_CODE;
            }

            if (produk.PR_POTONGAN != null){
                potongan = produk.PR_POTONGAN;
            }

            if (produk.PR_POTONGAN != null){
                min = produk.PR_MIN;
            }

            if (produk.PR_POTONGAN != null){
                status = produk.PR_STATUS;
            }


            b.kodePromo = promo;
            b.promoPotongan = potongan;
            b.promoMin = min;
            b.promoStatus = status;
            b.idToko = Integer.valueOf(produk.ID_TOKO);
            b.berat = Integer.valueOf(produk.BA_WEIGHT);
            b.penjual = produk.TOK_NAME;
            b.idProduk = Integer.valueOf(produk.ID_BARANG);
            b.nama = produk.BA_NAME;
            b.harga = Integer.valueOf(produk.BA_PRICE);
            b.jumlahPesan = jumlahPesan;
            b.catatan = catatan;
            b.gambar = produk.BA_IMAGE;
            b.berat = Integer.valueOf(produk.BA_WEIGHT);
            insertData(b);
        }

        TbKranjangByToko t = new TbKranjangByToko();
        dbtoko = Room.databaseBuilder(getApplicationContext(),
                AppDbKrenjangByToko.class, Config.db_keranjangToko).allowMainThreadQueries().build();
        TbKranjangByToko produkToko = dbtoko.daoKranjang().selectProdukId(Integer.valueOf(produk.ID_TOKO));

        if (produkToko != null) {
            int z = produkToko.jumlahProduk;
            dbtoko.daoKranjang().updateJumlah(Integer.valueOf(produk.ID_TOKO), z++);
            Log.d("Jumlah Barang", String.valueOf(z));
        } else {
            t.idToko = Integer.valueOf(produk.ID_TOKO);
            t.namaToko = produk.TOK_NAME;
            insertDataToko(t);
        }

    }

    private void mainButton() {

        btnTambahKeKeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stok != 0){
                    tambahKeranjang();
                }
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!(jumlahPesan >= stok)) {
                    if (jumlahPesan == 1) {
                        Picasso.with(getApplicationContext())
                                .load(R.drawable.ic_kurang)
                                .into(btnKurangi);
                    }

                    jumlahPesan++;
                    tvJumlah.setText("" + jumlahPesan);

                    int total = Integer.valueOf(produk.BA_PRICE) * jumlahPesan;
                    totalHarga = String.valueOf(total);
                    tvTotalHarga.setText(new Helper().convertRupiah(total));
                }
            }
        });

        btnKurangi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jumlahPesan != 1) {
                    jumlahPesan--;
                    tvJumlah.setText("" + jumlahPesan);
                    int total = Integer.valueOf(produk.BA_PRICE) * jumlahPesan;
                    totalHarga = String.valueOf(total);
                    tvTotalHarga.setText(new Helper().convertRupiah(total));
                }

                if (jumlahPesan == 1) {
                    Picasso.with(getApplicationContext())
                            .load(R.drawable.ic_kurang_disable)
                            .into(btnKurangi);
                }
            }
        });
    }

    private void alart(){
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.view_dialog_cart, null);
        alertDialog.setView(layout);
        alertDialog.setCancelable(false);

        TextView btnOk = layout.findViewById(R.id.btn_ok);
        TextView btnNo = layout.findViewById(R.id.btn_no);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("extra", "keranjang");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tambah Keranjang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @SuppressLint("StaticFieldLeak")
    private void insertData(final TbKranjang barang) {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses insert data
                long status = db.daoKranjang().insertBarang(barang);
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
                alart();
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void insertDataToko(final TbKranjangByToko barang) {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses insert data
                long status = dbtoko.daoKranjang().insertBarang(barang);
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
            }
        }.execute();
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgProduk = (ImageView) findViewById(R.id.img_produk);
        tvNamaproduk = (TextView) findViewById(R.id.tv_namaproduk);
        tvHargaProduk = (TextView) findViewById(R.id.tv_hargaProduk);
        tvKetStok = (TextView) findViewById(R.id.tv_ketStok);
        btnKurangi = (ImageView) findViewById(R.id.btn_kurangi);
        tvJumlah = (TextView) findViewById(R.id.tv_jumlah);
        btnTambah = (ImageView) findViewById(R.id.btn_tambah);
        edtCatatan = (EditText) findViewById(R.id.edt_catatan);
        tvTotalHarga = (TextView) findViewById(R.id.tv_totalHarga);
        btnTambahKeKeranjang = (CardView) findViewById(R.id.btn_tambahKeKeranjang);
        tvStok = (TextView) findViewById(R.id.tv_stok);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
