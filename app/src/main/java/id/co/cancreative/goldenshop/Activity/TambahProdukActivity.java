package id.co.cancreative.goldenshop.Activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.co.cancreative.goldenshop.Adapter.AdapterAddFilePicture;
import id.co.cancreative.goldenshop.Adapter.AdapterAddGambarP;
import id.co.cancreative.goldenshop.Adapter.AdapterGambarP;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahProdukActivity extends AppCompatActivity {

    private View parent_view;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;

    private ModelProduk produk;
    private User user;

    private File fileProduk, fileProduk1, fileProduk2, fileProduk3, fileProduk4;
    private File _fileProduk, _fileProduk1, _fileProduk2, _fileProduk3, _fileProduk4;
    private List<File> imgProduct = new ArrayList<>();
    private ArrayList<String> files = new ArrayList<>();
    private String allGambar = "";

    private ArrayList<File> _files = new ArrayList<>();
    private AdapterGambarP mAdapter;

    private String harga = "";
    private String kondisi = "NEW";

    private Toolbar toolbar;
    private EditText edtHarga;
    private EditText edtStok;
    private EditText edtBerat;
    private TextView tvKondisi;
    private TextView tvSatuanBerat;
    private TextView tvToolbarTitle;
    private ImageView btnSimpan;
    private EditText edtNama;
    private EditText edtDeskripsi;
    private EditText edtSku;
    private LinearLayout btnKondisi;
    private LinearLayout btnKategori;
    private TextView tvKategori;
    private RecyclerView rv;
    private LinearLayout btnTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_produk);
        initView();

        s = new SharedPref(this);
        produk = new ModelProduk();
        user = s.getUser();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        editText();
        mainButton();
        setToolbar();
        displayPicture();
    }

    private void displayPicture() {
        files.add("");
        mAdapter = new AdapterGambarP(files, this);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv.setAdapter(mAdapter);
    }

    private void mainButton() {
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fileProduk == null) {
                    Snackbar.make(parent_view, "Tambah Gambar", Snackbar.LENGTH_SHORT).show();
                } else if (edtNama.getText().toString().isEmpty()) {
                    edtNama.setError("Kolom Tidak Boleh Kosong");
                    edtNama.requestFocus();
                } else if (edtDeskripsi.getText().toString().isEmpty()) {
                    edtDeskripsi.setError("Kolom Tidak Boleh Kosong");
                    edtDeskripsi.requestFocus();
                } else if (edtHarga.getText().toString().isEmpty()) {
                    edtHarga.setError("Kolom Tidak Boleh Kosong");
                } else if (s.getString(SharedPref.ID_SubKategori) == null) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tvKategori.setError(null);
                        }
                    }, 4000);
                    tvKategori.setError("Pilih Kategori");
                    Snackbar.make(parent_view, "Pilih Kategori", Snackbar.LENGTH_SHORT).show();
                } else if (edtStok.getText().toString().isEmpty()) {
                    edtStok.setError("Kolom Tidak Boleh Kosong");
                } else if (edtBerat.getText().toString().isEmpty()) {
                    edtBerat.setError("Kolom Tidak Boleh Kosong");
                } else if (edtSku.getText().toString().isEmpty()) {
                    edtSku.setError("Kolom Tidak Boleh Kosong");
                } else {
                    sendData();
                }
            }
        });


        btnKondisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(TambahProdukActivity.this, btnKondisi);
                popupMenu.getMenuInflater().inflate(R.menu.menu_kondisi, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().toString().equals("Baru")) {
                            kondisi = "NEW";
                        } else if (item.getTitle().equals("Bekas")) {
                            kondisi = "SECOND";
                        }

                        tvKondisi.setText(item.getTitle());
                        return true;
                    }
                });

                popupMenu.show();
            }
        });

        btnKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TambahProdukActivity.this, PilihKategoriActivity.class);
                intent.putExtra("extra", "addProduk");
                startActivity(intent);
            }
        });
    }

    private void editText() {

        // Edit Harga
        edtHarga.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtHarga.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        harga = cleanString;
                        edtHarga.setText(formatted);
                        edtHarga.setSelection(formatted.length());

                        edtHarga.addTextChangedListener(this);
                    } else {
                        edtHarga.setText(current);
                        edtHarga.setSelection(current.length());

                        edtHarga.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        // Edit Berat
        edtBerat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {

                    if (s.toString().isEmpty()) {
                        tvSatuanBerat.setVisibility(View.GONE);
                    }
                    current = s.toString();
                    edtHarga.removeTextChangedListener(this);
                    tvSatuanBerat.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void sendData() {

        String Id = s.getString(SharedPref.ID_SubKategori);
        String Name = edtNama.getText().toString();
        String Price = harga;
        String Sku = edtSku.getText().toString();
        String Deskripsi = edtDeskripsi.getText().toString();
        String Stock = edtStok.getText().toString();
        String Weight = edtBerat.getText().toString();
        String Condition = kondisi;

        for (String d : files) {
            if (!d.isEmpty()) {
                Log.d("Gambar", d);
                allGambar = allGambar + d + "|";
            }
        }

        Log.d("Allgambar", "Size: " + " : " + allGambar);

        RequestBody sId = RequestBody.create(MediaType.parse("text/plain"), Id);
        RequestBody sName = RequestBody.create(MediaType.parse("text/plain"), Name);
        RequestBody sPrice = RequestBody.create(MediaType.parse("text/plain"), Price);
        RequestBody sSku = RequestBody.create(MediaType.parse("text/plain"), Sku);
        RequestBody sDeskripsi = RequestBody.create(MediaType.parse("text/plain"), Deskripsi);
        RequestBody sStock = RequestBody.create(MediaType.parse("text/plain"), Stock);
        RequestBody sWeight = RequestBody.create(MediaType.parse("text/plain"), Weight);
        RequestBody sCondition = RequestBody.create(MediaType.parse("text/plain"), Condition);
        RequestBody sAllgambar = RequestBody.create(MediaType.parse("text/plain"), allGambar);

        final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileProduk);

        MultipartBody.Part[] ImageProduk = new MultipartBody.Part[imgProduct.size()];
        for (int index = 0; index < imgProduct.size(); index++) {
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), imgProduct.get(index));
            ImageProduk[index] = MultipartBody.Part.createFormData("image[]", imgProduct.get(index).getName(), surveyBody);
        }

        SweetAlert.sendingData(this);

        String admin = "";
        if (user.role.equals("RL_ADMIN")) {
            admin = "_admin";
        }
        api.addProduk(admin, s.getApi(), s.getIdUser(), s.getSession(), sId, sName, sPrice, sSku, sDeskripsi, sStock, sWeight, sCondition, sAllgambar).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    if (response.body().success == 1) {
                        Produk p = response.body().product;
                        s.setProduk(p);

                        Intent intent = new Intent(TambahProdukActivity.this, BerhasilTambahProdukActivity.class);
                        intent.putExtra("extra", "addProduk");
                        startActivity(intent);
                        finish();

                        s.setString(SharedPref.NAMA_SubKategori, null);
                        s.setString(SharedPref.ID_SubKategori, null);
                    } else {
                        Toasti.error(TambahProdukActivity.this, response.body().message);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(TambahProdukActivity.this, t.getMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        } else if (requestCode != Activity.RESULT_CANCELED) {
            files.add("");
            mAdapter = new AdapterGambarP(files, TambahProdukActivity.this);
            rv.setAdapter(mAdapter);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            fileProduk = new File(Crop.getOutput(result).getPath());
            uploadGmabar();
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadGmabar() {

        try {
            _fileProduk = new Compressor(this).compressToFile(fileProduk);
        } catch (IOException e) {
            e.printStackTrace();
        }

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), _fileProduk);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", _fileProduk.getName(), requestFile);

        Log.d("gambar golden", "" + image);

        SweetAlert.sendingData(this);
        api.uploadFoto(s.getApi(), s.getIdUser(), s.getSession(), image).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, final Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {

                    files.add(response.body().image_name);
                    if (files.size() <= 5) {
                        files.add("");
                    }
                    mAdapter = new AdapterGambarP(files, TambahProdukActivity.this);
                    rv.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(TambahProdukActivity.this, t.getMessage());
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        tvToolbarTitle.setText("Tambah Produk");
        getSupportActionBar().setTitle("Tambah Produk");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        parent_view = findViewById(android.R.id.content);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        edtHarga = (EditText) findViewById(R.id.edt_harga);
        edtStok = (EditText) findViewById(R.id.edt_stok);
        edtBerat = (EditText) findViewById(R.id.edt_berat);
        tvKondisi = (TextView) findViewById(R.id.tv_kondisi);
        tvSatuanBerat = (TextView) findViewById(R.id.tv_satuanBerat);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        btnSimpan = (ImageView) findViewById(R.id.btn_simpan);
        edtNama = (EditText) findViewById(R.id.edt_nama);
        edtDeskripsi = (EditText) findViewById(R.id.edt_deskripsi);
        edtSku = (EditText) findViewById(R.id.edt_sku);
        btnKondisi = (LinearLayout) findViewById(R.id.btn_kondisi);
        btnKategori = (LinearLayout) findViewById(R.id.btn_kategori);
        tvKategori = (TextView) findViewById(R.id.tv_kategori);
        rv = (RecyclerView) findViewById(R.id.rv);
    }

    @Override
    protected void onResume() {
        if (s.getString(SharedPref.NAMA_SubKategori) != null) {
            tvKategori.setText(s.getString(SharedPref.NAMA_SubKategori));
        }
        super.onResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        s.setString(SharedPref.NAMA_SubKategori, null);
        s.setString(SharedPref.ID_SubKategori, null);
        super.onBackPressed();
    }
}
