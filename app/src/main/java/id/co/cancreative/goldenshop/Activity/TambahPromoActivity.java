package id.co.cancreative.goldenshop.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SaveData;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahPromoActivity extends AppCompatActivity {

    private View parent_view;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;

    private View viewTgl;
    private DatePickerDialog.OnDateSetListener setCalenderMulai;

    private String tgl, tanggal;
    private String sNama, sDeskripsi, sKode, sPotongan, sMinimal, sIdKat, sTglMulai = "2019-4-1", sTglExp = "2019-4-2";

    private String potongan = "0", minimalBelanja = "0";
    private String subKategori;
    private String idSub;

    private Toolbar toolbar;
    private TextView tvToolbarTitle;
    private ImageView btnSimpan;
    private EditText edtNama;
    private EditText edtDeskripsi;
    private LinearLayout btnTest;
    private LinearLayout btnProduk;
    private TextView tvKategori;
    private EditText edtPotongan;
    private EditText edtMinimumBelanja;
    private EditText edtKodePromo;
    private CardView btnCalenderMulai;
    private TextView tvTglMulai;
    private CardView btnCalenderSampai;
    private TextView tvTglSampai;
    private RelativeLayout divPilihKetegori;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_promo);
        initView();

        s = new SharedPref(this);
        idSub = getIntent().getStringExtra("extra");

        setValue();
        setToolbar();
        mainButton();

    }

    private void setValue() {

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        sTglMulai = df.format(c);

        Date dt = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.add(Calendar.DATE, 1);
        dt = cal.getTime();
        sTglExp = df.format(dt);

        tvTglMulai.setText("" + new Helper().convertDateFormat(sTglMulai));
        tvTglSampai.setText("" + new Helper().convertDateFormat(sTglExp));

        tvKategori.setText(subKategori);
        if (idSub != null) {
            divPilihKetegori.setVisibility(View.GONE);
        }
    }

    private void mainButton() {

        edtPotongan.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtPotongan.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        sPotongan = cleanString;
                        edtPotongan.setText(formatted);
                        edtPotongan.setSelection(formatted.length());

                        edtPotongan.addTextChangedListener(this);
                    } else {
                        edtPotongan.setText(current);
                        edtPotongan.setSelection(current.length());

                        edtPotongan.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        edtMinimumBelanja.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtMinimumBelanja.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        sMinimal = cleanString;
                        edtMinimumBelanja.setText(formatted);
                        edtMinimumBelanja.setSelection(formatted.length());

                        edtMinimumBelanja.addTextChangedListener(this);
                    } else {
                        edtMinimumBelanja.setText(current);
                        edtMinimumBelanja.setSelection(current.length());

                        edtPotongan.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        btnCalenderMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCalender(v);
            }
        });

        btnCalenderSampai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCalender(v);
            }
        });

        setCalenderMulai = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int thun, int month, int day) {
                tgl = thun + "-" + (month + 1) + "-" + day;
                String tanggal = day + " " + bulan(month) + " " + thun;

                if (viewTgl == btnCalenderMulai) {
                    tvTglMulai.setText(tanggal);
                    sTglMulai = tgl;
                }

                if (viewTgl == btnCalenderSampai) {
                    tvTglSampai.setText(tanggal);
                    sTglExp = tgl;
                }
            }
        };


        btnProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TambahPromoActivity.this, PilihKategoriActivity.class);
                intent.putExtra("extra", "addProduk");
                startActivity(intent);
//                startActivity(new Intent(TambahPromoActivity.this, PilihProdukActivity.class));
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPromo();
            }
        });
    }

    private void dialogCalender(View v) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(TambahPromoActivity.this, setCalenderMulai, year, month, day);
        dialog.show();
        viewTgl = v;
    }

    private void addPromo() {

        if (idSub != null) {
            sIdKat = idSub;
        } else {
            sIdKat = SaveData.getIdSubKat();
        }
        sNama = edtNama.getText().toString();
        sDeskripsi = edtDeskripsi.getText().toString();
        sKode = edtKodePromo.getText().toString().toUpperCase();

        if (sNama.isEmpty()) {
            edtNama.setError("Kolom Nama Tidak Boleh Kosong");
        } else if (sDeskripsi.isEmpty()) {
            edtDeskripsi.setError("Kolom Deskripsi Tidak Boleh Kosong");
        } else if (sIdKat.isEmpty() || sIdKat == null) {
            Snackbar.make(parent_view, "Pilih Kategori", Snackbar.LENGTH_SHORT).show();
        } else if (sPotongan.isEmpty()) {
            edtPotongan.setError("Kolom Potongan Tidak Boleh Kosong");
        } else if (sMinimal.isEmpty()) {
            edtMinimumBelanja.setError("Kolom Minimal Belanja Tidak Boleh Kosong");
        } else if (sKode.isEmpty()) {
            edtKodePromo.setError("Kolom Kode Promo Tidak Boleh Kosong");
        } else if (sKode.length() < 6) {
            edtKodePromo.setError("Minimal 6 Karakter");
            edtKodePromo.requestFocus();
        } else {

            Log.d("Test", "" + sKode);
            SweetAlert.sendingData(this);
            api.addPromo(
                    s.getApi(), s.getIdUser(), s.getSession(), sNama, sDeskripsi, sPotongan, sMinimal, sKode, sIdKat,
                    sTglMulai, sTglExp)
                    .enqueue(new Callback<ResponModel>() {
                        @Override
                        public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                            SweetAlert.dismis();
                            if (response.body().success == 1) {

                                new SweetAlertDialog(TambahPromoActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Berhasil Menambah Promo")
                                        .setContentText("" + response.body().message)
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                finish();
                                                CallFunction.getrefrashPromo();
                                                sweetAlertDialog.dismiss();
                                            }
                                        })
                                        .show();

                            } else {
                                Toasti.error(TambahPromoActivity.this, response.body().message);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponModel> call, Throwable t) {
                            SweetAlert.dismis();
                            SweetAlert.onFailure(TambahPromoActivity.this, t.getMessage());
                        }
                    });
        }
    }

    private String bulan(int nilai) {
        String bulan;
        switch (nilai + 1) {
            case 1:
                bulan = "January";
                break;
            case 2:
                bulan = "February";
                break;
            case 3:
                bulan = "March";
                break;
            case 4:
                bulan = "April";
                break;
            case 5:
                bulan = "Mey";
                break;
            case 6:
                bulan = "June";
                break;
            case 7:
                bulan = "July";
                break;
            case 8:
                bulan = "August";
                break;
            case 9:
                bulan = "September";
                break;
            case 10:
                bulan = "October";
                break;
            case 11:
                bulan = "November";
                break;
            default:
                bulan = "December";
        }
        return bulan;
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        tvToolbarTitle.setText("Tambah Promo");
        getSupportActionBar().setTitle("GoldenShop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        parent_view = findViewById(android.R.id.content);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        btnSimpan = (ImageView) findViewById(R.id.btn_simpan);
        edtNama = (EditText) findViewById(R.id.edt_nama);
        edtDeskripsi = (EditText) findViewById(R.id.edt_deskripsi);
        btnTest = (LinearLayout) findViewById(R.id.btn_test);
        btnProduk = (LinearLayout) findViewById(R.id.btn_produk);
        tvKategori = (TextView) findViewById(R.id.tv_kategori);
        edtPotongan = (EditText) findViewById(R.id.edt_potongan);
        edtMinimumBelanja = (EditText) findViewById(R.id.edt_minimumBelanja);
        edtKodePromo = (EditText) findViewById(R.id.edt_kodePromo);
        btnCalenderMulai = (CardView) findViewById(R.id.btn_calenderMulai);
        tvTglMulai = (TextView) findViewById(R.id.tv_tglMulai);
        btnCalenderSampai = (CardView) findViewById(R.id.btn_calenderSampai);
        tvTglSampai = (TextView) findViewById(R.id.tv_tglSampai);
        divPilihKetegori = (RelativeLayout) findViewById(R.id.div_pilihKetegori);
    }

    @Override
    protected void onResume() {

        if (SaveData.getNameSubKat() != null) {
            subKategori = SaveData.getNameSubKat();
            tvKategori.setText(subKategori);
        }
        super.onResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

