package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahUlasanActivity extends AppCompatActivity {

    private SharedPref s;
    private ApiService api = ApiConfig.getInstanceRetrofit();

    private String idTrans;
    private Produk produk;

    private ImageView imgBarang;
    private TextView tvNamaBarang;
    private AppCompatRatingBar rtBar;
    private TextView tvRate;
    private EditText edtKomentar;
    private Button btSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_ulasan);
        initView();

        s = new SharedPref(this);
        idTrans = getIntent().getStringExtra("idTrans");
        produk = s.getProduk();

        setValue();
        mainButton();
    }

    private void setValue() {
        String image = new Helper().splitText(produk.BA_IMAGE);
        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat1)
                .noFade()
                .into(imgBarang);

        tvNamaBarang.setText(""+ produk.BA_NAME);
    }

    private void mainButton() {

        rtBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                tvRate.setText("Rate : " + rating);
            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
    }

    private void submit() {
        SweetAlert.sendingData(this);

        api.tambahUlasan(s.getApi(),
                "" + s.getIdUser(),
                "" + s.getSession(),
                "" + idTrans,
                "" + rtBar.getRating(),
                "" + edtKomentar.getText().toString()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    Intent intent = new Intent(TambahUlasanActivity.this, DaftarTransaksiActivity.class);
                    intent.putExtra("FRAGMENT_ID", "4");
                    finish();
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(TambahUlasanActivity.this, t.getMessage());
            }
        });
    }

    private void initView() {
        imgBarang = (ImageView) findViewById(R.id.img_barang);
        tvNamaBarang = (TextView) findViewById(R.id.tv_nama_barang);
        rtBar = (AppCompatRatingBar) findViewById(R.id.rt_bar);
        tvRate = (TextView) findViewById(R.id.tv_rate);
        edtKomentar = (EditText) findViewById(R.id.edt_komentar);
        btSubmit = (Button) findViewById(R.id.bt_submit);
    }
}
