package id.co.cancreative.goldenshop.Activity;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import id.co.cancreative.goldenshop.Adapter.AdapterTerakhirDilihat;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.TbTerakhirDilihat;

public class TerakhirDilihatActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ArrayList<TbTerakhirDilihat> daftarBarang;
    private AppDbTerakhirDilihat db;
    AdapterTerakhirDilihat mAdapter;
    private RecyclerView rvTerakhirDilihat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terakhir_dilihat);
        initView();
        setToolbar();

        db = Room.databaseBuilder(this, AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();

        tampilData();
    }

    private void tampilData() {
        daftarBarang = new ArrayList<>();
        daftarBarang.addAll(Arrays.asList(db.daoTerakhirDilihat().selectTerakhirDilihat()));

        if (daftarBarang.size() == 0) {
            Toasti.info(TerakhirDilihatActivity.this, "Anda Belum Melihat Barang");
        } else {
            mAdapter = new AdapterTerakhirDilihat(TerakhirDilihatActivity.this, daftarBarang);
            rvTerakhirDilihat.setLayoutManager(new GridLayoutManager(TerakhirDilihatActivity.this, 2));
            rvTerakhirDilihat.setAdapter(mAdapter);
        }
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Terakhir Dilihat");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        rvTerakhirDilihat = (RecyclerView) findViewById(R.id.rv_terakhir_dilihat);
    }
}
