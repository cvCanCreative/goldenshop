package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.EditToko;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.R;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahNamaTokoActivity extends AppCompatActivity {

    private File imageFile, _imageFile;

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Toko toko;
    private EditToko editToko;

    private Toolbar toolbar;
    private AppBarLayout appbarLayout;
    private Button btnAddFoto;
    private EditText edtName;
    private EditText edtDeskripsi;
    private ImageView imgToko;
    private Button btnSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_toko);
        initView();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        s = new SharedPref(this);
        toko = s.getToko();
        editToko = s.getEditToko();

        setValue();
        mainButton();
        setToolbar();
    }

    private void mainButton() {
        btnAddFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openGallery(UbahNamaTokoActivity.this, 1);
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedata();
            }
        });
    }

    private void setValue() {

        String namaToko = toko.TOK_NAME;
        String deskripsi = toko.TOK_DETAIL;
        String image = toko.TOK_FOTO;

        // Cek Status informasi umum, apakah pernah di edit
        if (editToko != null) {
            if (editToko.status) {
                namaToko = editToko.TOK_NAME;
                deskripsi = editToko.TOK_DETAIL;
                image = editToko.TOK_FOTO;
            }
        }

        edtName.setText(namaToko);
        edtDeskripsi.setText(deskripsi);
        Picasso.with(this)
                .load(Config.url_toko + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(imgToko);

    }

    private void savedata() {
        if (_imageFile != null) {
            uploadFoto();
        } else {
            if (!edtName.getText().toString().equals(toko.TOK_NAME) || !edtDeskripsi.getText().toString().equals(toko.TOK_DETAIL)) {
                editToko = new EditToko();
                editToko.status = true;
                editToko.TOK_NAME = edtName.getText().toString();
                editToko.TOK_DETAIL = edtDeskripsi.getText().toString();
                editToko.TOK_FOTO = toko.TOK_FOTO;
                s.setEditToko(editToko);
                onBackPressed();
//                CallFunction.getPengaturanToko();
//                UbahNamaTokoActivity.this.finish();
            } else {
                UbahNamaTokoActivity.this.finish();
            }
        }
    }

    private void uploadFoto() {

        SweetAlert.onLoading(this);

        String _toko = edtName.getText().toString();

        try {
            imageFile = new Compressor(this).compressToFile(_imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        RequestBody sName = RequestBody.create(MediaType.parse("text/plain"), _toko);

        // Icon
        final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part fIcon = MultipartBody.Part.createFormData("icon", imageFile.getName(), requestFile);

        api.uploadEditFotoToko(s.getApi(), s.getIdUser(), s.getSession(), sName, fIcon).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    ResponModel res = response.body();
                    if (response.body().success == 1) {
                        editToko = new EditToko();
                        editToko.status = true;
                        editToko.TOK_NAME = edtName.getText().toString();
                        editToko.TOK_DETAIL = edtDeskripsi.getText().toString();
                        editToko.TOK_FOTO = res.result.icon;
                        s.setEditToko(editToko);
                        CallFunction.getPengaturanToko();
                        UbahNamaTokoActivity.this.finish();
                        Log.d("Respon", "Edited");
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(UbahNamaTokoActivity.this, t.getMessage());
            }
        });

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ubah Informasi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_simpan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        } else if (item_id == R.id.action_checkout) {
            savedata();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, UbahNamaTokoActivity.this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFiles, EasyImage.ImageSource source, int type) {
                _imageFile = imageFiles;
                Picasso.with(UbahNamaTokoActivity.this)
                        .load(_imageFile)
                        .placeholder(R.drawable.image_loading)
                        .error(R.drawable.image_error)
                        .noFade()
                        .into(imgToko);
            }
        });
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        appbarLayout = (AppBarLayout) findViewById(R.id.appbar_layout);
        btnAddFoto = (Button) findViewById(R.id.btn_addFoto);
        edtName = (EditText) findViewById(R.id.edt_name);
        edtDeskripsi = (EditText) findViewById(R.id.edt_deskripsi);
        imgToko = (ImageView) findViewById(R.id.img_toko);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
