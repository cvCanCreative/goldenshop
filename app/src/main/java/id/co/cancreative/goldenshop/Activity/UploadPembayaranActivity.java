package id.co.cancreative.goldenshop.Activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.R;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadPembayaranActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Transaksi trans;

    private File imageFile;

    //Alert Dialog
    private AlertDialog alertDialog;
    private LayoutInflater inflater;
    private View layout;

    private Toolbar toolbar;
    private Button btnPilihGambar;
    private Button btnSimpan;
    private ImageView image;
    private ImageView btnClose;
    private LinearLayout divPilihGambar;
    private ScrollView divGambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_pembayaran);
        initView();

        s = new SharedPref(this);
        trans = s.getDataTransaksi();
        setToolbar();
        mainButton();
    }

    private void mainButton() {
        btnPilihGambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert();
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                divGambar.setVisibility(View.GONE);
                divPilihGambar.setVisibility(View.VISIBLE);
            }
        });
    }

    private void upload() {

        RequestBody sIdRek = RequestBody.create(MediaType.parse("text/plain"), trans.ID_REKENING);
        RequestBody sJumlahTF = RequestBody.create(MediaType.parse("text/plain"), trans.TS_TF_GOLD);

        try {
            imageFile = new Compressor(this).compressToFile(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part fImage = MultipartBody.Part.createFormData("image", imageFile.getName(), requestFile);

        SweetAlert.sendingData(this);
        api.uploadPembayaran(s.getApi(), s.getIdUser(), s.getSession(), sIdRek, sJumlahTF, fImage).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {

                    Intent i = new Intent(UploadPembayaranActivity.this, DaftarTransaksiActivity.class);
                    startActivity(i);
                    finish();
                    Toasti.success(UploadPembayaranActivity.this, "Upload Berhasil");

                } else {
                    Toasti.error(UploadPembayaranActivity.this, response.body().message);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(UploadPembayaranActivity.this, t.getMessage());

            }
        });

    }

    private void alert() {

        alertDialog = new AlertDialog.Builder(UploadPembayaranActivity.this).create();
        inflater = UploadPembayaranActivity.this.getLayoutInflater();
        layout = inflater.inflate(R.layout.view_dialog_media, null);
        alertDialog.setView(layout);
        alertDialog.setCancelable(false);

        TextView btnOk = layout.findViewById(R.id.btn_ok);
        TextView btnNo = layout.findViewById(R.id.btn_no);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                EasyImage.openGallery(UploadPembayaranActivity.this, 1);
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                EasyImage.openCamera(UploadPembayaranActivity.this, 1);
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, UploadPembayaranActivity.this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFiles, EasyImage.ImageSource source, int type) {
                imageFile = imageFiles;
                divGambar.setVisibility(View.VISIBLE);
                divPilihGambar.setVisibility(View.GONE);

                Picasso.with(UploadPembayaranActivity.this)
                        .load(imageFile)
                        .placeholder(R.drawable.image_loading)
                        .error(R.drawable.image_error)
                        .noFade()
                        .into(image);

            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Upload Bukti");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnPilihGambar = (Button) findViewById(R.id.btn_pilihGambar);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        image = (ImageView) findViewById(R.id.image);
        btnClose = (ImageView) findViewById(R.id.btn_close);
        divPilihGambar = (LinearLayout) findViewById(R.id.div_pilihGambar);
        divGambar = (ScrollView) findViewById(R.id.div_gambar);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
