package id.co.cancreative.goldenshop.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import id.co.cancreative.goldenshop.Adapter.AdapterBankNew;
import id.co.cancreative.goldenshop.Adapter.AdapterRekeningGold;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.ItemClickSupport;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Withdraw1Activity extends AppCompatActivity {

    private View parent_view;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private User user;
    private Saldo saldo;
    private Saldo wd;
    private ColorStateList oldColor;

    private Rekening rekening;
    private ArrayList<Rekening> _rekeningUser = new ArrayList<>();
    private ArrayList<Rekening> _rekening = new ArrayList<>();

    private int iSaldo;
    private int minWD;
    private int minSaldoEndap;
    private String nominal = "0";
    private boolean simpan = false;

    private String idBank;

    private Toolbar toolbar;
    private TextView tvSaldo;
    private TextView btnTarikSemua;
    private EditText edtSaldo;
    private RelativeLayout divInfoBankUser;
    private ImageView imgBank;
    private TextView tvNamaBank;
    private TextView tvRekeningUser;
    private ImageView btnMenu;
    private TextView tvPesan;
    private LinearLayout divInputInfoBank;
    private ImageView imgBankTransfer;
    private TextView tvBankTransfer;
    private EditText edtNoRekening;
    private EditText edtNamaPemilik;
    private Button btnWithdraw;
    private LinearLayout divPilihMetodeSlide;
    private ImageView btnClose;
    private RecyclerView rvBank;
    private RelativeLayout divLayer;
    private TextView btnSimpan;
    private LinearLayout btnPilihBank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw1);
        initView();

        s = new SharedPref(this);

        divPilihMetodeSlide.animate().translationY(divPilihMetodeSlide.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divPilihMetodeSlide.setVisibility(View.GONE);
                    }
                });

        setValue();
        setToolbar();
        mainButton();
    }

    private void mainButton() {

        oldColor = tvPesan.getTextColors();

        // Edit Harga
        edtSaldo.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtSaldo.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        nominal = cleanString;
                        if (Integer.valueOf(nominal) > iSaldo) {
                            tvPesan.setText("Saldo tidak mencukupi");
                            tvPesan.setTextColor(Color.RED);
                        } else {
                            tvPesan.setText("Minimum penarikan " + new Helper().convertRupiah(minWD));
                            tvPesan.setTextColor(oldColor);
                        }
                        edtSaldo.setText(formatted);
                        edtSaldo.setSelection(formatted.length());
                        edtSaldo.addTextChangedListener(this);
                    } else {
                        edtSaldo.setText(current);
                        edtSaldo.setSelection(current.length());
                        edtSaldo.addTextChangedListener(this);
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        btnTarikSemua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nominal = String.valueOf(iSaldo);
                edtSaldo.setText("" + new Helper().convertRupiah(iSaldo));
            }
        });

        btnWithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valisasiWithdraw();
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayMenu();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideDown();
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateRekening();
            }
        });

        btnPilihBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pilihBank();
            }
        });
    }

    private void pilihBank(){
        if (_rekening.size() != 0){
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Withdraw1Activity.this, LinearLayoutManager.VERTICAL, false);
            RecyclerView.Adapter mAdapter = new AdapterBankNew(_rekening, Withdraw1Activity.this);
            rvBank.setLayoutManager(layoutManager);
            rvBank.setAdapter(mAdapter);
            ItemClickSupport.addTo(rvBank).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    selectItem(_rekening.get(position));
                }
            });
            slideUp();
            return;
        }
        getBank();
        slideUp();
    }

    private void getBank() {
        api.getBank(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                if (response.isSuccessful()) {
                    _rekening = response.body().bank;
                    s.setBank(response.body());
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Withdraw1Activity.this, LinearLayoutManager.VERTICAL, false);
                    RecyclerView.Adapter mAdapter = new AdapterBankNew(_rekening, Withdraw1Activity.this);
                    rvBank.setLayoutManager(layoutManager);
                    rvBank.setAdapter(mAdapter);

                    ItemClickSupport.addTo(rvBank).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                        @Override
                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                            selectItem(_rekening.get(position));
                        }
                    });
//                    slideUp();
                    setValue();
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.onFailure(Withdraw1Activity.this, t.getMessage());
            }
        });
    }

    private void valisasiWithdraw() {

        if (edtSaldo.getText().toString().isEmpty() || edtSaldo.getText().toString().equals("Rp0")) {
            edtSaldo.setError("Saldo Tidak Boleh Kosong");
            edtSaldo.requestFocus();
            Snackbar.make(parent_view, "Isi Nominal Saldo", Snackbar.LENGTH_SHORT).show();
            return;
        }

        if (!(Integer.valueOf(nominal) < minWD)) {

            new SweetAlertDialog(Withdraw1Activity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Apakah Anda Yakin?")
                    .setContentText("Anda akan menarik saldo senilai " + new Helper().convertRupiah(Integer.valueOf(nominal)))
                    .setCancelText("Batal")
                    .setConfirmText("Tarik Saldo")
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            simpan = true;
                            updateRekening();
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();
        } else {
            tvPesan.setTextColor(Color.RED);
            edtSaldo.setError("Minimum penarikan " + new Helper().convertRupiah(minWD));
            edtSaldo.requestFocus();
        }
    }

    private void selectItem(Rekening i) {
        tvBankTransfer.setText(i.BK_NAME);
        Picasso.with(this)
                .load(Config.url_bank + i.BK_IMAGE)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(imgBankTransfer);

        idBank = i.ID_BANK;
        slideDown();
    }

    private void updateRekening() {
        String noRek = edtNoRekening.getText().toString();
        String namaPemilik = edtNamaPemilik.getText().toString();

        if (rekening != null){
            if (!noRek.equals(rekening.RK_NOMOR) || !namaPemilik.equals(rekening.RK_NAME) || !idBank.equals(rekening.ID_BANK)) {
                SweetAlert.onLoading(this);
                api.editRekening(s.getApi(), s.getIdUser(), s.getSession(), rekening.ID_REKENING, idBank, noRek, namaPemilik).enqueue(new Callback<ResponModel>() {
                    @Override
                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                        if (response.body().success == 1) {
                            divInfoBankUser.setVisibility(View.VISIBLE);
                            divInputInfoBank.setVisibility(View.GONE);
                            s.setRekening(response.body().rekenings);
                            rekening = s.getRekening();
                            setValue();
//                        tvRekeningUser.setText(edtNoRekening.getText().toString() + " - " + edtNamaPemilik.getText().toString());

                            if (simpan) {
                                withdraw();
                            } else {
                                SweetAlert.dismis();
                            }

                        } else {
                            Toasti.error(Withdraw1Activity.this, "" + response.body().message);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponModel> call, Throwable t) {
                        SweetAlert.dismis();
                        SweetAlert.onFailure(Withdraw1Activity.this, "" + t.getMessage());
                    }
                });
            } else {
                if (simpan) {
                    SweetAlert.onLoading(Withdraw1Activity.this);
                    withdraw();
                }
                divInfoBankUser.setVisibility(View.VISIBLE);
                divInputInfoBank.setVisibility(View.GONE);
            }
        } else {
            addRekening();
        }
    }

    private void displayMenu() {
        final PopupMenu popupMenu = new PopupMenu(this, btnMenu);
        popupMenu.getMenuInflater().inflate(R.menu.menu_rekening, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_edit:
                        divInfoBankUser.setVisibility(View.GONE);
                        divInputInfoBank.setVisibility(View.VISIBLE);
                        return true;
                    case R.id.item_ganti:
                        ResponModel res = s.getListRekUser();
                        _rekeningUser = res.rekening;
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Withdraw1Activity.this, LinearLayoutManager.VERTICAL, false);
                        RecyclerView.Adapter mAdapter = new AdapterRekeningGold(_rekeningUser, Withdraw1Activity.this);
                        rvBank.setLayoutManager(layoutManager);
                        rvBank.setAdapter(mAdapter);
                        slideUp();

                        ItemClickSupport.addTo(rvBank).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                            @Override
                            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                showItemSelectedUser(_rekeningUser.get(position));
                            }
                        });

                        return true;
                }
                return false;
            }
        });

        popupMenu.show();
    }

    private void withdraw() {
        api.withdrawSaldo(s.getApi(), s.getIdUser(), s.getSession(), rekening.ID_REKENING, nominal).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {
                    new SweetAlertDialog(Withdraw1Activity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Withdraw Berhasil")
                            .setContentText("" + response.body().message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    onBackPressed();
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                } else if (response.body().success == 2) {
                    new SweetAlertDialog(Withdraw1Activity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Withdraw Gagal")
                            .setContentText("" + response.body().message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    onBackPressed();
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                } else {
                    Toasti.error(Withdraw1Activity.this, response.body().message);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(Withdraw1Activity.this, t.getMessage());
            }
        });
    }

    private void addRekening() {

        String sNoRek = edtNoRekening.getText().toString();
        String sNamaPemilik = edtNamaPemilik.getText().toString();

        if (sNoRek.isEmpty()) {
            edtNoRekening.setError("Kolom Tidak Boleh Kosong");
        } else if (sNamaPemilik.isEmpty()) {
            edtNamaPemilik.setError("Kolom Tidak Boleh Kosong");
        } else {
            SweetAlert.onLoading(this);
            api.addRekening(s.getApi(), s.getIdUser(), s.getSession(), idBank, sNoRek, sNamaPemilik).enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    SweetAlert.dismis();
                    if (response.body().success == 1) {
                        s.setRekening(response.body().rekenings);
                        rekening = s.getRekening();
                        withdraw();
                    }
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    SweetAlert.dismis();
                }
            });
        }
    }

    private void setValue() {
        user = s.getUser();
        saldo = s.getSaldo();
        wd = s.getMinWd();
        minWD = Integer.parseInt(wd.minimal_wd);
        minSaldoEndap = Integer.parseInt(wd.minimal_saldo_toko);
        tvPesan.setText("Minimum penarikan " + new Helper().convertRupiah(minWD));

        if (saldo.SA_SALDO != null && !saldo.SA_SALDO.equals("")) {
            iSaldo = Integer.valueOf(saldo.SA_SALDO);
            tvSaldo.setText("" + new Helper().convertRupiah(iSaldo));
        }

        if (_rekening.size() != 0){
            tvBankTransfer.setText(_rekening.get(0).BK_NAME);
            idBank = _rekening.get(0).ID_BANK;
            Picasso.with(this)
                    .load(Config.url_bank + _rekening.get(0).BK_IMAGE)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.image_error)
                    .noFade()
                    .into(imgBankTransfer);
        } else {
            getBank();
        }

        rekening = s.getRekening();
        if (rekening != null) {

            divInputInfoBank.setVisibility(View.GONE);
            divInfoBankUser.setVisibility(View.VISIBLE);
//            btnSimpan.setVisibility(View.GONE);

            tvNamaBank.setText(rekening.BK_NAME);
            tvRekeningUser.setText(rekening.RK_NOMOR + " - " + rekening.RK_NAME);
            tvBankTransfer.setText(rekening.BK_NAME);
            idBank = rekening.ID_BANK;
            Picasso.with(this)
                    .load(Config.url_bank + rekening.BK_IMAGE)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.image_error)
                    .noFade()
                    .into(imgBank);

            Picasso.with(this)
                    .load(Config.url_bank + rekening.BK_IMAGE)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.image_error)
                    .noFade()
                    .into(imgBankTransfer);

            edtNoRekening.setText(rekening.RK_NOMOR);
            edtNamaPemilik.setText(rekening.RK_NAME);

        } else {
            getRekening();
        }
    }

    private void getRekening() {

        api.getRekening(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {

                if (response.body().success == 1) {
                    s.setRekening(response.body().rekening.get(0));
                    s.setListRekUser(response.body());
                    divInfoBankUser.setVisibility(View.VISIBLE);
                    divInputInfoBank.setVisibility(View.GONE);
                    setValue();
                    SweetAlert.dismis();
                } else {
                    divInfoBankUser.setVisibility(View.GONE);
                    divInputInfoBank.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {

                SweetAlert.onFailure(Withdraw1Activity.this, t.getMessage());
            }
        });
    }

    private void showItemSelectedUser(Rekening i) {
        s.setRekening(i);
        idBank = i.ID_BANK;
        divInfoBankUser.setVisibility(View.VISIBLE);
        divInputInfoBank.setVisibility(View.GONE);
        setValue();
        slideDown();
    }

    private void slideDown() {
        divPilihMetodeSlide.animate().translationY(divPilihMetodeSlide.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        divPilihMetodeSlide.setVisibility(View.GONE);
                        divLayer.setVisibility(View.GONE);
                    }
                });
    }

    private void slideUp() {
        divLayer.setVisibility(View.VISIBLE);
        divPilihMetodeSlide.setVisibility(View.VISIBLE);
        divPilihMetodeSlide.animate().translationY(0)
                .alpha(1.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation, boolean isReverse) {
                        divPilihMetodeSlide.setVisibility(View.VISIBLE);
                        divLayer.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GoldenShop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        parent_view = findViewById(android.R.id.content);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvSaldo = (TextView) findViewById(R.id.tv_saldo);
        btnTarikSemua = (TextView) findViewById(R.id.btn_tarikSemua);
        edtSaldo = (EditText) findViewById(R.id.edt_saldo);
        divInfoBankUser = (RelativeLayout) findViewById(R.id.div_infoBankUser);
        imgBank = (ImageView) findViewById(R.id.img_bank);
        tvNamaBank = (TextView) findViewById(R.id.tv_namaBank);
        tvRekeningUser = (TextView) findViewById(R.id.tv_rekeningUser);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        divInputInfoBank = (LinearLayout) findViewById(R.id.div_inputInfoBank);
        imgBankTransfer = (ImageView) findViewById(R.id.img_bankTransfer);
        tvBankTransfer = (TextView) findViewById(R.id.tv_bankTransfer);
        edtNoRekening = (EditText) findViewById(R.id.edt_noRekening);
        edtNamaPemilik = (EditText) findViewById(R.id.edt_namaPemilik);
        btnWithdraw = (Button) findViewById(R.id.btn_withdraw);
        divPilihMetodeSlide = (LinearLayout) findViewById(R.id.div_pilihMetodeSlide);
        btnClose = (ImageView) findViewById(R.id.btn_close);
        rvBank = (RecyclerView) findViewById(R.id.rv_bank);
        divLayer = (RelativeLayout) findViewById(R.id.div_layer);
        btnSimpan = (TextView) findViewById(R.id.btn_simpan);
        btnPilihBank = (LinearLayout) findViewById(R.id.btn_pilihBank);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
