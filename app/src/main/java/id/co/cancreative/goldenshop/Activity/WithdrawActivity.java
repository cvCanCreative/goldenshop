package id.co.cancreative.goldenshop.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Adapter.AdapterWithdraw;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Fragment.LoginFragment;
import id.co.cancreative.goldenshop.Fragment.RegisterFragment;
import id.co.cancreative.goldenshop.Fragment.TransaksiTokoFragment;
import id.co.cancreative.goldenshop.Fragment.WithdrawFragment;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.Model.Withdraw;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WithdrawActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private DatePickerDialog.OnDateSetListener setCalenderMulai;
    private DatePickerDialog.OnDateSetListener setCalenderSampai;

    private String tgl_lahir, tanggal;
    private View viewTgl;
    private int iSaldo;
    private Saldo saldo;

    private Toolbar toolbar;
    private Button btnWithdraw;
    private TextView tvTitle;
    private TextView tvPesan;
    private CardView btnCalenderMulai;
    private CardView btnCalenderSampai;
    private TextView tvTglMulai;
    private TextView tvTglSampai;
    private TextView tvSaldo;
    private ProgressBar pd;
    private RecyclerView rv;
    private LinearLayout layoutKosong;
    private TabLayout loginTabsLayout;
    private ViewPager loginViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);
        initView();

        s = new SharedPref(this);

        setValue();
        setToolbar();
        mainButton();
//        getHistoryWithdraw();

        setupViewPager(loginViewPager);
        loginTabsLayout.setupWithViewPager(loginViewPager);

//        CallFunction.setrefrashSaldo(new Callable<Void>() {
//            @Override
//            public Void call() throws Exception {
//                getHistoryWithdraw();
//                return null;
//            }
//        });
    }

    private void getHistoryWithdraw() {
        api.getHistoryWithdraw(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                layoutKosong.setVisibility(View.GONE);

                if (response.body().success == 1) {
                    ArrayList<Withdraw> mItem = response.body().history_wd;
                    RecyclerView.Adapter mAdapter = new AdapterWithdraw(mItem, WithdrawActivity.this);
                    rv.setLayoutManager(new LinearLayoutManager(WithdrawActivity.this, LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);
                } else {
                    layoutKosong.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                layoutKosong.setVisibility(View.VISIBLE);
                Toasti.onFailure(WithdrawActivity.this, t.getMessage());
            }
        });
    }

    private void setValue() {
        saldo = s.getSaldo();
        if (saldo.SA_SALDO != null && !saldo.SA_SALDO.equals("")) {
            iSaldo = Integer.valueOf(saldo.SA_SALDO);
            tvSaldo.setText("" + new Helper().convertRupiah(iSaldo));
        }
    }

    private void mainButton() {
        btnWithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WithdrawActivity.this, Withdraw1Activity.class));
            }
        });

        btnCalenderMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCalender(v);
            }
        });

        btnCalenderSampai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCalender(v);
            }
        });

        setCalenderMulai = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int thun, int month, int day) {
                tgl_lahir = thun + "-" + (month + 1) + "-" + day;
                String tanggal = day + " " + bulan(month + 1) + " " + thun;

                if (viewTgl == btnCalenderMulai) {
                    tvTglMulai.setText(tanggal);
                }

                if (viewTgl == btnCalenderSampai) {
                    tvTglSampai.setText(tanggal);
                }
            }
        };
    }

    private void dialogCalender(View v) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(WithdrawActivity.this, setCalenderMulai, year, month, day);
        dialog.show();
        viewTgl = v;
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Withdraw");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private String bulan(int nilai) {
        String bulan;
        switch (nilai + 1) {
            case 1:
                bulan = "January";
                break;
            case 2:
                bulan = "February";
                break;
            case 3:
                bulan = "March";
                break;
            case 4:
                bulan = "April";
                break;
            case 5:
                bulan = "Mey";
                break;
            case 6:
                bulan = "June";
                break;
            case 7:
                bulan = "July";
                break;
            case 8:
                bulan = "August";
                break;
            case 9:
                bulan = "September";
                break;
            case 10:
                bulan = "October";
                break;
            case 11:
                bulan = "November";
                break;
            default:
                bulan = "December";
        }
        return bulan;
    }

    private void setupViewPager(ViewPager loginViewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new WithdrawFragment(), "Withdraw");
        adapter.addFragment(new TransaksiTokoFragment(), "Transaksi Toko");
        loginViewPager.setAdapter(adapter);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragmentList.get(i);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String detail) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(detail);
        }

    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnWithdraw = (Button) findViewById(R.id.btn_withdraw);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        btnCalenderMulai = (CardView) findViewById(R.id.btn_calenderMulai);
        btnCalenderSampai = (CardView) findViewById(R.id.btn_calenderSampai);
        tvTglMulai = (TextView) findViewById(R.id.tv_tglMulai);
        tvTglSampai = (TextView) findViewById(R.id.tv_tglSampai);
        tvSaldo = (TextView) findViewById(R.id.tv_saldo);
        pd = (ProgressBar) findViewById(R.id.pd);
        rv = (RecyclerView) findViewById(R.id.rv);
        layoutKosong = (LinearLayout) findViewById(R.id.layout_kosong);
        loginTabsLayout = (TabLayout) findViewById(R.id.login_tabsLayout);
        loginViewPager = (ViewPager) findViewById(R.id.login_viewPager);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
