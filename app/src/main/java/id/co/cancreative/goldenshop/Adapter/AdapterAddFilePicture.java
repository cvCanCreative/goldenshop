package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.R;
import pl.aprilapps.easyphotopicker.EasyImage;

public class AdapterAddFilePicture extends RecyclerView.Adapter<AdapterAddFilePicture.Holdr> {

    ArrayList<File> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterAddFilePicture(ArrayList<File> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_gambar, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterAddFilePicture.Holdr holder, final int i) {
        s = new SharedPref(context);

        final File a = data.get(i);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a == null) {
                    Crop.pickImage(context);
                    data.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    notifyItemRangeChanged(holder.getAdapterPosition(), data.size());
                }
            }
        });

        if (a != null) {
            Picasso.with(context)
                    .load(a)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.image_error)
                    .noFade()
                    .into(holder.image);
            holder.btnClose.setVisibility(View.VISIBLE);
        } else {
            Picasso.with(context)
                    .load(R.drawable.tambahproduk)
                    .placeholder(R.drawable.tambahproduk)
                    .error(R.drawable.image_error)
                    .noFade()
                    .into(holder.image);
        }

        holder.btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                data.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
                notifyItemRangeChanged(holder.getAdapterPosition(), data.size());
                Log.d("Test", "" + holder.getAdapterPosition() + " : " + data.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (data.size() > 5) {
            b = 5;
        } else {
            b = data.size();
        }
        return b;
    }

    public class Holdr extends RecyclerView.ViewHolder {
        ImageView image, btnClose;
        TextView discount, harga, nama;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_produk);
            btnClose = (ImageView) itemView.findViewById(R.id.btn_close);
//            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            layout = itemView.findViewById(R.id.div_layout);
        }
    }
}
