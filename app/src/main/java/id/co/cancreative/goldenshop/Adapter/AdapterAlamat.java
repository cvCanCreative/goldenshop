package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import id.co.cancreative.goldenshop.Activity.EditAlamatActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterAlamat extends RecyclerView.Adapter<AdapterAlamat.Holdr> {

    ArrayList<ModelAlamat> data;
    Activity context;
    int b;
    SharedPref s;
    ApiService api = ApiConfig.getInstanceRetrofit();

    public AdapterAlamat(ArrayList<ModelAlamat> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_alamat, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(AdapterAlamat.Holdr holder, final int i) {
        s = new SharedPref(context);

        final ModelAlamat a = data.get(i);

        holder.label.setText(a.PE_JUDUL);
        holder.penerima.setText(a.PE_NAMA);
        holder.alamat.setText(a.PE_ALAMAT);
        holder.kota.setText(a.PE_KECAMATAN+", "+a.PE_KOTA );
        holder.provinsi.setText(a.PE_PROVINSI+", "+a.PE_KODE_POS);
        holder.phone.setText(a.PE_TELP);

        holder.btn_ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s.setShipping(a);
                Intent i = new Intent(context, EditAlamatActivity.class);
                context.startActivity(i);
            }
        });

        holder.btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sPesan = "Apakah Anda yakin ingin menghapus alamat: " + "<b>" +"Alamat "+ a.PE_JUDUL+ "</b>" +"?";

                final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                pDialog.setTitleText("Hapus")
                        .setContentText(String.valueOf(Html.fromHtml(sPesan)))
                        .setCancelText("Batal")
                        .setConfirmText("Hapus")
                        .showCancelButton(true)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                pDialog.cancel();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                hapusAlamat(a.ID_PENGIRIMAN);
                                pDialog.dismiss();
                            }
                        })
                        .show();
            }
        });

    }

    public void hapusAlamat(String idAlamat){
        SweetAlert.onLoading(context);
        api.hapusAlamat(s.getApi(), s.getIdUser(), s.getSession(), idAlamat).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                CallFunction.getRefreshListAlamat();
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(context, t.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView label, penerima, alamat, kota, provinsi, phone;
        TextView btn_ubah, btn_hapus;
        CardView layout;

        public Holdr(final View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.tv_label);
            penerima = (TextView) itemView.findViewById(R.id.tv_penerima);
            alamat = (TextView) itemView.findViewById(R.id.tv_alamat);
            kota = (TextView) itemView.findViewById(R.id.tv_kota);
            provinsi = (TextView) itemView.findViewById(R.id.tv_provinsi);
            phone = (TextView) itemView.findViewById(R.id.tv_phone);
            btn_hapus = (TextView) itemView.findViewById(R.id.btn_hapus);
            btn_ubah = (TextView) itemView.findViewById(R.id.btn_ubah);
            layout = itemView.findViewById(R.id.layout);
        }
    }
}

