package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.R;

public class AdapterBank extends RecyclerView.Adapter<AdapterBank.Holdr> {
    ArrayList<Rekening> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterBank(ArrayList<Rekening> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_bank, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(AdapterBank.Holdr holder, final int i) {
        s = new SharedPref(context);

        final Rekening a = data.get(i);

        holder.label.setText(a.BK_NAME);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView label;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.tv_nama);
            layout = itemView.findViewById(R.id.card);
        }
    }
}

