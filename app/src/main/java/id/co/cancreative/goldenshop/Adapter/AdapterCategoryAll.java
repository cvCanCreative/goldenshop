package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.SubKategoriActivity;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelCategory;
import id.co.cancreative.goldenshop.Model.ModelSubKat;
import id.co.cancreative.goldenshop.R;

public class AdapterCategoryAll extends RecyclerView.Adapter<AdapterCategoryAll.Holdr> {

    ArrayList<ModelSubKat> subKats = new ArrayList<>();
    ArrayList<ModelCategory> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterCategoryAll(ArrayList<ModelCategory> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_kategori, viewGroup, false);
        return new Holdr(view);

//        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
//        View view = layoutInflater.inflate(R.layout.item_kategori, viewGroup, false);
//        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(AdapterCategoryAll.Holdr holder, final int i) {
        s = new SharedPref(context);

        final ModelCategory a = data.get(i);

        holder.title.setText(a.getkATNAME());
//        holder.harga.setText(""+new Helper().convertRupiah(a.getHarga()));

        Picasso.with(context)
                .load(Config.url_kategori + a.getkATIMAGE())
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat1)
                // To fit image into imageView
//					.fit()
                // To prevent fade animation
                .noFade()
                .into(holder.image);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                subKats = (ArrayList<ModelSubKat>) a.getSkat();

                if (subKats.size() >= 1) {
                    Intent intent = new Intent(context, SubKategoriActivity.class);
                    intent.putExtra("data", data.get(i));
                    context.startActivity(intent);
                } else {
                    SweetAlert.basicMessage(context, "Belum ada sub kategori");
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView title, harga;
        LinearLayout layout;
        ImageView image;

        public Holdr(final View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_nama);
            image = itemView.findViewById(R.id.img_produk);
            layout = itemView.findViewById(R.id.div_layout);
        }
    }
}
