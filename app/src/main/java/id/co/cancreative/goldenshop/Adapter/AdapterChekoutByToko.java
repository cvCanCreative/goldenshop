package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.AppDbKrenjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.TbKranjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.AppDbKrenjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterChekoutByToko extends RecyclerView.Adapter<AdapterChekoutByToko.ViewHolder> {

    private ArrayList<TbKranjangByToko> daftarBarang;
    private Activity context;
    private AppDbKrenjang db;
    private AppDbKrenjangByToko dbtoko;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private ModelAlamat alamat;
    private SharedPref s;

    private int totalHarga = 0, totalBayar = 0, ongkir = 0;
    private double jumlahItem = 1;
    private double hargaAsli = 0;
    private String agenKurir, idOrigin;
    private String sKurir, jenisKurir, sDurasi;

    List<String> jasaPengiriman = new ArrayList<>();
    List<String> listJenisKurir = new ArrayList<>();
    List<String> listHarga = new ArrayList<>();
    List<String> listTipe = new ArrayList<>();
    List<String> listDurasi = new ArrayList<>();

    TextView tvOngkir;
    Spinner spnType;
    TextView tvKurir;
    Spinner spnKurir;
    LinearLayout divKurir;
    LinearLayout divNoAlamat;
    TextView tvStatusKurir;

    TextView tvDurasi, _tvKurir, _tvOngkir;


    public AdapterChekoutByToko(ArrayList<TbKranjangByToko> barangs, Activity ctx) {
        daftarBarang = barangs;
        context = ctx;

        db = Room.databaseBuilder(context.getApplicationContext(),
                AppDbKrenjang.class, Config.db_keranjang).allowMainThreadQueries().build();
        dbtoko = Room.databaseBuilder(context.getApplicationContext(),
                AppDbKrenjangByToko.class, Config.db_keranjangToko).allowMainThreadQueries().build();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvHarga, tvSubTotal, tvBerat, tvCatatan, tvNamaToko;
        TextView tvDurasi, tvKurir, tvOngkir;
        CardView cvMain;
        Button btnPilih, btnPilihKurir, btnDurasi;
        ImageView imgBarang;
        RelativeLayout divPilihKurir;
        LinearLayout divJenisKurir, divNoAlamat;
        RecyclerView rv;


        ViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tv_namabarang);
            tvNamaToko = v.findViewById(R.id.tv_namaToko);
            tvHarga = v.findViewById(R.id.tv_hargaBarang);
            tvBerat = v.findViewById(R.id.tv_beratBarang);
            cvMain = v.findViewById(R.id.cv_main);
            tvSubTotal = v.findViewById(R.id.tv_subTotal);
            tvCatatan = v.findViewById(R.id.tv_catatan);
            imgBarang = v.findViewById(R.id.img_barang);
            btnPilih = v.findViewById(R.id.btn_pilih);
            tvDurasi = v.findViewById(R.id.tv_durasiPengriman);
            tvKurir = v.findViewById(R.id.tv_kurir);
            tvOngkir = v.findViewById(R.id.tv_ongkir);
            _tvKurir = v.findViewById(R.id.tv_kurir);
            _tvOngkir = v.findViewById(R.id.tv_ongkir);
            btnPilihKurir = v.findViewById(R.id.btn_pilihKurir);
            btnDurasi = v.findViewById(R.id.btn_pilihDurasi);
            divJenisKurir = v.findViewById(R.id.div_jenisPengiriman);
            divPilihKurir = v.findViewById(R.id.div_pilihKurir);
            rv = v.findViewById(R.id.rv);
            divNoAlamat = v.findViewById(R.id.div_noAlamat);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        s = new SharedPref(context);
        final TbKranjangByToko t = daftarBarang.get(position);

        int sum = 0, hargaAwal, hargaSekarang;
        int berat = 0;
        final String sBerat;
        final int subTotal;
        final String[] _durasi = new String[1];

        // Get data dari Sqlite
        final ArrayList<TbKranjang> listBarang = new ArrayList<>();
        listBarang.addAll(Arrays.asList(db.daoKranjang().selectByIdTokoTerpilih(t.idToko, true)));

        // Display ListProduk by toko
        holder.rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        holder.rv.setLayoutManager(layoutManager);
        RecyclerView.Adapter adapter = new AdapterChekoutProduk(listBarang, context);
        holder.rv.setAdapter(adapter);

        // Cek Status Alamat
        alamat = s.getShipping();
        if (alamat == null) {
            holder.divPilihKurir.setVisibility(View.GONE);
            holder.divNoAlamat.setVisibility(View.VISIBLE);
        }

        CallFunction.setPesananActivity(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                holder.divPilihKurir.setVisibility(View.VISIBLE);
                holder.divNoAlamat.setVisibility(View.GONE);
                return null;
            }
        });

        // Set Value
        holder.tvNamaToko.setText(t.namaToko);
        int totalberat = 0;
        for (TbKranjang b : listBarang) {
            hargaSekarang = b.jumlahPesan * b.harga;
            sum = sum + hargaSekarang;
            berat = b.berat * b.jumlahPesan;
            totalberat = totalberat + berat;
        }

        subTotal = sum;
        sBerat = String.valueOf(totalberat);

        holder.tvSubTotal.setText("" + new Helper().convertRupiah(subTotal));

        holder.btnPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toasti.info(context, "Fungsi Dalam Pengembangan");
                final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.dialog_kurir, null);

                tvStatusKurir = (TextView) view.findViewById(R.id.tv_statusKurir);
                divNoAlamat = (LinearLayout) view.findViewById(R.id.div_noAlamat);
                divKurir = (LinearLayout) view.findViewById(R.id.div_kurir);
                spnKurir = (Spinner) view.findViewById(R.id.spn_kurir);
                tvKurir = (TextView) view.findViewById(R.id.tv_kurir);
                spnType = (Spinner) view.findViewById(R.id.spn_type);
                tvOngkir = (TextView) view.findViewById(R.id.tv_ongkir);

                List<String> shipping_list = new ArrayList<>();
                shipping_list.add("Pilih Kurir");
                shipping_list.add("JNE");
                shipping_list.add("TIKI");
                shipping_list.add("POS");
                shipping_list.add("J&T");

                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, shipping_list);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnKurir.setAdapter(adapter);
                spnKurir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, final int i, long id) {

                        int x = spnKurir.getSelectedItemPosition();
                        listHarga.removeAll(listHarga);
                        jasaPengiriman.removeAll(jasaPengiriman);
                        listJenisKurir.removeAll(listJenisKurir);
                        listHarga.removeAll(listHarga);
                        listTipe.removeAll(listTipe);
                        listDurasi.removeAll(listDurasi);
                        tvKurir.setText(spnKurir.getSelectedItem().toString());

                        if (x != 0) {

                            sKurir = spnKurir.getSelectedItem().toString().toLowerCase();

                            if (sKurir.equals("j&t")) {
                                sKurir = "jnt";
                            }

                            alamat = s.getShipping();
                            SweetAlert.onLoading(context);

                            api.getOngkir(Config.origin, "subdistrict", alamat.PE_ID_KECAMATAN, "subdistrict", sBerat, sKurir).enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    SweetAlert.dismis();
                                    if (response.isSuccessful()) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response.body().string());
                                            final JSONObject rajaongkir = jsonObject.optJSONObject("rajaongkir");
                                            final JSONObject status = rajaongkir.optJSONObject("status");
                                            int code = status.optInt("code");
                                            String description = status.optString("description");
                                            if (code == 200) {
                                                JSONArray jsonArray = rajaongkir.optJSONArray("results");
                                                JSONObject jsonObject1 = jsonArray.optJSONObject(0);
                                                agenKurir = jsonObject1.optString("code");
                                                JSONArray jsonArray1 = jsonObject1.optJSONArray("costs");

                                                jasaPengiriman.add("Pilih Jenis Pengiriman");
                                                listJenisKurir.add("Jenis kurir");
                                                listHarga.add("0");
                                                listTipe.add("type");
                                                listDurasi.add("durasi");

                                                for (int j = 0; j < jsonArray1.length(); j++) {
                                                    JSONObject jsonObject2 = jsonArray1.optJSONObject(j);
                                                    String tipe = jsonObject2.optString("service");
                                                    JSONArray jsonArray2 = jsonObject2.optJSONArray("cost");
                                                    JSONObject jsonObject3 = jsonArray2.optJSONObject(0);
                                                    String harga = jsonObject3.optString("value");
                                                    String lama = jsonObject3.optString("etd");

                                                    listHarga.add(harga);
                                                    listTipe.add(tipe);
                                                    if (lama == "" || lama.equals("") || lama == null) {
                                                        lama = "5-6";
                                                    }

                                                    listDurasi.add(lama);
                                                    listJenisKurir.add(agenKurir.toUpperCase() + " " + tipe);
                                                    jasaPengiriman.add(agenKurir.toUpperCase() + " " + tipe + " (" + lama + " hari kerja) - " + new Helper().convertRupiah(Integer.parseInt(harga)));

                                                }

                                                adapter.notifyDataSetChanged();
                                                ArrayAdapter<String> adpHarga = new ArrayAdapter<String>(context,
                                                        android.R.layout.simple_spinner_item, jasaPengiriman);
                                                adpHarga.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                                spnType.setAdapter(adpHarga);
                                                spnType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                    @Override
                                                    public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {

                                                        int x = spnType.getSelectedItemPosition();
                                                        tvOngkir.setText(spnType.getSelectedItem().toString());

                                                        if (x != 0) {
                                                            _durasi[0] = jasaPengiriman.get(x);
                                                            jenisKurir = listJenisKurir.get(x);
                                                            ongkir = Integer.valueOf(listHarga.get(x));
                                                            holder.tvOngkir.setText("" + new Helper().convertRupiah(ongkir));
                                                            holder.tvSubTotal.setText("" + new Helper().convertRupiah(ongkir + subTotal));
                                                            holder.tvKurir.setText(jenisKurir);

                                                            String durasi = listDurasi.get(x).replace("HARI", "");

                                                            holder.tvDurasi.setText("Durasi (" + durasi + " Hari Kerja)");
                                                            alertDialog.dismiss();
                                                            holder.divPilihKurir.setVisibility(View.GONE);
                                                            holder.divJenisKurir.setVisibility(View.VISIBLE);

                                                            for (TbKranjang b : listBarang) {
                                                                b.jenisKurir = jenisKurir;
                                                                b.kurir = sKurir;
                                                                b.ongkir = ongkir;
                                                                b.durasiPengiriman = durasi;
                                                                db.daoKranjang().updateBarang(b);
                                                            }
                                                            CallFunction.getrefrashChekout();
                                                        }
                                                    }

                                                    @Override
                                                    public void onNothingSelected(AdapterView<?> parent) {

                                                    }
                                                });
                                            } else {

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    SweetAlert.dismis();
                                    SweetAlert.onFailure(context, t.getMessage());
                                }
                            });
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                alertDialog.setView(view);
                alertDialog.setCancelable(false);
                alertDialog.show();

            }
        });

        holder.btnDurasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.dialog_durasi, null);
                spnType = (Spinner) view.findViewById(R.id.spn_type);
                tvOngkir = (TextView) view.findViewById(R.id.tv_ongkir);

                ArrayAdapter<String> adpHarga = new ArrayAdapter<String>(context,
                        android.R.layout.simple_spinner_item, jasaPengiriman);
                adpHarga.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnType.setAdapter(adpHarga);
                spnType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {

                        int x = spnType.getSelectedItemPosition();
                        tvOngkir.setText(_durasi[0]);

                        if (x != 0) {
                            jenisKurir = listJenisKurir.get(x);
                            ongkir = Integer.valueOf(listHarga.get(x));
                            holder.tvOngkir.setText("" + new Helper().convertRupiah(ongkir));
                            holder.tvSubTotal.setText("" + new Helper().convertRupiah(ongkir + subTotal));
                            holder.tvKurir.setText(jenisKurir);

                            String durasi = listDurasi.get(x).replace("HARI", "");
                            holder.tvDurasi.setText("Durasi (" + durasi + " Hari Kerja)");

                            alertDialog.dismiss();
                            holder.divPilihKurir.setVisibility(View.GONE);
                            holder.divJenisKurir.setVisibility(View.VISIBLE);

                            for (TbKranjang b : listBarang) {
                                b.jenisKurir = jenisKurir;
                                b.kurir = sKurir;
                                b.ongkir = ongkir;
                                b.durasiPengiriman = durasi;
                                db.daoKranjang().updateBarang(b);
                            }
                            CallFunction.getrefrashChekout();

                        } else {
                            tvOngkir.setText(_durasi[0]);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                alertDialog.setView(view);
                alertDialog.setCancelable(true);
                alertDialog.show();
            }
        });

        holder.btnPilihKurir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                Toasti.info(context, "Fungsi Dalam Pengembangan");
                final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.dialog_kurir, null);

                tvStatusKurir = (TextView) view.findViewById(R.id.tv_statusKurir);
                divNoAlamat = (LinearLayout) view.findViewById(R.id.div_noAlamat);
                divKurir = (LinearLayout) view.findViewById(R.id.div_kurir);
                spnKurir = (Spinner) view.findViewById(R.id.spn_kurir);
                tvKurir = (TextView) view.findViewById(R.id.tv_kurir);
                spnType = (Spinner) view.findViewById(R.id.spn_type);
                tvOngkir = (TextView) view.findViewById(R.id.tv_ongkir);

                List<String> shipping_list = new ArrayList<>();
                shipping_list.add("Pilih Kurir");
                shipping_list.add("JNE");
                shipping_list.add("TIKI");
                shipping_list.add("POS");
                shipping_list.add("J&T");

                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, shipping_list);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnKurir.setAdapter(adapter);
                spnKurir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, final int i, long id) {

                        int x = spnKurir.getSelectedItemPosition();
                        listHarga.removeAll(listHarga);
                        jasaPengiriman.removeAll(jasaPengiriman);
                        listJenisKurir.removeAll(listJenisKurir);
                        listHarga.removeAll(listHarga);
                        listTipe.removeAll(listTipe);
                        listDurasi.removeAll(listDurasi);
                        tvKurir.setText(spnKurir.getSelectedItem().toString());

                        if (x != 0) {

                            sKurir = spnKurir.getSelectedItem().toString().toLowerCase();

                            if (sKurir.equals("j&t")) {
                                sKurir = "jnt";
                            }

                            SweetAlert.onLoading(context);
                            api.getOngkir(Config.origin, "subdistrict", alamat.PE_ID_KECAMATAN, "subdistrict", sBerat, sKurir).enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                    SweetAlert.dismis();
                                    if (response.isSuccessful()) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response.body().string());
                                            final JSONObject rajaongkir = jsonObject.optJSONObject("rajaongkir");
                                            final JSONObject status = rajaongkir.optJSONObject("status");
                                            int code = status.optInt("code");
                                            String description = status.optString("description");
                                            if (code == 200) {
                                                JSONArray jsonArray = rajaongkir.optJSONArray("results");
                                                JSONObject jsonObject1 = jsonArray.optJSONObject(0);
                                                agenKurir = jsonObject1.optString("code");
                                                JSONArray jsonArray1 = jsonObject1.optJSONArray("costs");

                                                jasaPengiriman.add("Pilih Jenis Pengiriman");
                                                listJenisKurir.add("Jenis kurir");
                                                listHarga.add("0");
                                                listTipe.add("type");
                                                listDurasi.add("durasi");

                                                for (int j = 0; j < jsonArray1.length(); j++) {
                                                    JSONObject jsonObject2 = jsonArray1.optJSONObject(j);
                                                    String tipe = jsonObject2.optString("service");
                                                    JSONArray jsonArray2 = jsonObject2.optJSONArray("cost");
                                                    JSONObject jsonObject3 = jsonArray2.optJSONObject(0);
                                                    String harga = jsonObject3.optString("value");
                                                    String lama = jsonObject3.optString("etd");

                                                    listHarga.add(harga);
                                                    listTipe.add(tipe);
                                                    if (lama == "" || lama.equals("") || lama == null) {
                                                        lama = "5-6";
                                                    }

                                                    listDurasi.add(lama);
                                                    listJenisKurir.add(agenKurir.toUpperCase() + " " + tipe);
                                                    jasaPengiriman.add(agenKurir.toUpperCase() + " " + tipe + " (" + lama + " hari kerja) - " + new Helper().convertRupiah(Integer.parseInt(harga)));

                                                }

                                                adapter.notifyDataSetChanged();
                                                ArrayAdapter<String> adpHarga = new ArrayAdapter<String>(context,
                                                        android.R.layout.simple_spinner_item, jasaPengiriman);
                                                adpHarga.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                                spnType.setAdapter(adpHarga);
                                                spnType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                    @Override
                                                    public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {

                                                        int x = spnType.getSelectedItemPosition();
                                                        tvOngkir.setText(spnType.getSelectedItem().toString());

                                                        if (x != 0) {
                                                            _durasi[0] = jasaPengiriman.get(x);
                                                            jenisKurir = listJenisKurir.get(x);
                                                            ongkir = Integer.valueOf(listHarga.get(x));
                                                            holder.tvOngkir.setText("" + new Helper().convertRupiah(ongkir));
                                                            holder.tvSubTotal.setText("" + new Helper().convertRupiah(ongkir + subTotal));
                                                            holder.tvKurir.setText(jenisKurir);

                                                            String durasi = listDurasi.get(x).replace("HARI", "");

                                                            holder.tvDurasi.setText("Durasi (" + durasi + " Hari Kerja)");
                                                            alertDialog.dismiss();
                                                            holder.divPilihKurir.setVisibility(View.GONE);
                                                            holder.divJenisKurir.setVisibility(View.VISIBLE);

                                                            for (TbKranjang b : listBarang) {
                                                                b.jenisKurir = jenisKurir;
                                                                b.kurir = sKurir;
                                                                b.ongkir = ongkir;
                                                                b.durasiPengiriman = durasi;
                                                                db.daoKranjang().updateBarang(b);
                                                            }
                                                            CallFunction.getrefrashChekout();

                                                        }
                                                    }

                                                    @Override
                                                    public void onNothingSelected(AdapterView<?> parent) {

                                                    }
                                                });
                                            } else {

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    SweetAlert.dismis();
                                    SweetAlert.onFailure(context, t.getMessage());
                                }
                            });
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                alertDialog.setView(view);
                alertDialog.setCancelable(false);
                alertDialog.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return daftarBarang.size();
    }
}



