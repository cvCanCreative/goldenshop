package id.co.cancreative.goldenshop.Adapter;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.TbKranjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.AppDbKrenjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;

public class AdapterChekoutProduk extends RecyclerView.Adapter<AdapterChekoutProduk.ViewHolder> {

    private ArrayList<TbKranjang> daftarBarang;
    private Context context;
    private AppDbKrenjang db;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private ModelAlamat alamat;
    private SharedPref s;

    public AdapterChekoutProduk(ArrayList<TbKranjang> barangs, Context ctx) {
        daftarBarang = barangs;
        context = ctx;

        db = Room.databaseBuilder(context.getApplicationContext(),
                AppDbKrenjang.class, Config.db_keranjang).allowMainThreadQueries().build();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkout_produk, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvHarga, tvBerat;
        ImageView imgBarang;


        ViewHolder(View v) {
            super(v);
            imgBarang = v.findViewById(R.id.img_barang);
            tvTitle = v.findViewById(R.id.tv_namabarang);
            tvBerat = v.findViewById(R.id.tv_beratBarang);
            tvHarga = v.findViewById(R.id.tv_hargaBarang);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TbKranjang t = daftarBarang.get(position);
        final String gambar = t.gambar;
        final int harga = t.harga;

        double weight = (double) t.berat * (double) t.jumlahPesan;
        String satuan = "gr";
        if (weight > 1000) {
            weight = weight / 1000;
            satuan = "kg";
        }
        NumberFormat nf = new DecimalFormat("#.#");
        String berat = nf.format(weight).replace(".0", "");

        holder.tvBerat.setText("" + t.jumlahPesan + " barang (" + berat + satuan + ")");
        holder.tvTitle.setText(t.nama);
        holder.tvHarga.setText(new Helper().convertRupiah(Integer.valueOf(harga)));

        if (gambar != null && !gambar.isEmpty()) {

            String gb = gambar.replace("|", " ");
            String strArray[] = gb.split(" ");

            Picasso.with(context)
                    .load(Config.URL_produkGolden + strArray[0])
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.img_kosong)
                    .noFade()
                    .into(holder.imgBarang);
        } else {
            holder.imgBarang.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.img_kosong));
        }

    }

    @Override
    public int getItemCount() {
        return daftarBarang.size();
    }
}



