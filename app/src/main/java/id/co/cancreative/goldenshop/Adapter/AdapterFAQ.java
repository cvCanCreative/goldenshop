package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Model.Faq;
import id.co.cancreative.goldenshop.Model.ModelFAQ;
import id.co.cancreative.goldenshop.R;


/**
 * Created by CAN Creative on 29/05/2018.
 */

public class AdapterFAQ extends RecyclerView.Adapter<AdapterFAQ.KotaHolder> {
    Activity context;
    private ArrayList<Faq> data;

    public AdapterFAQ(ArrayList<Faq> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public AdapterFAQ.KotaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_faq, parent, false);
        return new AdapterFAQ.KotaHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterFAQ.KotaHolder holder, final int position) {
        holder.judul.setText(data.get(position).FAQ_JUDUL);
        holder.info.setText(Html.fromHtml(data.get(position).FAQ_TEXT));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class KotaHolder extends RecyclerView.ViewHolder {
        public TextView judul, info;
        public LinearLayout divfaq, details_layout;
        public ImageView  load_details;
        public boolean sa = false;
        public KotaHolder(View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.judul_faq);
            info = itemView.findViewById(R.id.info_faq);
            load_details = itemView.findViewById(R.id.see_deatails);
            details_layout = itemView.findViewById(R.id.layout_detail);
            divfaq = itemView.findViewById(R.id.divfaq);
            details_layout.setVisibility(View.GONE);

            load_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sa == false){
                        details_layout.setVisibility(View.VISIBLE);
                        load_details.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                        sa = true;
                    }else {
                        details_layout.setVisibility(View.GONE);
                        load_details.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                        sa = false;
                    }
                }
            });

            divfaq.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sa == false){
                        details_layout.setVisibility(View.VISIBLE);
                        load_details.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                        sa = true;
                    }else {
                        details_layout.setVisibility(View.GONE);
                        load_details.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                        sa = false;
                    }
                }
            });


        }
    }
}
