package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Activity.DetailProdukActivity;
import id.co.cancreative.goldenshop.Activity.EditProfileUserActivity;
import id.co.cancreative.goldenshop.Activity.TambahProdukActivity;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SaveData;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.GambarProduk;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.R;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class AdapterGambarP extends RecyclerView.Adapter<AdapterGambarP.Holdr> {

    ArrayList<String> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterGambarP(ArrayList<String> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_gambar, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterGambarP.Holdr holder, final int i) {
        s = new SharedPref(context);

        final String a = data.get(i);
//        holder.nama.setText(a);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a.equals("")) {
                    Crop.pickImage(context);
//                EasyImage.openGallery(context, 1);
                    data.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    notifyItemRangeChanged(holder.getAdapterPosition(), data.size());
                }
            }
        });

        if (!a.equals("")) {
            Picasso.with(context)
                    .load(Config.URL_produkGolden + a)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.image_error)
                    .noFade()
                    .into(holder.image);
            holder.btnClose.setVisibility(View.VISIBLE);
        } else {
            Picasso.with(context)
                    .load(R.drawable.tambahproduk)
                    .placeholder(R.drawable.tambahproduk)
                    .error(R.drawable.image_error)
                    .noFade()
                    .into(holder.image);
            holder.btnClose.setVisibility(View.GONE);
        }

        holder.btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
                notifyItemRangeChanged(holder.getAdapterPosition(), data.size());
                Log.d("Test", "" + holder.getAdapterPosition() + " : " + data.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (data.size() > 5) {
            b = 5;
        } else {
            b = data.size();
        }
        return b;
    }

    public class Holdr extends RecyclerView.ViewHolder {
        ImageView image, btnClose;
        TextView discount, harga, nama;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_produk);
            btnClose = (ImageView) itemView.findViewById(R.id.btn_close);
//            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            layout = itemView.findViewById(R.id.div_layout);
        }
    }
}
