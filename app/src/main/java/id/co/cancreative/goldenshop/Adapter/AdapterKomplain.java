package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.FAQActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.IjinAplikasi;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.Komplain;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterKomplain extends RecyclerView.Adapter<AdapterKomplain.Holdr> {

    ArrayList<Komplain> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterKomplain(ArrayList<Komplain> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_history_komplain, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterKomplain.Holdr holder, final int i) {
        s = new SharedPref(context);
        final Komplain a = data.get(i);

        holder.tglTransaksi.setText("" + new Helper().convertDateTimeToDate(a.CREATED_AT, "yyyy-MM-dd hh:mm:s"));

        holder.tvJudul.setText(a.KOM_JUDUL);
        holder.tvDeskripis.setText(a.KOM_DETAIL);
        holder.tvNamaToko.setText(a.toko.TOK_NAME);
        holder.tvPhone.setText(a.user.US_TELP);
        holder.tvEmail.setText(a.user.US_EMAIL);

        holder.btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IjinAplikasi.mintsijintelepon(context)) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + a.user.US_TELP));
                    context.startActivity(intent);
                }
            }
        });

        if (a.transaksi.TS_STATUS.equals("TERIMA")) {
            holder.tvStatus.setText("Komplain Selesai");
            holder.layout.setBackgroundResource(R.color.diterima);
            holder.btnCancel.setVisibility(View.GONE);
        }

        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Selesaikan Komplain?")
                        .setContentText("Anda akan yakin ingin Menyelesaikan Komplain")
                        .setCancelText("Tidak")
                        .setConfirmText("Selesaikan")
                        .showCancelButton(true)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                selesaikanTransaksi(a.transaksi.ID_TRANSAKSI);
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }
        });

    }

    private void selesaikanTransaksi(String id) {
        SweetAlert.onLoading(context);
        ApiService api = ApiConfig.getInstanceRetrofit();
        api.transaksiSelesai(s.getApi(), s.getIdUser(), s.getSession(), id).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1){
                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Pembatalan Berhasil")
                            .setContentText("" + response.body().message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    CallFunction.getrefrashSaldo();
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(context, t.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView tvStatus, tglTransaksi, tvJudul, tvDeskripis, btnCancel;
        TextView tvNamaToko;
        TextView tvPhone;
        TextView tvEmail;
        RelativeLayout layout, btn_call;

        public Holdr(final View itemView) {
            super(itemView);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            tvJudul = (TextView) itemView.findViewById(R.id.tv_judul);
            tglTransaksi = (TextView) itemView.findViewById(R.id.tv_tglTansaksi);
            tvDeskripis = (TextView) itemView.findViewById(R.id.tv_deskripsi);
            btnCancel = (TextView) itemView.findViewById(R.id.btn_cancel);
            tvNamaToko = (TextView) itemView.findViewById(R.id.tv_namaToko);
            tvPhone = (TextView) itemView.findViewById(R.id.tv_noTlp);
            tvEmail = (TextView) itemView.findViewById(R.id.tv_email);
            layout = itemView.findViewById(R.id.layout_status);
            btn_call = itemView.findViewById(R.id.btn_call);
        }
    }
}


