package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.AppDbKrenjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.AppDbKrenjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;

public class AdapterKranjang extends RecyclerView.Adapter<AdapterKranjang.ViewHolder> {

    private ArrayList<TbKranjang> daftarBarang;
    private Context context;
    private AppDbKrenjang db;
    private AppDbKrenjangByToko dbToko;


    public AdapterKranjang(ArrayList<TbKranjang> barangs, Context ctx) {
        daftarBarang = barangs;
        context = ctx;

        db = Room.databaseBuilder(context.getApplicationContext(),
                AppDbKrenjang.class, Config.db_keranjang).allowMainThreadQueries().build();

        dbToko = Room.databaseBuilder(context.getApplicationContext(),
                AppDbKrenjangByToko.class, Config.db_keranjangToko).allowMainThreadQueries().build();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_barang, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvHarga, btnUbah, tvCatatan, btnSimpan, edtJumlahPesan, tvNamaToko;
        EditText edtCatatan;
        CardView cvMain;
        ImageView btnDelete, btnTambahJmlPesan, btnKurangiJmlPesan, imgBarang;
        LinearLayout lyCatatan, lyAddCatatan;
        RelativeLayout lyedtCatatan;
        CheckBox checkBox, checkBox1;


        ViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tv_namabarang);
            tvHarga = v.findViewById(R.id.tv_hargaBarang);
            cvMain = v.findViewById(R.id.cv_main);
            btnDelete = v.findViewById(R.id.btn_delete);
            btnTambahJmlPesan = (ImageView) v.findViewById(R.id.btn_tambahJmlPesan);
            btnKurangiJmlPesan = (ImageView) v.findViewById(R.id.btn_kurangiJmlPesan);
            imgBarang = (ImageView) v.findViewById(R.id.img_barang);
            edtJumlahPesan = (TextView) v.findViewById(R.id.edt_jumlahPesan);
            lyCatatan = v.findViewById(R.id.ly_catatan);
            lyAddCatatan = v.findViewById(R.id.ly_addCatatan);
            lyedtCatatan = v.findViewById(R.id.ly_edtCatatan);
            btnUbah = v.findViewById(R.id.btn_edit);
            edtCatatan = v.findViewById(R.id.edt_catatan);
            tvCatatan = v.findViewById(R.id.tv_catatan);
            btnSimpan = v.findViewById(R.id.btn_simpan);
            checkBox = v.findViewById(R.id.check_box);
            checkBox1 = v.findViewById(R.id.check_box1);
            tvNamaToko = v.findViewById(R.id.tv_namaToko);

        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        /**
         *  Menampilkan data pada view
         */

        final TbKranjang t = daftarBarang.get(position);
        final String name = t.nama;
        final String gambar = t.gambar;
        final String catatan = t.catatan;
        final int harga = t.harga;
        final int jumlahBarang = t.jumlahPesan;
        final int[] jumlahPesan = {jumlahBarang};
        final int[] nilai = {1};

//        final int[] hargaAwal = {PenjumlahanKeranjang.getHargabarangAwal()};
//        final int[] hargaSekarang = {PenjumlahanKeranjang.getHargabarangSekarang()};

        final TbKranjang barang = null;

        if (t.pilih == true){
            holder.checkBox.setChecked(true);
            holder.checkBox1.setChecked(true);
        }

        if (t.jumlahPesan >= 2){
            Glide.with(context).load(R.drawable.ic_kurang)
                    .into(holder.btnKurangiJmlPesan);
        }

        if (!catatan.isEmpty()) {
            holder.lyCatatan.setVisibility(View.VISIBLE);
            holder.lyAddCatatan.setVisibility(View.GONE);
            holder.edtCatatan.setText(catatan);
            holder.tvCatatan.setText(catatan);
        }

        holder.btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.lyCatatan.setVisibility(View.GONE);
                holder.lyedtCatatan.setVisibility(View.VISIBLE);
            }
        });

        holder.lyAddCatatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.lyAddCatatan.setVisibility(View.GONE);
                holder.lyedtCatatan.setVisibility(View.VISIBLE);
            }
        });

        holder.btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.lyedtCatatan.setVisibility(View.GONE);
                holder.lyCatatan.setVisibility(View.VISIBLE);
                holder.tvCatatan.setText(holder.edtCatatan.getText().toString());




                t.catatan = holder.edtCatatan.getText().toString();
                upadateJumlah(position);
            }
        });


        if (gambar != null && !gambar.isEmpty()) {

            String gb = gambar.replace("|", " ");
            String strArray[] = gb.split(" ");

            Picasso.with(context)
                    .load(Config.URL_produkGolden + strArray[0])
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.img_kosong)
                    .noFade()
                    .into(holder.imgBarang);
        } else {
            holder.imgBarang.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.img_kosong));
        }


        holder.tvTitle.setText(name);
        holder.tvHarga.setText(new Helper().convertRupiah(Integer.valueOf(harga)));
        holder.edtJumlahPesan.setText(String.valueOf(jumlahBarang));
        holder.tvNamaToko.setText("Penjual: "+t.penjual);

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                pDialog.setTitleText("Hapus Barang Terpilih")
                        .setContentText("Barang yang terpilih akan dihapus dari keranjang!")
                        .setCancelText("Batal")
                        .setConfirmText("Hapus")
                        .showCancelButton(true)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                pDialog.dismissWithAnimation();

                                // Jika Product pada Keranjang Toko sisa satu, Hapus Kranjang Toko
                                final ArrayList<TbKranjang> listBarang = new ArrayList<>();
                                listBarang.addAll(Arrays.asList(db.daoKranjang().selectByIdToko(t.idToko)));
                                if (listBarang.size() == 1){
                                    dbToko.daoKranjang().deleteById(t.idToko);
                                    CallFunction.getdeleteKranjangToko();
                                }

                                // Hapus Produk pada keranjang
                                onDeteleBarang(position);

                            }
                        });
                pDialog.show();
            }
        });

        holder.btnTambahJmlPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jumlahPesan[0] == 1) {
                    Glide.with(context).load(R.drawable.ic_kurang)
                            .into(holder.btnKurangiJmlPesan);
                }

                jumlahPesan[0]++;
                holder.edtJumlahPesan.setText(Integer.toString(jumlahPesan[0]));

                //Update Jumlah Barang Sekarang
                t.jumlahPesan = jumlahPesan[0];
                upadateJumlah(position);

                CallFunction.gethitungUlangKeranjang();
            }
        });

        holder.btnKurangiJmlPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!(jumlahPesan[0] <= 1)) {
                    jumlahPesan[0]--;
                    String jumlah = Integer.toString(jumlahPesan[0]);

                    //Update Jumlah Barang Sekarang
                    t.jumlahPesan =jumlahPesan[0];
                    upadateJumlah(position);
                    holder.edtJumlahPesan.setText(jumlah);

                    CallFunction.gethitungUlangKeranjang();
                }

                if (jumlahPesan[0] == 1) {
                    Glide.with(context).load(R.drawable.ic_kurang_disable)
                            .into(holder.btnKurangiJmlPesan);
                }

            }
        });

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    t.pilih = true;
                    upadateJumlah(position);
                    CallFunction.gethitungUlangKeranjang();
                    holder.checkBox1.setChecked(true);
                }else {
                    t.pilih = false;
                    upadateJumlah(position);
                    CallFunction.gethitungUlangKeranjang();
                    holder.checkBox1.setChecked(false);
                }
            }
        });

        holder.checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    t.pilih = true;
                    upadateJumlah(position);
                    CallFunction.gethitungUlangKeranjang();
                    holder.checkBox.setChecked(true);
                }else {
                    t.pilih = false;
                    upadateJumlah(position);
                    CallFunction.gethitungUlangKeranjang();
                    holder.checkBox.setChecked(false);
                }
            }
        });

//
//        CallFunction.setcheckBoxCart(new Callable<Void>() {
//            @Override
//            public Void call() throws Exception {
//                t.pilih = true;
//                upadateJumlah(position);
//                CallFunction.gethitungUlangKeranjang();
//                holder.checkBox.setChecked(true);
//                holder.checkBox1.setChecked(true);
//                return null;
//            }
//        });
//
//        CallFunction.setuncheckBoxCart(new Callable<Void>() {
//            @Override
//            public Void call() throws Exception {
//                t.pilih = true;
//                upadateJumlah(position);
//                CallFunction.gethitungUlangKeranjang();
//                holder.checkBox.setChecked(false);
//                holder.checkBox1.setChecked(false);
//                return null;
//            }
//        });
    }

    void upadateJumlah(int posisi) {
        db.daoKranjang().updateBarang(daftarBarang.get(posisi));
    }

    private void onDeteleBarang(int position) {
        db.daoKranjang().deleteBarang(daftarBarang.get(position));
        daftarBarang.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeRemoved(position, daftarBarang.size());

        CallFunction.getRefreshKeranjang();
    }

    @Override
    public int getItemCount() {
        return daftarBarang.size();
    }
}


