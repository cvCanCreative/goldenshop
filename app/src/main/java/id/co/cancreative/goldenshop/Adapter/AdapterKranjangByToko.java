package id.co.cancreative.goldenshop.Adapter;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.AppDbKrenjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.TbKranjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.AppDbKrenjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;

public class AdapterKranjangByToko extends RecyclerView.Adapter<AdapterKranjangByToko.ViewHolder> {

    private ArrayList<TbKranjangByToko> daftarBarang;
    private Context context;
    private AppDbKrenjang db;
    private AppDbKrenjangByToko dbToko;


    public AdapterKranjangByToko(ArrayList<TbKranjangByToko> barangs, Context ctx) {
        daftarBarang = barangs;
        context = ctx;

        db = Room.databaseBuilder(context.getApplicationContext(),
                AppDbKrenjang.class, Config.db_keranjang).allowMainThreadQueries().build();
        dbToko = Room.databaseBuilder(context.getApplicationContext(),
                AppDbKrenjangByToko.class, Config.db_keranjangToko).allowMainThreadQueries().build();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_barang_by_toko, parent, false);
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNamaToko;
        CheckBox checkBox;
        RecyclerView rv;

        ViewHolder(View v) {
            super(v);
            checkBox = v.findViewById(R.id.check_box);
            tvNamaToko = v.findViewById(R.id.tv_namaToko);
            rv = v.findViewById(R.id.rv);

        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TbKranjangByToko t = daftarBarang.get(position);
        final ArrayList<TbKranjang> listBarang = new ArrayList<>();
        String status = "";

        listBarang.addAll(Arrays.asList(db.daoKranjang().selectByIdToko(t.idToko)));

        holder.rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        holder.rv.setLayoutManager(layoutManager);
        RecyclerView.Adapter adapter = new AdapterKranjang(listBarang, context);
        holder.rv.setAdapter(adapter);
        holder.tvNamaToko.setText(t.namaToko);

        if (listBarang.size() == 1){
            CallFunction.setdeleteKranjangToko(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    onDeteleBarang();
                    return null;
                }
            });
        }

        for (TbKranjang c : listBarang){
            status = status + c.pilih;
        }

        if (!status.contains("false")){
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    for (TbKranjang d : listBarang) {
                        d.pilih = true;
                        updateBarang(d);
                    }
                } else {
                    for (TbKranjang d : listBarang) {
                        d.pilih = false;
                        updateBarang(d);
                    }
                    CallFunction.gethitungUlangKeranjang();
                }
            }
        });

    }

    @SuppressLint("StaticFieldLeak")
    private void updateBarang(final TbKranjang barang) {
        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
//                AppDbKrenjang db;
//                db = Room.databaseBuilder(getActivity(),
//                        AppDbKrenjang.class, Config.db_keranjang)
//                        .build();
                long status = db.daoKranjang().updateBarang(barang);
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
                CallFunction.getRefreshKeranjang();
            }
        }.execute();
    }

    private void onDeteleBarang() {
        CallFunction.getRefreshKeranjang();
    }

    @Override
    public int getItemCount() {
        return daftarBarang.size();
    }
}


