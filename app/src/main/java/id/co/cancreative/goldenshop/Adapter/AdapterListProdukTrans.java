package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.TransaksiDetail;
import id.co.cancreative.goldenshop.R;

public class AdapterListProdukTrans extends RecyclerView.Adapter<AdapterListProdukTrans.Holdr> {

    ArrayList<TransaksiDetail> data;
    Activity context;
    int b;
    SharedPref s;


    public AdapterListProdukTrans(ArrayList<TransaksiDetail> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_produk_trans, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final Holdr holder, final int i) {
        s = new SharedPref(context);
        final TransaksiDetail a = data.get(i);
        final Produk p = a.barang;

        holder.tvNamaProduk.setText("" + a.barang.BA_NAME);
        int qt = Integer.parseInt(a.TSD_QTY);
        int berat = Integer.parseInt(a.barang.BA_WEIGHT);

        int nilai = qt * berat;
        float kg = nilai / 1000;

        holder.tvjumlahBeli.setText("" + a.TSD_QTY + " barang - " + kg + " kg");

        String image = new Helper().splitText(a.barang.BA_IMAGE);

        Picasso.with(context)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat1)
                .noFade()
                .into(holder.imageProduk);

        int hargaBar = Integer.parseInt(a.barang.BA_PRICE);
        int tot = hargaBar * qt;
        holder.tvTotalBayar.setText("" + new Helper().convertRupiah(tot));
        holder.tvKurir.setText(a.TSD_KURIR.toUpperCase() + " - " + a.TSD_JENIS_KURIR);
        holder.tvPenerima.setText(a.alamat_kirim.PE_NAMA);
        holder.tvPhone.setText(a.alamat_kirim.PE_TELP);
        holder.tvAlamat.setText(a.alamat_kirim.PE_ALAMAT + "\n"
                + a.alamat_kirim.PE_KECAMATAN + "\n"
                + a.alamat_kirim.PE_PROVINSI + "\n"
                + a.alamat_kirim.PE_KODE_POS);


//        holder.tvKode.setText(a.TS_KODE_TRX);
//        holder.tglBelanja.setText(""+new Helper().convertDateTimeToDate(a.CREATED_AT, "yyyy-MM-dd hh:mm:s"));

//        String image = new Helper().splitText(a.transaksi_detail.get(0).barang.BA_IMAGE);
//        Picasso.with(context)
//                .load(Config.URL_produkGolden+image)
//                .placeholder(R.drawable.ic_cat1)
//                .error(R.drawable.ic_cat1)
//                .noFade()
//                .into(holder.imageProduk);
//
//        int totalBayar = 0;
//        int total = 0;
//        for (TransaksiDetail t : a.transaksi_detail){
//            totalBayar = totalBayar + Integer.valueOf(t.TSD_HARGA_TOTAL);
//            total = totalBayar + Integer.valueOf(t.TSD_ONGKIR);
//        }
//        holder.tvTotalBayar.setText(""+new Helper().convertRupiah(total));


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class Holdr extends RecyclerView.ViewHolder {
        TextView tvStatus, tglBelanja, tvKode, tvNamaProduk, tvTotalBayar;
        ImageView imageProduk, btn_menu;
        LinearLayout layout;
        private ImageView imgProduk;
        private TextView tvPenerima;
        private TextView tvPhone;
        private TextView tvAlamat, tvjumlahBeli, tvKurir;

        public Holdr(final View itemView) {
            super(itemView);
//            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
//            tvKode = (TextView) itemView.findViewById(R.id.tv_kode);
//            tglBelanja = (TextView) itemView.findViewById(R.id.tv_tglBelanja);
            tvNamaProduk = (TextView) itemView.findViewById(R.id.tv_namaProduk);
            tvTotalBayar = (TextView) itemView.findViewById(R.id.tv_totalBayar);
            imageProduk = itemView.findViewById(R.id.img_produk);
            tvjumlahBeli = itemView.findViewById(R.id.tv_jumlah_beli);
//            btn_menu = itemView.findViewById(R.id.btn_menu);
            imgProduk = (ImageView) itemView.findViewById(R.id.img_produk);
            tvPenerima = (TextView) itemView.findViewById(R.id.tv_penerima);
            tvPhone = (TextView) itemView.findViewById(R.id.tv_phone);
            tvAlamat = (TextView) itemView.findViewById(R.id.tv_alamat);
            tvKurir = (TextView) itemView.findViewById(R.id.tv_kurir);
        }
    }
}

