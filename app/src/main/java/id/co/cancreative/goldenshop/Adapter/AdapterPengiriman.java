package id.co.cancreative.goldenshop.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Model.Pengiriman;
import id.co.cancreative.goldenshop.R;

public class AdapterPengiriman extends RecyclerView.Adapter<AdapterPengiriman.Holder> {

    Context context;
    List<Pengiriman> list;

    public AdapterPengiriman(Context context, List<Pengiriman> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cek_pengiriman, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.tvAlamat.setText(""+list.get(i).manifest_description);
        holder.tvTanggal.setText(""+new Helper().convertDateFormatPengiriman(list.get(i).manifest_date) + " "+list.get(i).manifest_time);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private TextView tvAlamat;
        private TextView tvTanggal;

        public Holder(@NonNull View itemView) {
            super(itemView);

            tvAlamat = (TextView) itemView.findViewById(R.id.tv_alamat);
            tvTanggal = (TextView) itemView.findViewById(R.id.tv_tanggal);
        }
    }
}
