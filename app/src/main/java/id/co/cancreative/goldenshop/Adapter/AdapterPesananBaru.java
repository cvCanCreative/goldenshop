package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.DetailTransaksiTokoActivity;
import id.co.cancreative.goldenshop.Activity.UploadPembayaranActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.Model.TransaksiDetail;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterPesananBaru extends RecyclerView.Adapter<AdapterPesananBaru.Holdr> {

    ArrayList<Transaksi> data;
    Activity context;
    int b;
    SharedPref s;
    Transaksi a;

    public AdapterPesananBaru(ArrayList<Transaksi> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_pesanan_baru, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final Holdr holder, final int i) {
        s = new SharedPref(context);
         a = data.get(i);

        String status = "Menunggu Konfirmasi";

        if (a.TS_STATUS.equals("PROSES_ADMIN")){
            status = "Menunggu Konfirmasi";
        }

        if (a.TS_STATUS.equals("PROSES_VENDOR")){
            status = "Pesanan Diproses";
        }

        if (a.TS_STATUS.equals("PROSES_KIRIM")){
            status = "Pesanan Dikirim";
        }

        if (a.TS_STATUS.equals("DITERIMA_USER")){
            status = "Pesanan Tiba";
        }

        if (a.TS_STATUS.equals("TERIMA")){
            status = "Pesanan Selesai";
        }

        holder.tvStatus.setText(status);
        holder.tvNamaProduk.setText(""+a.transaksi_detail.get(0).barang.BA_NAME);
        holder.tvKode.setText(a.TS_KODE_TRX);
        holder.tglBelanja.setText(""+new Helper().convertDateTimeToDate(a.CREATED_AT, "yyyy-MM-dd hh:mm:s"));

        String image = new Helper().splitText(a.transaksi_detail.get(0).barang.BA_IMAGE);
        Picasso.with(context)
                .load(Config.URL_produkGolden+image)
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat1)
                .noFade()
                .into(holder.imageProduk);

        int totalBayar = 0;
        int total = 0;
        for (TransaksiDetail t : a.transaksi_detail){
            totalBayar = totalBayar + Integer.valueOf(t.TSD_HARGA_TOTAL);
            total = totalBayar + Integer.valueOf(t.TSD_ONGKIR);
        }
        int totAll = total + Integer.parseInt(a.TS_KODE_UNIK);

        holder.tvTotalBayar.setText(""+new Helper().convertRupiah(totAll));

        holder.btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(context, holder.btn_menu);
                popupMenu.getMenuInflater().inflate(R.menu.menu_pesanan_baru, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_batal:
                                final ApiService api = ApiConfig.getInstanceRetrofit();

                                String sPesan = "Apakah Anda yakin ingin Membatalkan Transaksi Ini" +"?";
                                final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                                pDialog.setTitleText("Hapus")
                                        .setContentText(String.valueOf(Html.fromHtml(sPesan)))
                                        .setCancelText("Tidak")
                                        .setConfirmText("Iya,Batalkan")
                                        .showCancelButton(true)
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                pDialog.cancel();
                                            }
                                        })
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                                SweetAlert.onLoading(context);
                                                api.getBatalTransaksi(s.getApi(), s.getIdUser(), s.getSession(), a.TS_KODE_PAYMENT).enqueue(new Callback<ResponModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                                        SweetAlert.dismis();
                                                        if (response.body().success == 1){
                                                            sweetAlertDialog
                                                                    .setTitleText("Transaksi Dibatalkan")
                                                                    .setContentText("Transaksi anda telah kami Batalkan")
                                                                    .setConfirmText("OK")
                                                                    .showCancelButton(false)
                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sweet) {
                                                                            data.remove(i);
                                                                            notifyItemRemoved(i);
                                                                            notifyItemRangeRemoved(i, data.size());
                                                                            sweetAlertDialog.dismiss();
                                                                        }
                                                                    })
                                                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                        } else {
                                                            Toasti.error(context, response.body().message);
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponModel> call, Throwable t) {
                                                        pDialog.dismiss();
                                                        SweetAlert.dismis();
                                                        SweetAlert.onFailure(context, t.getMessage());
                                                    }
                                                });
                                            }
                                        })
                                        .show();
                                return true;
                            case R.id.item_detail:
                                s.setDataTransaksi(a);
                                Intent i = new Intent(context, DetailTransaksiTokoActivity.class);
                                context.startActivity(i);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView tvStatus, tglBelanja, tvKode, tvNamaProduk, tvTotalBayar;
        ImageView imageProduk, btn_menu;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            tvKode = (TextView) itemView.findViewById(R.id.tv_kode);
            tglBelanja = (TextView) itemView.findViewById(R.id.tv_tglBelanja);
            tvNamaProduk = (TextView) itemView.findViewById(R.id.tv_namaProduk);
            tvTotalBayar = (TextView) itemView.findViewById(R.id.tv_totalBayar);
            imageProduk = itemView.findViewById(R.id.img_produk);
            btn_menu = itemView.findViewById(R.id.btn_menu);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    s.setDataTransaksi(a);
                    Intent i = new Intent(context, DetailTransaksiTokoActivity.class);
                    context.startActivity(i);
                }
            });
        }
    }
}

