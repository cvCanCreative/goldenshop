package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;
import java.util.Random;

import id.co.cancreative.goldenshop.Activity.EditProdukActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SaveData;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterPilihProduk extends RecyclerView.Adapter<AdapterPilihProduk.Holdr> {

    ArrayList<Produk> data;
    Activity context;
    int b;
    SharedPref s;
    ApiService api = ApiConfig.getInstanceRetrofit();

    public AdapterPilihProduk(ArrayList<Produk> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_pilih_produk, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterPilihProduk.Holdr holder, final int i) {
        s = new SharedPref(context);

        final Produk a = data.get(i);
        final String image = new Helper().splitText(a.BA_IMAGE);
        final int min = 20;
        final int max = 80;
        final int random = new Random().nextInt((max - min) + 1) + min;

        holder.nama.setText("" + a.BA_NAME);
        holder.harga.setText("" + new Helper().convertRupiah(Integer.valueOf(a.BA_PRICE)));

        Glide.with(context)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .into(new GlideDrawableImageViewTarget(holder.image));

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.setIdSubKat(a.ID_SUB_KATEGORI);
                context.finish();
            }
        });

        CallFunction.getrefrashSaldo();

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        ImageView image;
        TextView discount, harga, nama, stok;
        LinearLayout layout;
        RelativeLayout card;

        public Holdr(final View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_produk);
            harga = (TextView) itemView.findViewById(R.id.tv_harga);
            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            card = itemView.findViewById(R.id.card);

        }
    }
}
