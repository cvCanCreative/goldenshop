package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.DaftarProdukActivity;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SaveData;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.ModelSubKat;
import id.co.cancreative.goldenshop.R;

public class AdapterPilihSubCategory extends RecyclerView.Adapter<AdapterPilihSubCategory.Holdr> {

    ArrayList<ModelSubKat> subKats = new ArrayList<>();
    ArrayList<ModelSubKat> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterPilihSubCategory(ArrayList<ModelSubKat> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pilih_kategori, viewGroup, false);
        return new Holdr(view);

//        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
//        View view = layoutInflater.inflate(R.layout.item_kategori, viewGroup, false);
//        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(AdapterPilihSubCategory.Holdr holder, final int i) {
        s = new SharedPref(context);

        final ModelSubKat a = data.get(i);

        holder.title.setText(a.getsKATNAME());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s.setString(SharedPref.ID_SubKategori, a.getiDSUBKATEGORI());
                s.setString(SharedPref.NAMA_SubKategori, a.getsKATNAME());
                SaveData.setIdSubKat(a.getiDSUBKATEGORI());
                SaveData.setNameSubKat(a.getsKATNAME());
                context.onBackPressed();
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView title, harga;
        LinearLayout layout;
        ImageView image;

        public Holdr(final View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_nama);
            layout = itemView.findViewById(R.id.div_layout);
        }
    }
}
