package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.co.cancreative.goldenshop.Activity.DetailProdukActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SaveData;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.R;

public class AdapterProdukCari extends RecyclerView.Adapter<AdapterProdukCari.Holdr> {

    List<Produk> data;
    Activity context;
    int b;
    SharedPref s;
    ApiService api = ApiConfig.getInstanceRetrofit();

    public AdapterProdukCari(List<Produk> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_cari_produk, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final Holdr holder, final int i) {
        s = new SharedPref(context);

        final Produk a = data.get(i);
        final String image = new Helper().splitText(a.BA_IMAGE);
        final int min = 20;
        final int max = 80;
        final int random = new Random().nextInt((max - min) + 1) + min;

        holder.nama.setText("" + a.BA_NAME);
        holder.harga.setText("" + new Helper().convertRupiah(Integer.valueOf(a.BA_PRICE)));

        Picasso.with(context)
                .load(Config.URL_produkGolden+image)
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat1)
                .noFade()
                .into(holder.image);

        holder.tv_terjual.setText(random + " Terjual");


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s.setProduk(data.get(i));
                Intent intent = new Intent(context, DetailProdukActivity.class);
                intent.putExtra("dataInten","biasa");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

//        CallFunction.getrefrashSaldo();

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        ImageView image;
        TextView  harga, nama, tv_terjual;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_produk);
            harga = (TextView) itemView.findViewById(R.id.tv_harga);
            tv_terjual = (TextView) itemView.findViewById(R.id.tv_terjual);
            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);

        }
    }
}
