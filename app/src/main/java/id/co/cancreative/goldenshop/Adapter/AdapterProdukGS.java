package id.co.cancreative.goldenshop.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import id.co.cancreative.goldenshop.Activity.DetailProdukActivity;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.TbTerakhirDilihat;

public class AdapterProdukGS extends RecyclerView.Adapter<AdapterProdukGS.Holdr> {

    ArrayList<Produk> data;
    Activity context;
    ArrayList<Integer> idBarang = new ArrayList<>();
    int b;
    SharedPref s;
    private ArrayList<TbTerakhirDilihat> daftarBarang;
    private AppDbTerakhirDilihat db;
    int index = 0;

    public AdapterProdukGS(ArrayList<Produk> data, Activity context,AppDbTerakhirDilihat db) {
        this.data = data;
        this.context = context;
        this.db = db;

        daftarBarang = new ArrayList<>();

        daftarBarang.addAll(Arrays.asList(db.daoTerakhirDilihat().selectTerakhirDilihat()));
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_produk, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(Holdr holder, final int i) {
        s = new SharedPref(context);

        final Produk a = data.get(i);
        final int min = 10;
        final int max = 50;
        final int random = new Random().nextInt((max - min) + 1) + min;
        int hargaAsli = Integer.valueOf(a.BA_PRICE);
        int diskon = (int) (hargaAsli * ((double) random / 100));
        int hargaAkhir = hargaAsli - diskon;

        holder.nama.setText(a.BA_NAME);
        holder.harga.setText(""+new Helper().convertRupiah(hargaAsli));

        if (a.promo_status == 1) {
            holder.harga.setPaintFlags(holder.harga.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvHargaDiscount.setVisibility(View.VISIBLE);
            int potongan = 5000;
            if (a.PR_POTONGAN != null && a.PR_STATUS.equals("ACTIVE")) {
                potongan = Integer.parseInt(a.PR_POTONGAN);
            } else {
                potongan = Integer.parseInt(a.promo_admin.get(0).PR_POTONGAN);
            }

            int discount = Integer.valueOf(a.BA_PRICE) - Integer.valueOf(potongan);

            if (discount < 0) discount = 0;
            holder.tvHargaDiscount.setText(new Helper().convertRupiah(discount));
        }

        String image = new Helper().splitText(a.BA_IMAGE);
        Picasso.with(context)
                .load(Config.URL_produkGolden+image)
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat1)
                // To fit image into imageView
//					.fit()
                // To prevent fade animation
                .noFade()
                .into(holder.image);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TbTerakhirDilihat b = new TbTerakhirDilihat();

                if (daftarBarang.size() == 0) {
                    setTerakhir(b, i);
                } else {
                    if (daftarBarang.size() >= 3) {
                        for (int j = 0; j < daftarBarang.size(); j++) {
                            idBarang.add(daftarBarang.get(j).idProduk);
                        }

                        if (idBarang.contains(Integer.parseInt(a.ID_BARANG))) {

                        } else {
                            index = daftarBarang.get(0).idProduk;
                            delete(index);
                            setTerakhir(b, i);
                        }
                    } else {
                        for (int j = 0; j < daftarBarang.size(); j++) {
                            idBarang.add(daftarBarang.get(j).idProduk);
                        }

                        if (idBarang.contains(Integer.parseInt(a.ID_BARANG))) {

                        } else {
                            setTerakhir(b, i);
                        }
                    }

                }

                s.setProduk(data.get(i));
                Intent intent = new Intent(context, DetailProdukActivity.class);
                intent.putExtra("dataInten","biasa");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    private void setTerakhir(TbTerakhirDilihat b, int posisi) {
        b.idProduk = Integer.valueOf(data.get(posisi).ID_BARANG);
        b.idToko = Integer.valueOf(data.get(posisi).ID_TOKO);
        b.nama = data.get(posisi).BA_NAME;
        b.id_kategori = data.get(posisi).ID_KATEGORI;
        b.nama_kategori = "";
        b.harga = Integer.valueOf(data.get(posisi).BA_PRICE);
        b.stok = Integer.valueOf(data.get(posisi).BA_STOCK);
        b.berat = Integer.valueOf(data.get(posisi).BA_WEIGHT);
        b.sku = data.get(posisi).BA_SKU;
        b.kondisi = data.get(posisi).BA_CONDITION;
        b.gambar = data.get(posisi).BA_IMAGE;
        b.penjual = data.get(posisi).TOK_NAME;
        b.deskripsi = data.get(posisi).BA_DESCRIPTION;
        b.kotaPenjual = data.get(posisi).TOK_KOTA;
        b.fotoPenjual = data.get(posisi).TOK_FOTO;
        b.kodePromo = data.get(posisi).PR_CODE;
        b.promoPotongan = data.get(posisi).PR_POTONGAN;
        b.promoMin = data.get(posisi).PR_MIN;
        b.promoStatus = data.get(posisi).PR_STATUS;
        b.promoExp = data.get(posisi).PR_EXPIRED;

        insertData(b);
    }

    @SuppressLint("StaticFieldLeak")
    private void insertData(final TbTerakhirDilihat barang) {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses insert data
                long status = db.daoTerakhirDilihat().insertTerakhirDilihat(barang);
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {


            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void delete(final int id) {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses Delete data
                db = Room.databaseBuilder(context,
                        AppDbTerakhirDilihat.class, Config.db_terakhirDilihat)
                        .build();

                long status = db.daoTerakhirDilihat().deleteTerakhirDilihat(id);

                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
//                Toast.makeText(context, "hapus", Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        ImageView image;
        TextView discount, harga, nama, tvHargaDiscount;
        LinearLayout layout;
        RelativeLayout divDiscount;

        public Holdr(final View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_produk);
            discount = (TextView) itemView.findViewById(R.id.tv_discount);
            harga = (TextView) itemView.findViewById(R.id.tv_harga);
            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            layout = itemView.findViewById(R.id.div_layout);
            divDiscount = itemView.findViewById(R.id.div_discount);
            tvHargaDiscount = itemView.findViewById(R.id.tv_hargaDiscount);
        }
    }
}
