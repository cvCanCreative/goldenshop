package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.Random;

import id.co.cancreative.goldenshop.Activity.DaftarPromoActivity;
import id.co.cancreative.goldenshop.Activity.EditProdukActivity;
import id.co.cancreative.goldenshop.Activity.TambahProdukActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Helper.Value;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterProdukToko extends RecyclerView.Adapter<AdapterProdukToko.Holdr> {

    ArrayList<Produk> data;
    Activity context;
    int b;
    SharedPref s;
    ApiService api = ApiConfig.getInstanceRetrofit();

    public AdapterProdukToko(ArrayList<Produk> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_produk_toko, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterProdukToko.Holdr holder, final int i) {
        s = new SharedPref(context);

        final Produk a = data.get(i);
        final String image = new Helper().splitText(a.BA_IMAGE);
        final int min = 20;
        final int max = 80;
        final int random = new Random().nextInt((max - min) + 1) + min;

        holder.nama.setText("" + a.BA_NAME);
        holder.stok.setText("" + a.BA_STOCK);
        holder.harga.setText("" + new Helper().convertRupiah(Integer.valueOf(a.BA_PRICE)));

        Glide.with(context)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .into(new GlideDrawableImageViewTarget(holder.image));

        holder.btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(context, holder.btn_menu);
                popupMenu.getMenuInflater().inflate(R.menu.menu_produk, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_edit:
                                Intent i = new Intent(context, EditProdukActivity.class);
                                s.setProduk(a);
                                context.startActivity(i);
                                return true;
                            case R.id.item_hapus:
                                final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                                pDialog.setTitleText("Hapus")
                                        .setContentText("Apakah Anda ingin menghapus produk ini?")
                                        .setCancelText("Batal")
                                        .setConfirmText("Hapus")
                                        .showCancelButton(true)
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                pDialog.cancel();
                                            }
                                        })
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                hapus(a.ID_BARANG);
                                                pDialog.dismiss();
                                            }
                                        })
                                        .show();

                                return true;
                            case R.id.item_promo:
                                Intent intent = new Intent(context, DaftarPromoActivity.class);
                                intent.putExtra("extra", "pilihPromo");
                                intent.putExtra("idSub", a.ID_SUB_KATEGORI);
                                s.setProduk(a);
                                context.startActivity(intent);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });

    }

    private void hapus(String idProduk) {
        SweetAlert.onLoading(context);
        api.hapusProduk(s.getApi(), s.getIdUser(), s.getSession(), idProduk).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success==1){
                    CallFunction.getRefreshListProdukTK();
                } else {
                    Toasti.error(context, ""+response.body().message);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        ImageView image;
        TextView discount, harga, nama, stok;
        LinearLayout layout, btn_menu;

        public Holdr(final View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_produk);
            harga = (TextView) itemView.findViewById(R.id.tv_harga);
            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            stok = (TextView) itemView.findViewById(R.id.tv_stok);
            btn_menu = itemView.findViewById(R.id.btn_menu);

        }
    }
}
