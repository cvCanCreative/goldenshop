package id.co.cancreative.goldenshop.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import id.co.cancreative.goldenshop.Activity.DetailProdukActivity;
import id.co.cancreative.goldenshop.Activity.DetailProdukToko2Activity;
import id.co.cancreative.goldenshop.Activity.DetailProdukTokoActivity;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.TbTerakhirDilihat;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.AppDbKrenjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;

public class AdapterProdukTrading extends RecyclerView.Adapter<AdapterProdukTrading.Holdr> {

    ArrayList<Produk> data;
    Activity context;
    ArrayList<Integer> idBarang = new ArrayList<>();
    int b;
    SharedPref s;
    private ArrayList<TbTerakhirDilihat> daftarBarang;
    private AppDbTerakhirDilihat db;
    int index = 0;

    public AdapterProdukTrading(ArrayList<Produk> data, Activity context, AppDbTerakhirDilihat db) {
        this.data = data;
        this.context = context;
        this.db = db;

        daftarBarang = new ArrayList<>();
        daftarBarang.addAll(Arrays.asList(db.daoTerakhirDilihat().selectTerakhirDilihat()));

    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        return new Holder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_produk_trading, null));
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_produk_trading, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(Holdr holder, final int i) {
        s = new SharedPref(context);

        final Produk a = data.get(i);
        final int min = 20;
        final int max = 580;
        final int random = new Random().nextInt((max - min) + 1) + min;

        holder.nama.setText(a.BA_NAME);
        holder.tvHarga.setText("" + new Helper().convertRupiah(Integer.valueOf(a.BA_PRICE)));
        holder.tvTerjual.setText("" + a.TERJUAL + " Terjual");

        if (a.promo_status == 1) {
            holder.tvHarga.setPaintFlags(holder.tvHarga.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvHargaDiscount.setVisibility(View.VISIBLE);
            int potongan = 10000;
            if (a.PR_POTONGAN != null && a.PR_STATUS.equals("ACTIVE")) {
                potongan = Integer.parseInt(a.PR_POTONGAN);
            } else {
                potongan = Integer.parseInt(a.promo_admin.get(0).PR_POTONGAN);
            }

            int discount = Integer.valueOf(a.BA_PRICE) - Integer.valueOf(potongan);

            if (discount < 0) discount = 0;
            holder.tvHargaDiscount.setText(new Helper().convertRupiah(discount));
        }

        String image = new Helper().splitText(a.BA_IMAGE);

        Picasso.with(context)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(holder.image);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TbTerakhirDilihat b = new TbTerakhirDilihat();

                if (daftarBarang.size() == 0) {
                    setTerakhir(b, i);
                } else {
                    if (daftarBarang.size() >= 3) {
                        for (int j = 0; j < daftarBarang.size(); j++) {
                            idBarang.add(daftarBarang.get(j).idProduk);
                        }

                        if (idBarang.contains(Integer.parseInt(a.ID_BARANG))) {

                        } else {
                            index = daftarBarang.get(0).idProduk;
                            delete(index);
                            setTerakhir(b, i);
                        }
                    } else {
                        for (int j = 0; j < daftarBarang.size(); j++) {
                            idBarang.add(daftarBarang.get(j).idProduk);
                        }

                        if (idBarang.contains(Integer.parseInt(a.ID_BARANG))) {

                        } else {
                            setTerakhir(b, i);
                        }
                    }

                }

                Intent intent;
                if (a.ID_TOKO.equals(s.getString(SharedPref.ID_TOKO))) {
                    intent = new Intent(context, DetailProdukToko2Activity.class);
                } else {
                    intent = new Intent(context, DetailProdukActivity.class);
                }

                Log.d("Cek Toko", a.ID_TOKO + " : " + s.getString(SharedPref.ID_TOKO));
                s.setProduk(a);
                intent.putExtra("dataInten", "biasa");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    private void setTerakhir(TbTerakhirDilihat b, int posisi) {
        b.idProduk = Integer.valueOf(data.get(posisi).ID_BARANG);
        b.idToko = Integer.valueOf(data.get(posisi).ID_TOKO);
        b.nama = data.get(posisi).BA_NAME;
        b.id_kategori = data.get(posisi).ID_KATEGORI;
        b.nama_kategori = "";
        b.harga = Integer.valueOf(data.get(posisi).BA_PRICE);
        b.stok = Integer.valueOf(data.get(posisi).BA_STOCK);
        b.berat = Integer.valueOf(data.get(posisi).BA_WEIGHT);
        b.sku = data.get(posisi).BA_SKU;
        b.kondisi = data.get(posisi).BA_CONDITION;
        b.gambar = data.get(posisi).BA_IMAGE;
        b.penjual = data.get(posisi).TOK_NAME;
        b.deskripsi = data.get(posisi).BA_DESCRIPTION;
        b.kotaPenjual = data.get(posisi).TOK_KOTA;
        b.fotoPenjual = data.get(posisi).TOK_FOTO;

        insertData(b);
    }

    @SuppressLint("StaticFieldLeak")
    private void insertData(final TbTerakhirDilihat barang) {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses insert data
                long status = db.daoTerakhirDilihat().insertTerakhirDilihat(barang);
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {


            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void delete(final int id) {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses Delete data
                db = Room.databaseBuilder(context,
                        AppDbTerakhirDilihat.class, Config.db_terakhirDilihat)
                        .build();

                long status = db.daoTerakhirDilihat().deleteTerakhirDilihat(id);

                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
//                Toast.makeText(context, "hapus", Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        ImageView image;
        TextView nama, tvHarga, tvTerjual, tvHargaDiscount;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            image = (ImageView) itemView.findViewById(R.id.img_produk);
            tvHarga = (TextView) itemView.findViewById(R.id.tv_harga);
            tvTerjual = (TextView) itemView.findViewById(R.id.tv_terjual);
            tvHargaDiscount = (TextView) itemView.findViewById(R.id.tv_hargaDiscount);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
        }
    }

    public void addData(List<Produk> produks){
        for (Produk d : produks){
            data.add(d);
        }
        notifyDataSetChanged();
    }
}
