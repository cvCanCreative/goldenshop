package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Promo;
import id.co.cancreative.goldenshop.R;

public class AdapterPromoPilih extends RecyclerView.Adapter<AdapterPromoPilih.Holdr> {

    ArrayList<Promo> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterPromoPilih(ArrayList<Promo> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_promo, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterPromoPilih.Holdr holder, final int i) {
        s = new SharedPref(context);
        final Promo a = data.get(i);

        String potongan = new Helper().convertRupiah(Integer.valueOf(a.PR_POTONGAN));
        String minimum = new Helper().convertRupiah(Integer.valueOf(a.PR_MIN));

        holder.tvTitle.setText("Belanja Minimal "+ minimum +", Potongan "+potongan +" (Termasuk ongkos kirim)");
        holder.tvPorongan.setText(""+potongan);
        holder.tglExpired.setText("Expired "+new Helper().convertDateFormatPromo(a.PR_EXPIRED));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView tvTitle, tglExpired, tvPorongan;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvPorongan = (TextView) itemView.findViewById(R.id.tv_potongan);
            tglExpired = (TextView) itemView.findViewById(R.id.tv_tglExpired);
            layout = itemView.findViewById(R.id.layout);
        }
    }
}

