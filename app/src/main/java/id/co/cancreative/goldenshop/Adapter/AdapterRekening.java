package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.EditRekeningActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterRekening extends RecyclerView.Adapter<AdapterRekening.Holdr> {

    ArrayList<Rekening> data;
    Activity context;
    int b;
    SharedPref s;
    ApiService api = ApiConfig.getInstanceRetrofit();

    public AdapterRekening(ArrayList<Rekening> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_rekening, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(AdapterRekening.Holdr holder, final int i) {
        s = new SharedPref(context);

        final Rekening a = data.get(i);

        holder.label.setText(a.BK_NAME);
        holder.noRek.setText(a.RK_NOMOR);
        holder.pemilik.setText(a.RK_NAME);
        holder.btn_ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EditRekeningActivity.class);
                s.setRekening(a);
                context.startActivity(i);
            }
        });
        holder.btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sPesan = "Apakah Anda yakin ingin menghapus rekeing: " + "<b>" +"Rekening "+ a.RK_NAME+ "</b>" +"?";
                final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                pDialog.setTitleText("Hapus")
                        .setContentText(String.valueOf(Html.fromHtml(sPesan)))
                        .setCancelText("Batal")
                        .setConfirmText("Hapus")
                        .showCancelButton(true)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                pDialog.cancel();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                hapusRekening(a.ID_REKENING);
                                pDialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        Picasso.with(context)
                .load(Config.url_bank + a.BK_IMAGE)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(holder.image);

    }

    private void hapusRekening(String idRekening){
        SweetAlert.onLoading(context);
        api.hapusRekening(s.getApi(), s.getIdUser(), s.getSession(), idRekening).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                CallFunction.getRefreshListRekening();
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(context, t.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView label, noRek, pemilik;
        TextView btn_ubah, btn_hapus;
        CardView layout;
        ImageView image;

        public Holdr(final View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.tv_bank);
            noRek = (TextView) itemView.findViewById(R.id.tv_noRek);
            pemilik = (TextView) itemView.findViewById(R.id.tv_namaPemilik);
            btn_hapus = (TextView) itemView.findViewById(R.id.btn_hapus);
            btn_ubah = (TextView) itemView.findViewById(R.id.btn_ubah);
            layout = itemView.findViewById(R.id.card);
            image = itemView.findViewById(R.id.image);
        }
    }
}

