package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.R;

public class AdapterRekeningGold extends RecyclerView.Adapter<AdapterRekeningGold.Holdr> {

    ArrayList<Rekening> data;
    Activity context;
    int b;
    SharedPref s;
    ApiService api = ApiConfig.getInstanceRetrofit();

    public AdapterRekeningGold(ArrayList<Rekening> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_rekening_gold, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(AdapterRekeningGold.Holdr holder, final int i) {
        s = new SharedPref(context);

        final Rekening a = data.get(i);

        holder.label.setText(a.BK_NAME);
        holder.pemilik.setText(a.RK_NOMOR+ " - "+a.RK_NAME);

        Picasso.with(context)
                .load(Config.url_bank + a.BK_IMAGE)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView label, noRek, pemilik;
        TextView btn_ubah, btn_hapus;
        CardView layout;
        ImageView image;

        public Holdr(final View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.tv_bank);
            pemilik = (TextView) itemView.findViewById(R.id.tv_namaPemilik);
            image = itemView.findViewById(R.id.image);
        }
    }
}

