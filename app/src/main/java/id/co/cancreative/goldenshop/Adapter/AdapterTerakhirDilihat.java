package id.co.cancreative.goldenshop.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import id.co.cancreative.goldenshop.Activity.DetailProdukActivity;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.TbTerakhirDilihat;

public class AdapterTerakhirDilihat extends RecyclerView.Adapter<AdapterTerakhirDilihat.Holder> {

    Context context;
    ArrayList<TbTerakhirDilihat> dilihats;

    int b;
    SharedPref s;

    public AdapterTerakhirDilihat(Context context, ArrayList<TbTerakhirDilihat> dilihats) {
        this.context = context;
        this.dilihats = dilihats;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_produk_trading, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        s = new SharedPref(context);

        final TbTerakhirDilihat a = dilihats.get(i);

        final int min = 20;
        final int max = 580;
        final int random = new Random().nextInt((max - min) + 1) + min;

        holder.tvNama.setText(dilihats.get(i).nama);
        holder.tvHarga.setText(""+new Helper().convertRupiah(Integer.valueOf(dilihats.get(i).harga)));
        holder.tvTerjual.setText(random +" Terjual");

        String image = new Helper().splitText(dilihats.get(i).gambar);

        Picasso.with(context)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(holder.imgProduk);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailProdukActivity.class);
                s.setProdukT(a);
                intent.putExtra("dataInten","terakhir");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dilihats.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private LinearLayout layout;
        private ImageView imgProduk;
        private TextView tvNama;
        private TextView tvHarga;
        private TextView tvTerjual;


        public Holder(@NonNull View itemView) {
            super(itemView);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            imgProduk = (ImageView) itemView.findViewById(R.id.img_produk);
            tvNama = (TextView) itemView.findViewById(R.id.tv_nama);
            tvHarga = (TextView) itemView.findViewById(R.id.tv_harga);
            tvTerjual = (TextView) itemView.findViewById(R.id.tv_terjual);
        }
    }
}
