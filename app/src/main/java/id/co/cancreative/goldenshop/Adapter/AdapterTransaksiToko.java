package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.TransaksiToko;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterTransaksiToko extends RecyclerView.Adapter<AdapterTransaksiToko.Holdr> {

    ArrayList<TransaksiToko> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterTransaksiToko(ArrayList<TransaksiToko> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_transaksi_toko, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterTransaksiToko.Holdr holder, final int i) {
        s = new SharedPref(context);
        final TransaksiToko a = data.get(i);

        holder.tglTransaksi.setText("" + new Helper().convertDateTimeToDate(a.TGL_TRX, "yyyy-MM-dd hh:mm:s"));
        holder.tvKode.setText(a.SAB_KODE_PAYMENT);
        holder.tvNominal.setText("" + new Helper().convertRupiah(Integer.valueOf(a.HSA_SALDO)));

        String statusTransaksi = "Berhasil";
        String status = "Transaksi Berhasil";
        if (a.transaksi.get(0).TS_STATUS.equals("DITERIMA_USER")) {
            statusTransaksi = "Transaksi Berhasil";

            holder.layout.setBackgroundResource(R.color.diterima);
            holder.tvStatus.setText("Transaksi Toko");

        } else if (a.transaksi.get(0).TS_STATUS.equals("BATAL")){
            holder.tvStatusTransaksi.setTextColor(Color.parseColor("#ff0000"));
            statusTransaksi = "Transaksi Dibatalkan";

            holder.layout.setBackgroundResource(R.color.ditolak);
            holder.tvStatus.setText("Pembatalan Transaksi");
        }

        holder.tvStatusTransaksi.setText("" + statusTransaksi);

        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Cancel Withdraw?")
                        .setContentText("Anda akan yakin ingin membatalkan penarikan saldo")
                        .setCancelText("Tidak")
                        .setConfirmText("Lanjutkan")
                        .showCancelButton(true)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                cancelWithdraw(a.ID_HISTORY_SALDO);
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }
        });

    }

    private void cancelWithdraw(String id) {
        SweetAlert.onLoading(context);
        ApiService api = ApiConfig.getInstanceRetrofit();
        api.cancelWithdraw(s.getApi(), s.getIdUser(), s.getSession(), id).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1) {
                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Pembatalan Berhasil")
                            .setContentText("" + response.body().message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    CallFunction.getrefrashSaldo();
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(context, t.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView tvStatus, tglTransaksi, tvKode, tvStatusTransaksi, tvNoRek, tvNominal, btnCancel;
        ImageView imageProduk, btn_menu;
        RelativeLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            tvKode = (TextView) itemView.findViewById(R.id.tv_kode);
            tglTransaksi = (TextView) itemView.findViewById(R.id.tv_tglTansaksi);
            tvStatusTransaksi = (TextView) itemView.findViewById(R.id.tv_ststusTransaksi);
            tvNoRek = (TextView) itemView.findViewById(R.id.tv_noRek);
            tvNominal = (TextView) itemView.findViewById(R.id.tv_nominal);
            btnCancel = (TextView) itemView.findViewById(R.id.btn_cancel);
            layout = itemView.findViewById(R.id.layout_status);
        }
    }
}


