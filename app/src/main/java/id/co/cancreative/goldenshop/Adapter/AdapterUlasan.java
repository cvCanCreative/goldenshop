package id.co.cancreative.goldenshop.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Model.Feedback;
import id.co.cancreative.goldenshop.R;

public class AdapterUlasan extends RecyclerView.Adapter<AdapterUlasan.Holder> {

    Context context;
    List<Feedback> list;

    public AdapterUlasan(Context context, List<Feedback> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_fedd_back, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.tvNamaAsli.setText("Oleh "+list.get(i).US_USERNAME);
        holder.tvNama.setText(""+new Helper().convertDateTime(list.get(0).CREATED_AT, "yyyy-MM-dd hh:mm:ss") + " WIB");
        holder.tvKomentar.setText(list.get(i).FE_COMMENT);
        holder.rtBar.setRating(Float.parseFloat(list.get(i).FE_RATING));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private TextView tvNamaAsli;
        private AppCompatRatingBar rtBar;
        private TextView tvKomentar;
        private TextView tvNama;
        public Holder(@NonNull View itemView) {
            super(itemView);

            tvNamaAsli = (TextView) itemView.findViewById(R.id.tv_nama_asli);
            rtBar = (AppCompatRatingBar) itemView.findViewById(R.id.rt_bar);
            tvKomentar = (TextView) itemView.findViewById(R.id.tv_komentar);
            tvNama = (TextView) itemView.findViewById(R.id.tv_nama);
        }
    }
}
