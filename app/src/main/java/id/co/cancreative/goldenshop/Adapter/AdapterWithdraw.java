package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.Withdraw1Activity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Withdraw;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterWithdraw extends RecyclerView.Adapter<AdapterWithdraw.Holdr> {

    ArrayList<Withdraw> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterWithdraw(ArrayList<Withdraw> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_history_withdraw, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterWithdraw.Holdr holder, final int i) {
        s = new SharedPref(context);
        final Withdraw a = data.get(i);

        String status = "Menunggu Konfirmasi";
        if (a.HSA_STATUS.equals("PROSES")) {
            status = "Menunggu Konfirmasi";
            holder.layout.setBackgroundResource(R.color.proses);
            holder.btnCancel.setVisibility(View.VISIBLE);
        }

        if (a.HSA_STATUS.equals("INVALID")) {
            status = "Withdraw Ditolak";
            holder.layout.setBackgroundResource(R.color.ditolak);
        }

        if (a.HSA_STATUS.equals("ACC")) {
            status = "Withdraw Berhasil";
            holder.layout.setBackgroundResource(R.color.diterima);
        }

        holder.tvStatus.setText(status);
        holder.tglTransaksi.setText("" + new Helper().convertDateTimeToDate(a.TGL_TRX, "yyyy-MM-dd hh:mm:s"));
        holder.tvKode.setText(a.HSA_TRX_SALDO);
        holder.tvNominal.setText("" + new Helper().convertRupiah(Integer.valueOf(a.SAWD_NOMINAL)));
        holder.tvNamaPemilik.setText("" + a.RK_NAME);
        holder.tvNoRek.setText("" + a.RK_NOMOR);

        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Cancel Withdraw?")
                        .setContentText("Anda akan yakin ingin membatalkan penarikan saldo")
                        .setCancelText("Tidak")
                        .setConfirmText("Lanjutkan")
                        .showCancelButton(true)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                cancelWithdraw(a.ID_HISTORY_SALDO, i);
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            }
        });

    }

    private void cancelWithdraw(String id, final int i) {
        SweetAlert.onLoading(context);
        ApiService api = ApiConfig.getInstanceRetrofit();
        api.cancelWithdraw(s.getApi(), s.getIdUser(), s.getSession(), id).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.body().success == 1){
                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Pembatalan Berhasil")
                            .setContentText("" + response.body().message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    data.remove(i);
                                    notifyItemRemoved(i);
                                    notifyItemRangeChanged(i, data.size());
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
                SweetAlert.onFailure(context, t.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView tvStatus, tglTransaksi, tvKode, tvNamaPemilik, tvNoRek, tvNominal, btnCancel;
        ImageView imageProduk, btn_menu;
        RelativeLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            tvKode = (TextView) itemView.findViewById(R.id.tv_kode);
            tglTransaksi = (TextView) itemView.findViewById(R.id.tv_tglTansaksi);
            tvNamaPemilik = (TextView) itemView.findViewById(R.id.tv_namaPemilik);
            tvNoRek = (TextView) itemView.findViewById(R.id.tv_noRek);
            tvNominal = (TextView) itemView.findViewById(R.id.tv_nominal);
            btnCancel = (TextView) itemView.findViewById(R.id.btn_cancel);
            layout = itemView.findViewById(R.id.layout_status);
        }
    }
}


