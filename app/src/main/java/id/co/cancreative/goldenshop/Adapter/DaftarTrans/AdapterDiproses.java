package id.co.cancreative.goldenshop.Adapter.DaftarTrans;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.DetailTransaksiActivity;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.Model.TransaksiDetail;
import id.co.cancreative.goldenshop.R;

public class AdapterDiproses extends RecyclerView.Adapter<AdapterDiproses.Holdr> {

    ArrayList<Transaksi> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterDiproses(ArrayList<Transaksi> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_history_proses, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterDiproses.Holdr holder, final int i) {
        s = new SharedPref(context);
        final Transaksi a = data.get(i);

        String status = "Menunggu Konfirmasi";
        if (a.TS_STATUS.equals("PROSES_ADMIN")){
            status = "Menunggu Konfirmasi";
        }

        if (a.TS_STATUS.equals("PROSES_VENDOR")){
            status = "Pesanan Diproses";
        }

        if (a.TS_STATUS.equals("PROSES_KIRIM")){
            status = "Pesanan Dikirim";
        }

        if (a.TS_STATUS.equals("DITERIMA_USER")){
            status = "Pesanan Tiba";
        }

        if (a.TS_STATUS.equals("TERIMA")){
            status = "Pesanan Selesai";
        }

        holder.tvStatus.setText(status);
        String namaBarang = "Nama Produk";
        if (a.transaksi_detail.get(0).barang != null) {
            namaBarang = a.transaksi_detail.get(0).barang.BA_NAME;

        }

        String image = null;
        if (a.transaksi_detail.get(0).barang != null){
            image = new Helper().splitText(a.transaksi_detail.get(0).barang.BA_IMAGE);
        }

        holder.tvNamaProduk.setText("" + namaBarang);
        holder.tvKode.setText(a.TS_KODE_TRX);
        holder.tglBelanja.setText(""+new Helper().convertDateTimeToDate(a.CREATED_AT, "yyyy-MM-dd hh:mm:s"));

        Picasso.with(context)
                .load(Config.URL_produkGolden+image)
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat1)
                .noFade()
                .into(holder.imageProduk);

        int totalBayar = 0;
        int total = 0;
        for (TransaksiDetail t : a.transaksi_detail){
            totalBayar = totalBayar + Integer.valueOf(t.TSD_HARGA_TOTAL);
            total = totalBayar + Integer.valueOf(t.TSD_ONGKIR);
        }
        
        holder.tvTotalBayar.setText(""+new Helper().convertRupiah(Integer.valueOf(a.TS_TF_GOLD)));

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s.setDataTransaksi(a);
                Intent i = new Intent(context, DetailTransaksiActivity.class);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView tvStatus, tglBelanja, tvKode, tvNamaProduk, tvTotalBayar;
        ImageView imageProduk, btn_menu;
        LinearLayout layout;
        CardView card;

        public Holdr(final View itemView) {
            super(itemView);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            tvKode = (TextView) itemView.findViewById(R.id.tv_kode);
            tglBelanja = (TextView) itemView.findViewById(R.id.tv_tglBelanja);
            tvNamaProduk = (TextView) itemView.findViewById(R.id.tv_namaProduk);
            tvTotalBayar = (TextView) itemView.findViewById(R.id.tv_totalBayar);
            imageProduk = itemView.findViewById(R.id.img_produk);
            card = itemView.findViewById(R.id.card);
        }
    }
}

