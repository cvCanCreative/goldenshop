package id.co.cancreative.goldenshop.Adapter.DaftarTrans;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.DetailTransaksiTokoActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.Model.TransaksiDetail;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterListProdukTrans extends RecyclerView.Adapter<AdapterListProdukTrans.Holdr> {

    ArrayList<TransaksiDetail> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterListProdukTrans(ArrayList<TransaksiDetail> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_produk_trans, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterListProdukTrans.Holdr holder, final int i) {
        s = new SharedPref(context);
        final TransaksiDetail a = data.get(i);
        final Produk p = a.barang;

        float weight = Float.parseFloat(p.BA_WEIGHT) / 1000;
        NumberFormat nf = new DecimalFormat("#.#");
        String berat = nf.format(weight * Integer.valueOf(a.TSD_QTY)).replace(".0", "");

        String namaBarang = "Nama Produk";
        if (a.barang != null) {
            namaBarang = a.barang.BA_NAME;

        }

        String image = null;
        if (a.barang != null){
            image = new Helper().splitText(a.barang.BA_IMAGE);
        }

        holder.tvNamaProduk.setText("" + namaBarang);
        holder.tvJumlahBeli.setText(a.TSD_QTY + " barang (" + berat +" kg)");
        holder.tvJenisKurir.setText(a.TSD_JENIS_KURIR);
        holder.tvTotalBayar.setText(""+new Helper().convertRupiah(Integer.valueOf(p.BA_PRICE) * Integer.valueOf(a.TSD_QTY)));
        holder.tvHargaProduk.setText(""+new Helper().convertRupiah(Integer.valueOf(p.BA_PRICE)));

        holder.tvPenerima.setText(a.alamat_kirim.PE_NAMA);
        holder.tvPhone.setText(a.alamat_kirim.PE_TELP);
        holder.tvAlamat.setText(a.alamat_kirim.PE_ALAMAT +", " + a.alamat_kirim.PE_KOTA+", " + a.alamat_kirim.PE_PROVINSI +" " +a.alamat_kirim.PE_KODE_POS);

//        holder.tglBelanja.setText(""+new Helper().convertDateTimeToDate(a.CREATED_AT, "yyyy-MM-dd hh:mm:s"));

        Picasso.with(context)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(holder.imageProduk);

//        int totalBayar = 0;
//        int total = 0;
//        for (TransaksiDetail t : a.transaksi_detail){
//            totalBayar = totalBayar + Integer.valueOf(t.TSD_HARGA_TOTAL);
//            total = totalBayar + Integer.valueOf(t.TSD_ONGKIR);
//        }

    }

    @Override
    public int getItemCount() {
        if (data.size() > 90) {
            b = 1;
        } else {
            b = data.size();
        }
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView tvJumlahBeli, tvJenisKurir, tvKode, tvNamaProduk, tvTotalBayar, tvPenerima, tvPhone, tvAlamat, tvHargaProduk;
        ImageView imageProduk, btn_menu;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            tvJumlahBeli = (TextView) itemView.findViewById(R.id.tv_jumlah_beli);
            tvJenisKurir = (TextView) itemView.findViewById(R.id.tv_jenisKurir);
//            tglBelanja = (TextView) itemView.findViewById(R.id.tv_tglBelanja);
            tvNamaProduk = (TextView) itemView.findViewById(R.id.tv_namaProduk);
            tvTotalBayar = (TextView) itemView.findViewById(R.id.tv_totalBayar);
            imageProduk = itemView.findViewById(R.id.img_produk);
            tvPenerima = itemView.findViewById(R.id.tv_penerima);
            tvPhone = itemView.findViewById(R.id.tv_phone);
            tvAlamat = itemView.findViewById(R.id.tv_alamat);
            tvHargaProduk = itemView.findViewById(R.id.tv_hargaProduk);
        }
    }
}


