package id.co.cancreative.goldenshop.Adapter.DaftarTrans;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.DetailTransaksiActivity;
import id.co.cancreative.goldenshop.Activity.UploadPembayaranActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterMenungguPembayaran extends RecyclerView.Adapter<AdapterMenungguPembayaran.Holdr> {

    ArrayList<Transaksi> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterMenungguPembayaran(ArrayList<Transaksi> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_history_transaksi, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterMenungguPembayaran.Holdr holder, final int i) {
        s = new SharedPref(context);
        final Transaksi a = data.get(i);
        final Rekening ru = data.get(i).rekening_user;
        final Rekening rg = data.get(i).rekening_golden;

        holder.tvKodePayment.setText(a.TS_KODE_PAYMENT);
        holder.totalBayar.setText("" + new Helper().convertRupiah(Integer.valueOf(a.TS_TF_GOLD)));
        holder.tglBelanja.setText("" + new Helper().convertDateTime(a.CREATED_AT, "yyyy-MM-dd hh:mm:s") + " WIB");
        holder.tglExp.setText("Bayar Sebelum " + new Helper().convertDateTime(a.TS_EXP, "yyyy-MM-dd hh:mm:s") + " WIB");

        holder.tvNoRekUser.setText(ru.RK_NOMOR);
        holder.tvNoRekGolden.setText(rg.RK_NOMOR);
        holder.tvRekUser.setText("a/n " + ru.RK_NAME);
        holder.tvRekGolden.setText("a/n " + rg.RK_NAME);

        Picasso.with(context)
                .load(Config.url_bank + ru.BK_IMAGE)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(holder.imgBank);

        holder.btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(context, holder.btn_menu);
                popupMenu.getMenuInflater().inflate(R.menu.menu_history, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_batal:
                                final ApiService api = ApiConfig.getInstanceRetrofit();

                                String sPesan = "Apakah Anda yakin ingin Membatalkan Transaksi Ini" +"?";
                                final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                                pDialog.setTitleText("Hapus")
                                        .setContentText(String.valueOf(Html.fromHtml(sPesan)))
                                        .setCancelText("Tidak")
                                        .setConfirmText("Iya,Batalkan")
                                        .showCancelButton(true)
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                pDialog.cancel();
                                            }
                                        })
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                                SweetAlert.onLoading(context);
                                                api.getBatalTransaksi(s.getApi(), s.getIdUser(), s.getSession(), a.TS_KODE_PAYMENT).enqueue(new Callback<ResponModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                                        SweetAlert.dismis();
                                                        if (response.body().success == 1){
                                                            sweetAlertDialog
                                                                    .setTitleText("Transaksi Dibatalkan")
                                                                    .setContentText("Transaksi anda telah kami Batalkan")
                                                                    .setConfirmText("OK")
                                                                    .showCancelButton(false)
                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sweet) {
                                                                            data.remove(i);
                                                                            notifyItemRemoved(i);
                                                                            notifyItemRangeRemoved(i, data.size());
                                                                            sweetAlertDialog.dismiss();
                                                                        }
                                                                    })
                                                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                        } else {
                                                            Toasti.error(context, response.body().message);
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponModel> call, Throwable t) {
                                                        pDialog.dismiss();
                                                        SweetAlert.dismis();
                                                        SweetAlert.onFailure(context, t.getMessage());
                                                    }
                                                });
                                            }
                                        })
                                        .show();
                                return true;
                            case R.id.item_upload:
                                s.setDataTransaksi(a);
                                Intent i = new Intent(context, UploadPembayaranActivity.class);
                                context.startActivity(i);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s.setDataTransaksi(a);
                Intent i = new Intent(context, DetailTransaksiActivity.class);
                i.putExtra("extra", "menungguPembayaran");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView totalBayar, tglBelanja, tglExp, tvKodePayment, tvNoRekUser, tvNoRekGolden, tvRekUser, tvRekGolden;
        ImageView imgBank, btn_menu;
        LinearLayout layout;
        CardView card;

        public Holdr(final View itemView) {
            super(itemView);
            totalBayar = (TextView) itemView.findViewById(R.id.tv_totalBayar);
            tvNoRekUser = (TextView) itemView.findViewById(R.id.tv_noRekUser);
            tvNoRekGolden = (TextView) itemView.findViewById(R.id.tv_noRekGolden);
            tvRekGolden = (TextView) itemView.findViewById(R.id.tv_rekGolden);
            tvRekUser = (TextView) itemView.findViewById(R.id.tv_rekUser);
            tvKodePayment = (TextView) itemView.findViewById(R.id.tv_kodePayment);
            tglExp = (TextView) itemView.findViewById(R.id.tv_tglExpired);
            tglBelanja = (TextView) itemView.findViewById(R.id.tv_tglbelanja);
            imgBank = itemView.findViewById(R.id.image_bank);
            btn_menu = itemView.findViewById(R.id.btn_menu);
            card = itemView.findViewById(R.id.card);
        }
    }
}

