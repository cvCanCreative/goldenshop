package id.co.cancreative.goldenshop.Adapter.DaftarTrans;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Activity.DaftarTransaksiActivity;
import id.co.cancreative.goldenshop.Activity.DetailTransaksiActivity;
import id.co.cancreative.goldenshop.Activity.DetailTransaksiSelesaiActivity;
import id.co.cancreative.goldenshop.Activity.DetailTransaksiTokoActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.Model.TransaksiDetail;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterPesanSelesai extends RecyclerView.Adapter<AdapterPesanSelesai.Holdr> {

    ArrayList<Transaksi> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterPesanSelesai(ArrayList<Transaksi> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_history_proses, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(final AdapterPesanSelesai.Holdr holder, final int i) {
        s = new SharedPref(context);
        final Transaksi a = data.get(i);

        String status = "Menunggu Konfirmasi";
        if (a.TS_STATUS.equals("PROSES_ADMIN")){
            status = "Menunggu Konfirmasi";
        }

        if (a.TS_STATUS.equals("PROSES_VENDOR")){
            status = "Pesanan Diproses";
        }

        if (a.TS_STATUS.equals("PROSES_KIRIM")){
            status = "Pesanan Dikirim";
        }

        if (a.TS_STATUS.equals("DITERIMA_USER")){
            status = "Pesanan Tiba";
        }

        if (a.TS_STATUS.equals("TERIMA")){
            status = "Pesanan Selesai";
        }

        holder.tvStatus.setText(status);
        String namaBarang = "Nama Produk";
        if (a.transaksi_detail.get(0).barang != null) {
            namaBarang = a.transaksi_detail.get(0).barang.BA_NAME;

        }

        String image = null;
        if (a.transaksi_detail.get(0).barang != null){
            image = new Helper().splitText(a.transaksi_detail.get(0).barang.BA_IMAGE);
        }

        holder.tvNamaProduk.setText("" + namaBarang);
        holder.tvKode.setText(a.TS_KODE_TRX);
        holder.tglBelanja.setText(""+new Helper().convertDateTimeToDate(a.CREATED_AT, "yyyy-MM-dd hh:mm:s"));

        Picasso.with(context)
                .load(Config.URL_produkGolden+image)
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat1)
                .noFade()
                .into(holder.imageProduk);

        int totalBayar = 0;
        int total = 0;
        for (TransaksiDetail t : a.transaksi_detail){
            totalBayar = totalBayar + Integer.valueOf(t.TSD_HARGA_TOTAL);
            total = totalBayar + Integer.valueOf(t.TSD_ONGKIR);
        }
        
        holder.tvTotalBayar.setText(""+new Helper().convertRupiah(Integer.valueOf(a.TS_TF_GOLD)));

        holder.btn_menu.setVisibility(View.VISIBLE);
        holder.btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(context, holder.btn_menu);
                popupMenu.getMenuInflater().inflate(R.menu.menu_pesanan_selesai, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_detail:
                                s.setDataTransaksi(a);
                                Intent i = new Intent(context, DetailTransaksiSelesaiActivity.class);
                                context.startActivity(i);
                                return true;

                            case R.id.item_komplain:
                                Toasti.info(context, "Sedang dalam pengembangan");
//                                s.setDataTransaksi(a);
//                                Intent ia = new Intent(context, DetailTransaksiTokoActivity.class);
//                                context.startActivity(ia);
                                return true;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s.setDataTransaksi(a);
                Intent i = new Intent(context, DetailTransaksiSelesaiActivity.class);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        TextView tvStatus, tglBelanja, tvKode, tvNamaProduk, tvTotalBayar;
        ImageView imageProduk, btn_menu;
        LinearLayout layout;
        CardView card;

        public Holdr(final View itemView) {
            super(itemView);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            tvKode = (TextView) itemView.findViewById(R.id.tv_kode);
            tglBelanja = (TextView) itemView.findViewById(R.id.tv_tglBelanja);
            tvNamaProduk = (TextView) itemView.findViewById(R.id.tv_namaProduk);
            tvTotalBayar = (TextView) itemView.findViewById(R.id.tv_totalBayar);
            imageProduk = itemView.findViewById(R.id.img_produk);
            btn_menu = itemView.findViewById(R.id.btn_menu);
            card = itemView.findViewById(R.id.card);

        }
    }
}

