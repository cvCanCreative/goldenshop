package id.co.cancreative.goldenshop.App;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import id.co.cancreative.goldenshop.Helper.Config;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiConfigDemo {
    //    private static String BASE_URL = "http://qs.dokterapps.com/heroshop/admin_project/";
//    private static String BASE_URL = "http://mybird.silobur.com";
//    private static String BASE_URL = "http://nandhief.web.id/absensi/api/absences/";
    private static String BASE_URL = Config.BASEURL + "public/";

    private static Retrofit getClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        return retrofit;
    }

    public static ApiService getInstanceRetrofit() {
        return getClient().create(ApiService.class);
    }
}