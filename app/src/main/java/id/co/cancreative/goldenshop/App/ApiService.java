package id.co.cancreative.goldenshop.App;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelList;
import id.co.cancreative.goldenshop.Model.ResponModelNew;
import id.co.cancreative.goldenshop.Model.ResponModelProvinsi;
import id.co.cancreative.goldenshop.Model.ResponModelSerializ;
import id.co.cancreative.goldenshop.Model.ResponseCari;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    String CACHE = "Cache-Control: max-age=0";
    String AGENT = "User-Agent: Markeet";

    @FormUrlEncoded
    @POST("login.php")
    Call<ResponModel> login(
            @Query("api") String api,
            @Field("email") String email,
            @Field("password") String password);

    @FormUrlEncoded
    @POST("firebase_token.php")
    Call<ResponseBody> post_fcmtoken(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("fcm_token") String fcm_token
    );


    @FormUrlEncoded
    @POST("register_user.php")
    Call<ResponModel> register(
            @Query("api") String api,
            @Field("fullname") String fullname,
            @Field("username") String username,
            @Field("password") String password,
            @Field("email") String email,
            @Field("telp") String telp,
            @Field("alamat") String alamat);

    @GET("api/dashboard")
    Call<ResponModel> getDashbord();

    @GET("view.php")
    Call<ResponModel> getMahsiswa();

    @GET("get_saldo.php")
    Call<ResponModel> getSaldo(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("get_toko.php")
    Call<ResponModel> getToko(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("get_account.php")
    Call<ResponModel> getAccount(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("get_home.php")
    Call<ResponModel> getHome(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("get_tbarang_byid.php")
    Call<ResponModel> getProdukByT(
            @Query("api") String api,
            @Query("id_toko") String id
    );

    @GET("get_barang_bykategori.php")
    Call<ResponModel> getProdukByK(
            @Query("api") String api,
            @Query("id_sub_kategori") String id_kat,
            @Query("page") int page
    );

    @GET("get_barang_admin.php")
    Call<ResponModelSerializ> getProdukGS(
            @Query("api") String api,
            @Query("page") int page
    );

    @GET("get_barang_bykategori.php")
    Call<ResponModelSerializ> getAllProduk(
            @Query("api") String api,
            @Query("id_sub_kategori") String id_kat
    );

    @GET("get_barang_byterlaris1.php")
    Call<ResponModel> getTerlarisProduk(
            @Query("api") String api,
            @Query("page") int page
    );

    @GET("get_barang_byterlaris.php")
    Call<ResponModel> getDetailBarang(
            @Query("api") String api,
            @Query("id_barang") int id
    );

    @GET("api/products")
    Call<ResponModel> getProdukBySubkat(
            @Query("category") Integer id,
            @Query("page") Integer page
    );

    @GET("get_barangkat_bykondisi.php")
    Call<ResponModel> getProdukByKFilter(
            @Query("api") String api,
            @Query("id_sub_kategori") String id_kat,
            @Query("kondisi") String kondisi,
            @Query("page") Integer page
    );

    @GET("get_barang_bykategori.php")
    Call<ResponModel> getProdukByKAndStatToko(
            @Query("api") String api,
            @Query("id_sub_kategori") String id_kat,
            @Query("stoko") String sToko,
            @Query("page") Integer page
    );

    @FormUrlEncoded
    @POST("get_barangkat_byharga.php")
    Call<ResponModel> getProdukByKbyHarga(
            @Query("api") String api,
            @Query("id_sub_kategori") String id_kat,
            @Field("min") String min,
            @Field("max") String max,
            @Query("page") Integer page
    );

    @GET("get_kategori.php")
    Call<ResponModel> getKategori(
            @Query("api") String api
    );

    @GET("get_bank.php")
    Call<ResponModel> getBank(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("cek_toko.php")
    Call<ResponModel> cekToko(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @FormUrlEncoded
    @POST("add_toko.php")
    Call<ResponModel> addToko(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("toko") String namaToko,
            @Field("deskripsi") String deskripsi,
            @Field("id_prov") String idPorf,
            @Field("id_kota") String idKota,
            @Field("id_kecamatan") String idkec,
            @Field("nama_kota") String kota,
            @Field("nama_kecamatan") String kecamatan,
            @Field("no_ktp") String noKtp,
            @Field("address") String alamat,
            @Field("icon") String icon,
            @Field("selfie") String selfie,
            @Field("ktp") String ktp
    );

    @Multipart
    @POST("upload_file_toko.php")
    Call<ResponModel> uploadFotoToko(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("toko") RequestBody toko,
            @Part MultipartBody.Part icon,
            @Part MultipartBody.Part ktp,
            @Part MultipartBody.Part selfie
    );

    @Multipart
    @POST("upload_" + "{status}" + ".php")
    Call<ResponModel> uploadImgToko(
            @Path("status") String status,
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("toko") RequestBody toko,
            @Part MultipartBody.Part icon
    );

    @Multipart
    @POST("upload_toko_ktp.php")
    Call<ResponModel> uploadKtp(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("toko") RequestBody toko,
            @Part MultipartBody.Part ktp
    );

    @Multipart
    @POST("upload_toko_selfie.php")
    Call<ResponModel> uploadSelfi(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("toko") RequestBody toko,
            @Part MultipartBody.Part selfie
    );

    @Multipart
    @POST("update_pfoto.php")
    Call<ResponModel> uploadFotoProfile(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part MultipartBody.Part image
    );

    @Multipart
    @POST("upload_file_toko.php")
    Call<ResponModel> uploadEditFotoToko(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("toko") RequestBody toko,
            @Part MultipartBody.Part icon
    );

    @FormUrlEncoded
    @POST("update_profile.php")
    Call<ResponModel> editProfil(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("nama") String nama,
            @Field("email") String email,
            @Field("gender") String gender,
            @Field("alamat") String alamat,
            @Field("telp") String telp,
            @Field("birth") String birth
    );

    @FormUrlEncoded
    @POST("add_pengiriman.php")
    Call<ResponModel> addAlamat(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_user") String id_user,
            @Field("judul") String judul,
            @Field("nama") String nama,
            @Field("alamat") String alamat,
            @Field("provinsi") String provinsi,
            @Field("kota") String kota,
            @Field("kecamatan") String kecamatan,
            @Field("id_kota") String id_kota,
            @Field("id_kecamatan") String id_kecamatan,
            @Field("kode_pos") String kode_pos,
            @Field("telp") String telp
    );

    @FormUrlEncoded
    @POST("edit_pengiriman.php")
    Call<ResponModel> editAlamat(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_pengiriman") String idPengiriman,
            @Field("judul") String judul,
            @Field("nama") String nama,
            @Field("alamat") String alamat,
            @Field("provinsi") String provinsi,
            @Field("kota") String kota,
            @Field("kecamatan") String kecamatan,
            @Field("id_kota") String id_kota,
            @Field("id_kecamatan") String id_kecamatan,
            @Field("kode_pos") String kode_pos,
            @Field("telp") String telp
    );

    @FormUrlEncoded
    @POST("update_ppass.php")
    Call<ResponseBody> gantiPassword(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("pass_old") String pass_old,
            @Field("pass_new") String pass_new
    );

    @FormUrlEncoded
    @POST("forget_pass.php")
    Call<ResponseBody> lupaPassowrd(
            @Query("api") String api,
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("delete_pengiriman.php")
    Call<ResponModel> hapusAlamat(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_pengiriman") String idAlamat
    );

    @GET("get_pengiriman_byid.php")
    Call<ResponModel> getAlamat(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("get_rekening_byid.php")
    Call<ResponModel> getRekening(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("get_rekening_admin.php")
    Call<ResponModel> getRekeningGolden(
            @Query("api") String api
    );

    @FormUrlEncoded
    @POST("add_rekening.php")
    Call<ResponModel> addRekening(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_bank") String id_bank,
            @Field("no_rekening") String noRekening,
            @Field("atas_nama") String nama
    );

    @FormUrlEncoded
    @POST("edit_rekening.php")
    Call<ResponModel> editRekening(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_rekening") String id_rek,
            @Field("id_bank") String id_bank,
            @Field("no_rekening") String noRekening,
            @Field("atas_nama") String nama
    );

    @FormUrlEncoded
    @POST("delete_rekening.php")
    Call<ResponModel> hapusRekening(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_rekening") String id_rekening
    );

    @Multipart
    @POST("add_barang" + "{admin}" + ".php")
    Call<ResponModel> addProduk(
            @Path("admin") String admin,
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("id_sub_kategori") RequestBody id_sub_kategori,
            @Part("name") RequestBody name,
            @Part("price") RequestBody price,
            @Part("sku") RequestBody sku,
            @Part("deskripsi") RequestBody deskripsi,
            @Part("stock") RequestBody stock,
            @Part("weight") RequestBody weight,
            @Part("condition") RequestBody condition,
            @Part("image") RequestBody image
//            @Part MultipartBody.Part[] image
    );

    @FormUrlEncoded
    @POST("edit_barang.php")
    Call<ResponModel> editProduk(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_barang") String id_barang,
            @Field("id_sub_kategori") String id_sub_kategori,
            @Field("id_promo") String id_promo,
            @Field("name") String name,
            @Field("price") String price,
            @Field("sku") String sku,
            @Field("deskripsi") String deskripsi,
            @Field("stock") String stock,
            @Field("weight") String weight,
            @Field("condition") String condition,
            @Field("image") String image
    );

    @FormUrlEncoded
    @POST("delete_barang.php")
    Call<ResponModel> hapusProduk(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_barang") String idProduk
    );

    @FormUrlEncoded
    @POST("edit_promo.php")
    Call<ResponModel> editPromo(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_promo") String id_promo,
            @Field("nama") String nama,
            @Field("deskripsi") String deskripsi,
            @Field("code") String kode,
            @Field("potongan") String potongan,
            @Field("minimum") String minimum,
            @Field("mulai") String mulai,
            @Field("expired") String expired
    );

    @FormUrlEncoded
    @POST("delete_promo.php")
    Call<ResponModel> deletePromo(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_promo") String id_promo
    );


    @FormUrlEncoded
    @POST("RajaOngkir/get_kota.php")
    Call<ResponModel> getListoKota(
            @Field("prov") String idProv
    );

    @FormUrlEncoded
    @POST("RajaOngkir/get_kecamatan.php")
    Call<ResponModel> getListoKec(
            @Field("kota") String idKota
    );

    @GET("RajaOngkir/get_provinsi.php")
    Call<ResponModelProvinsi> getListProv();

    @FormUrlEncoded
    @Headers({CACHE, AGENT})
    @POST("RajaOngkir/get_ongkir.php")
    Call<ResponseBody> getOngkir(
            @Field("origin") String origin,
            @Field("originType") String originType,
            @Field("destination") String destination,
            @Field("destinationType") String destinationType,
            @Field("weight") String weight,
            @Field("courier") String courier
    );

    @FormUrlEncoded
    @POST("edit_toko.php")
    Call<ResponModelProvinsi> editToko(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_toko") String id_toko,
            @Field("toko") String toko,
            @Field("deskripsi") String deskripsi,
            @Field("address") String address,
            @Field("id_prov") String id_prov,
            @Field("id_kota") String id_kota,
            @Field("id_kecamatan") String id_kecamatan,
            @Field("nama_kota") String nama_kota,
            @Field("nama_kecamatan") String nama_kecamatan,
            @Field("icon") String icon
    );

    @FormUrlEncoded
    @POST("transaksi_shop.php")
    Call<ResponModel> transaksi(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_rekening") String id_rekening,
            @Field("id_rekening_gold") String id_rekening_gold,
            @Field("kode_unik") Integer kode_unik,
            @Field("kode_promo") String kode_promo,
            @Field("id_penerima[]") List<String> id_penerima,
            @Field("id_barang[]") List<Integer> id_barang,
            @Field("qty[]") List<Integer> qty,
            @Field("kurir[]") List<String> kurir,
            @Field("jns_kurir[]") List<String> jns_kurir,
            @Field("ongkir[]") List<Integer> ongkir,
            @Field("catatan[]") List<String> catatan
    );

    @FormUrlEncoded
    @POST("get_tstat_byid.php")
    Call<ResponModel> getHistoryTransaksi(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("status") String status
    );

    @FormUrlEncoded
    @POST("get_tstat_bytoko.php")
    Call<ResponModel> getDaftarTrans(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("status") String idKota
    );

    @FormUrlEncoded
    @POST("batal_trx.php")
    Call<ResponModel> getBatalTransaksi(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("kode_payment") String kode
    );

    @Multipart
    @POST("add_bukti_transfer.php")
    Call<ResponModel> uploadPembayaran(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("id_rekening") RequestBody id_rekening,
            @Part("jumlah_tf") RequestBody jumlah_tf,
            @Part MultipartBody.Part image
    );

    @GET("get_promo_byid.php")
    Call<ResponModel> getPromo(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("get_pskat_byid.php")
    Call<ResponModel> getPromoById(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Query("id_sub_kategori") String subId
    );

    @GET("get_promo_bybrg.php")
    Call<ResponModel> getPromoByProduk(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Query("id_barang") String idProduk
    );

    @FormUrlEncoded
    @POST("cek_promo_bybarang.php")
    Call<ResponModel> cekPromo(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_barang[]") List<String> id_barang,
            @Field("total_harga") String total_harga,
            @Field("kode_promo") String kode_promo
    );

    @FormUrlEncoded
    @POST("add_promo.php")
    Call<ResponModel> addPromo(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("nama") String nama,
            @Field("deskripsi") String deskripsi,
            @Field("potongan") String potongan,
            @Field("minimum") String minimum,
            @Field("code") String kode,
            @Field("id_sub_kategori") String idkategory,
            @Field("mulai") String mulai,
            @Field("expired") String expired
    );

    @FormUrlEncoded
    @POST("withdraw_saldo.php")
    Call<ResponModel> withdrawSaldo(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_rekening") String idRek,
            @Field("nominal") String nominal
    );

    @GET("get_historywd_byid.php")
    Call<ResponModel> getHistoryWithdraw(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("get_historytp_byid.php")
    Call<ResponModel> getTransaksiToko(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @GET("get_ktrx_byid.php")
    Call<ResponModel> getHistoryKomplain(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session
    );

    @FormUrlEncoded
    @POST("decline_withdraw.php")
    Call<ResponModel> cancelWithdraw(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_history") String idtrans
    );

    @FormUrlEncoded
    @POST("add_feedback.php")
    Call<ResponModel> tambahUlasan(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_trx_detail") String idtrans,
            @Field("rating") String reting,
            @Field("comment") String comment

    );

    @FormUrlEncoded
    @POST("acc_shop.php")
    Call<ResponModel> accShop(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_trx") String idTrx,
            @Field("status") String status,
            @Field("resi") String resi
    );

    @FormUrlEncoded
    @POST("transaksi_selesai.php")
    Call<ResponModel> transaksiSelesai(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_trx") String idTrx
    );

    @FormUrlEncoded
    @POST("acc_pembeli.php")
    Call<ResponModel> accPembeli(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("id_trx") String idTrx
    );

    @GET("get_tfeedback_bybarang.php")
    Call<ResponModel> getFeedback(
            @Query("api") String api,
            @Query("id_barang") String id
    );

    //CARI BARANG SEMUA
    @FormUrlEncoded
    @POST("get_barang_bysearch.php")
    Call<ResponModel> cari_barang(
            @Query("api") String api,
            @Field("search") String produk,
            @Query("page") Integer page
    );

    @FormUrlEncoded
    @POST("get_barsearch_byharga.php")
    Call<ResponModel> cari_barangByHarga(
            @Query("api") String api,
            @Field("search") String id_user,
            @Field("min") String min,
            @Field("max") String max,
            @Query("page") Integer page
    );

    @FormUrlEncoded
    @POST("get_barsearch_bystatus.php")
    Call<ResponModel> cari_ByStatus(
            @Query("api") String api,
            @Field("search") String id_user,
            @Query("status") String status,
            @Query("page") Integer page
    );

    @FormUrlEncoded
    @POST("get_barang_bysearch.php")
    Call<ResponModel> cari_ByStoko(
            @Query("api") String api,
            @Field("search") String id_user,
            @Query("stoko") String status,
            @Query("page") Integer page
    );

    @FormUrlEncoded
    @POST("get_barsearch_bystatus.php")
    Call<ResponseCari> cari_barang_berdasarkan_status(
            @Query("api") String api,
            @Field("search") String id_user,
            @Query("status") String status
    );
    //SELESAI CARI BARANG SEMUA

    //CARI BARANG TOKO
    @FormUrlEncoded
    @POST("get_tbarang_bysearch.php")
    Call<ResponseCari> cari_barang_toko(
            @Query("api") String api,
            @Field("search") String id_user,
            @Query("id_toko") Integer id_toko
    );


    @FormUrlEncoded
    @POST("get_tbarang_bystatus.php")
    Call<ResponseCari> cari_barang_berdasarkan_status_toko(
            @Query("api") String api,
            @Field("search") String id_user,
            @Query("status") String status,
            @Query("id_toko") Integer id_toko
    );
    //SELESAI CARI BARANG TOKO

    @Multipart
    @POST("upload_barang.php")
    Call<ResponModel> uploadFoto(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part MultipartBody.Part image
    );

    @Multipart
    @POST("start")
    Call<ResponModel> absenStart(
            @Query("token") String api,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("start_at") RequestBody finish_at,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("start")
    Call<ResponseCari> absenMasuk(
            @Query("token") String api,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("start_at") String finish_at
    );

    @Multipart
    @POST("transaksi_komplain.php")
    Call<ResponModel> addKomplain(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("id_trx") RequestBody id_trx,
            @Part("judul") RequestBody judul,
            @Part("detail") RequestBody detail,
            @Part MultipartBody.Part[] image
    );

    @Multipart
    @POST("komplain_all.php")
    Call<ResponModel> addKomplainAll(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("judul") RequestBody judul,
            @Part("detail") RequestBody detail,
            @Part MultipartBody.Part[] image
    );

    @FormUrlEncoded
    @POST("komplain_all.php")
    Call<ResponModel> addKomplainAllNoGbr(
            @Query("api") String api,
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("judul") String judul,
            @Field("detail") String detail
    );

    @FormUrlEncoded
    @POST("RajaOngkir/get_tracking.php")
    Call<ResponModel> getPengiriman(
            @Field("waybill") String waybill,
            @Field("courier") String courier
    );

    @GET("get_about.php")
    Call<ResponModel> getAbout(
            @Query("api") String api
    );

}

