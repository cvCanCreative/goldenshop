package id.co.cancreative.goldenshop.App;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import id.co.cancreative.goldenshop.Activity.SplashActivity;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.Model.TokoStatus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Connection {

    public static ApiService api = ApiConfig.getInstanceRetrofit();
    public static SharedPref s;

    public static void getData(final Activity context){
        s = new SharedPref(context);
        SweetAlert.onLoading(context);
        api.getAccount(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    ResponModel respon = response.body();
                    if (respon.getSuccess() == 1) {

                        Saldo saldo = respon.result.saldo;
                        Toko toko = respon.result.toko;
                        TokoStatus statusToko = respon.result.cek_toko;

                        // Set Saldo
                        s.setSaldo(saldo);
                        s.setToko(toko);
                        s.setString(SharedPref.ID_TOKO, toko.ID_TOKO);
                        s.setMinWd(respon.result.min_wd);
                        Saldo min = s.getMinWd();
                        Log.d("Test", ""+min.minimal_wd);

                        // Set Status Toko
                        if (statusToko.success == 1) {
                            s.setBoolean(SharedPref.TOK_STATUS, true);
                            String status = statusToko.status;
                            if (status.equals("TK_BLM_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, "TK_BLM_AKTIF");
                            } else if (status.equals("TK_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, status);
                            }
                        } else if (statusToko.success == 0) {
                            s.setBoolean(SharedPref.TOK_STATUS, false);
                        }

                    } else if (respon.getSuccess() == 0) {
                        if (respon.message.contains("Silahkan login kembali")){
                            s.clearAll();
                            Intent intent = new Intent(context, SplashActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(intent);
                            context.finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
            }
        });
    }
}
