package id.co.cancreative.goldenshop.DataDemo;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Value;
import id.co.cancreative.goldenshop.Model.ModelCategory;

public class DataCategory {

    public static String[][] data = new String[][]{
            {"ACCESSORIES", "Presiden Pertama RI", Value.url_kategori +"category_accessories.jpg"},
            {"BEAUTY", "Presiden Pertama RI", Value.url_kategori+"category_beauty.jpg"},
            {"FASHION", "Presiden Pertama RI", Value.url_kategori+"category_fashion.jpg"},
            {"HOME", "Presiden Pertama RI", Value.url_kategori+"category_home.jpg"},
            {"SPORTS", "Presiden Pertama RI", Value.url_kategori+"category_sports.jpg"},
            {"KIDS", "Presiden Pertama RI", Value.url_kategori+"category_kids.jpg"},
            {"TECH", "Presiden Pertama RI", Value.url_kategori+"category_tech.jpg"},
            {"HAIR", "Presiden Pertama RI", Value.url_kategori+"category_hair.jpg"},
            {"RENOVATION", "Presiden Pertama RI", Value.url_kategori+"category_renovation.jpg"}
    };

    public static ArrayList<ModelCategory> getListData(){
        ModelCategory kategori = null;
        ArrayList<ModelCategory> list = new ArrayList<>();
        for (String[] aData : data) {
            kategori = new ModelCategory();
//            kategori.setNama(aData[0]);
//            kategori.setKeterangan(aData[1]);
//            kategori.setGambar(aData[2]);

            list.add(kategori);
        }
        return list;
    }
}
