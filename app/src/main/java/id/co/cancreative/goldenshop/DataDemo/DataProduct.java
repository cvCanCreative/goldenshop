package id.co.cancreative.goldenshop.DataDemo;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Model.ModelProduk;

public class DataProduct {

    public static String[][] data = new String[][]{

            {"Fashion Faux Leauther Watch", "148000", Config.URL_produk+"jam.jpg"},
            {"GIMTO Sport Watch Mens Digital", "158000", Config.URL_produk+"jam2.jpg"},
            {"Smart Watch Men Bluetooth Pedometer", "438000", Config.URL_produk+"jam3.jpg"},
            {"New EZOn Multifunctional Hiking", "378000", Config.URL_produk+"jam4.jpg"},
            {"2019 NEW Men Running Shoes Height Incrasing Style Moderen Fachion", "98000", Config.URL_produk+"spatu.jpg"},
            {"Shoes Men Woman Outdoor Sports", "78000", Config.URL_produk+"spatu2.jpg"},
            {"Peralatan Masak Lengkap", "190000", Config.URL_produk+"alatdapur.jpg"},
            {"Centong Dari kayu berkualitas Tinggi", "90000", Config.URL_produk+"alatdapur2.jpg"}
    };

    public static ArrayList<ModelProduk> getListData(){
        ModelProduk kategori = null;
        ArrayList<ModelProduk> list = new ArrayList<>();
        for (String[] aData : data) {
            kategori = new ModelProduk();
            kategori.setName(aData[0]);
            kategori.setHarga(Integer.valueOf(aData[1]));
            kategori.setPath(aData[2]);

            list.add(kategori);
        }
        return list;
    }
}
