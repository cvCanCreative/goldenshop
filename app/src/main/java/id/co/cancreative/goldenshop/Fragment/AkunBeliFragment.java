package id.co.cancreative.goldenshop.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.cancreative.goldenshop.Activity.DaftarKomplainActivity;
import id.co.cancreative.goldenshop.Activity.DaftarTransaksiActivity;
import id.co.cancreative.goldenshop.Activity.DaftarTransaksiMenunguPembayaran;
import id.co.cancreative.goldenshop.Activity.FAQActivity;
import id.co.cancreative.goldenshop.Activity.KomplainBarangActivity;
import id.co.cancreative.goldenshop.Activity.PengaturanAkunActivity;
import id.co.cancreative.goldenshop.Activity.SplashActivity;
import id.co.cancreative.goldenshop.Activity.TerakhirDilihatActivity;
import id.co.cancreative.goldenshop.Activity.WithdrawActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.Model.TokoStatus;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AkunBeliFragment extends Fragment {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private View view;
    private Saldo saldo;
    private User user;

    private ImageView btnSetting;
    private LinearLayout divAkun;
    private ImageView btnKeranjang;
    private CircleImageView imgUser;
    private TextView tvNama;
    private TextView tvSaldo;
    private TextView btnAddSaldo;
    private TextView btnDetailsaldo;
    private TextView tvAkunStatus;
    private RelativeLayout btnHistoryTransaksi;
    private CircleImageView imgPenjual;
    private LinearLayout btnMenungguKonfirmasi;
    private CardView btnSaldo;
    private RelativeLayout fabRelativeBantuan;

    private LinearLayout btnPesananDiproses;
    private LinearLayout btnSedangDiproses;
    private LinearLayout btnSampaiTujuan;
    private RelativeLayout btnFavourit;
    private RelativeLayout btnTerakhirDilihat;
    private RelativeLayout btnKomplain;
    private RelativeLayout btnHistoryKomplain;
    private TextView btnSemuaTransaksi;
    private SwipeRefreshLayout swipeRefrash;

    public AkunBeliFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_akun_beli, container, false);
        initView(view);

        s = new SharedPref(getActivity());

        getValue();
        mainButton();
        pullrefrash();
        return view;
    }

    private void getValue() {
        saldo = s.getSaldo();
        user = s.getUser();

        setValue();
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefrash.setRefreshing(false);
                getData();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void setValue() {

        String name = user.fullname;
        if (name.isEmpty() || name == null) {
            name = user.username;
        }

        tvNama.setText(name);
        if (saldo.SA_SALDO != null && !saldo.SA_SALDO.equals("")) {
            tvSaldo.setText("" + new Helper().convertRupiah(Integer.valueOf(saldo.SA_SALDO)));
        }

        if (user.role.equals("RL_ADMIN")) {
            tvAkunStatus.setText("Login Sebagai Admin");
        }

        Picasso.with(getActivity())
                .load(Config.url_user + user.foto)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(imgUser);
    }

    private void getData() {

        SweetAlert.onLoading(getActivity());
        api.getAccount(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                swipeRefrash.setRefreshing(false);
                if (response.isSuccessful()) {
                    ResponModel respon = response.body();
                    if (respon.getSuccess() == 1) {

                        Saldo saldo = respon.result.saldo;
                        Toko toko = respon.result.toko;
                        TokoStatus statusToko = respon.result.cek_toko;

                        // Set Saldo
                        s.setSaldo(saldo);
                        s.setToko(toko);
                        s.setString(SharedPref.ID_TOKO, toko.ID_TOKO);
                        s.setMinWd(respon.result.min_wd);
                        Saldo min = s.getMinWd();
                        Log.d("Test", "" + min.minimal_wd);

                        // Set Status Toko
                        if (statusToko.success == 1) {
                            s.setBoolean(SharedPref.TOK_STATUS, true);
                            String status = statusToko.status;
                            if (status.equals("TK_BLM_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, "TK_BLM_AKTIF");
                            } else if (status.equals("TK_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, status);
                            }
                        } else if (statusToko.success == 0) {
                            s.setBoolean(SharedPref.TOK_STATUS, false);
                        }

                        getValue();

                    } else if (respon.getSuccess() == 0) {
                        if (respon.message.contains("Silahkan login kembali")) {
                            s.clearAll();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                swipeRefrash.setRefreshing(false);
                SweetAlert.dismis();
            }
        });
    }

    private void mainButton() {

        btnKeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("extra", "keranjang");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        });

        btnKomplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ia = new Intent(getActivity(), KomplainBarangActivity.class);
                ia.putExtra("extra", "komplainAll");
                ia.putExtra("status", "Komplain Sebagai Pembeli");
                startActivity(ia);
            }
        });

        btnHistoryKomplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ia = new Intent(getActivity(), DaftarKomplainActivity.class);
                startActivity(ia);
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PengaturanAkunActivity.class);
                startActivity(intent);
            }
        });

        btnHistoryTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiMenunguPembayaran.class);
                startActivity(intent);
            }
        });

        btnMenungguKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiActivity.class);
                intent.putExtra("FRAGMENT_ID", "0");
                startActivity(intent);
            }
        });

        btnSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WithdrawActivity.class);
                startActivity(intent);
            }
        });

        fabRelativeBantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FAQActivity.class);
                startActivity(intent);
            }
        });
        btnPesananDiproses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiActivity.class);
                intent.putExtra("FRAGMENT_ID", "1");
                startActivity(intent);
            }
        });

        btnSedangDiproses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiActivity.class);
                intent.putExtra("FRAGMENT_ID", "2");
                startActivity(intent);
            }
        });

        btnSampaiTujuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiActivity.class);
                intent.putExtra("FRAGMENT_ID", "3");
                startActivity(intent);
            }
        });

        btnSemuaTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiActivity.class);
                intent.putExtra("FRAGMENT_ID", "5");
                startActivity(intent);
            }
        });

        btnFavourit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasti.info(getActivity(), "Fungsi dalam Pengembangan");
            }
        });

        btnTerakhirDilihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toasti.info(getActivity(), "Fungsi dalam Pengembangan");
                startActivity(new Intent(getActivity(), TerakhirDilihatActivity.class));
            }
        });
    }

    private void initView(View view) {
        btnSetting = (ImageView) view.findViewById(R.id.btn_setting);
        divAkun = (LinearLayout) view.findViewById(R.id.div_akun);
        btnKeranjang = (ImageView) view.findViewById(R.id.btn_keranjang);
        imgUser = (CircleImageView) view.findViewById(R.id.img_penjual);
        tvNama = (TextView) view.findViewById(R.id.tv_nama);
        tvSaldo = (TextView) view.findViewById(R.id.tv_saldo);
        tvAkunStatus = (TextView) view.findViewById(R.id.tv_akunStatus);
        btnHistoryTransaksi = (RelativeLayout) view.findViewById(R.id.btn_historyTransaksi);
        imgPenjual = (CircleImageView) view.findViewById(R.id.img_penjual);
        btnMenungguKonfirmasi = (LinearLayout) view.findViewById(R.id.btn_menungguKonfirmasi);
        btnSaldo = (CardView) view.findViewById(R.id.btn_saldo);
        fabRelativeBantuan = (RelativeLayout) view.findViewById(R.id.fab_relative_bantuan);
        btnPesananDiproses = (LinearLayout) view.findViewById(R.id.btn_pesananDiproses);
        btnSedangDiproses = (LinearLayout) view.findViewById(R.id.btn_sedangDiproses);
        btnSampaiTujuan = (LinearLayout) view.findViewById(R.id.btn_sampaiTujuan);
        btnFavourit = (RelativeLayout) view.findViewById(R.id.btn_favourit);
        btnTerakhirDilihat = (RelativeLayout) view.findViewById(R.id.btn_terakhirDilihat);
        btnKomplain = (RelativeLayout) view.findViewById(R.id.btn_komplain);
        btnHistoryKomplain = (RelativeLayout) view.findViewById(R.id.btn_historyKomplain);
        btnSemuaTransaksi = (TextView) view.findViewById(R.id.btn_semuaTransaksi);
        swipeRefrash = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefrash);
    }


}
