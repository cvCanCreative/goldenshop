package id.co.cancreative.goldenshop.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import id.co.cancreative.goldenshop.Activity.LoginActivity;
import id.co.cancreative.goldenshop.Activity.PengaturanAkunActivity;
import id.co.cancreative.goldenshop.Activity.SplashActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SectionPageAdapter;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.MinWD;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.Model.TokoStatus;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AkunFragment extends Fragment {

    private SharedPref s;
    private Handler handler = new Handler();
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private Toko toko;
    private Saldo saldo;

    private String about_title_array[];
    private String about_description_array[];
    private TypedArray about_images_array;

    private MyViewPagerAdapter myViewPagerAdapter;
    private ImageView[] dots;
    private TabLayout tabsLayout;
    private ViewPager viewPager;
    private LinearLayout divLogin;
    private RelativeLayout divKeuntungan;
    private ImageView keuntunganIvClose;
    private ViewPager keuntunganViewpager;
    private LinearLayout layoutDots;
    private LinearLayout divKeuntunganLogin;
    private SwipeRefreshLayout swipeRefrash;


    public AkunFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_akun, container, false);
        initView(view);

        s = new SharedPref(getActivity());
        toko = s.getToko();
        saldo = s.getSaldo();

        pullrefrash();
        cekLoginStatus();
        getExtraIntent();

        return view;
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefrash.setRefreshing(false);
                getData();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void cekLoginStatus() {
        if (s.getStatusLogin()) {
            divLogin.setVisibility(View.VISIBLE);

            if (toko != null && saldo != null) {
                tabLayoutSetup();
            } else {
                getData();
            }
        } else {
            divLogin.setVisibility(View.GONE);
            instruksi();
        }
    }

    private void getExtraIntent() {
        String extra = getActivity().getIntent().getStringExtra("extra");

        if (extra != null) {

            if (extra.equals("akun")) {
                getData();
            }
        }
    }

    private void getData() {

        SweetAlert.onLoading(getActivity());
        api.getAccount(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    ResponModel respon = response.body();
                    if (respon.getSuccess() == 1) {

                        Saldo saldo = respon.result.saldo;
                        Toko toko = respon.result.toko;
                        TokoStatus statusToko = respon.result.cek_toko;

                        // Set Saldo
                        s.setSaldo(saldo);
                        s.setToko(toko);
                        s.setString(SharedPref.ID_TOKO, toko.ID_TOKO);
                        s.setMinWd(respon.result.min_wd);
                        Saldo min = s.getMinWd();
                        Log.d("Test", ""+min.minimal_wd);

                        // Set Status Toko
                        if (statusToko.success == 1) {
                            s.setBoolean(SharedPref.TOK_STATUS, true);
                            String status = statusToko.status;
                            if (status.equals("TK_BLM_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, "TK_BLM_AKTIF");
                            } else if (status.equals("TK_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, status);
                            }
                        } else if (statusToko.success == 0) {
                            s.setBoolean(SharedPref.TOK_STATUS, false);
                        }

                        tabLayoutSetup();

                    } else if (respon.getSuccess() == 0) {
                        if (respon.message.contains("Silahkan login kembali")){
                            s.clearAll();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
            }
        });
    }

    private void tabLayoutSetup() {
        this.handler = new Handler();
        this.handler.postDelayed(this.runnable, 500);
        setupViewPager(viewPager);
        tabsLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionPageAdapter adapter = new SectionPageAdapter(getFragmentManager());
        adapter.addFragment(new AkunBeliFragment(), "Beli");
        adapter.addFragment(new AkunJualFragment(), "Jual");
        viewPager.setAdapter(adapter);
    }

    public final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            setupViewPager(viewPager);
            tabsLayout.setupWithViewPager(viewPager);
        }
    };

    private void instruksi() {

        about_title_array = getResources().getStringArray(R.array.about_title_array);
        about_description_array = getResources().getStringArray(R.array.about_description_array);
        about_images_array = getResources().obtainTypedArray(R.array.about_images_array);

        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        keuntunganViewpager.setAdapter(myViewPagerAdapter);
        keuntunganViewpager.addOnPageChangeListener(viewPagerPageChangeListener);

        keuntunganIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });

        divKeuntunganLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("status", "login");
                startActivity(intent);
            }
        });

    }

    private void addBottomDots(int currentPage) {
        dots = new ImageView[about_title_array.length];

        layoutDots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(getActivity());
            int width_height = 20;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle_outline);
            layoutDots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setBackgroundResource(R.drawable.shape_circle);

    }

    private void initView(View view) {
        tabsLayout = (TabLayout) view.findViewById(R.id.tabsLayout);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        divLogin = (LinearLayout) view.findViewById(R.id.div_login);
        divKeuntungan = (RelativeLayout) view.findViewById(R.id.div_keuntungan);
        keuntunganIvClose = (ImageView) view.findViewById(R.id.keuntungan_iv_close);
        keuntunganViewpager = (ViewPager) view.findViewById(R.id.keuntungan_viewpager);
        layoutDots = (LinearLayout) view.findViewById(R.id.layoutDots);
        divKeuntunganLogin = (LinearLayout) view.findViewById(R.id.div_keuntungan_login);
        swipeRefrash = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefrash);
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.row_keuntungan, container, false);
            ((TextView) view.findViewById(R.id.tv_keuntungan_message)).setText(about_description_array[position]);
            ((ImageView) view.findViewById(R.id.iv_keuntungan)).setImageResource(about_images_array.getResourceId(position, -1));
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return about_title_array.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }

    }

}
