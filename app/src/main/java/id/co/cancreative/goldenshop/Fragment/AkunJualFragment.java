package id.co.cancreative.goldenshop.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.cancreative.goldenshop.Activity.BukaTokoActivity;
import id.co.cancreative.goldenshop.Activity.DaftarKomplainActivity;
import id.co.cancreative.goldenshop.Activity.DaftarPDTokoActivity;
import id.co.cancreative.goldenshop.Activity.DaftarPromoActivity;
import id.co.cancreative.goldenshop.Activity.DaftarTransaksiTokoActivity;
import id.co.cancreative.goldenshop.Activity.FAQActivity;
import id.co.cancreative.goldenshop.Activity.PengaturanAkunActivity;
import id.co.cancreative.goldenshop.Activity.SplashActivity;
import id.co.cancreative.goldenshop.Activity.TambahProdukActivity;
import id.co.cancreative.goldenshop.Activity.WithdrawActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.Model.TokoStatus;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AkunJualFragment extends Fragment {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private Saldo saldo;
    private Toko toko;
    private User user;

    private View view;
    private RelativeLayout divNoToko;
    private Button btnBukaToko;
    private LinearLayout divPesan;
    private TextView tvTitle;
    private TextView tvPesan;
    private TextView tvPesan2;
    private TextView tvNamaToko;
    private TextView tvNamaToko1;
    private RelativeLayout divTokoBelumAktif;
    private TextView tvSaldoToko;
    private ImageView btnSetting;
    private Button btnAddProduk;
    private LinearLayout btnSettingProduk;
    private ImageView imgToko;
    private Button btnHalamanToko;
    private LinearLayout divAkun;
    private ImageView btnKeranjang;
    private CircleImageView imgUser;
    private TextView tvAkunStatus;
    private CircleImageView imgPenjual;
    private CardView btnSaldo;
    private RelativeLayout btnSettingPromo;
    private LinearLayout btnPesananBaru;
    private LinearLayout btnSiapKirim;
    private LinearLayout btnSedangDikirim;
    private LinearLayout btnSampaiTujuan;
    private RelativeLayout btnBantuan;
    private RelativeLayout btnKomplain;
    private TextView btnSemuaTransaksi;
    private SwipeRefreshLayout swipeRefrash;

    public AkunJualFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_akun_jual, container, false);
        initView(view);

        s = new SharedPref(getActivity());

        getValue();
        mainButton();
        pullrefrash();
        return view;
    }


    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefrash.setRefreshing(false);
                getData();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void getValue() {
        saldo = s.getSaldo();
        toko = s.getToko();
        user = s.getUser();

        setValue();
    }

    private void setValue() {

        if (saldo.SA_SALDO != null && !saldo.SA_SALDO.equals("")) {
            tvSaldoToko.setText("" + new Helper().convertRupiah(Integer.valueOf(saldo.SA_SALDO)));
        }
        tvNamaToko.setText(toko.TOK_NAME);
        tvNamaToko1.setText(toko.TOK_NAME);

        Picasso.with(getActivity())
                .load(Config.url_toko + toko.TOK_FOTO)
//                .load(R.drawable.image_loading)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(imgUser);

        cekStatusToko();
        cekAkunRole();
    }

    private void cekStatusToko() {
        if (s.getStatusToko() == true) {
            divNoToko.setVisibility(View.GONE);
//            String status = s.getString(SharedPref.TOK_AKTIF);
//            Log.d("Status Toko", "Status " + status);
//            if (status != null) {
//                if (status.equals("TK_BLM_AKTIF")) {
//                    divTokoBelumAktif.setVisibility(View.VISIBLE);
//                    tvTitle.setText("Toko Anda Belum Aktif!");
//                    tvPesan.setText("Akun Toko anda belum terverifikasi, silahkan tunggu konfirmasi dari kami");
//                    tvPesan2.setVisibility(View.GONE);
//                }
//            }
        } else {
            divNoToko.setVisibility(View.VISIBLE);
            divPesan.setVisibility(View.VISIBLE);
            btnBukaToko.setVisibility(View.VISIBLE);
        }
    }

    private void cekAkunRole() {
        if (user.role.equals("RL_ADMIN")) {
            tvAkunStatus.setText("Login Sebagai Admin");
        } else {
            if (toko.TOK_STATUS.equals("TK_AKTIF")) {
                tvAkunStatus.setText("Verified Account");
            } else if (toko.TOK_STATUS.equals("TK_BLM_AKTIF")) {
                tvAkunStatus.setText("Toko Belum Terverifikasi");
            } else if (toko.TOK_STATUS.equals("TK_TDK_AKTIF")) {
                tvAkunStatus.setText("Toko Tidak Aktif");
            }
        }
    }

    private void getData() {

        SweetAlert.onLoading(getActivity());
        api.getAccount(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                SweetAlert.dismis();
                if (response.isSuccessful()) {
                    ResponModel respon = response.body();
                    if (respon.getSuccess() == 1) {

                        Saldo saldo = respon.result.saldo;
                        Toko toko = respon.result.toko;
                        TokoStatus statusToko = respon.result.cek_toko;

                        // Set Saldo
                        s.setSaldo(saldo);
                        s.setToko(toko);
                        s.setString(SharedPref.ID_TOKO, toko.ID_TOKO);
                        s.setMinWd(respon.result.min_wd);
                        Saldo min = s.getMinWd();
                        Log.d("Test", ""+min.minimal_wd);

                        // Set Status Toko
                        if (statusToko.success == 1) {
                            s.setBoolean(SharedPref.TOK_STATUS, true);
                            String status = statusToko.status;
                            if (status.equals("TK_BLM_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, "TK_BLM_AKTIF");
                            } else if (status.equals("TK_AKTIF")) {
                                s.setString(SharedPref.TOK_AKTIF, status);
                            }
                        } else if (statusToko.success == 0) {
                            s.setBoolean(SharedPref.TOK_STATUS, false);
                        }

                        getValue();

                    } else if (respon.getSuccess() == 0) {
                        if (respon.message.contains("Silahkan login kembali")){
                            s.clearAll();
                            Intent intent = new Intent(getActivity(), SplashActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.dismis();
            }
        });
    }

    private void mainButton() {

        btnKeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("extra", "keranjang");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        });

        btnKomplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ia = new Intent(getActivity(), DaftarKomplainActivity.class);
                ia.putExtra("extra", "komplainAll");
                ia.putExtra("status", "Komplain Sebagai Penjual");
                startActivity(ia);
            }
        });

        btnBukaToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BukaTokoActivity.class);
                startActivity(intent);
            }
        });


        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PengaturanAkunActivity.class);
                startActivity(intent);
            }
        });

        btnAddProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (toko.TOK_STATUS.equals("TK_TDK_AKTIF")) {
                    SweetAlert.success(getActivity(), "Tidak Bisa Menambah Produk", "Toko Anda Tidak Aktif, hubungi Admin untuk mengaktifkan kembali Toko Anda");
                    return;
                }

                Intent intent = new Intent(getActivity(), TambahProdukActivity.class);
                startActivity(intent);
            }
        });

        btnSettingProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarPDTokoActivity.class);
                startActivity(intent);
            }
        });

        btnSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WithdrawActivity.class);
                startActivity(intent);
            }
        });

        btnSettingPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarPromoActivity.class);
                intent.putExtra("extra", "produk");
                startActivity(intent);
            }
        });

        btnPesananBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiTokoActivity.class);
                intent.putExtra("FRAGMENT_ID", "0");
                startActivity(intent);
            }
        });

        btnSiapKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiTokoActivity.class);
                intent.putExtra("FRAGMENT_ID", "1");
                startActivity(intent);
            }
        });

        btnSedangDikirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiTokoActivity.class);
                intent.putExtra("FRAGMENT_ID", "2");
                startActivity(intent);
            }
        });

        btnSampaiTujuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiTokoActivity.class);
                intent.putExtra("FRAGMENT_ID", "4");
                startActivity(intent);
            }
        });

        btnSemuaTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiTokoActivity.class);
                intent.putExtra("FRAGMENT_ID", "4");
                startActivity(intent);
            }
        });

        btnBantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FAQActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initView(View view) {
        divNoToko = (RelativeLayout) view.findViewById(R.id.div_noToko);
        btnBukaToko = (Button) view.findViewById(R.id.btn_bukaToko);
        divPesan = (LinearLayout) view.findViewById(R.id.div_pesan);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvPesan = (TextView) view.findViewById(R.id.tv_pesan);
        tvPesan2 = (TextView) view.findViewById(R.id.tv_pesan2);
        tvNamaToko = (TextView) view.findViewById(R.id.tv_namaToko);
        tvNamaToko1 = (TextView) view.findViewById(R.id.tv_namaToko1);
        divTokoBelumAktif = (RelativeLayout) view.findViewById(R.id.div_tokoBelumAktif);
        tvSaldoToko = (TextView) view.findViewById(R.id.tv_saldoToko);
        btnSetting = (ImageView) view.findViewById(R.id.btn_setting);
        btnAddProduk = (Button) view.findViewById(R.id.btn_addProduk);
        btnSettingProduk = (LinearLayout) view.findViewById(R.id.btn_settingProduk);
        imgToko = (ImageView) view.findViewById(R.id.img_toko);
        btnHalamanToko = (Button) view.findViewById(R.id.btn_halamanToko);
        divAkun = (LinearLayout) view.findViewById(R.id.div_akun);
        btnKeranjang = (ImageView) view.findViewById(R.id.btn_keranjang);
        imgUser = (CircleImageView) view.findViewById(R.id.img_penjual);
        tvAkunStatus = (TextView) view.findViewById(R.id.tv_akunStatus);
        imgPenjual = (CircleImageView) view.findViewById(R.id.img_penjual);
        btnSaldo = (CardView) view.findViewById(R.id.btn_saldo);
        btnSettingPromo = (RelativeLayout) view.findViewById(R.id.btn_settingPromo);
        btnPesananBaru = (LinearLayout) view.findViewById(R.id.btn_pesananBaru);
        btnSiapKirim = (LinearLayout) view.findViewById(R.id.btn_siapKirim);
        btnSedangDikirim = (LinearLayout) view.findViewById(R.id.btn_sedangDikirim);
        btnSampaiTujuan = (LinearLayout) view.findViewById(R.id.btn_sampaiTujuan);
        btnBantuan = (RelativeLayout) view.findViewById(R.id.btn_bantuan);
        btnKomplain = (RelativeLayout) view.findViewById(R.id.btn_komplain);
        btnSemuaTransaksi = (TextView) view.findViewById(R.id.btn_semuaTransaksi);
        swipeRefrash = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefrash);
    }
}
