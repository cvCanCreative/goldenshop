package id.co.cancreative.goldenshop.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaruFragment extends Fragment {

    private SharedPref s;
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private ProgressBar pd;
    private RecyclerView rv;
    private LinearLayout layoutKosong;
    private TextView tvTitle;
    private TextView tvPesan;

    public BaruFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_withdraw, container, false);
        initView(view);

        s = new SharedPref(getContext());

        return view;
    }

    private void initView(View view) {
        pd = (ProgressBar) view.findViewById(R.id.pd);
    }
}
