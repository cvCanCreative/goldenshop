package id.co.cancreative.goldenshop.Fragment;


import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Activity.LoginActivity;
import id.co.cancreative.goldenshop.Activity.PesananAturActivity;
import id.co.cancreative.goldenshop.Activity.PesananCheckoutActivity;
import id.co.cancreative.goldenshop.Adapter.AdapterKranjang;
import id.co.cancreative.goldenshop.Adapter.AdapterKranjangByToko;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.AppDbKrenjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko.TbKranjangByToko;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.AppDbKrenjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment {

    private SharedPref s;
    private View view;
    private View parent_view;

    //SQLite
    private AppDbKrenjang db;
    private AppDbKrenjangByToko dbtoko;
    private RecyclerView rvView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<TbKranjang> daftarBarang;
    private ArrayList<TbKranjangByToko> listBarang;

    private int sum, hargaAwal, hargaSekarang;

    private RelativeLayout lyDisplay;
    private RecyclerView rvMain;
    private LinearLayout layoutku;
    private TextView tvTotalHarga;
    private Button btnCheckout;
    private LinearLayout lyKosong;
    private TextView tvPesan;
    private CardView divHeader;
    private CardView divSelect;
    private AppCompatCheckBox checkBox;
    private TextView tvHeader;

    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_cart, container, false);
        initView(view);

        s = new SharedPref(getActivity());
        db = Room.databaseBuilder(getActivity(), AppDbKrenjang.class, Config.db_keranjang).allowMainThreadQueries().build();
        dbtoko = Room.databaseBuilder(getActivity(), AppDbKrenjangByToko.class, Config.db_keranjangToko).allowMainThreadQueries().build();

        callFunction();
        displayKranjang();
        displayKranjangByToko();
        mainButton();

        tvHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDataKranjang();
            }
        });

        return view;
    }

    private void displayKranjangByToko() {
        listBarang = new ArrayList<>();
        listBarang.addAll(Arrays.asList(dbtoko.daoKranjang().selectAllBarangs()));

        rvView = view.findViewById(R.id.rv_main);
        rvView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rvView.setLayoutManager(layoutManager);
        adapter = new AdapterKranjangByToko(listBarang, getActivity());
        rvView.setAdapter(adapter);
    }

    private void mainButton() {
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    for (TbKranjang d : daftarBarang) {
                        d.pilih = true;
                        updateBarang(d);
                    }
                } else {
                    for (TbKranjang d : daftarBarang) {
                        d.pilih = false;
                        updateBarang(d);
                    }
                    hitungUlang();
                }
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (s.getStatusLogin()) {
                    daftarBarang = new ArrayList<>();
                    daftarBarang.addAll(Arrays.asList(db.daoKranjang().selectBarangsTerpilih(true)));
                    if (daftarBarang.size() != 0) {
                        String status = "";
                        for (TbKranjang d : daftarBarang){

                        }

                        Intent i = new Intent(getActivity(), PesananCheckoutActivity.class);
                        startActivity(i);
                    } else {
                        Toasti.error(getActivity(), "Silahkan pilih terlebih dahulu produk yang ingin anda chekout");
                    }
                    return;
                }

                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);


            }
        });
    }

    private void callFunction() {
        CallFunction.setRefreshKeranjang(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                refrashKeranjang();
                return null;
            }
        });

        CallFunction.sethitungUlangKeranjang(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                hitungUlang();
                return null;
            }
        });
    }

    private void displayKranjang() {

//        rvView = view.findViewById(R.id.rv_main);
//        rvView.setHasFixedSize(true);
//        layoutManager = new LinearLayoutManager(getActivity());
//        rvView.setLayoutManager(layoutManager);
//        adapter = new AdapterKranjang(daftarBarang, getActivity());
//        rvView.setAdapter(adapter);

        daftarBarang = new ArrayList<>();
        daftarBarang.addAll(Arrays.asList(db.daoKranjang().selectAllBarangs()));

        int nilai = daftarBarang.size();
//        Log.d("Test", "d: " + nilai);
        tvHeader.setText("Cart (" + nilai + ")");
        if (nilai == 0) {
            lyDisplay.setVisibility(View.GONE);
            lyKosong.setVisibility(View.VISIBLE);
        } else {
            lyDisplay.setVisibility(View.VISIBLE);
            lyKosong.setVisibility(View.GONE);
        }

        for (TbKranjang b : daftarBarang) {

            if (b.pilih == true) {
                hargaSekarang = b.jumlahPesan * b.harga;
                sum = sum + hargaSekarang;
            }
        }
        tvTotalHarga.setText("" + new Helper().convertRupiah(sum));
    }

    private void refrashKeranjang() {

        daftarBarang = new ArrayList<>();
        daftarBarang.addAll(Arrays.asList(db.daoKranjang().selectAllBarangs()));

        listBarang = new ArrayList<>();
        listBarang.addAll(Arrays.asList(dbtoko.daoKranjang().selectAllBarangs()));
        int nilai = daftarBarang.size();
        tvHeader.setText("Cart (" + nilai + ")");
        if (nilai == 0) {
            lyDisplay.setVisibility(View.GONE);
            lyKosong.setVisibility(View.VISIBLE);
        } else {
            lyDisplay.setVisibility(View.VISIBLE);
            lyKosong.setVisibility(View.GONE);
        }

        sum = 0;
        for (TbKranjang b : daftarBarang) {
            if (b.pilih == true) {
                hargaSekarang = b.jumlahPesan * b.harga;
                sum = sum + hargaSekarang;
            }
        }
        tvTotalHarga.setText("" + new Helper().convertRupiah(sum));

        adapter = new AdapterKranjangByToko(listBarang, getActivity());
        rvView.setAdapter(adapter);
    }

    private void hitungUlang() {
        daftarBarang = new ArrayList<>();
        daftarBarang.addAll(Arrays.asList(db.daoKranjang().selectAllBarangs()));
        sum = 0;
        for (TbKranjang b : daftarBarang) {

            if (b.pilih == true) {
                hargaSekarang = b.jumlahPesan * b.harga;
                sum = sum + hargaSekarang;
            }
        }
        tvTotalHarga.setText("" + new Helper().convertRupiah(sum));
    }

    @SuppressLint("StaticFieldLeak")
    private void updateBarang(final TbKranjang barang) {
        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
//                AppDbKrenjang db;
//                db = Room.databaseBuilder(getActivity(),
//                        AppDbKrenjang.class, Config.db_keranjang)
//                        .build();
                long status = db.daoKranjang().updateBarang(barang);
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
                refrashKeranjang();
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void deleteDataKranjang() {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses Delete data
                AppDbKrenjangByToko db;
                db = Room.databaseBuilder(getActivity(),
                        AppDbKrenjangByToko.class, Config.db_keranjangToko)
                        .build();

                long status = db.daoKranjang().deleteAll();
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
//                Toast.makeText(getApplicationContext(), "barangByH add " + status, Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

    private void initView(View view) {
        lyDisplay = (RelativeLayout) view.findViewById(R.id.ly_display);
        rvMain = (RecyclerView) view.findViewById(R.id.rv_main);
        layoutku = (LinearLayout) view.findViewById(R.id.layoutku);
        tvTotalHarga = (TextView) view.findViewById(R.id.tv_totalHarga);
        btnCheckout = (Button) view.findViewById(R.id.btn_checkout);
        lyKosong = (LinearLayout) view.findViewById(R.id.ly_kosong);
        tvPesan = (TextView) view.findViewById(R.id.tv_pesan);
        divSelect = (CardView) view.findViewById(R.id.div_select);
        checkBox = (AppCompatCheckBox) view.findViewById(R.id.check_box);
        tvHeader = (TextView) view.findViewById(R.id.tv_header);
    }
}
