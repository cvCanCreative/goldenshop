package id.co.cancreative.goldenshop.Fragment.DaftarTransaksi;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.DaftarTrans.AdapterDiproses;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class WaitingConfFragment extends Fragment {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private View view;
    private ArrayList<Transaksi> mItem = new ArrayList<>();

    private String Status;
    private LinearLayout layout;
    private TextView tvPesan;
    private SwipeRefreshLayout swipeRefrash;
    private RecyclerView rv;
    private ProgressBar pd;
    private LinearLayout layoutKosong;

    @SuppressLint("ValidFragment")
    public WaitingConfFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_menunggu_konf, container, false);
        initView(view);

        s = new SharedPref(getActivity());

        mainButton();
        getTransaksi();
        pullrefrash();
        return view;
    }

    private void getTransaksi() {
        api.getHistoryTransaksi(s.getApi(), s.getIdUser(), s.getSession(), "PROSES_ADMIN").enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                if (response.body().success == 1) {
                    pd.setVisibility(View.GONE);
                    swipeRefrash.setRefreshing(false);
                    layoutKosong.setVisibility(View.GONE);
                    mItem = response.body().transaksi;

                    RecyclerView.Adapter mAdapter = new AdapterDiproses(mItem, getActivity());
                    rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);
                } else {
                    pd.setVisibility(View.GONE);
                    layoutKosong.setVisibility(View.VISIBLE);
                    swipeRefrash.setRefreshing(false);

                    mItem = new ArrayList<>();
                    RecyclerView.Adapter mAdapter = new AdapterDiproses(mItem, getActivity());
                    rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                layoutKosong.setVisibility(View.VISIBLE);
                Toasti.onFailure(getActivity(), t.getMessage());
            }
        });
    }

    private void mainButton() {

    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getTransaksi();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void initView(View view) {

        layout = (LinearLayout) view.findViewById(R.id.layout);
        tvPesan = (TextView) view.findViewById(R.id.tv_pesan);
        swipeRefrash = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefrash);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        pd = (ProgressBar) view.findViewById(R.id.pd);
        layoutKosong = (LinearLayout) view.findViewById(R.id.layout_kosong);
    }

    @Override
    public void onResume() {
        getTransaksi();
        super.onResume();
    }
}
