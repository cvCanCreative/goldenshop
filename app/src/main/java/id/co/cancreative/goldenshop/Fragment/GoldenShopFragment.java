package id.co.cancreative.goldenshop.Fragment;


import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiConfigDemo;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelSerializ;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GoldenShopFragment extends Fragment {

    private ApiService apiDemo = ApiConfigDemo.getInstanceRetrofit();
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private View view;
    private SharedPref s;
    private AppDbTerakhirDilihat db;

    private ArrayList<Produk> mItem = new ArrayList<>();
    private int pastVisibleitem, visibleItemCount, totalItemCount, previousTotal = 0;
    private int indexLoad = 1;
    private int viewThreshold = 8;
    private boolean isLoading = true;

    private GridLayoutManager layoutManager;
    private AdapterProdukTrading mAdapter;

    private RecyclerView rv;
    private ProgressBar pd;
    private LinearLayout layout;
    private TextView tvPesan;
    private ProgressBar pb;

    public GoldenShopFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);
        initView(view);

        s = new SharedPref(getActivity());
        db = Room.databaseBuilder(getActivity(), AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();

        setlayoutManager();
        getProduk();

        return view;
    }

    private void setlayoutManager() {
        layoutManager = new GridLayoutManager(getActivity(), 2);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(layoutManager);
    }

    private void getProduk() {

        api.getProdukGS(s.getApi(), 1).enqueue(new Callback<ResponModelSerializ>() {
            @Override
            public void onResponse(Call<ResponModelSerializ> call, Response<ResponModelSerializ> response) {
//                swipeRefrash.setRefreshing(false);
                pd.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ResponModelSerializ produk = response.body();

                    mItem = (ArrayList<Produk>) produk.products;
                    mAdapter = new AdapterProdukTrading(mItem, getActivity(), db);
                    rv.setAdapter(mAdapter);
                }

            }

            @Override
            public void onFailure(Call<ResponModelSerializ> call, Throwable t) {
                if (getActivity() != null) {
                    SweetAlert.onFailure(getActivity(), t.getMessage());
                }
            }
        });

        setupPageination();
    }

    private void setupPageination() {
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
                        indexLoad = indexLoad + 1;
                        loadProductGS(indexLoad);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadProductGS(int index) {
        pb.setVisibility(View.VISIBLE);
        api.getProdukGS(s.getApi(), index).enqueue(new Callback<ResponModelSerializ>() {
            @Override
            public void onResponse(Call<ResponModelSerializ> call, Response<ResponModelSerializ> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    List<Produk> produks = response.body().products;
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModelSerializ> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(getActivity(), t.getMessage());
            }
        });
    }

    private void initView(View view) {
        rv = (RecyclerView) view.findViewById(R.id.rv);
        pd = (ProgressBar) view.findViewById(R.id.pd);
        layout = (LinearLayout) view.findViewById(R.id.layout);
        tvPesan = (TextView) view.findViewById(R.id.tv_pesan);
        pb = (ProgressBar) view.findViewById(R.id.progressBar);
    }
}
