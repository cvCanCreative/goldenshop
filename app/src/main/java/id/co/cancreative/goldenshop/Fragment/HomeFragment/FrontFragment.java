package id.co.cancreative.goldenshop.Fragment.HomeFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FrontFragment extends Fragment {

    private SharedPref s;
    private ApiService api = ApiConfig.getInstanceRetrofit();

    public FrontFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_baru2, container, false);
        initView(view);

        s = new SharedPref(getContext());

        return view;
    }

    private void initView(View view) {
    }
}
