package id.co.cancreative.goldenshop.Fragment.HomeFragment;


import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.co.cancreative.goldenshop.Adapter.AdapterProductNew;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiConfigDemo;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Tools;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelSerializ;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TerlarisFragment extends Fragment {

    private ApiService apiDemo = ApiConfigDemo.getInstanceRetrofit();
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private View view;
    private SharedPref s;
    private AppDbTerakhirDilihat db;

    private ArrayList<Produk> mItem = new ArrayList<>();
    private int pastVisibleitem, visibleItemCount, totalItemCount, previousTotal = 0;
    private int indexLoad = 1;
    private int viewThreshold = 8;
    private boolean isLoading = true;
    private int post_total = 0;

    private GridLayoutManager layoutManager;
    private AdapterProductNew mAdapter;

    private RecyclerView rv;
    private ProgressBar pd;
    private LinearLayout layout;
    private TextView tvPesan;
    private ProgressBar pb;

    public TerlarisFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);
        initView(view);

        s = new SharedPref(getActivity());
        db = Room.databaseBuilder(getActivity(), AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();

        setlayoutManager();
        requestAction(1);
//        getProduk(1);

        return view;
    }

    private void requestAction(final int page_no) {
        if (page_no == 1) {

        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadProductGS(page_no);
            }
        }, 1000);
    }

    private void setlayoutManager() {
        layoutManager = new GridLayoutManager(getActivity(), Tools.getGridSpanCount(getActivity()));
        rv.setHasFixedSize(true);
        rv.setLayoutManager(layoutManager);

        //set data and list adapter
        mAdapter = new AdapterProductNew(mItem, getActivity(), db, rv);
        rv.setAdapter(mAdapter);

        mAdapter.setOnLoadMoreListener(new AdapterProductNew.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (post_total > mAdapter.getItemCount() && current_page != 0) {
                    requestAction(current_page + 1);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });
    }

    private void loadProductGS(int index) {
        pb.setVisibility(View.VISIBLE);
        api.getTerlarisProduk(s.getApi(), index).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    List<Produk> produks = response.body().products;
                    if (response.body().jml_produk != null){
                        post_total = response.body().jml_produk;
                    } else {
                        post_total = 0;
                    }
                    mAdapter.addData(produks, rv);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(getActivity(), t.getMessage());
            }
        });
    }

    private void initView(View view) {
        rv = (RecyclerView) view.findViewById(R.id.rv);
        pd = (ProgressBar) view.findViewById(R.id.pd);
        layout = (LinearLayout) view.findViewById(R.id.layout);
        tvPesan = (TextView) view.findViewById(R.id.tv_pesan);
        pb = (ProgressBar) view.findViewById(R.id.progressBar);
    }
}
