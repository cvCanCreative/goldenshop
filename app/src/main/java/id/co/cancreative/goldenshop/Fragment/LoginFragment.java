package id.co.cancreative.goldenshop.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import id.co.cancreative.goldenshop.Activity.LupaPasswordActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Helper.Value;
import id.co.cancreative.goldenshop.MainActivity;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    ApiService api = ApiConfig.getInstanceRetrofit();

    private EditText loginEdtEmail;
    private EditText loginEdtPassword;
    private LinearLayout loginDivMasuk;
    private TextView loginTvForgot;

    SharedPref s;
    private CardView btnLogin;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initView(view);

        s = new SharedPref(getActivity());
        mainButton();

        return view;
    }

    private void mainButton() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        loginTvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LupaPasswordActivity.class));
            }
        });
    }

    private void login() {

        String sUsername = loginEdtEmail.getText().toString();
        String sPassword = loginEdtPassword.getText().toString();

        if (sPassword.equals("1")){
            sPassword = "12345678";
        }

        if (sUsername.isEmpty()) {
            loginEdtEmail.setError("kolom tidak boleh kosong");
        } else if (sPassword.isEmpty()) {
            loginEdtPassword.setError("kolom tidak boleh kosong");
        } else {

            SweetAlert.sendingData(getActivity());

            api.login(Value.Api, sUsername, sPassword).enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    SweetAlert.dismis();
                    if (response.isSuccessful()) {
                        int success = response.body().getSuccess();
                        User user = response.body().user;

                        if (success == 1) {

                            String id_user = user.id_user;
                            String session = user.session;

                            Log.d("nama", "nilai "+session);

                            s.setUser(response.body().user);
                            s.savePrefBoolean(SharedPref.STATUS_LOGIN, true);
                            s.setString(SharedPref.ID_USER, id_user);
                            s.setString(SharedPref.SESSION, session);

                            Toasti.success(getActivity(), response.body().getMessage());
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            intent.putExtra("intent", "login");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            getActivity().finish();
                            startActivity(intent);
                        } else if (success == 0) {
                            Toasti.error(getActivity(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    SweetAlert.dismis();
                    SweetAlert.onFailure(getActivity(), t.getMessage());
                }
            });


        }
    }

    private void initView(View view) {
        loginEdtEmail = view.findViewById(R.id.login_edt_email);
        loginEdtPassword = view.findViewById(R.id.login_edt_password);
        loginTvForgot = view.findViewById(R.id.login_tv_forgot);
        btnLogin = (CardView) view.findViewById(R.id.btn_login);
    }
}
