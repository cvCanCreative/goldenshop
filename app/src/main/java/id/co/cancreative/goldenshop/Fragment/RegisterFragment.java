package id.co.cancreative.goldenshop.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import id.co.cancreative.goldenshop.Activity.LoginActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Helper.Value;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    ApiService api = ApiConfig.getInstanceRetrofit();

    private EditText registerEdtUsername;
    private EditText registerEdtTelpon;
    private EditText registerEdtAlamat;
    private EditText registerEdtEmail;
    private EditText registerEdtPassword;

    SharedPref pref;
    private CardView btnRegister;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        initView(view);

        pref = new SharedPref(getActivity());
        mainButton();

        return view;
    }

    private void mainButton() {

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

    }

    private void register() {

        String sUsername = registerEdtUsername.getText().toString();
        String sPassword = registerEdtPassword.getText().toString();
        String sEmail = registerEdtEmail.getText().toString();
        String sTelp = registerEdtTelpon.getText().toString();
        String sAlamat = registerEdtAlamat.getText().toString();

        if (sUsername.isEmpty()) {
            registerEdtUsername.setError("kolom tidak boleh kosong");
        } else if (sPassword.isEmpty()) {
            registerEdtPassword.setError("kolom tidak boleh kosong");
        }
//        else if (sPassword.length() <= 6) {
//            registerEdtPassword.setError("Minimal 6 karakter");
//        }
        else if (sEmail.isEmpty()) {
            registerEdtEmail.setError("kolom tidak boleh kosong");
        } else if (sTelp.isEmpty()) {
            registerEdtTelpon.setError("kolom tidak boleh kosong");
        } else if (sAlamat.isEmpty()) {
            registerEdtAlamat.setError("kolom tidak boleh kosong");
        } else {

            String phonenum1 = sTelp;
            phonenum1 = phonenum1.replace(" ", "");
            phonenum1 = phonenum1.replace("-", "");
            if (phonenum1.startsWith("08")) {
                phonenum1 = "+62" + phonenum1.substring(1);
            } else if (phonenum1.startsWith("+")) {
                phonenum1 = phonenum1;
            } else if (phonenum1.startsWith("8")) {
                phonenum1 = "+" + "62" + phonenum1;
            } else if (!phonenum1.startsWith("8") && !phonenum1.startsWith("+") && !phonenum1.startsWith("08")){
                Toasti.error(getActivity(), "Nomor Telpon Invalid");
            }

            Log.d("Nomer", phonenum1);

            SweetAlert.sendingData(getActivity());

            api.register(Value.Api, sUsername , sEmail, sPassword, sEmail, phonenum1, sAlamat).enqueue(new Callback<ResponModel>() {
                @Override
                public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                    SweetAlert.dismis();
                    if (response.isSuccessful()) {
                        int success = response.body().getSuccess();
                        if (success == 1) {
                            Toasti.success(getActivity(), response.body().getMessage());
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            getActivity().finish();
                            startActivity(intent);
                        } else if (success == 0) {
                            Toasti.error(getActivity(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponModel> call, Throwable t) {
                    SweetAlert.dismis();
                    SweetAlert.onFailure(getActivity(), t.getMessage());
                }
            });
        }
    }

    private void initView(View view) {
        registerEdtUsername = view.findViewById(R.id.register_edt_username);
        registerEdtTelpon = view.findViewById(R.id.register_edt_telpon);
        registerEdtAlamat = view.findViewById(R.id.register_edt_alamat);
        registerEdtEmail = view.findViewById(R.id.register_edt_email);
        registerEdtPassword = view.findViewById(R.id.register_edt_password);
        btnRegister = (CardView) view.findViewById(R.id.btn_register);
    }
}
