package id.co.cancreative.goldenshop.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.AdapterTransaksiToko;
import id.co.cancreative.goldenshop.Adapter.AdapterWithdraw;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.TransaksiToko;
import id.co.cancreative.goldenshop.Model.Withdraw;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransaksiTokoFragment extends Fragment {

    private SharedPref s;
    private ApiService api = ApiConfig.getInstanceRetrofit();

    private ProgressBar pd;
    private RecyclerView rv;
    private LinearLayout layoutKosong;
    private TextView tvTitle;
    private TextView tvPesan;
    private SwipeRefreshLayout swipeRefrash;

    public TransaksiTokoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_withdraw, container, false);
        initView(view);

        s = new SharedPref(getContext());
        getHistoryWithdraw();
        pullrefrash();

        return view;
    }

    private void getHistoryWithdraw() {
        api.getTransaksiToko(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                layoutKosong.setVisibility(View.GONE);

                if (response.body().success == 1) {



                    ArrayList<TransaksiToko> mItem = response.body().history_tp;
                    RecyclerView.Adapter mAdapter = new AdapterTransaksiToko(mItem, getActivity());
                    rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);
                } else {
                    layoutKosong.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                layoutKosong.setVisibility(View.VISIBLE);
                Toasti.onFailure(getActivity(), t.getMessage());
            }
        });
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHistoryWithdraw();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void initView(View view) {
        pd = (ProgressBar) view.findViewById(R.id.pd);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        layoutKosong = (LinearLayout) view.findViewById(R.id.layout_kosong);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvPesan = (TextView) view.findViewById(R.id.tv_pesan);
        swipeRefrash = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefrash);
    }
}
