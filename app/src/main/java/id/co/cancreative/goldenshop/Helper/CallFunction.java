package id.co.cancreative.goldenshop.Helper;

import java.util.concurrent.Callable;

public class CallFunction {

    private static Callable<Void> setValuePesananActivity;
    private static Callable<Void> setValuePengaturanToko;
    private static Callable<Void> setValuePengaturanTokoAlamat;
    private static Callable<Void> refreshListProdukTK;
    private static Callable<Void> refreshListAlamat;
    private static Callable<Void> refreshListRekening;
    private static Callable<Void> refreshKeranjang;
    private static Callable<Void> hitungUlangKeranjang;
    private static Callable<Void> checkBoxCart;
    private static Callable<Void> refrashChekout;
    private static Callable<Void> valueChekout;
    private static Callable<Void> refrashSaldo;
    private static Callable<Void> setNamaProduk;
    private static Callable<Void> refrashPromo;
    private static Callable<Void> deleteKranjangToko;
    private static Callable<Void> ReftashTransSelesai;

    public static void setReftashTransSelesai(Callable<Void> addFunctions){
        ReftashTransSelesai = addFunctions;
    }

    public static Callable<Void> getReftashTransSelesai(){
        try {
            ReftashTransSelesai.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ReftashTransSelesai;

    }

    public static void setdeleteKranjangToko(Callable<Void> addFunctions){
        deleteKranjangToko = addFunctions;
    }

    public static Callable<Void> getdeleteKranjangToko(){
        try {
            deleteKranjangToko.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deleteKranjangToko;

    }

    public static void setRefrashPromo(Callable<Void> addFunctions){
        refrashPromo = addFunctions;
    }

    public static Callable<Void> getrefrashPromo(){
        try {
            refrashPromo.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return refrashPromo;

    }

    public static void setNamaProduk(Callable<Void> addFunctions){
        setNamaProduk = addFunctions;
    }

    public static Callable<Void> getNamaProduk(){
        try {
            setNamaProduk.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return setNamaProduk;

    }

    public static void setrefrashSaldo(Callable<Void> addFunctions){
        refrashSaldo = addFunctions;
    }

    public static Callable<Void> getrefrashSaldo(){
        try {
            refrashSaldo.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return refrashSaldo;

    }

    public static void setvalueChekout(Callable<Void> addFunctions){
        valueChekout = addFunctions;
    }

    public static Callable<Void> getvalueChekouty(){
        try {
            valueChekout.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valueChekout;

    }

    public static void setPesananActivity(Callable<Void> addFunctions){
        setValuePesananActivity = addFunctions;
    }

    public static Callable<Void> getPesananActivity(){
        try {
            setValuePesananActivity.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return setValuePesananActivity;

    }

    public static void setPengaturanToko(Callable<Void> addFunctions){
        setValuePengaturanToko = addFunctions;
    }

    public static Callable<Void> getPengaturanToko(){
        try {
            setValuePengaturanToko.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return setValuePengaturanToko;

    }

    public static void setPengaturanTokoAlamat(Callable<Void> addFunctions){
        setValuePengaturanTokoAlamat = addFunctions;
    }

    public static Callable<Void> getPengaturanTokoAlamat(){
        try {
            setValuePengaturanTokoAlamat.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return setValuePengaturanTokoAlamat;

    }

    public static void setRefreshListProdukTK(Callable<Void> addFunctions){
        refreshListProdukTK = addFunctions;
    }

    public static Callable<Void> getRefreshListProdukTK(){
        try {
            refreshListProdukTK.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return refreshListProdukTK;

    }

    public static void setRefreshListAlamat(Callable<Void> addFunctions){
        refreshListAlamat = addFunctions;
    }

    public static Callable<Void> getRefreshListAlamat(){
        try {
            refreshListAlamat.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return refreshListAlamat;

    }

    public static void setRefreshListRekening(Callable<Void> addFunctions){
        refreshListRekening = addFunctions;
    }

    public static Callable<Void> getRefreshListRekening(){
        try {
            refreshListRekening.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return refreshListRekening;

    }

    public static void setRefreshKeranjang(Callable<Void> addFunctions){
        refreshKeranjang = addFunctions;
    }

    public static Callable<Void> getRefreshKeranjang(){
        try {
            refreshKeranjang.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return refreshKeranjang;

    }

    public static void sethitungUlangKeranjang(Callable<Void> addFunctions){
        hitungUlangKeranjang = addFunctions;
    }

    public static Callable<Void> gethitungUlangKeranjang(){
        try {
            hitungUlangKeranjang.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return hitungUlangKeranjang;

    }

    public static void setcheckBoxCart(Callable<Void> addFunctions){
        checkBoxCart = addFunctions;
    }

    public static Callable<Void> getcheckBoxCart(){
        try {
            checkBoxCart.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return checkBoxCart;

    }

    public static void setrefrashChekout(Callable<Void> addFunctions){
        refrashChekout = addFunctions;
    }

    public static Callable<Void> getrefrashChekout(){
        try {
            refrashChekout.call();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return refrashChekout;

    }

}


