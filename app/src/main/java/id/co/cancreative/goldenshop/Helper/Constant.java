package id.co.cancreative.goldenshop.Helper;

public class Constant {

    /**
     * -------------------- EDIT THIS WITH YOURS -------------------------------------------------
     */

    // Edit WEB_URL with your url. Make sure you have backslash('/') in the end url

//    public static String WEB_URL = "http://qs.dokterapps.com/heroshop/admin_project/";
    public static String WEB_URL = "http://demo.dokterapps.com/tokoku/admin_project/";

    public static String url_category = "http://demo.dokterapps.com/tokoku/admin_project/uploads/category/";

    /* [ IMPORTANT ] be careful when edit this security code */
    /* This string must be same with security code at Server, if its different android unable to submit order */
    public static final String SECURITY_CODE = "YJz6alJ1bzPGhuq9PWjt6ZomyL23eBpjC2zC23medy5kuSHLpgUMknv9gfgbW9IccA2ezDpOyJwLSLAX32I9vFbyKRNuKiiTQIXG";

    // Device will re-register the device data to server when open app N-times
    public static int OPEN_COUNTER = 50;


    /**
     * ------------------- DON'T EDIT THIS -------------------------------------------------------
     */

    // this limit value used for give pagination (request and display) to decrease payload
    public static int NEWS_PER_REQUEST = 10;
    public static int PRODUCT_PER_REQUEST = 10;
    public static int NOTIFICATION_PAGE = 20;
    public static int WISHLIST_PAGE = 20;

    // retry load image notification
    public static int LOAD_IMAGE_NOTIF_RETRY = 3;

    // Method get path to image
    public static String getURLimgProduct(String file_name) {
        return WEB_URL + "uploads/product/" + file_name;
    }

    public static String getURLimgNews(String file_name) {
        return WEB_URL + "uploads/news/" + file_name;
    }

    public static String getURLimgCategory(String file_name) {
        return WEB_URL + "uploads/category/" + file_name;
    }

}
