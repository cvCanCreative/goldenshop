package id.co.cancreative.goldenshop.Helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class IjinAplikasi {

    public static boolean mintsijintelepon(Context context){
        int ijintelepon = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CALL_PHONE);


        List<String> listPermissionsNeed = new ArrayList<>();
        if (ijintelepon != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeed.add(Manifest.permission.CALL_PHONE);
        }

        if (!listPermissionsNeed.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context, listPermissionsNeed.toArray(new String[listPermissionsNeed.size()]), 443);
            return false;
        }

        return true;
    }
}
