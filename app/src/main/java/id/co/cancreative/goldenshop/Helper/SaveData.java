package id.co.cancreative.goldenshop.Helper;

import java.io.File;
import java.util.ArrayList;

import id.co.cancreative.goldenshop.Model.ModelCategory;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.Slider;

public class SaveData {
    public static File image;
    public static String idSubKat;
    public static String nameSubKat;
    public static String namaProduk;

    public static ArrayList<Produk> gsProduk = new ArrayList<>();
    public static ArrayList<Produk> tradingProduk = new ArrayList<>();
    public static ArrayList<ModelCategory> kategori = new ArrayList<>();
    public static ArrayList<Slider> slider = new ArrayList<>();

    public static ArrayList<Slider> getSlider() {
        return slider;
    }

    public static void setSlider(ArrayList<Slider> slider) {
        SaveData.slider = slider;
    }

    public static String getNameSubKat() {
        return nameSubKat;
    }

    public static void setNameSubKat(String nameSubKat) {
        SaveData.nameSubKat = nameSubKat;
    }

    public static String getNamaProduk() {
        return namaProduk;
    }

    public static void setNamaProduk(String namaProduk) {
        SaveData.namaProduk = namaProduk;
    }

    public static String getIdSubKat() {
        return idSubKat;
    }

    public static void setIdSubKat(String idSubKat) {
        SaveData.idSubKat = idSubKat;
    }

    public static ArrayList<ModelCategory> getKategori() {
        return kategori;
    }

    public static void setKategori(ArrayList<ModelCategory> kategori) {
        SaveData.kategori = kategori;
    }

    public static ArrayList<Produk> getGsProduk() {
        return gsProduk;
    }

    public static void setGsProduk(ArrayList<Produk> gsProduk) {
        SaveData.gsProduk = gsProduk;
    }

    public static ArrayList<Produk> getTradingProduk() {
        return tradingProduk;
    }

    public static void setTradingProduk(ArrayList<Produk> tradingProduk) {
        SaveData.tradingProduk = tradingProduk;
    }

    public static File getImage() {
        return image;
    }

    public static void setImage(File image) {
        SaveData.image = image;
    }
}
