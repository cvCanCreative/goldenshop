package id.co.cancreative.goldenshop.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import id.co.cancreative.goldenshop.Model.EditToko;
import id.co.cancreative.goldenshop.Model.ModelAlamat;
import id.co.cancreative.goldenshop.Model.Rekening;
import id.co.cancreative.goldenshop.Model.ModelTransaksi;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.Promo;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResultProvinsi;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.Model.Toko;
import id.co.cancreative.goldenshop.Model.Transaksi;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.TbTerakhirDilihat;

public class SharedPref {

    public static final String STATUS_LOGIN = "login";
    public static final String MYPREF = "MAIN_PREF";
    public static final String SECONDPREF = "SECOND_PREF";
    public static final String CATEGORY = "CATEGORY";
    public static final String PRODUK = "PRODUK";
    public static final String PRODUKT = "PRODUKT";
    public static final String SHIPPING = "Shiping";
    private static final String PROVINSI = "_.PROVINSI_KEY";
    private static final String SALDO = "saldo";
    private static final String TOKO = "toko";
    private static final String EDITTOKO = "editToko";
    private static final String USER = "user";
    private static final String BANK = "bank";
    private static final String BANKGOLDEN = "bankgolden";
    private static final String TRANSAKSI = "transaksi";
    private static final String DATATRANSAKSI = "datatransaksi";
    private static final String LISTREKGOLDEN = "ListRekGolden";
    private static final String LISTBANK = "Listbank";
    private static final String LISTREKUSER = "ListRekUser";
    private static final String LISTFEEDBACK = "ListFeedback";
    private static final String PROMO = "Promo";
    private static final String ABOUT = "about";
    private static final String WD = "witraw";

    // Informasi User
    public static final String API = "api";
    public static final String ID_USER = "id_user";
    public static final String SESSION = "session";
    public static final String USERNAME = "username";

    // Informasi toko
    public static final String ID_TOKO = "id_toko";
    public static final String TOK_NAME = "namaToko";
    public static final String TOK_STATUS = "statusToko";
    public static final String TOK_AKTIF = "AktifToko";

    public static final String ID_SubKategori = "id_subkategory";
    public static final String NAMA_SubKategori = "NamaSubKategori";

    private Context context;

    // Main PREF
    private SharedPreferences sp;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    // Second PREF
    private SharedPreferences sp2;
    private SharedPreferences.Editor editor2;

    public SharedPref(Context context) {
        this.context = context;
        sp = context.getSharedPreferences(MYPREF, Context.MODE_PRIVATE);
        sp2 = context.getSharedPreferences(SECONDPREF, Context.MODE_PRIVATE);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sp.edit();
        editor2 = sp2.edit();
    }


    // -------------- Setting Informasi List PRoduk Terakhir di lihat --------------
    public TbTerakhirDilihat setProdukT(TbTerakhirDilihat produkt) {
        if (produkt == null) return null;
        String json = new Gson().toJson(produkt, TbTerakhirDilihat.class);
        sp.edit().putString(PRODUKT, json).apply();
        return getProdukT();
    }

    public TbTerakhirDilihat getProdukT() {
        String data = sp.getString(PRODUKT, null);
        if (data == null) return null;
        return new Gson().fromJson(data, TbTerakhirDilihat.class);
    }

    // -------------- Setting Informasi Promo --------------
    public Promo setPromo(Promo data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, Promo.class);
        sp.edit().putString(PROMO, json).apply();
        return getPromo();
    }

    public Promo getPromo() {
        String data = sp.getString(PROMO, null);
        if (data == null) return null;
        return new Gson().fromJson(data, Promo.class);
    }

    public void clearPromo() {
        sp.edit().putString(PROMO, null).apply();
    }


    // -------------- Setting Informasi List Rekening Golden --------------
    public ResponModel setListRek(ResponModel data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, ResponModel.class);
        sp.edit().putString(LISTREKGOLDEN, json).apply();
        return getListRek();
    }

    public ResponModel getListRek() {
        String data = sp.getString(LISTREKGOLDEN, null);
        if (data == null) return null;
        return new Gson().fromJson(data, ResponModel.class);
    }

    // -------------- Setting Informasi List Rekening Golden --------------
    public ResponModel setBank(ResponModel data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, ResponModel.class);
        sp.edit().putString(LISTBANK, json).apply();
        return getListRek();
    }

    public ResponModel getBank() {
        String data = sp.getString(LISTBANK, null);
        if (data == null) return null;
        return new Gson().fromJson(data, ResponModel.class);
    }

    // -------------- Setting Informasi List Rekening Golden --------------
    public ResponModel setListFeedbeck(ResponModel data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, ResponModel.class);
        sp.edit().putString(LISTFEEDBACK, json).apply();
        return getListFeedbeck();
    }

    public ResponModel getListFeedbeck() {
        String data = sp.getString(LISTFEEDBACK, null);
        if (data == null) return null;
        return new Gson().fromJson(data, ResponModel.class);
    }

    // -------------- Setting Informasi About Golden --------------
    public ResponModel setAbout(ResponModel data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, ResponModel.class);
        sp.edit().putString(ABOUT, json).apply();
        return getAbout();
    }

    public ResponModel getAbout() {
        String data = sp.getString(ABOUT, null);
        if (data == null) return null;
        return new Gson().fromJson(data, ResponModel.class);
    }

    public void clearListRek() {
        sp.edit().putString(LISTREKGOLDEN, null).apply();
    }

    // -------------- Setting Informasi List Rekening USER --------------
    public ResponModel setListRekUser(ResponModel data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, ResponModel.class);
        sp.edit().putString(LISTREKUSER, json).apply();
        return getListRekUser();
    }

    public ResponModel getListRekUser() {
        String data = sp.getString(LISTREKUSER, null);
        if (data == null) return null;
        return new Gson().fromJson(data, ResponModel.class);
    }

    public void clearListRekUser() {
        sp.edit().putString(LISTREKUSER, null).apply();
    }

    // -------------- Setting Informasi Kirim Data Transaksi --------------
    public ModelTransaksi setTransaksi(ModelTransaksi data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, ModelTransaksi.class);
        sp.edit().putString(TRANSAKSI, json).apply();
        return getTransaksi();
    }

    public ModelTransaksi getTransaksi() {
        String data = sp.getString(TRANSAKSI, null);
        if (data == null) return null;
        return new Gson().fromJson(data, ModelTransaksi.class);
    }

    public void clearTransaksi() {
        sp.edit().putString(TRANSAKSI, null).apply();
    }

    // -------------- Setting Informasi Transaksi --------------
    public Transaksi setDataTransaksi(Transaksi data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, Transaksi.class);
        sp.edit().putString(DATATRANSAKSI, json).apply();
        return getDataTransaksi();
    }

    public Transaksi getDataTransaksi() {
        String data = sp.getString(DATATRANSAKSI, null);
        if (data == null) return null;
        return new Gson().fromJson(data, Transaksi.class);
    }

    public void clearDataTransaksi() {
        sp.edit().putString(DATATRANSAKSI, null).apply();
    }

    // -------------- Setting Informasi Min WD --------------
    public Saldo setMinWd(Saldo data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, Saldo.class);
        sp.edit().putString(WD, json).apply();
        return getMinWd();
    }

    public Saldo getMinWd() {
        String data = sp.getString(WD, null);
        if (data == null) return null;
        return new Gson().fromJson(data, Saldo.class);
    }

    // -------------- Setting Informasi Bank --------------
    public Rekening setRekening(Rekening data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, Rekening.class);
        sp.edit().putString(BANK, json).apply();
        return getRekening();
    }

    public Rekening getRekening() {
        String data = sp.getString(BANK, null);
        if (data == null) return null;
        return new Gson().fromJson(data, Rekening.class);
    }

    public void clearRekening() {
        sp.edit().putString(BANK, null).apply();
    }

    // -------------- Setting Informasi Bank Golden --------------
    public Rekening setRekeningGolden(Rekening data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, Rekening.class);
        sp.edit().putString(BANKGOLDEN, json).apply();
        return getRekening();
    }

    public Rekening getRekeningGolden() {
        String data = sp.getString(BANKGOLDEN, null);
        if (data == null) return null;
        return new Gson().fromJson(data, Rekening.class);
    }

    public void clearRekeningGolden() {
        sp.edit().putString(BANKGOLDEN, null).apply();
    }

    // -------------- Setting Informasi Category --------------
    public Produk setProduk(Produk produk) {
        if (produk == null) return null;
        String json = new Gson().toJson(produk, Produk.class);
        sp.edit().putString(PRODUK, json).apply();
        return getProduk();
    }

    public Produk getProduk() {
        String data = sp.getString(PRODUK, null);
        if (data == null) return null;
        return new Gson().fromJson(data, Produk.class);
    }

    public void clearProduk() {
        sp.edit().putString(PRODUK, null).apply();
    }


    // -------------- Setting Informasi Shipping --------------//
    public ModelAlamat setShipping(ModelAlamat data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, ModelAlamat.class);
        sp.edit().putString(SHIPPING, json).apply();
        return getShipping();
    }

    public ModelAlamat getShipping() {
        String data = sp.getString(SHIPPING, null);
        if (data == null) return null;
        return new Gson().fromJson(data, ModelAlamat.class);
    }

    public void clearShipping() {
        sp.edit().putString(SHIPPING, null).apply();
    }

    // -------------- Setting Informasi Saldo --------------//
    public Saldo setSaldo(Saldo data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, Saldo.class);
        sp.edit().putString(SALDO, json).apply();
        return getSaldo();
    }

    public Saldo getSaldo() {
        String data = sp.getString(SALDO, null);
        if (data == null) return null;
        return new Gson().fromJson(data, Saldo.class);
    }

    public void clearSaldo() {
        sp.edit().putString(SALDO, null).apply();
    }


    // -------------- Setting Informasi Toko --------------//
    public EditToko setEditToko(EditToko data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, EditToko.class);
        sp.edit().putString(EDITTOKO, json).apply();
        return getEditToko();
    }

    public EditToko getEditToko() {
        String data = sp.getString(EDITTOKO, null);
        if (data == null) return null;
        return new Gson().fromJson(data, EditToko.class);
    }

    public void clearEditToko() {
        sp.edit().putString(EDITTOKO, null).apply();
    }

    //-------------- Setting Informasi Edit Toko--------------------//
    public Toko setToko(Toko data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, Toko.class);
        sp.edit().putString(TOKO, json).apply();
        return getToko();
    }

    public Toko getToko() {
        String data = sp.getString(TOKO, null);
        if (data == null) return null;
        return new Gson().fromJson(data, Toko.class);
    }

    public void clearToko() {
        sp.edit().putString(TOKO, null).apply();
    }

    // Setting Informasi USER
    public User setUser(User data) {
        if (data == null) return null;
        String json = new Gson().toJson(data, User.class);
        sp.edit().putString(USER, json).apply();
        return getUser();
    }

    public User getUser() {
        String data = sp.getString(USER, null);
        if (data == null) return null;
        return new Gson().fromJson(data, User.class);
    }

    public void clearUser() {
        sp.edit().putString(USER, null).apply();
    }

    // Provinsi API loaded
    public ResultProvinsi setProvinsi(ResultProvinsi rajaOangkir) {
        if (rajaOangkir == null) return null;
        String json = new Gson().toJson(rajaOangkir, ResultProvinsi.class);
        sp.edit().putString(PROVINSI, json).apply();
        return getProvinsi();
    }

    public ResultProvinsi getProvinsi() {
        String data = sp.getString(PROVINSI, null);
        if (data == null) return null;
        return new Gson().fromJson(data, ResultProvinsi.class);
    }

    public void clearProvinsi() {
        sp.edit().putString(PROVINSI, null).apply();
    }

    public void savePrefBoolean(String keySP, boolean value) {
        editor.putBoolean(keySP, value);
        editor.commit();
    }

    public void savePrefString(String keySP, String value) {
        editor.putString(keySP, value);
        editor.commit();
    }

    public void setBoolean(String keySP, boolean value) {
        editor.putBoolean(keySP, value);
        editor.commit();
    }

    public void setString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getString(String value) {
        return sp.getString(value, null);
    }

    public void clearAll() {
        editor.clear();
        editor.commit();
    }

    public Boolean getStatusLogin() {
        return sp.getBoolean(STATUS_LOGIN, false);
    }

    public Boolean getStatusToko() {
        return sp.getBoolean(TOK_STATUS, false);
    }

    public Boolean getAktifToko() {
        return sp.getBoolean(TOK_AKTIF, false);
    }

    public String getUsername() {
        return sp.getString(USERNAME, null);
    }

    public String getIdUser() {
        return sp.getString(ID_USER, null);
    }

    public String getSession() {
        return sp.getString(SESSION, null);
    }

    public String getApi() {
        return sp.getString(API, "iasajSIASHJQsiqscancoid2019sia");
    }

}

