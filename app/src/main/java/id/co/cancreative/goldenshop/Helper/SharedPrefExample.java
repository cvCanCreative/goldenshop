package id.co.cancreative.goldenshop.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPrefExample {

    public static final String STATUS_LOGIN = "login";
    public static final String MYPREF = "MAIN_PREF";

    private Context context;

    // Main PREF
    private SharedPreferences sp;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    public SharedPrefExample(Context context) {
        this.context = context;
        sp = context.getSharedPreferences(MYPREF, Context.MODE_PRIVATE);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sp.edit();
    }
}

