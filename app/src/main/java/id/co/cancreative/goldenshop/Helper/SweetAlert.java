package id.co.cancreative.goldenshop.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;


public class SweetAlert {

    public static SweetAlertDialog asd;
    public static SweetAlertDialog asda;

    public static void onFailure(Context context, String pesan) {
        if (context != null){
            SweetAlertDialog dialogKoneksi = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Oops")
                    .setContentText("Periksa Koneksi Anda");
            Log.d("onFilure", " " + pesan);
            if (pesan == "timeout") {
                dialogKoneksi.setContentText("Koneksi Timeout");
                dialogKoneksi.show();
            } else if (pesan.contains("Expected BEGIN_OBJECT")) {
                dialogKoneksi.setContentText("Gagal Mengirim Data");
                dialogKoneksi.show();
            } else {
                dialogKoneksi.show();
            }
        }
    }

    public static void success(Context context, String title, String pesan) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(pesan)
                .show();
    }

    public static void onLoading(Activity context) {
        asd = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        asd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        asd.setTitleText("Loading");
        asd.setCancelable(false);
        asd.show();
    }

    public static void sendingData(Context context) {
        asd = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        asd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        asd.setTitleText("Sending Data...");
        asd.setCancelable(false);
        asd.show();
    }

    public static void getingData(Context context) {
        asd = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        asd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        asd.setTitleText("geting Data...");
        asd.setCancelable(false);
        asd.show();
    }

    public static void onLoadingCustom(Context context, String pesan) {
        asd = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        asd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        asd.setTitleText(pesan);
        asd.setCancelable(false);
        asd.show();
    }

    public static void basicMessage(Context context, String pesan) {
        new SweetAlertDialog(context)
                .setTitleText(pesan)
                .show();
    }

    public static void waring(final Context context, String title, String pesan) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Ok");
        pDialog.show();
    }

    public static void waringWithIntent(final Context context, String title, String pesan, final Class<?> activityTujuan) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        context.startActivity(new Intent(context, activityTujuan));
                    }
                });
        pDialog.show();
    }

    public static void successWithIntent(final Context context, String title, String pesan, final Class<?> activityTujuan) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        context.startActivity(new Intent(context, activityTujuan));
                    }
                });
        pDialog.show();
    }

    public static void successWithIntentString(final Context context, String title, String pesan, final Class<?> activityTujuan, final String extra) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText(title)
                .setContentText(pesan)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(context, activityTujuan);
                        intent.putExtra("extra", extra);
                        context.startActivity(intent);
                    }
                });
        pDialog.show();
    }

    public static void dismis() {
        asd.dismiss();
    }
}
