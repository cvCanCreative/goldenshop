package id.co.cancreative.goldenshop.Helper;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

public class Toasti {
    public static void success(Context context, String pesan){
        Toasty.success(context, pesan, Toast.LENGTH_SHORT, true).show();
    }

    public static void error(Context context, String pesan){
        Toasty.error(context, pesan, Toast.LENGTH_SHORT, true).show();
    }

    public static void info(Context context, String pesan){
        Toasty.info(context, pesan, Toast.LENGTH_SHORT, true).show();
    }

    public static void onFailure(Context context, String pesan){

        if (context != null){
            if (pesan == "timeout") {
                pesan = "Koneksi Timeout";
            } else if (pesan.contains("Expected BEGIN_OBJECT")) {
                pesan = "Gagal Mengirim Data";
            } else {
                pesan = "Periksa Koneksi Anda";
            }
            Toasty.error(context, pesan, Toast.LENGTH_SHORT, true).show();
        }
    }
}
