package id.co.cancreative.goldenshop;


import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.HashMap;

import id.co.cancreative.goldenshop.Activity.CariBarangActivity;
import id.co.cancreative.goldenshop.Activity.CariProductActivity;
import id.co.cancreative.goldenshop.Activity.DaftarKategoriActivity;
import id.co.cancreative.goldenshop.Activity.DaftarProdukActivity;
import id.co.cancreative.goldenshop.Activity.DetailPromoActivity;
import id.co.cancreative.goldenshop.Adapter.AdapterCategory;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukGS;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiConfigDemo;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Fragment.HomeFragment.TerlarisFragment;
import id.co.cancreative.goldenshop.Helper.ChildAnimation;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SaveData;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelCategory;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.Promo;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.Slider;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ViewPagerEx.OnPageChangeListener {

    private ApiService apiDemo = ApiConfigDemo.getInstanceRetrofit();
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private View view;
    private SharedPref s;
    private AppDbTerakhirDilihat db;

    private int indexLoad = 1;
    private int viewThreshold = 6;
    private int pastVisibleitem, visibleItemCount, totalItemCount, previousTotal = 0;
    private boolean isLoading = true;

    // New AutoScroll
    private Snackbar failed_snackbar = null;
    private int post_total = 0;
    private Call<ResponModel> callbackCall = null;

    // Slider
    private SliderLayout sliderHome;
    private PagerIndicator indicator;

    private GridLayoutManager layoutManager;
    private AdapterProdukTrading mAdapter;

    private ArrayList<Produk> mItem = new ArrayList<>();
    private ArrayList<Produk> mProduk = new ArrayList<>();
    private ArrayList<Slider> mSlider = new ArrayList<>();
    private ArrayList<ModelCategory> mKategori = new ArrayList<>();

    private RelativeLayout lySearch;
    private ImageView btnSearch;
    private ImageView btnCart;
    private RecyclerView rvFlash;
    private RecyclerView rvKategori;
    private RecyclerView recyclerView;
    private ProgressBar pd;
    private ProgressBar pd1;
    private ProgressBar pd2;
    private SwipeRefreshLayout swipe_refresh;
    private TextView btnAllGSProduk;
    private TextView btnAllKategori;
    private TextView btnAlltradingProduk;
    private EditText edtSearch;
    private ProgressBar pb;
    private ProgressBar progressBar;
    private TextView btnSelanjutnya;
    private ImageView btnTest;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);

        s = new SharedPref(getActivity());
        db = Room.databaseBuilder(getActivity(), AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();


        setLayoutManager();
        mainButton();
        getSaveData();
        pullrefrash();
        setupSlider();

//        btnTest.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), Main2Activity.class));
//            }
//        });

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        TerlarisFragment newProdukFragment = new TerlarisFragment();
        fragmentTransaction.replace(R.id.frame_content_produk, newProdukFragment);

        fragmentTransaction.commit();

        return view;
    }

    private void mainButton() {

        edtSearch.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                actionId == EditorInfo.IME_ACTION_DONE ||
                                event != null &&
                                        event.getAction() == KeyEvent.ACTION_DOWN &&
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            if (event == null || !event.isShiftPressed()) {
                                if (!edtSearch.getText().toString().isEmpty()) {
                                    Intent intent = new Intent(getActivity(), CariProductActivity.class);
                                    intent.putExtra("query", edtSearch.getText().toString());
                                    intent.putExtra("by_toko", false);
                                    intent.putExtra("id_toko", 0);
                                    startActivity(intent);
                                }

                                return true; // consume.
                            }
                        }
                        return false; // pass on to other listeners.
                    }
                }
        );

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CariBarangActivity.class);
                intent.putExtra("query", edtSearch.getText().toString());
                intent.putExtra("by_toko", false);
                intent.putExtra("id_toko", 0);
                startActivity(intent);
            }
        });

        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("extra", "keranjang");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        });

        btnAllGSProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("Test", "Test");
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("extra", "gsproduk");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        });

        btnAllKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DaftarKategoriActivity.class);
                i.putExtra("extra", "kategori");
                startActivity(i);
            }
        });

        btnAlltradingProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DaftarProdukActivity.class);
                i.putExtra("extra", "tradingProduk");
                startActivity(i);
            }
        });

    }

    private void getSaveData() {
        mItem = SaveData.getGsProduk();
        mProduk = SaveData.getTradingProduk();
        mKategori = SaveData.getKategori();
        mSlider = SaveData.getSlider();

        if (mItem.size() != 0 && mProduk.size() != 0 && mKategori.size() != 0 && mSlider.size() != 0) {
            pd2.setVisibility(View.GONE);
            pd1.setVisibility(View.GONE);
            pd.setVisibility(View.GONE);

            RecyclerView.Adapter mAdapter = new AdapterProdukGS(mItem, getActivity(), db);
            rvFlash.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            rvFlash.setAdapter(mAdapter);

            mAdapter = new AdapterProdukTrading(mProduk, getActivity(), db);
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            recyclerView.setAdapter(mAdapter);

            mAdapter = new AdapterCategory(mKategori, getActivity());
            rvKategori.setLayoutManager(new GridLayoutManager(getActivity(), 3));
            rvKategori.setAdapter(mAdapter);

            sliderHome.removeAllSliders();
            for (final Slider d : mSlider) {

                TextSliderView textSliderView = new TextSliderView(getActivity());
                textSliderView.description(d.SLI_JUDUL)
                        .image(Config.url_slider + d.SLI_FOTO)
                        .setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                    @Override
                    public void onSliderClick(BaseSliderView slider) {

                        if (d.PR_CODE != null) {
                            Intent intent = new Intent(getActivity(), DetailPromoActivity.class);
                            Promo p = new Promo();
                            p.PR_CODE = d.PR_CODE;
                            p.PR_MIN = d.PR_MIN;
                            p.PR_STATUS = d.PR_STATUS;
                            p.PR_POTONGAN = d.PR_POTONGAN;
                            p.ID_PROMO = d.ID_PROMO;
                            p.PR_EXPIRED = d.PR_EXPIRED;
                            p.PR_NAME = d.PR_NAME;
                            s.setPromo(p);
                            startActivity(intent);
                        } else {
                            SweetAlert.basicMessage(getActivity(), "Promo Expired");
                        }
                    }
                });
                sliderHome.addSlider(textSliderView);
            }

        } else {
            getProduk();
        }
    }

    private void setLayoutManager() {
        layoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void getProduk() {

        damiSilder();
        api.getHome(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                swipe_refresh.setRefreshing(false);
                pd.setVisibility(View.GONE);
                pd1.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ResponModel produk = response.body();
                    mItem = produk.products;
                    RecyclerView.Adapter mAdapter = new AdapterProdukGS(mItem, getActivity(), db);
                    rvFlash.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                    rvFlash.setAdapter(mAdapter);

                    mKategori = response.body().kategori;
                    mAdapter = new AdapterCategory(mKategori, getActivity());
                    rvKategori.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                    rvKategori.setAdapter(mAdapter);

                    sliderHome.removeAllSliders();
                    mSlider = response.body().slider;

                    if (mSlider.size() != 0) {
                        sliderHome.removeAllSliders();
                        for (final Slider d : mSlider) {

                            TextSliderView textSliderView = new TextSliderView(getActivity());
                            textSliderView.description(d.SLI_JUDUL)
                                    .image(Config.url_slider + d.SLI_FOTO)
                                    .setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                    if (d.PR_CODE != null) {
                                        Intent intent = new Intent(getActivity(), DetailPromoActivity.class);
                                        Promo p = new Promo();
                                        p.PR_CODE = d.PR_CODE;
                                        p.PR_MIN = d.PR_MIN;
                                        p.PR_STATUS = d.PR_STATUS;
                                        p.PR_POTONGAN = d.PR_POTONGAN;
                                        p.ID_PROMO = d.ID_PROMO;
                                        p.PR_EXPIRED = d.PR_EXPIRED;
                                        p.PR_NAME = d.PR_NAME;
                                        s.setPromo(p);
                                        startActivity(intent);
                                    } else {
                                        SweetAlert.basicMessage(getActivity(), "Promo Expired");
                                    }
                                }
                            });
                            sliderHome.addSlider(textSliderView);
                        }
                    } else {
                        damiSilder();
                    }

                }

            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                if (getActivity() != null) {
                    SweetAlert.onFailure(getActivity(), t.getMessage());
                }
                damiSilder();
            }
        });

//        getTerlaris(1);
//        setupPageination();

    }

    private void getTerlaris(final int page_no) {
        callbackCall = api.getTerlarisProduk(s.getApi(), page_no);
        callbackCall.enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                swipe_refresh.setRefreshing(false);
                pd2.setVisibility(View.GONE);
                pd1.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = response.body().products;
                    post_total = response.body().jml_produk;
                    mAdapter = new AdapterProdukTrading(mProduk, getActivity(), db);
                    recyclerView.setAdapter(mAdapter);
                }

            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                SweetAlert.onFailure(getActivity(), t.getMessage());
            }
        });
    }

    private void setupPageination() {
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                visibleItemCount = layoutManager.getChildCount();
//                totalItemCount = layoutManager.getItemCount();
//                pastVisibleitem = layoutManager.findFirstVisibleItemPosition();
//
//                if (dy > 0) {
//                    if (isLoading) {
//                        if (totalItemCount > previousTotal) {
//                            isLoading = false;
//                            previousTotal = totalItemCount;
//                        }
//                    }
//                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleitem + viewThreshold)) {
//                        indexLoad = indexLoad + 1;
//                        loadProductTrading(indexLoad);
//                        isLoading = true;
//                    }
//                }
//            }
//        });

        btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexLoad = indexLoad + 1;
                loadProductTrading(indexLoad);
                btnSelanjutnya.setVisibility(View.GONE);
            }
        });
    }

    private void loadProductTrading(int index) {
        pb.setVisibility(View.VISIBLE);
        api.getTerlarisProduk(s.getApi(), index).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pb.setVisibility(View.GONE);
                btnSelanjutnya.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    ArrayList<Produk> produks = response.body().products;
                    if (produks.size() == 8){
                        btnSelanjutnya.setVisibility(View.GONE);
                    }
                    mAdapter.addData(produks);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pb.setVisibility(View.GONE);
                SweetAlert.onFailure(getActivity(), t.getMessage());
            }
        });
    }

    private void damiSilder() {
        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("TM 1", R.drawable.slider_1);
        file_maps.put("TM 2", R.drawable.asset_slider_goldenshop_2);

        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            sliderHome.addSlider(textSliderView);
        }
    }

    private void setupSlider() {

        sliderHome.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderHome.setCustomAnimation(new ChildAnimation());
//        sliderHome.setCustomIndicator(indicator);
        sliderHome.setDuration(8000);
        sliderHome.addOnPageChangeListener(this);
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProduk();
            }
        });
        // Configure the refreshing colors
        swipe_refresh.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void initView(View view) {
        lySearch = (RelativeLayout) view.findViewById(R.id.ly_search);
        btnSearch = (ImageView) view.findViewById(R.id.btn_search);
        btnCart = (ImageView) view.findViewById(R.id.btn_cart);
        rvFlash = (RecyclerView) view.findViewById(R.id.rv_flash);
        sliderHome = (SliderLayout) view.findViewById(R.id.slider_home);
        rvKategori = (RecyclerView) view.findViewById(R.id.rv_kategori);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_tradingProduct);
        pd = (ProgressBar) view.findViewById(R.id.pd);
        pd1 = (ProgressBar) view.findViewById(R.id.pd_1);
        pd2 = (ProgressBar) view.findViewById(R.id.pd_2);
        swipe_refresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefrash);
        btnAllGSProduk = (TextView) view.findViewById(R.id.btn_allGSProduk);
        btnAllKategori = (TextView) view.findViewById(R.id.btn_allKategori);
        btnAlltradingProduk = (TextView) view.findViewById(R.id.btn_alltradingProduk);
        edtSearch = (EditText) view.findViewById(R.id.edt_search);
        pb = (ProgressBar) view.findViewById(R.id.progressBar);
        btnSelanjutnya = (TextView) view.findViewById(R.id.btn_selanjutnya);
        btnTest = (ImageView) view.findViewById(R.id.btn_test);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SaveData.setGsProduk(mItem);
        SaveData.setKategori(mKategori);
        SaveData.setTradingProduk(mProduk);
        SaveData.setSlider(mSlider);
    }
}
