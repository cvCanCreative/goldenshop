package id.co.cancreative.goldenshop;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Fragment.GoldenShopFragment;
import id.co.cancreative.goldenshop.Fragment.HomeFragment.TerlarisFragment;
import id.co.cancreative.goldenshop.Helper.SharedPref;

public class Main2Activity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        initView();

        s = new SharedPref(this);
        initFragment();
    }

    private void initFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        TerlarisFragment newProdukFragment = new TerlarisFragment();
        fragmentTransaction.replace(R.id.frame_content_produk, newProdukFragment);

        fragmentTransaction.commit();
    }

    private void initView() {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
