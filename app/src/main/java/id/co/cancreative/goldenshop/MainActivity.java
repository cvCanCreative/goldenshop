package id.co.cancreative.goldenshop;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.google.firebase.iid.FirebaseInstanceId;

import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Fragment.AkunFragment;
import id.co.cancreative.goldenshop.Fragment.CartFragment;
import id.co.cancreative.goldenshop.Fragment.GoldenShopFragment;
import id.co.cancreative.goldenshop.Helper.BottomNavigationViewHelper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.NotificationPackage.Configuration;
import id.co.cancreative.goldenshop.NotificationPackage.NotificationUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Menu menu;
    private MenuItem menuItem;
    private int chacked;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        buttomNav();
        getExtraIntent();
        permition();
        permitionCamera();

        // register broadcast notifikasi
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Configuration.REGISTRATION_COMPLETE)) {
                    // fcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
//                    FirebaseMessaging.getInstance().subscribeToTopic(Configuration.TOPIC_GLOBAL);
                }
            }
        };

        post_fcm_token();
    }

    private void permitionCamera(){

    }

    private void post_fcm_token() {
        ApiService api = ApiConfig.getInstanceRetrofit();
        SharedPref s  = new SharedPref(MainActivity.this);
        String fcm_token = FirebaseInstanceId.getInstance().getToken();
        api.post_fcmtoken(s.getApi(), s.getIdUser(), s.getSession(), fcm_token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getExtraIntent() {
        String extra = getIntent().getStringExtra("extra");

        if (extra != null) {
            if (extra.equals("keranjang") || extra == "keranjang") {
                menuItem = menu.getItem(2);
                menuItem.setChecked(true);
                callFragment(new CartFragment());
            }

            if (extra.equals("akun") || extra == "akun") {
                menuItem = menu.getItem(3);
                menuItem.setChecked(true);
                callFragment(new AkunFragment());
            }

            if (extra.equals("gsproduk") || extra == "gsproduk") {
                menuItem = menu.getItem(1);
                menuItem.setChecked(true);
                callFragment(new GoldenShopFragment());
            }
        } else {
            callFragment(new HomeFragment());
        }
    }

    private void buttomNav() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        menu = bottomNavigationView.getMenu();
        menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        menuItem = menu.getItem(0);
                        menuItem.setChecked(true);
                        callFragment(new HomeFragment());
                        break;
                    case R.id.navigation_search:
                        menuItem = menu.getItem(1);
                        menuItem.setChecked(true);
                        callFragment(new GoldenShopFragment());
                        break;
                    case R.id.navigation_troli:
                        menuItem = menu.getItem(2);
                        menuItem.setChecked(true);
                        callFragment(new CartFragment());
                        break;
                    case R.id.navigation_akun:
                        menuItem = menu.getItem(3);
                        menuItem.setChecked(true);
                        callFragment(new AkunFragment());
                        break;
                }
                return false;
            }
        });
    }

    private void callFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private void permition(){
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
            }
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.CAMERA)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setCancelable(true);
        dialog.setMessage("Apakah anda yakin ingin keluar ?");
        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.finish();

            }
        });

        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        AlertDialog alert = dialog.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Configuration.REGISTRATION_COMPLETE));

        // register new push postUrlFromNotification receiver
        // by doing this, the activity will be notified each time a new postUrlFromNotification arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Configuration.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }
}
