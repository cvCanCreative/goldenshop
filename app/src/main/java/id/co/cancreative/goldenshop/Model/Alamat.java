package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Alamat implements Serializable {
    public String ID_PENGIRIMAN;
    public String ID_USER;
    public String PE_JUDUL;
    public String PE_NAMA;
    public String PE_ALAMAT;
    public String PE_PROVINSI;
    public String PE_KOTA;
    public String PE_ID_KOTA;
    public String PE_KECAMATAN;
    public String PE_ID_KECAMATAN;
    public String PE_KODE_POS;
    public String PE_TELP;
    public String CREATED_AT;
    public String UPDATED_AT;
}
