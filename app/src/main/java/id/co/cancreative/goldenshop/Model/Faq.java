package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Faq implements Serializable {
    public String ID_FAQ;
    public String FAQ_JUDUL;
    public String FAQ_TEXT;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String status_faq;
    public String message_kontak;
}
