package id.co.cancreative.goldenshop.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Feedback implements Serializable{
    public String ID_FEEDBACK;
    public String ID_TRANSAKSI_DETAIL;
    public String FE_RATING;
    public String FE_COMMENT;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String ID_USER_DETAIL;
    public String ID_USER;
    public String USD_FOTO;
    public String USD_FULLNAME;
    public String USD_BIRTH;
    public String USD_ADDRESS;
    public String USD_GENDER;
    public String ID_TOKO;
    public String USD_STATUS;
    public String US_USERNAME;
    public String US_PASSWORD;
    public String US_TELP;
    public String US_EMAIL;
    public String US_RULE;
    public String US_FCM;
    public String US_TOKEN;
    public String US_FORGOT_PASS;
}
