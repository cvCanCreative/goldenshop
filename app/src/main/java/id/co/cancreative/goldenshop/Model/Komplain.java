package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Komplain implements Serializable {
    public String ID_KOMPLAIN;
    public String ID_USER;
    public String KOM_JNS;
    public String KOM_JUDUL;
    public String KOM_DETAIL;
    public String KOM_IMAGE;
    public String KOM_STATUS;
    public String KOM_UPDATED;
    public String CREATED_AT;
    public String UPDATED_AT;
    public Transaksi transaksi;
    public User user;
    public Toko toko;
}
