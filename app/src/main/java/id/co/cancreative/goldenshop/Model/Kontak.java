package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Kontak implements Serializable {
    public String ID_KONTAK;
    public String KON_EMAIL;
    public String KON_TELP;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String status_kontak;
    public String message_kontak;
}
