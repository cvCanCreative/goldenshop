package id.co.cancreative.goldenshop.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelAcount {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("ID_SALDO")
    @Expose
    private Integer iDSALDO;
    @SerializedName("SA_SALDO")
    @Expose
    private String sASALDO;

    @SerializedName("id_toko")
    @Expose
    private String idToko;
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("ID_TOKO")
    @Expose
    private String iDTOKO;
    @SerializedName("TOK_NAME")
    @Expose
    private String tOKNAME;
    @SerializedName("TOK_FOTO")
    @Expose
    private String tOKFOTO;
    @SerializedName("TOK_DETAIL")
    @Expose
    private String tOKDETAIL;
    @SerializedName("TOK_NO_KTP")
    @Expose
    private String tOKNOKTP;
    @SerializedName("TOK_FOTO_KTP")
    @Expose
    private String tOKFOTOKTP;
    @SerializedName("TOK_SELFIE")
    @Expose
    private String tOKSELFIE;
    @SerializedName("TOK_ADDRESS")
    @Expose
    private String tOKADDRESS;
    @SerializedName("TOK_ID_PROV")
    @Expose
    private String tOKIDPROV;
    @SerializedName("TOK_ID_KOTA")
    @Expose
    private String tOKIDKOTA;
    @SerializedName("TOK_KOTA")
    @Expose
    private String tOKKOTA;
    @SerializedName("TOK_ID_KECAMATAN")
    @Expose
    private String tOKIDKECAMATAN;
    @SerializedName("TOK_KECAMATAN")
    @Expose
    private String tOKKECAMATAN;
    @SerializedName("TOK_STATUS")
    @Expose
    private String tOKSTATUS;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("US_USERNAME")
    @Expose
    private String uSUSERNAME;
    @SerializedName("US_PASSWORD")
    @Expose
    private String uSPASSWORD;
    @SerializedName("US_TELP")
    @Expose
    private String uSTELP;
    @SerializedName("US_EMAIL")
    @Expose
    private String uSEMAIL;
    @SerializedName("US_RULE")
    @Expose
    private String uSRULE;
    @SerializedName("US_FCM")
    @Expose
    private String uSFCM;
    @SerializedName("US_TOKEN")
    @Expose
    private String uSTOKEN;
    @SerializedName("US_FORGOT_PASS")
    @Expose
    private String uSFORGOTPASS;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getiDSALDO() {
        return iDSALDO;
    }

    public void setiDSALDO(Integer iDSALDO) {
        this.iDSALDO = iDSALDO;
    }

    public String getsASALDO() {
        return sASALDO;
    }

    public void setsASALDO(String sASALDO) {
        this.sASALDO = sASALDO;
    }

    public String getIdToko() {
        return idToko;
    }

    public void setIdToko(String idToko) {
        this.idToko = idToko;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getiDTOKO() {
        return iDTOKO;
    }

    public void setiDTOKO(String iDTOKO) {
        this.iDTOKO = iDTOKO;
    }

    public String gettOKNAME() {
        return tOKNAME;
    }

    public void settOKNAME(String tOKNAME) {
        this.tOKNAME = tOKNAME;
    }

    public String gettOKFOTO() {
        return tOKFOTO;
    }

    public void settOKFOTO(String tOKFOTO) {
        this.tOKFOTO = tOKFOTO;
    }

    public String gettOKDETAIL() {
        return tOKDETAIL;
    }

    public void settOKDETAIL(String tOKDETAIL) {
        this.tOKDETAIL = tOKDETAIL;
    }

    public String gettOKNOKTP() {
        return tOKNOKTP;
    }

    public void settOKNOKTP(String tOKNOKTP) {
        this.tOKNOKTP = tOKNOKTP;
    }

    public String gettOKFOTOKTP() {
        return tOKFOTOKTP;
    }

    public void settOKFOTOKTP(String tOKFOTOKTP) {
        this.tOKFOTOKTP = tOKFOTOKTP;
    }

    public String gettOKSELFIE() {
        return tOKSELFIE;
    }

    public void settOKSELFIE(String tOKSELFIE) {
        this.tOKSELFIE = tOKSELFIE;
    }

    public String gettOKADDRESS() {
        return tOKADDRESS;
    }

    public void settOKADDRESS(String tOKADDRESS) {
        this.tOKADDRESS = tOKADDRESS;
    }

    public String gettOKIDPROV() {
        return tOKIDPROV;
    }

    public void settOKIDPROV(String tOKIDPROV) {
        this.tOKIDPROV = tOKIDPROV;
    }

    public String gettOKIDKOTA() {
        return tOKIDKOTA;
    }

    public void settOKIDKOTA(String tOKIDKOTA) {
        this.tOKIDKOTA = tOKIDKOTA;
    }

    public String gettOKKOTA() {
        return tOKKOTA;
    }

    public void settOKKOTA(String tOKKOTA) {
        this.tOKKOTA = tOKKOTA;
    }

    public String gettOKIDKECAMATAN() {
        return tOKIDKECAMATAN;
    }

    public void settOKIDKECAMATAN(String tOKIDKECAMATAN) {
        this.tOKIDKECAMATAN = tOKIDKECAMATAN;
    }

    public String gettOKKECAMATAN() {
        return tOKKECAMATAN;
    }

    public void settOKKECAMATAN(String tOKKECAMATAN) {
        this.tOKKECAMATAN = tOKKECAMATAN;
    }

    public String gettOKSTATUS() {
        return tOKSTATUS;
    }

    public void settOKSTATUS(String tOKSTATUS) {
        this.tOKSTATUS = tOKSTATUS;
    }

    public String getcREATEDAT() {
        return cREATEDAT;
    }

    public void setcREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getuPDATEDAT() {
        return uPDATEDAT;
    }

    public void setuPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public String getiDUSER() {
        return iDUSER;
    }

    public void setiDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getuSUSERNAME() {
        return uSUSERNAME;
    }

    public void setuSUSERNAME(String uSUSERNAME) {
        this.uSUSERNAME = uSUSERNAME;
    }

    public String getuSPASSWORD() {
        return uSPASSWORD;
    }

    public void setuSPASSWORD(String uSPASSWORD) {
        this.uSPASSWORD = uSPASSWORD;
    }

    public String getuSTELP() {
        return uSTELP;
    }

    public void setuSTELP(String uSTELP) {
        this.uSTELP = uSTELP;
    }

    public String getuSEMAIL() {
        return uSEMAIL;
    }

    public void setuSEMAIL(String uSEMAIL) {
        this.uSEMAIL = uSEMAIL;
    }

    public String getuSRULE() {
        return uSRULE;
    }

    public void setuSRULE(String uSRULE) {
        this.uSRULE = uSRULE;
    }

    public String getuSFCM() {
        return uSFCM;
    }

    public void setuSFCM(String uSFCM) {
        this.uSFCM = uSFCM;
    }

    public String getuSTOKEN() {
        return uSTOKEN;
    }

    public void setuSTOKEN(String uSTOKEN) {
        this.uSTOKEN = uSTOKEN;
    }

    public String getuSFORGOTPASS() {
        return uSFORGOTPASS;
    }

    public void setuSFORGOTPASS(String uSFORGOTPASS) {
        this.uSFORGOTPASS = uSFORGOTPASS;
    }
}
