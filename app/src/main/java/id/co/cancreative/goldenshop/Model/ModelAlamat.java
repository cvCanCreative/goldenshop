package id.co.cancreative.goldenshop.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ModelAlamat implements Serializable {

    public String ID_PENGIRIMAN;
    public String ID_USER;
    public String PE_JUDUL;
    public String PE_NAMA;
    public String PE_ALAMAT;
    public String PE_PROVINSI;
    public String PE_KOTA;
    public String PE_ID_KOTA;
    public String PE_KECAMATAN;
    public String PE_ID_KECAMATAN;
    public String PE_KODE_POS;
    public String PE_TELP;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String US_USERNAME;
    public String US_PASSWORD;
    public String US_TELP;
    public String US_EMAIL;
    public String US_RULE;
    public String US_FCM;
    public String US_TOKEN;
    public String US_FORGOT_PASS;
    public String CR_PENGIRIMAN;
    public String UP_PENGIRIMAN;

}
