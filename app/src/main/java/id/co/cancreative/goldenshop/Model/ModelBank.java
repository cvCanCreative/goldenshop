package id.co.cancreative.goldenshop.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelBank {

    @SerializedName("ID_BANK")
    @Expose
    private String iDBANK;
    @SerializedName("BK_NAME")
    @Expose
    private String bKNAME;
    @SerializedName("BK_IMAGE")
    @Expose
    private String bKIMAGE;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;

    public String getiDBANK() {
        return iDBANK;
    }

    public void setiDBANK(String iDBANK) {
        this.iDBANK = iDBANK;
    }

    public String getbKNAME() {
        return bKNAME;
    }

    public void setbKNAME(String bKNAME) {
        this.bKNAME = bKNAME;
    }

    public String getbKIMAGE() {
        return bKIMAGE;
    }

    public void setbKIMAGE(String bKIMAGE) {
        this.bKIMAGE = bKIMAGE;
    }

    public String getcREATEDAT() {
        return cREATEDAT;
    }

    public void setcREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getuPDATEDAT() {
        return uPDATEDAT;
    }

    public void setuPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }
}
