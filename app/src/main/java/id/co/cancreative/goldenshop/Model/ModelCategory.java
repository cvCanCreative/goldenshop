package id.co.cancreative.goldenshop.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelCategory implements Parcelable {

    @SerializedName("ID_KATEGORI")
    @Expose
    private String iDKATEGORI;
    @SerializedName("KAT_NAME")
    @Expose
    private String kATNAME;
    @SerializedName("KAT_IMAGE")
    @Expose
    private String kATIMAGE;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;

    @SerializedName("skat")
    @Expose
    private List<ModelSubKat> skat = null;

    public List<ModelSubKat> getSkat() {
        return skat;
    }

    public void setSkat(List<ModelSubKat> skat) {
        this.skat = skat;
    }

    public String getiDKATEGORI() {
        return iDKATEGORI;
    }

    public void setiDKATEGORI(String iDKATEGORI) {
        this.iDKATEGORI = iDKATEGORI;
    }

    public String getkATNAME() {
        return kATNAME;
    }

    public void setkATNAME(String kATNAME) {
        this.kATNAME = kATNAME;
    }

    public String getkATIMAGE() {
        return kATIMAGE;
    }

    public void setkATIMAGE(String kATIMAGE) {
        this.kATIMAGE = kATIMAGE;
    }

    public String getcREATEDAT() {
        return cREATEDAT;
    }

    public void setcREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getuPDATEDAT() {
        return uPDATEDAT;
    }

    public void setuPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.iDKATEGORI);
        dest.writeString(this.kATNAME);
        dest.writeString(this.kATIMAGE);
        dest.writeString(this.cREATEDAT);
        dest.writeString(this.uPDATEDAT);
        dest.writeTypedList(this.skat);
    }

    public ModelCategory() {
    }

    protected ModelCategory(Parcel in) {
        this.iDKATEGORI = in.readString();
        this.kATNAME = in.readString();
        this.kATIMAGE = in.readString();
        this.cREATEDAT = in.readString();
        this.uPDATEDAT = in.readString();
        this.skat = in.createTypedArrayList(ModelSubKat.CREATOR);
    }

    public static final Parcelable.Creator<ModelCategory> CREATOR = new Parcelable.Creator<ModelCategory>() {
        @Override
        public ModelCategory createFromParcel(Parcel source) {
            return new ModelCategory(source);
        }

        @Override
        public ModelCategory[] newArray(int size) {
            return new ModelCategory[size];
        }
    };
}
