package id.co.cancreative.goldenshop.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CAN Creative on 30/05/2018.
 */

public class ModelFAQ {
    private String fAID;
    private String fAIMGPATH;
    private String fATITLE;
    private String fAINFO;
    private String fACREATEDUSER;
    private String fASYSDATE;

    public String getFAID() {
        return fAID;
    }

    public void setFAID(String fAID) {
        this.fAID = fAID;
    }

    public String getFAIMGPATH() {
        return fAIMGPATH;
    }

    public void setFAIMGPATH(String fAIMGPATH) {
        this.fAIMGPATH = fAIMGPATH;
    }

    public String getFATITLE() {
        return fATITLE;
    }

    public void setFATITLE(String fATITLE) {
        this.fATITLE = fATITLE;
    }

    public String getFAINFO() {
        return fAINFO;
    }

    public void setFAINFO(String fAINFO) {
        this.fAINFO = fAINFO;
    }

    public String getFACREATEDUSER() {
        return fACREATEDUSER;
    }

    public void setFACREATEDUSER(String fACREATEDUSER) {
        this.fACREATEDUSER = fACREATEDUSER;
    }

    public String getFASYSDATE() {
        return fASYSDATE;
    }

    public void setFASYSDATE(String fASYSDATE) {
        this.fASYSDATE = fASYSDATE;
    }
}
