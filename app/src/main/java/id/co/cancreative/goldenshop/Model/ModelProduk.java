package id.co.cancreative.goldenshop.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelProduk implements Parcelable{

    @SerializedName("category_utama")
    @Expose
    private Integer category_utama;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("kode")
    @Expose
    private String kode;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("harga")
    @Expose
    private Integer harga;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("category_id")
    @Expose
    private String category_id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("stock")
    @Expose
    private Integer stock;
    @SerializedName("weight")
    @Expose
    private Integer weight;
    @SerializedName("path")
    @Expose
    private String path;

    @SerializedName("ID_BARANG")
    @Expose
    private String iDBARANG;
    @SerializedName("ID_TOKO")
    @Expose
    private String iDTOKO;
    @SerializedName("ID_PROMO")
    @Expose
    private String iDPROMO;
    @SerializedName("ID_SUB_KATEGORI")
    @Expose
    private String iDSUBKATEGORI;
    @SerializedName("BA_IMAGE")
    @Expose
    private String bAIMAGE;
    @SerializedName("BA_NAME")
    @Expose
    private String bANAME;
    @SerializedName("BA_PSHOP")
    @Expose
    private String bAPSHOP;
    @SerializedName("BA_PGLD")
    @Expose
    private String bAPGLD;
    @SerializedName("BA_PRICE")
    @Expose
    private String bAPRICE;
    @SerializedName("BA_SKU")
    @Expose
    private String bASKU;
    @SerializedName("BA_DESCRIPTION")
    @Expose
    private String bADESCRIPTION;
    @SerializedName("BA_STOCK")
    @Expose
    private String bASTOCK;
    @SerializedName("BA_WEIGHT")
    @Expose
    private String bAWEIGHT;
    @SerializedName("BA_CONDITION")
    @Expose
    private String bACONDITION;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;

    @SerializedName("TOK_NAME")
    @Expose
    private String tOKNAME;
    @SerializedName("TOK_FOTO")
    @Expose
    private String tOKFOTO;
    @SerializedName("TOK_DETAIL")
    @Expose
    private String tOKDETAIL;
    @SerializedName("TOK_NO_KTP")
    @Expose
    private String tOKNOKTP;
    @SerializedName("TOK_FOTO_KTP")
    @Expose
    private String tOKFOTOKTP;
    @SerializedName("TOK_SELFIE")
    @Expose
    private String tOKSELFIE;
    @SerializedName("TOK_ADDRESS")
    @Expose
    private String tOKADDRESS;
    @SerializedName("TOK_ID_PROV")
    @Expose
    private String tOKIDPROV;
    @SerializedName("TOK_ID_KOTA")
    @Expose
    private String tOKIDKOTA;
    @SerializedName("TOK_KOTA")
    @Expose
    private String tOKKOTA;
    @SerializedName("TOK_ID_KECAMATAN")
    @Expose
    private String tOKIDKECAMATAN;
    @SerializedName("TOK_KECAMATAN")
    @Expose
    private String tOKKECAMATAN;
    @SerializedName("TOK_STATUS")
    @Expose
    private String tOKSTATUS;
    @SerializedName("PR_JENIS")
    @Expose
    private Object pRJENIS;
    @SerializedName("PR_NAME")
    @Expose
    private Object pRNAME;
    @SerializedName("PR_DESKRIPSI")
    @Expose
    private Object pRDESKRIPSI;
    @SerializedName("PR_CODE")
    @Expose
    private Object pRCODE;
    @SerializedName("PR_POTONGAN")
    @Expose
    private Object pRPOTONGAN;
    @SerializedName("PR_MIN")
    @Expose
    private Object pRMIN;
    @SerializedName("PR_STATUS")
    @Expose
    private Object pRSTATUS;
    @SerializedName("PR_MULAI")
    @Expose
    private Object pRMULAI;
    @SerializedName("PR_EXPIRED")
    @Expose
    private Object pREXPIRED;
    @SerializedName("ID_KATEGORI")
    @Expose
    private String iDKATEGORI;
    @SerializedName("SKAT_NAME")
    @Expose
    private String sKATNAME;
    @SerializedName("SKAT_IMAGE")
    @Expose
    private String sKATIMAGE;

    public String gettOKNAME() {
        return tOKNAME;
    }

    public void settOKNAME(String tOKNAME) {
        this.tOKNAME = tOKNAME;
    }

    public String gettOKFOTO() {
        return tOKFOTO;
    }

    public void settOKFOTO(String tOKFOTO) {
        this.tOKFOTO = tOKFOTO;
    }

    public String gettOKDETAIL() {
        return tOKDETAIL;
    }

    public void settOKDETAIL(String tOKDETAIL) {
        this.tOKDETAIL = tOKDETAIL;
    }

    public String gettOKNOKTP() {
        return tOKNOKTP;
    }

    public void settOKNOKTP(String tOKNOKTP) {
        this.tOKNOKTP = tOKNOKTP;
    }

    public String gettOKFOTOKTP() {
        return tOKFOTOKTP;
    }

    public void settOKFOTOKTP(String tOKFOTOKTP) {
        this.tOKFOTOKTP = tOKFOTOKTP;
    }

    public String gettOKSELFIE() {
        return tOKSELFIE;
    }

    public void settOKSELFIE(String tOKSELFIE) {
        this.tOKSELFIE = tOKSELFIE;
    }

    public String gettOKADDRESS() {
        return tOKADDRESS;
    }

    public void settOKADDRESS(String tOKADDRESS) {
        this.tOKADDRESS = tOKADDRESS;
    }

    public String gettOKIDPROV() {
        return tOKIDPROV;
    }

    public void settOKIDPROV(String tOKIDPROV) {
        this.tOKIDPROV = tOKIDPROV;
    }

    public String gettOKIDKOTA() {
        return tOKIDKOTA;
    }

    public void settOKIDKOTA(String tOKIDKOTA) {
        this.tOKIDKOTA = tOKIDKOTA;
    }

    public String gettOKKOTA() {
        return tOKKOTA;
    }

    public void settOKKOTA(String tOKKOTA) {
        this.tOKKOTA = tOKKOTA;
    }

    public String gettOKIDKECAMATAN() {
        return tOKIDKECAMATAN;
    }

    public void settOKIDKECAMATAN(String tOKIDKECAMATAN) {
        this.tOKIDKECAMATAN = tOKIDKECAMATAN;
    }

    public String gettOKKECAMATAN() {
        return tOKKECAMATAN;
    }

    public void settOKKECAMATAN(String tOKKECAMATAN) {
        this.tOKKECAMATAN = tOKKECAMATAN;
    }

    public String gettOKSTATUS() {
        return tOKSTATUS;
    }

    public void settOKSTATUS(String tOKSTATUS) {
        this.tOKSTATUS = tOKSTATUS;
    }

    public Object getpRJENIS() {
        return pRJENIS;
    }

    public void setpRJENIS(Object pRJENIS) {
        this.pRJENIS = pRJENIS;
    }

    public Object getpRNAME() {
        return pRNAME;
    }

    public void setpRNAME(Object pRNAME) {
        this.pRNAME = pRNAME;
    }

    public Object getpRDESKRIPSI() {
        return pRDESKRIPSI;
    }

    public void setpRDESKRIPSI(Object pRDESKRIPSI) {
        this.pRDESKRIPSI = pRDESKRIPSI;
    }

    public Object getpRCODE() {
        return pRCODE;
    }

    public void setpRCODE(Object pRCODE) {
        this.pRCODE = pRCODE;
    }

    public Object getpRPOTONGAN() {
        return pRPOTONGAN;
    }

    public void setpRPOTONGAN(Object pRPOTONGAN) {
        this.pRPOTONGAN = pRPOTONGAN;
    }

    public Object getpRMIN() {
        return pRMIN;
    }

    public void setpRMIN(Object pRMIN) {
        this.pRMIN = pRMIN;
    }

    public Object getpRSTATUS() {
        return pRSTATUS;
    }

    public void setpRSTATUS(Object pRSTATUS) {
        this.pRSTATUS = pRSTATUS;
    }

    public Object getpRMULAI() {
        return pRMULAI;
    }

    public void setpRMULAI(Object pRMULAI) {
        this.pRMULAI = pRMULAI;
    }

    public Object getpREXPIRED() {
        return pREXPIRED;
    }

    public void setpREXPIRED(Object pREXPIRED) {
        this.pREXPIRED = pREXPIRED;
    }

    public String getiDKATEGORI() {
        return iDKATEGORI;
    }

    public void setiDKATEGORI(String iDKATEGORI) {
        this.iDKATEGORI = iDKATEGORI;
    }

    public String getsKATNAME() {
        return sKATNAME;
    }

    public void setsKATNAME(String sKATNAME) {
        this.sKATNAME = sKATNAME;
    }

    public String getsKATIMAGE() {
        return sKATIMAGE;
    }

    public void setsKATIMAGE(String sKATIMAGE) {
        this.sKATIMAGE = sKATIMAGE;
    }

    public String getiDBARANG() {
        return iDBARANG;
    }

    public void setiDBARANG(String iDBARANG) {
        this.iDBARANG = iDBARANG;
    }

    public String getiDTOKO() {
        return iDTOKO;
    }

    public void setiDTOKO(String iDTOKO) {
        this.iDTOKO = iDTOKO;
    }

    public String getiDPROMO() {
        return iDPROMO;
    }

    public void setiDPROMO(String iDPROMO) {
        this.iDPROMO = iDPROMO;
    }

    public String getiDSUBKATEGORI() {
        return iDSUBKATEGORI;
    }

    public void setiDSUBKATEGORI(String iDSUBKATEGORI) {
        this.iDSUBKATEGORI = iDSUBKATEGORI;
    }

    public String getbAIMAGE() {
        return bAIMAGE;
    }

    public void setbAIMAGE(String bAIMAGE) {
        this.bAIMAGE = bAIMAGE;
    }

    public String getbANAME() {
        return bANAME;
    }

    public void setbANAME(String bANAME) {
        this.bANAME = bANAME;
    }

    public String getbAPSHOP() {
        return bAPSHOP;
    }

    public void setbAPSHOP(String bAPSHOP) {
        this.bAPSHOP = bAPSHOP;
    }

    public String getbAPGLD() {
        return bAPGLD;
    }

    public void setbAPGLD(String bAPGLD) {
        this.bAPGLD = bAPGLD;
    }

    public String getbAPRICE() {
        return bAPRICE;
    }

    public void setbAPRICE(String bAPRICE) {
        this.bAPRICE = bAPRICE;
    }

    public String getbASKU() {
        return bASKU;
    }

    public void setbASKU(String bASKU) {
        this.bASKU = bASKU;
    }

    public String getbADESCRIPTION() {
        return bADESCRIPTION;
    }

    public void setbADESCRIPTION(String bADESCRIPTION) {
        this.bADESCRIPTION = bADESCRIPTION;
    }

    public String getbASTOCK() {
        return bASTOCK;
    }

    public void setbASTOCK(String bASTOCK) {
        this.bASTOCK = bASTOCK;
    }

    public String getbAWEIGHT() {
        return bAWEIGHT;
    }

    public void setbAWEIGHT(String bAWEIGHT) {
        this.bAWEIGHT = bAWEIGHT;
    }

    public String getbACONDITION() {
        return bACONDITION;
    }

    public void setbACONDITION(String bACONDITION) {
        this.bACONDITION = bACONDITION;
    }

    public String getcREATEDAT() {
        return cREATEDAT;
    }

    public void setcREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getuPDATEDAT() {
        return uPDATEDAT;
    }

    public void setuPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public static Creator<ModelProduk> getCREATOR() {
        return CREATOR;
    }

    public ModelProduk(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCategory_utama() {
        return category_utama;
    }

    public void setCategory_utama(Integer category_utama) {
        this.category_utama = category_utama;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ModelProduk() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.category_utama);
        dest.writeValue(this.id);
        dest.writeString(this.kode);
        dest.writeString(this.type);
        dest.writeString(this.name);
        dest.writeValue(this.harga);
        dest.writeString(this.description);
        dest.writeString(this.category_id);
        dest.writeString(this.status);
        dest.writeValue(this.stock);
        dest.writeValue(this.weight);
        dest.writeString(this.path);
        dest.writeString(this.iDBARANG);
        dest.writeString(this.iDTOKO);
        dest.writeString(this.iDPROMO);
        dest.writeString(this.iDSUBKATEGORI);
        dest.writeString(this.bAIMAGE);
        dest.writeString(this.bANAME);
        dest.writeString(this.bAPSHOP);
        dest.writeString(this.bAPGLD);
        dest.writeString(this.bAPRICE);
        dest.writeString(this.bASKU);
        dest.writeString(this.bADESCRIPTION);
        dest.writeString(this.bASTOCK);
        dest.writeString(this.bAWEIGHT);
        dest.writeString(this.bACONDITION);
        dest.writeString(this.cREATEDAT);
        dest.writeString(this.uPDATEDAT);
    }

    protected ModelProduk(Parcel in) {
        this.category_utama = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.kode = in.readString();
        this.type = in.readString();
        this.name = in.readString();
        this.harga = (Integer) in.readValue(Integer.class.getClassLoader());
        this.description = in.readString();
        this.category_id = in.readString();
        this.status = in.readString();
        this.stock = (Integer) in.readValue(Integer.class.getClassLoader());
        this.weight = (Integer) in.readValue(Integer.class.getClassLoader());
        this.path = in.readString();
        this.iDBARANG = in.readString();
        this.iDTOKO = in.readString();
        this.iDPROMO = in.readString();
        this.iDSUBKATEGORI = in.readString();
        this.bAIMAGE = in.readString();
        this.bANAME = in.readString();
        this.bAPSHOP = in.readString();
        this.bAPGLD = in.readString();
        this.bAPRICE = in.readString();
        this.bASKU = in.readString();
        this.bADESCRIPTION = in.readString();
        this.bASTOCK = in.readString();
        this.bAWEIGHT = in.readString();
        this.bACONDITION = in.readString();
        this.cREATEDAT = in.readString();
        this.uPDATEDAT = in.readString();
    }

    public static final Creator<ModelProduk> CREATOR = new Creator<ModelProduk>() {
        @Override
        public ModelProduk createFromParcel(Parcel source) {
            return new ModelProduk(source);
        }

        @Override
        public ModelProduk[] newArray(int size) {
            return new ModelProduk[size];
        }
    };
}
