package id.co.cancreative.goldenshop.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelResult {

    public Saldo saldo = null;
    public Toko toko = null;
    public TokoStatus cek_toko = null;
    public Saldo min_wd = null;
    public String ktp;
    public String icon;

    @SerializedName("npm")
    @Expose
    private String npm;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("kelas")
    @Expose
    private String kelas;
    @SerializedName("sesi")
    @Expose
    private String sesi;
    @SerializedName("ID_SALDO")
    @Expose
    private String ID_SALDO;
    @SerializedName("ID_USER")
    @Expose
    private String ID_USER;
    @SerializedName("SA_SALDO")
    @Expose
    private String SA_SALDO;
    @SerializedName("CREATED_AT")
    @Expose
    private String CREATED_AT;

    @SerializedName("selfie")
    @Expose
    private String selfie;
    @SerializedName("id_toko")
    @Expose
    private String id_toko;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId_toko() {
        return id_toko;
    }

    public void setId_toko(String id_toko) {
        this.id_toko = id_toko;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKtp() {
        return ktp;
    }

    public void setKtp(String ktp) {
        this.ktp = ktp;
    }

    public String getSelfie() {
        return selfie;
    }

    public void setSelfie(String selfie) {
        this.selfie = selfie;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getID_SALDO() {
        return ID_SALDO;
    }

    public void setID_SALDO(String ID_SALDO) {
        this.ID_SALDO = ID_SALDO;
    }

    public String getID_USER() {
        return ID_USER;
    }

    public void setID_USER(String ID_USER) {
        this.ID_USER = ID_USER;
    }

    public String getSA_SALDO() {
        return SA_SALDO;
    }

    public void setSA_SALDO(String SA_SALDO) {
        this.SA_SALDO = SA_SALDO;
    }

    public String getCREATED_AT() {
        return CREATED_AT;
    }

    public void setCREATED_AT(String CREATED_AT) {
        this.CREATED_AT = CREATED_AT;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getSesi() {
        return sesi;
    }

    public void setSesi(String sesi) {
        this.sesi = sesi;
    }
}
