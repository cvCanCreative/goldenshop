package id.co.cancreative.goldenshop.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelSubKat implements Parcelable {

    @SerializedName("ID_SUB_KATEGORI")
    @Expose
    private String iDSUBKATEGORI;
    @SerializedName("ID_KATEGORI")
    @Expose
    private String iDKATEGORI;
    @SerializedName("SKAT_NAME")
    @Expose
    private String sKATNAME;
    @SerializedName("SKAT_IMAGE")
    @Expose
    private String sKATIMAGE;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;

    public String getiDSUBKATEGORI() {
        return iDSUBKATEGORI;
    }

    public void setiDSUBKATEGORI(String iDSUBKATEGORI) {
        this.iDSUBKATEGORI = iDSUBKATEGORI;
    }

    public String getiDKATEGORI() {
        return iDKATEGORI;
    }

    public void setiDKATEGORI(String iDKATEGORI) {
        this.iDKATEGORI = iDKATEGORI;
    }

    public String getsKATNAME() {
        return sKATNAME;
    }

    public void setsKATNAME(String sKATNAME) {
        this.sKATNAME = sKATNAME;
    }

    public String getsKATIMAGE() {
        return sKATIMAGE;
    }

    public void setsKATIMAGE(String sKATIMAGE) {
        this.sKATIMAGE = sKATIMAGE;
    }

    public String getcREATEDAT() {
        return cREATEDAT;
    }

    public void setcREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getuPDATEDAT() {
        return uPDATEDAT;
    }

    public void setuPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.iDSUBKATEGORI);
        dest.writeString(this.iDKATEGORI);
        dest.writeString(this.sKATNAME);
        dest.writeString(this.sKATIMAGE);
        dest.writeString(this.cREATEDAT);
        dest.writeString(this.uPDATEDAT);
    }

    public ModelSubKat() {
    }

    protected ModelSubKat(Parcel in) {
        this.iDSUBKATEGORI = in.readString();
        this.iDKATEGORI = in.readString();
        this.sKATNAME = in.readString();
        this.sKATIMAGE = in.readString();
        this.cREATEDAT = in.readString();
        this.uPDATEDAT = in.readString();
    }

    public static final Parcelable.Creator<ModelSubKat> CREATOR = new Parcelable.Creator<ModelSubKat>() {
        @Override
        public ModelSubKat createFromParcel(Parcel source) {
            return new ModelSubKat(source);
        }

        @Override
        public ModelSubKat[] newArray(int size) {
            return new ModelSubKat[size];
        }
    };
}
