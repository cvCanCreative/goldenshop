package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ModelTransaksi implements Serializable {

    public String id_rekening;
    public String id_rekening_gold;
    public Integer kode_unik;
    public String kode_promo;
    public Integer totalBayar;
    public List<String> id_penerima = new ArrayList<>();
    public List<Integer> id_barang = new ArrayList<>();
    public List<Integer> harga_barang = new ArrayList<>();
    public List<Integer> qty = new ArrayList<>();
    public List<String> catatan = new ArrayList<>();
    public List<String> kurir = new ArrayList<>();
    public List<String> jns_kurir = new ArrayList<>();
    public List<Integer> ongkir = new ArrayList<>();

}

