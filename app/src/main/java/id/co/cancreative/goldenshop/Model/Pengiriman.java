package id.co.cancreative.goldenshop.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Pengiriman implements Serializable {
    public String manifest_code;
    public String manifest_description;
    public String manifest_date;
    public String manifest_time;
    public String city_name;
}
