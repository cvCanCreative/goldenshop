package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Promo implements Serializable {
    public String ID_PROMO;
    public String ID_TOKO;
    public String PR_JENIS;
    public String ID_SUB_KATEGORI;
    public String PR_NAME;
    public String PR_DESKRIPSI;
    public String PR_CODE;
    public String PR_POTONGAN;
    public String PR_MIN;
    public String PR_STATUS;
    public String PR_MULAI;
    public String PR_EXPIRED;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String TOK_NAME;
    public String TOK_FOTO;
    public String TOK_DETAIL;
    public String TOK_NO_KTP;
    public String TOK_FOTO_KTP;
    public String TOK_SELFIE;
    public String TOK_ADDRESS;
    public String TOK_ID_PROV;
    public String TOK_ID_KOTA;
    public String TOK_KOTA;
    public String TOK_ID_KECAMATAN;
    public String TOK_KECAMATAN;
    public String TOK_STATUS;
    public String ID_USER_DETAIL;
    public String ID_USER;
    public String USD_FOTO;
    public String USD_FULLNAME;
    public String USD_BIRTH;
    public String USD_ADDRESS;
    public String USD_GENDER;
    public String USD_STATUS;
    public String SKAT_NAME;
}
