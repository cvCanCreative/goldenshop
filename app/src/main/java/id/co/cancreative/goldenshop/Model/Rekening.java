package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Rekening implements Serializable {
    public String ID_REKENING;
    public String ID_USER;
    public String ID_BANK;
    public String RK_NOMOR;
    public String RK_NAME;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String BK_NAME;
    public String BK_IMAGE;
    public String CR_REKENING;
    public String UD_REKENING;
}
