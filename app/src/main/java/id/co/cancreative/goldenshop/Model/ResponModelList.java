package id.co.cancreative.goldenshop.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponModelList {

    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("result")
    @Expose
    private List<ModelResult> result = null;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user")
    @Expose
    private ModelUsers user = null;
    @SerializedName("products")
    @Expose
    private List<ModelProduk> products = null;

    private List<Feedback> feedback = null;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public List<ModelResult> getResult() {
        return result;
    }

    public void setResult(List<ModelResult> result) {
        this.result = result;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ModelUsers getUser() {
        return user;
    }

    public void setUser(ModelUsers user) {
        this.user = user;
    }

    public List<ModelProduk> getProducts() {
        return products;
    }

    public void setProducts(List<ModelProduk> products) {
        this.products = products;
    }

    public List<Feedback> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<Feedback> feedback) {
        this.feedback = feedback;
    }
}
