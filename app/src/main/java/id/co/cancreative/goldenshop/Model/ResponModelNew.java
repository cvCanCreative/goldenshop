package id.co.cancreative.goldenshop.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponModelNew implements Serializable{

    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("toko")
    @Expose
    private ModelAcount toko = null;
    @SerializedName("rajaongkir")
    @Expose
    private ResponRajaOangkir rajaongkir;

    public Integer success;
    public Integer jml_produk;
    public String message;
    public String picture;
    public String image_name;
    public String exp_trx;
    public String total_bayar;

    public Produk product = null;
    public ModelResult result = null;
    public User user = null;
    public Toko tokos = null;
    public Saldo saldo = null;
    public Rekening rekenings = null;
    public Promo detail_promo = null;
    public Kontak kontak = null;
    public MinWD min_wd = null;

    public List<Product> products = new ArrayList<>();
    public ArrayList<ModelCategory> kategori = new ArrayList<>();
    public ArrayList<Rekening> rekening = new ArrayList<>();
    public ArrayList<Rekening> bank = new ArrayList<>();
    public ArrayList<Transaksi> transaksi = new ArrayList<>();
    public ArrayList<Withdraw> history_wd = new ArrayList<>();
    public ArrayList<TransaksiToko> history_tp = new ArrayList<>();
    public ArrayList<Promo> promo = new ArrayList<>();
    public ArrayList<Slider> slider = new ArrayList<>();
    public ArrayList<Pengiriman> manifest = new ArrayList<>();
    public ArrayList<Komplain> komplain = new ArrayList<>();
    public ArrayList<Feedback> feedback = new ArrayList<>();
    public ArrayList<Faq> faq = new ArrayList<>();

    public ResponRajaOangkir getRajaongkir() {
        return rajaongkir;
    }

    public void setRajaongkir(ResponRajaOangkir rajaongkir) {
        this.rajaongkir = rajaongkir;
    }

    //Serializable
    public List<ModelAlamat> alamat = new ArrayList<>();

    public ArrayList<ModelCategory> getKategori() {
        return kategori;
    }

    public void setKategori(ArrayList<ModelCategory> kategori) {
        this.kategori = kategori;
    }

    public ModelAcount getToko() {
        return toko;
    }

    public void setToko(ModelAcount toko) {
        this.toko = toko;
    }

    public ModelResult getResult() {
        return result;
    }

    public void setResult(ModelResult result) {
        this.result = result;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}



