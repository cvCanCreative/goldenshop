package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Saldo implements Serializable {
    public int success;
    public String message;
    public int ID_SALDO;
    public int ID_USER;
    public String SA_SALDO;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String minimal_wd;
    public String minimal_saldo_toko;
}
