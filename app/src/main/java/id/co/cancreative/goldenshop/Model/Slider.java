package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Slider implements Serializable {
    public String ID_SLIDER;
    public String ID_PROMO;
    public String SLI_JUDUL;
    public String SLI_FOTO;
    public String SLI_DESKRIP;
    public String SLI_STATUS;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String ID_TOKO;
    public String PR_JENIS;
    public String ID_SUB_KATEGORI;
    public String PR_NAME;
    public String PR_DESKRIPSI;
    public String PR_CODE;
    public String PR_POTONGAN;
    public String PR_MIN;
    public String PR_STATUS;
    public String PR_MULAI;
    public String PR_EXPIRED;
    public String SLI_CREATED_AT;
    public String SLI_UPDATED_AT;
}
