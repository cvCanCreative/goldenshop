package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Toko implements Serializable {
    public int success;
    public String id_toko;
    public String status;
    public String ID_TOKO;
    public String TOK_NAME;
    public String TOK_FOTO;
    public String TOK_DETAIL;
    public String TOK_NO_KTP;
    public String TOK_FOTO_KTP;
    public String TOK_SELFIE;
    public String TOK_ADDRESS;
    public String TOK_ID_PROV;
    public String TOK_ID_KOTA;
    public String TOK_KOTA;
    public String TOK_ID_KECAMATAN;
    public String TOK_KECAMATAN;
    public String TOK_STATUS;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String ID_USER;
    public String US_USERNAME;
    public String US_PASSWORD;
    public String US_TELP;
    public String US_EMAIL;
    public String US_RULE;
    public String US_FCM;
    public String US_TOKEN;
    public String US_FORGOT_PASS;
}
