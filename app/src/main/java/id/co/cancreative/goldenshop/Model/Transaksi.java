package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class Transaksi implements Serializable {
    public String ID_TRANSAKSI;
    public String ID_USER;
    public String ID_TOKO;
    public String ID_KOMPLAIN;
    public String TS_KODE_PAYMENT;
    public String TS_BUKTI_TF;
    public String ID_REKENING;
    public String ID_REKENING_GOLD;
    public String TS_TF_GOLD;
    public String TS_KODE_TRX;
    public String TS_TOTAL_ITEM;
    public String TS_METODE_BAYAR;
    public String TS_KODE_UNIK;
    public String TS_HARGA;
    public String TS_KODE_PROMO;
    public String TS_JNS_PROMO;
    public String TS_POT_PROMO;
    public String TS_BAYAR_ONGKIR;
    public String TS_TOTAL_BAYAR;
    public String TS_STATUS;
    public String TS_ID_CEKRESI;
    public String TS_RESI;
    public String TS_SLUG_KURIR;
    public String TS_EXP;
    public String CREATED_AT;
    public String UPDATED_AT;
    public Rekening rekening_golden;
    public Rekening rekening_user;
    public User pembeli;
    public ArrayList<TransaksiDetail> transaksi_detail = new ArrayList<>();

}
