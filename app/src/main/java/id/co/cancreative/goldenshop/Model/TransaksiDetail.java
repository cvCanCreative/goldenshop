package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class TransaksiDetail implements Serializable {
    public String ID_TRANSAKSI_DETAIL;
    public String ID_TRANSAKSI;
    public String ID_USER;
    public String ID_BARANG;
    public String ID_PENGIRIMAN;
    public String TSD_QTY;
    public String TSD_CATATAN;
    public String TSD_KODE_PROMO;
    public String TSD_SLUG_KURIR;
    public String TSD_KURIR;
    public String TSD_JENIS_KURIR;
    public String TSD_ONGKIR;
    public String TSD_HARGA_ASLI;
    public String TSD_HARGA_TOTAL;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String feedback;
    public Produk barang;
    public Alamat alamat_kirim;
}
