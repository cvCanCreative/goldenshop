package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class TransaksiToko implements Serializable {
    public String ID_HISTORY_SALDO;
    public String ID_USER;
    public String HSA_TRX_SALDO;
    public String HSA_TYPE;
    public String HSA_FROM;
    public String HSA_STATUS;
    public String HSA_SALDO;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String ID_SALDO_BACK;
    public String SAB_KODE_PAYMENT;
    public String SAB_NOMINAL;
    public String TGL_TRX;
    public ArrayList<Transaksi> transaksi= new ArrayList<>();
}
