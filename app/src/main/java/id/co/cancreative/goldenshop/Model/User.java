package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class User implements Serializable {
    public String id_user;
    public String US_USERNAME;
    public String US_TELP;
    public String US_EMAIL;
    public String id_toko;
    public String session;
    public String username;
    public String fullname;
    public String birth;
    public String email;
    public String telp;
    public String alamat;
    public String gender;
    public String foto;
    public String role;
}

