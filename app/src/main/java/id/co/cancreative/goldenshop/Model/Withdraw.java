package id.co.cancreative.goldenshop.Model;

import java.io.Serializable;

public class Withdraw implements Serializable {
    public String ID_HISTORY_SALDO;
    public String ID_USER;
    public String HSA_TRX_SALDO;
    public String HSA_TYPE;
    public String HSA_FROM;
    public String HSA_STATUS;
    public String HSA_SALDO;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String ID_WITHDRAW;
    public String ID_REKENING;
    public String SAWD_NOMINAL;
    public String ID_BANK;
    public String RK_NOMOR;
    public String RK_NAME;
    public String BK_NAME;
    public String BK_IMAGE;
    public String TGL_TRX;
}
