package id.co.cancreative.goldenshop.NotificationPackage;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.cancreative.goldenshop.Activity.DaftarTransaksiMenunguPembayaran;
import id.co.cancreative.goldenshop.Activity.DaftarTransaksiActivity;
import id.co.cancreative.goldenshop.Activity.DaftarTransaksiTokoActivity;
import id.co.cancreative.goldenshop.Activity.SplashActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }else{

        }
    }

    private void handleDataMessage(JSONObject json) {
        try {
            Intent intent = new Intent();
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
            String imageUrl = data.getString("image");
            String datenotifikasi = data.getString("timestamp");

            //Convert Date To TimeStamp
            SimpleDateFormat sdfnotif = new SimpleDateFormat("yyyy-MM-DD");
            Date dateNotif = sdfnotif.parse(datenotifikasi);
            Timestamp timestamp = new Timestamp(dateNotif.getTime());


            JSONObject payload = data.getJSONObject("payload");
            String type_data = payload.getString("type");

            //Mngarahkan Intent sesui type notifikasi
            //Mulai  Bagian Pembeli
            switch (type_data) {
                case "pembayaran":
                    intent = new Intent(getApplicationContext(), DaftarTransaksiMenunguPembayaran.class);
                    break;
                case "proses_admin":
                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                    intent.putExtra("FRAGMENT_ID", "0");
                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        intent.putExtra("status_notif", true);
                    } else {
                        intent.putExtra("status_notif", false);
                    }

                    break;
                case "proses_vendor_user":
                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                    intent.putExtra("FRAGMENT_ID", "1");
                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        intent.putExtra("status_notif", true);
                    } else {
                        intent.putExtra("status_notif", false);
                    }

                    break;
                case "proses_kirim":
                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                    intent.putExtra("FRAGMENT_ID", "2");
                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        intent.putExtra("status_notif", true);
                    } else {
                        intent.putExtra("status_notif", false);
                    }

                    break;
                case "terima_user":
                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                    intent.putExtra("FRAGMENT_ID", "3");
                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        intent.putExtra("status_notif", true);
                    } else {
                        intent.putExtra("status_notif", false);
                    }

                    break;
                case "batal":
                    intent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                    intent.putExtra("FRAGMENT_ID", "4");
                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        intent.putExtra("status_notif", true);
                    } else {
                        intent.putExtra("status_notif", false);
                    }

                    break;
                //Selesai  Bagian Pembeli
                //Mulai Bagian Penjual
                case "proses_vendor":
                    intent = new Intent(getApplicationContext(), DaftarTransaksiTokoActivity.class);
                    intent.putExtra("FRAGMENT_ID", "0");
                    if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        intent.putExtra("status_notif", true);
                    } else {
                        intent.putExtra("status_notif", false);
                    }

                    break;
                //Selesai  Bagian Penjual
                default:
                    intent = new Intent(getApplicationContext(), SplashActivity.class);
                    break;
            }

            intent.putExtra("title", title);
            intent.putExtra("timestamp", timestamp);
            intent.putExtra("type_data", type_data);
            intent.putExtra("image", imageUrl);

            // check for image attachment
            if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, String.valueOf(timestamp), intent);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, String.valueOf(timestamp), intent, imageUrl);
            }

        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}