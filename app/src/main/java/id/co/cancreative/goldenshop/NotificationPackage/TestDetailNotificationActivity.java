package id.co.cancreative.goldenshop.NotificationPackage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import id.co.cancreative.goldenshop.R;

public class TestDetailNotificationActivity extends AppCompatActivity {

    private ImageView featureGraphics;
    private TextView header;
    private TextView timeStamp;
    private TextView article;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_detail_notification);
        initView();

        //receive data from MyFirebaseMessagingService class
        String title = getIntent().getStringExtra("title");
        String timeStamps = getIntent().getStringExtra("timestamp");
        String articles = getIntent().getStringExtra("article_data");
        String imageUrl = getIntent().getStringExtra("image");

        //Set data on UI
        header.setText(title);
        timeStamp.setText(timeStamps);
        article.setText(articles);

    }

    private void initView() {
        featureGraphics = (ImageView) findViewById(R.id.featureGraphics);
        header = (TextView) findViewById(R.id.header);
        timeStamp = (TextView) findViewById(R.id.timeStamp);
        article = (TextView) findViewById(R.id.article);
    }
}
