package id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import id.co.cancreative.goldenshop.SQLiteTable.keranjang.DAOKranjang;
import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;

@Database(entities = {TbKranjangByToko.class}, version = 1, exportSchema = false)
public abstract class AppDbKrenjangByToko extends RoomDatabase {
    public abstract DAOKranjangByToko daoKranjang();
}
