package id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;

@Dao
public interface DAOKranjangByToko {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertBarang(TbKranjangByToko barang);

    @Update
    int updateBarang(TbKranjangByToko barang);

    @Delete
    int deleteBarang(TbKranjangByToko barang);

    @Query("SELECT * FROM produkByToko")
    TbKranjangByToko[] selectAllBarangs();

    @Query("SELECT * FROM produkByToko WHERE idToko = :id LIMIT 1")
    TbKranjangByToko selectProdukId(int id);

    @Query("SELECT * FROM produkByToko WHERE idToko = :id LIMIT 1")
    TbKranjangByToko selectProdukIdList(int id);

    @Query("UPDATE produkByToko SET jumlahProduk =:jumlahBarangs WHERE idToko = :id")
    int updateJumlah(int id, int jumlahBarangs);

    @Query("DELETE FROM produkByToko")
    int deleteAll();

    @Query("DELETE FROM produkByToko WHERE idToko = :id")
    int deleteById(int id);

}
