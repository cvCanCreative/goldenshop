package id.co.cancreative.goldenshop.SQLiteTable.KeranjangByToko;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "produkByToko")
public class TbKranjangByToko implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int ID;

    @ColumnInfo(name = "idProduk")
    public int idProduk;

    @ColumnInfo(name = "idToko")
    public int idToko;

    @ColumnInfo(name = "jumlahProduk")
    public int jumlahProduk;

    @ColumnInfo(name = "namaToko")
    public String namaToko;

    @ColumnInfo(name = "pilih")
    public Boolean pilih;

}
