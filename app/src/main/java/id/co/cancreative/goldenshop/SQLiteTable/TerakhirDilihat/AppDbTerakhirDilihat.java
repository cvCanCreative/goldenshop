package id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


@Database(entities = {TbTerakhirDilihat.class}, version = 2, exportSchema = false)
public abstract class AppDbTerakhirDilihat extends RoomDatabase {
    public abstract DAOTerakhirDilihat daoTerakhirDilihat();
}
