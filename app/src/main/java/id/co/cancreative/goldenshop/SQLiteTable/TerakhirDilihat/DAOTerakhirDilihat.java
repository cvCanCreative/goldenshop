package id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import id.co.cancreative.goldenshop.SQLiteTable.keranjang.TbKranjang;

@Dao
public interface DAOTerakhirDilihat {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertTerakhirDilihat(TbTerakhirDilihat tbTerakhirDilihat);

    @Query("DELETE FROM tdProduk WHERE idProduk = :id")
    int deleteTerakhirDilihat(int id);

    @Query("SELECT * FROM tdProduk ORDER BY id DESC")
    TbTerakhirDilihat[] selectTerakhirDilihat();

}
