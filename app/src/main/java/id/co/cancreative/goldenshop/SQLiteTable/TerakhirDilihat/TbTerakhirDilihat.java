package id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "tdProduk")
public class TbTerakhirDilihat implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public int barangId;

    @ColumnInfo(name = "idProduk")
    public int idProduk;

    @ColumnInfo(name = "idToko")
    public int idToko;

    @ColumnInfo(name = "nama")
    public String nama;

    @ColumnInfo(name = "id_kategori")
    public String id_kategori;

    @ColumnInfo(name = "nama_kategori")
    public String nama_kategori;

    @ColumnInfo(name = "harga")
    public int harga;

    @ColumnInfo(name = "stok")
    public int stok;

    @ColumnInfo(name = "berat")
    public int berat;

    @ColumnInfo(name = "sku")
    public String sku;

    @ColumnInfo(name = "kondisi")
    public String kondisi;

    @ColumnInfo(name = "gambar")
    public String gambar;

    @ColumnInfo(name = "penjual")
    public String penjual;

    @ColumnInfo(name = "deskripsi")
    public String deskripsi;

    @ColumnInfo(name = "kotaPenjual")
    public String kotaPenjual;

    @ColumnInfo(name = "fotoPenjual")
    public String fotoPenjual;

    @ColumnInfo(name = "kodePromo")
    public String kodePromo;

    @ColumnInfo(name = "promoPotongan")
    public String promoPotongan;

    @ColumnInfo(name = "promoMin")
    public String promoMin;

    @ColumnInfo(name = "promoStatus")
    public String promoStatus;

    @ColumnInfo(name = "promoExpaired")
    public String promoExp;
}
