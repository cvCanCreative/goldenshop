package id.co.cancreative.goldenshop.SQLiteTable.keranjang;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {TbKranjang.class}, version = 1, exportSchema = false)
public abstract class AppDbKrenjang extends RoomDatabase {
    public abstract DAOKranjang daoKranjang();
}
