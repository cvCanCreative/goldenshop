package id.co.cancreative.goldenshop.SQLiteTable.keranjang;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface DAOKranjang {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertBarang(TbKranjang barang);

    @Update
    int updateBarang(TbKranjang barang);

    @Delete
    int deleteBarang(TbKranjang barang);

    @Query("SELECT * FROM produk")
    TbKranjang[] selectAllBarangs();

    @Query("SELECT * FROM produk WHERE pilih = :status")
    TbKranjang[] selectBarangsTerpilih(boolean status);

    @Query("SELECT * FROM produk WHERE idToko = :id")
    TbKranjang[] selectByIdToko(int id);

    @Query("SELECT * FROM produk WHERE idToko = :id AND pilih = :status")
    TbKranjang[] selectByIdTokoTerpilih(int id, boolean status);

    @Query("SELECT * FROM produk WHERE barangId = :id LIMIT 1")
    TbKranjang selectBarangDetail(int id);

    @Query("SELECT * FROM produk WHERE idProduk = :id LIMIT 1")
    TbKranjang selectProdukId(int id);

    @Query("UPDATE produk SET jumlah_pesan =:jumlahBarangs WHERE idProduk = :id")
    int updateJumlah(int id, int jumlahBarangs);

    @Query("UPDATE produk SET pilih =:status WHERE idProduk = :id")
    int updateStatus(int id, boolean status);

    @Query("DELETE FROM produk")
    int deleteAll();

    @Query("DELETE FROM produk WHERE idProduk = :id")
    int deleteById(int id);

}
