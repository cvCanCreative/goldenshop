package id.co.cancreative.goldenshop.SQLiteTable.keranjang;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

import id.co.cancreative.goldenshop.Model.Promo;

@Entity(tableName = "produk")
public class TbKranjang implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int barangId;

    @ColumnInfo(name = "idProduk")
    public int idProduk;

    @ColumnInfo(name = "idToko")
    public int idToko;

    @ColumnInfo(name = "nama")
    public String nama;

    @ColumnInfo(name = "catatan")
    public String catatan;

    @ColumnInfo(name = "kodePromo")
    public String kodePromo;

    @ColumnInfo(name = "promoPotongan")
    public String promoPotongan;

    @ColumnInfo(name = "promoMin")
    public String promoMin;

    @ColumnInfo(name = "promoStatus")
    public String promoStatus;

    @ColumnInfo(name = "promoExp")
    public String kodeExp;

    @ColumnInfo(name = "id_kategori")
    public String id_kategori;

    @ColumnInfo(name = "nama_kategori")
    public String nama_kategori;

    @ColumnInfo(name = "harga")
    public int harga;

    @ColumnInfo(name = "jumlah_pesan")
    public int jumlahPesan;

    @ColumnInfo(name = "stok")
    public int stok;

    @ColumnInfo(name = "berat")
    public int berat;

    @ColumnInfo(name = "ongkir")
    public int ongkir = 0;

    @ColumnInfo(name = "kurir")
    public String kurir;

    @ColumnInfo(name = "durasiPengiriman")
    public String durasiPengiriman;

    @ColumnInfo(name = "jenisKurir")
    public String jenisKurir;

    @ColumnInfo(name = "sku")
    public String sku;

    @ColumnInfo(name = "kondisi")
    public String kondisi;

    @ColumnInfo(name = "gambar")
    public String gambar;

    @ColumnInfo(name = "penjual")
    public String penjual;

    @ColumnInfo(name = "pilih")
    public boolean pilih = false;

}
