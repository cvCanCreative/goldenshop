package id.co.cancreative.goldenshop.utils;

import android.app.Dialog;

public interface CallbackDialog {

    void onPositiveClick(Dialog dialog);

    void onNegativeClick(Dialog dialog);
}
