package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import id.co.cancreative.goldenshop.Activity.DetailProdukActivity;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.R;

public class AdapterProdukGS extends RecyclerView.Adapter<AdapterProdukGS.Holdr> {

    ArrayList<Produk> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterProdukGS(ArrayList<Produk> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_produk, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(AdapterProdukGS.Holdr holder, final int i) {
        s = new SharedPref(context);

        final Produk a = data.get(i);
        final int min = 10;
        final int max = 50;
        final int random = new Random().nextInt((max - min) + 1) + min;
        int hargaAsli = Integer.valueOf(a.BA_PRICE);
        int diskon = (int) (hargaAsli * ((double) random / 100));
        int hargaAkhir = hargaAsli - diskon;

        holder.nama.setText(a.BA_NAME);
        holder.discount.setText("-"+String.valueOf(random)+"%");
        holder.harga.setText(""+new Helper().convertRupiah(hargaAsli));

        String image = new Helper().splitText(a.BA_IMAGE);
        Picasso.with(context)
                .load(Config.URL_produkGolden+image)
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat1)
                // To fit image into imageView
//					.fit()
                // To prevent fade animation
                .noFade()
                .into(holder.image);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s.setProduk(data.get(i));
                Intent intent = new Intent(context, DetailProdukActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        ImageView image;
        TextView discount, harga, nama;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_produk);
            discount = (TextView) itemView.findViewById(R.id.tv_discount);
            harga = (TextView) itemView.findViewById(R.id.tv_harga);
            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            layout = itemView.findViewById(R.id.div_layout);
        }
    }
}
