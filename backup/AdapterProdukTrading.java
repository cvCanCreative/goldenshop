package id.co.cancreative.goldenshop.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import id.co.cancreative.goldenshop.Activity.DetailProdukActivity;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.R;


public class AdapterProdukTrading extends RecyclerView.Adapter<AdapterProdukTrading.Holdr> {

    ArrayList<Produk> data;
    Activity context;
    int b;
    SharedPref s;

    public AdapterProdukTrading(ArrayList<Produk> data, Activity context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Holdr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        return new Holdr(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_produk_trading, null));
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_produk_trading, viewGroup, false);
        return new Holdr(view);
    }

    @Override
    public void onBindViewHolder(AdapterProdukTrading.Holdr holder, final int i) {
        s = new SharedPref(context);

        final Produk a = data.get(i);
        final int min = 20;
        final int max = 580;
        final int random = new Random().nextInt((max - min) + 1) + min;

        holder.nama.setText(a.BA_NAME);
        holder.harga.setText(""+new Helper().convertRupiah(Integer.valueOf(a.BA_PRICE)));
        holder.tvTerjual.setText(random +" Terjual");

        String image = new Helper().splitText(a.BA_IMAGE);

        Picasso.with(context)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .noFade()
                .into(holder.image);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, DetailProdukActivity.class);
                s.setProduk(a);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holdr extends RecyclerView.ViewHolder {
        ImageView image;
        TextView nama, harga, tvTerjual;
        LinearLayout layout;

        public Holdr(final View itemView) {
            super(itemView);
            nama = (TextView) itemView.findViewById(R.id.tv_nama);
            image = (ImageView) itemView.findViewById(R.id.img_produk);
            harga = (TextView) itemView.findViewById(R.id.tv_harga);
            tvTerjual = (TextView) itemView.findViewById(R.id.tv_terjual);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
        }
    }
}
