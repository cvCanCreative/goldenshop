package id.co.cancreative.goldenshop.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.cancreative.goldenshop.Activity.DaftarHistoryTransActivity;
import id.co.cancreative.goldenshop.Activity.DaftarTransaksiActivity;
import id.co.cancreative.goldenshop.Activity.FAQActivity;
import id.co.cancreative.goldenshop.Activity.PengaturanAkunActivity;
import id.co.cancreative.goldenshop.Activity.WithdrawActivity;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Saldo;
import id.co.cancreative.goldenshop.Model.User;
import id.co.cancreative.goldenshop.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AkunBeliFragment extends Fragment {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;
    private View view;
    private Saldo saldo;
    private User user;

    private ImageView btnSetting;
    private LinearLayout divAkun;
    private ImageView btnKeranjang;
    private CircleImageView imgUser;
    private TextView tvNama;
    private TextView tvSaldo;
    private TextView btnAddSaldo;
    private TextView btnDetailsaldo;
    private TextView tvAkunStatus;
    private RelativeLayout btnHistoryTransaksi;
    private CircleImageView imgPenjual;
    private LinearLayout btnMenungguKonfirmasi;
    private CardView btnSaldo;
    private RelativeLayout fabRelativeBantuan;

    private LinearLayout btnPesananDiproses;
    private LinearLayout btnSedangDiproses;
    private LinearLayout btnSampaiTujuan;
    private RelativeLayout btnFavourit;
    private RelativeLayout btnTerakhirDilihat;
    public AkunBeliFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_akun_beli, container, false);
        initView(view);

        s = new SharedPref(getActivity());
        saldo = s.getSaldo();
        user = s.getUser();

        cekAkunRole();

        setValue();
        mainButton();
        return view;
    }

    private void cekAkunRole() {
        if (user.role.equals("RL_ADMIN")) {
            prossesAsAdmin();
        }
    }

    private void prossesAsAdmin() {
        tvAkunStatus.setText("Login Sebagai Admin");
    }

    private void setValue() {

        String name = user.fullname;
        if (name.isEmpty() || name == null) {
            name = user.username;
        }

        tvNama.setText(name);
        if (saldo.SA_SALDO != null && !saldo.SA_SALDO.equals("")) {
            tvSaldo.setText("" + new Helper().convertRupiah(Integer.valueOf(saldo.SA_SALDO)));
        }

        Picasso.with(getActivity())
                .load(Config.url_user + user.foto)
//                .load(R.drawable.image_loading)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(imgUser);
    }

    private void mainButton() {
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PengaturanAkunActivity.class);
                startActivity(intent);
            }
        });

        btnHistoryTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarHistoryTransActivity.class);
                startActivity(intent);
            }
        });

        btnMenungguKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiActivity.class);
                intent.putExtra("FRAGMENT_ID", "0");
                startActivity(intent);
            }
        });

        btnSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WithdrawActivity.class);
                startActivity(intent);
            }
        });

        fabRelativeBantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FAQActivity.class);
                startActivity(intent);
            }
        });
        btnPesananDiproses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiActivity.class);
                intent.putExtra("FRAGMENT_ID", "1");
                startActivity(intent);
            }
        });

        btnSedangDiproses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiActivity.class);
                intent.putExtra("FRAGMENT_ID", "2");
                startActivity(intent);
            }
        });

        btnSampaiTujuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarTransaksiActivity.class);
                intent.putExtra("FRAGMENT_ID", "3");
                startActivity(intent);
            }
        });

        btnFavourit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasti.info(getActivity(), "Fungsi dalam Pengembangan");
            }
        });

        btnTerakhirDilihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasti.info(getActivity(), "Fungsi dalam Pengembangan");
            }
        });
    }

    private void initView(View view) {
        btnSetting = (ImageView) view.findViewById(R.id.btn_setting);
        divAkun = (LinearLayout) view.findViewById(R.id.div_akun);
        btnKeranjang = (ImageView) view.findViewById(R.id.btn_keranjang);
        imgUser = (CircleImageView) view.findViewById(R.id.img_penjual);
        tvNama = (TextView) view.findViewById(R.id.tv_nama);
        tvSaldo = (TextView) view.findViewById(R.id.tv_saldo);
        tvAkunStatus = (TextView) view.findViewById(R.id.tv_akunStatus);
        btnHistoryTransaksi = (RelativeLayout) view.findViewById(R.id.btn_historyTransaksi);
        imgPenjual = (CircleImageView) view.findViewById(R.id.img_penjual);
        btnMenungguKonfirmasi = (LinearLayout) view.findViewById(R.id.btn_menungguKonfirmasi);
        btnSaldo = (CardView) view.findViewById(R.id.btn_saldo);
        fabRelativeBantuan = (RelativeLayout) view.findViewById(R.id.fab_relative_bantuan);
        btnPesananDiproses = (LinearLayout) view.findViewById(R.id.btn_pesananDiproses);
        btnSedangDiproses = (LinearLayout) view.findViewById(R.id.btn_sedangDiproses);
        btnSampaiTujuan = (LinearLayout) view.findViewById(R.id.btn_sampaiTujuan);
        btnFavourit = (RelativeLayout) view.findViewById(R.id.btn_favourit);
        btnTerakhirDilihat = (RelativeLayout) view.findViewById(R.id.btn_terakhirDilihat);
    }


}
