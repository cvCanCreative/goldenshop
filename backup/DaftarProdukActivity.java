package id.co.cancreative.goldenshop.Activity;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.cancreative.goldenshop.Adapter.AdapterProdukGS;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelCategory;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.ModelSubKat;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelSerializ;
import id.co.cancreative.goldenshop.R;

import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarProdukActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;

    private ArrayList<ModelProduk> mItem = new ArrayList<>();
    private ArrayList<Produk> mProduk = new ArrayList<>();
    private ArrayList<ModelCategory> mKategori = new ArrayList<>();

    private ModelSubKat data;

    private Toolbar toolbar;
    private RecyclerView rv;
    private ProgressBar pd;
    private LinearLayout divNoProduk;
    private TextView tvPesan;
    private AppDbTerakhirDilihat db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_produk);
        initView();

        data = new ModelSubKat();
        s = new SharedPref(this);
        db = Room.databaseBuilder(this, AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();

        getIntentExtra();
        setToolbar();
    }

    private void getIntentExtra() {
        data = getIntent().getParcelableExtra("data");

        String extra = getIntent().getStringExtra("extra");
        if (extra != null) {
            if (extra.equals("gsproduk")) {
                getGSProduk();
            }
        } else {
            getProduk();
        }

    }

    private void getGSProduk() {
        api.getProdukGS(s.getApi()).enqueue(new Callback<ResponModelSerializ>() {
            @Override
            public void onResponse(Call<ResponModelSerializ> call, Response<ResponModelSerializ> response) {
//                swipeRefrash.setRefreshing(false);
                pd.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ResponModelSerializ produk = response.body();

                    mProduk = (ArrayList<Produk>) produk.products;
                    RecyclerView.Adapter mAdapter = new AdapterProdukTrading(mProduk, DaftarProdukActivity.this, db);
                    rv.setLayoutManager(new GridLayoutManager(DaftarProdukActivity.this, 2));
                    rv.setAdapter(mAdapter);
                }

            }

            @Override
            public void onFailure(Call<ResponModelSerializ> call, Throwable t) {
                if (DaftarProdukActivity.this != null) {
                    SweetAlert.onFailure(DaftarProdukActivity.this, t.getMessage());
                }
            }
        });
    }

    private void getProduk() {
        api.getProdukByK(s.getApi(), data.getiDSUBKATEGORI()).enqueue(new Callback<ResponModelSerializ>() {
            @Override
            public void onResponse(Call<ResponModelSerializ> call, Response<ResponModelSerializ> response) {
                pd.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    mProduk = (ArrayList<Produk>) response.body().products;
                    if (mProduk.size() >= 1) {
//                        mProduk.addAll(DataProduct.getListData());
                        mAdapter = new AdapterProdukTrading(mProduk, DaftarProdukActivity.this, db);
                        rv.setLayoutManager(new GridLayoutManager(DaftarProdukActivity.this, 2));
                        rv.setAdapter(mAdapter);
                    } else {
                        tvPesan.setText("Produk pada kategori " + data.getsKATNAME() + " kosong");
                        divNoProduk.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponModelSerializ> call, Throwable t) {

            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        if (data != null) {
            getSupportActionBar().setTitle(data.getsKATNAME());
        } else {
            getSupportActionBar().setTitle(getString(R.string.produk_txt));
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        rv = (RecyclerView) findViewById(R.id.rv);
        pd = (ProgressBar) findViewById(R.id.pd);
        divNoProduk = (LinearLayout) findViewById(R.id.div_noProduk);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
