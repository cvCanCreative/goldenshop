package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import id.co.cancreative.goldenshop.Adapter.AdapterPromo;
import id.co.cancreative.goldenshop.Adapter.AdapterPromoPilih;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.CallFunction;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.ItemClickSupport;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Helper.Toasti;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.Promo;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarPromoActivity extends AppCompatActivity {

    private ApiService api = ApiConfig.getInstanceRetrofit();
    private SharedPref s;

    private RecyclerView.Adapter mAdapter;
    private Produk produk;

    private String extra, idSub, toolbarText, namaProduk, promoProduk;
    
    private Toolbar toolbar;
    private SwipeRefreshLayout swipeRefrash;
    private TextView tvToolbarTitle;
    private ImageView btnAdd1;
    private ImageView btnAdd2;
    private LinearLayout divProdukKosong;
    private TextView tvPesan;
    private Button btnTambahPromo;
    private RecyclerView rv;
    private ProgressBar pd;
    private TextView tvTitle;
    private LinearLayout btnReload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_promo);
        initView();

        s = new SharedPref(this);
        produk =s.getProduk();
        extra = getIntent().getStringExtra("extra");
        idSub = getIntent().getStringExtra("idSub");
        promoProduk = getIntent().getStringExtra("produk");

        if (extra != null){
            if (extra.equals("pilihPromo")){
                getPromoById();
                toolbarText = "Pilih Promo";
            } else {
                getPromo();
                toolbarText = "Promo";
            }
        }

        setToolbar();
        mainButton();
        pullrefrash();

        CallFunction.setRefrashPromo(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (extra != null){
                    if (extra.equals("pilihPromo")){
                        getPromoById();
                    } else {
                        getPromo();
                    }
                }
                return null;
            }
        });
    }

    private void mainButton() {
        btnAdd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DaftarPromoActivity.this, TambahPromoActivity.class);
                if (extra != null){
                    if (extra.equals("pilihPromo")){
                        i.putExtra("extra", idSub);
                    }
                }
                startActivity(i);
            }
        });

        btnTambahPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DaftarPromoActivity.this, TambahPromoActivity.class);
                if (extra != null){
                    if (extra.equals("pilihPromo")){
                        i.putExtra("extra", idSub);
                    }
                }
                startActivity(i);
            }
        });

        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPromo();
            }
        });
    }

    private void getPromo() {
        api.getPromo(s.getApi(), s.getIdUser(), s.getSession()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                if (response.body().success == 1) {
                    divProdukKosong.setVisibility(View.GONE);
                    final ArrayList<Promo> mItem = response.body().promo;
                    mAdapter = new AdapterPromo(mItem, DaftarPromoActivity.this);
                    rv.setLayoutManager(new LinearLayoutManager(DaftarPromoActivity.this, LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);

                    ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                        @Override
                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                            onItemSelectedPromo(mItem.get(position));
                        }
                    });

                } else {
                    divProdukKosong.setVisibility(View.VISIBLE);
                    tvTitle.setText("Daftar Promo Kosong");
                    tvPesan.setText("Tambah Promo Anda untuk meningkatkan penjualan");
                    btnTambahPromo.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                tvPesan.setText("Gagal memuat data");
                tvTitle.setText("Tab untuk memuat ulang");
                divProdukKosong.setVisibility(View.VISIBLE);
                btnTambahPromo.setVisibility(View.GONE);
                SweetAlert.onFailure(DaftarPromoActivity.this, t.getMessage());
            }
        });
    }

    private void getPromoById(){
        api.getPromoById(s.getApi(), s.getIdUser(), s.getSession(), idSub).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                if (response.body().success == 1) {
                    divProdukKosong.setVisibility(View.GONE);
                    final ArrayList<Promo> mItem = response.body().promo;
                    mAdapter = new AdapterPromo(mItem, DaftarPromoActivity.this);
                    rv.setLayoutManager(new LinearLayoutManager(DaftarPromoActivity.this, LinearLayoutManager.VERTICAL, false));
                    rv.setAdapter(mAdapter);

                    ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                        @Override
                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                            if (promoProduk.equals("produk")){
                                onItemSelectedPromo(mItem.get(position));
                            } else {
                                onItemSelected(mItem.get(position));
                            }
                        }
                    });

                } else {
                    divProdukKosong.setVisibility(View.VISIBLE);
                    tvTitle.setText("Daftar Promo Kosong");
                    tvPesan.setText("Tidak ada Promo pada sub-kategori terkait");
                    btnTambahPromo.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {
                pd.setVisibility(View.GONE);
                swipeRefrash.setRefreshing(false);
                tvPesan.setText("Gagal memuat data");
                tvTitle.setText("Tab untuk memuat ulang");
                divProdukKosong.setVisibility(View.VISIBLE);
                btnTambahPromo.setVisibility(View.GONE);
                SweetAlert.onFailure(DaftarPromoActivity.this, t.getMessage());
            }
        });
    }

    private void onItemSelectedPromo(final Promo p){
        Intent i = new Intent(this, DetailPromoActivity.class);
        s.setPromo(p);
        startActivity(i);
    }

    private void onItemSelected(final Promo p) {
        String sPesan = "Apakah Anda yakin memilih " + "<b>" +"Promo dengan potongan "+ new Helper().convertRupiah(Integer.valueOf(p.PR_POTONGAN)) + "</b>" +" untuk produk terkait?";
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Pilih Promo")
                .setContentText(String.valueOf(Html.fromHtml(sPesan)))
                .setCancelText("Batal")
                .setConfirmText("Iya")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        pDialog.cancel();
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(final SweetAlertDialog sweetAlertDialog) {
                        SweetAlert.onLoading(DaftarPromoActivity.this);
                        api.editProduk(s.getApi(), s.getIdUser(), s.getSession(), produk.ID_BARANG, produk.ID_SUB_KATEGORI, p.ID_PROMO, produk.BA_NAME, produk.BA_PRICE, produk.BA_SKU, produk.BA_DESCRIPTION, produk.BA_STOCK, produk.BA_WEIGHT, produk.BA_CONDITION, produk.BA_IMAGE).enqueue(new Callback<ResponModel>() {
                            @Override
                            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {
                                SweetAlert.dismis();
                                if (response.isSuccessful()) {
                                    if (response.body().success == 1) {
                                        sweetAlertDialog
                                                .setTitleText("Berhasil")
                                                .setContentText("Promo Berhasil Ditambahkan")
                                                .setConfirmText("OK")
                                                .showCancelButton(false)
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweet) {
                                                        onBackPressed();
                                                        finish();
                                                        sweetAlertDialog.dismiss();
                                                    }
                                                })
                                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);


                                    } else {
                                        Toasti.error(DaftarPromoActivity.this, response.body().message);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponModel> call, Throwable t) {
                                SweetAlert.dismis();
                                SweetAlert.onFailure(DaftarPromoActivity.this, t.getMessage());
                            }
                        });
                    }
                })
                .show();
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (extra != null){
                    if (extra.equals("pilihPromo")){
                        getPromoById();
                        toolbarText = "Pilih Promo";
                    } else {
                        getPromo();
                        toolbarText = "Promo";
                    }
                }
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        tvToolbarTitle.setText(toolbarText);
        getSupportActionBar().setTitle("GoldenShop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        swipeRefrash = (SwipeRefreshLayout) findViewById(R.id.swipeRefrash);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbarTitle);
        btnAdd1 = (ImageView) findViewById(R.id.btn_add1);
        btnAdd2 = (ImageView) findViewById(R.id.btn_add2);
        divProdukKosong = (LinearLayout) findViewById(R.id.div_produkKosong);
        tvPesan = (TextView) findViewById(R.id.tv_pesan);
        btnTambahPromo = (Button) findViewById(R.id.btn_tambahPromo);
        rv = (RecyclerView) findViewById(R.id.rv);
        pd = (ProgressBar) findViewById(R.id.pd);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        btnReload = (LinearLayout) findViewById(R.id.btn_reload);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
