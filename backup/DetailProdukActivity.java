package id.co.cancreative.goldenshop.Activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.cancreative.goldenshop.Helper.ChildAnimation;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.Helper;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Model.ModelProduk;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.R;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.TbTerakhirDilihat;

public class DetailProdukActivity extends AppCompatActivity implements ViewPagerEx.OnPageChangeListener {

    private ArrayList<ModelProduk> mProduk = new ArrayList<>();
    private ModelProduk data;
    private Produk produk;
    private TbTerakhirDilihat dilihat;
    private SharedPref s;

    private String dataInten;

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;
    private Toolbar toolbar;
    private ImageView tvImgProduk, btnBack, imgBintang;
    private TextView tvNamaProduk;
    private CardView card;
    private RelativeLayout layout;
    private TextView tvDiscount, tvHargaAsli, tvHargaProduk;
    private ImageView image, image2, image3, image4, image5;
    private TextView tvStok;
    private TextView tvKeterangan;
    private CircleImageView imgUser;
    private RecyclerView rvRecomedPrdk;
    private LinearLayout divFooter;
    private LinearLayout btnBeli;
    private TextView tvTerjual, tvKondisi, tvNamaToko, tvAsalToko;
    private LinearLayout btnAddToCart;
    private CircleImageView imgPenjual;
    private SliderLayout sliderHome;
    private RelativeLayout divPopUpImage;
    private ImageView btnClose;
    private PhotoView imgView;
    private RelativeLayout divPromo;
    private TextView tvLihatUlasan;
    String idBarang;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);
        initView();

        s = new SharedPref(this);
        data = new ModelProduk();
        dataInten = getIntent().getStringExtra("dataInten");
        if (dataInten.equals("terakhir")) {
            dilihat = s.getProdukT();
            setupSliderD();
            setValueD();
        } else {
            produk = s.getProduk();
            setValue();
            setupSlider();
        }

        getIntentExtra();
//        setValue();
        mainButton();
        setToolbar();
//        setupSlider();
    }

    private void setupSlider() {

        String sliderList = produk.BA_IMAGE.replace("|", " ");
        final String strArray[] = sliderList.split(" ");

        for (final String d : strArray) {
            TextSliderView textSliderView = new TextSliderView(getApplicationContext());
            textSliderView
                    .image(Config.URL_produkGolden + d)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            divPopUpImage.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(Config.URL_produkGolden + d)
                                    .error(R.drawable.ic_cat1)
                                    .placeholder(R.drawable.image_loading)
                                    .into(imgView);
                        }
                    });

            sliderHome.addSlider(textSliderView);
        }

        sliderHome.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderHome.setCustomAnimation(new ChildAnimation());
//        sliderHome.setCustomIndicator(indicator);
        sliderHome.setDuration(8000);
        sliderHome.addOnPageChangeListener(this);
    }

    private void setupSliderD() {

        String sliderList = dilihat.gambar.replace("|", " ");
        final String strArray[] = sliderList.split(" ");

        for (final String d : strArray) {
            TextSliderView textSliderView = new TextSliderView(getApplicationContext());
            textSliderView
                    .image(Config.URL_produkGolden + d)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            divPopUpImage.setVisibility(View.VISIBLE);
                            Glide.with(getApplicationContext()).load(Config.URL_produkGolden + d)
                                    .error(R.drawable.ic_cat1)
                                    .placeholder(R.drawable.image_loading)
                                    .into(imgView);
                        }
                    });

            sliderHome.addSlider(textSliderView);
        }

        sliderHome.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderHome.setCustomAnimation(new ChildAnimation());
//        sliderHome.setCustomIndicator(indicator);
        sliderHome.setDuration(8000);
        sliderHome.addOnPageChangeListener(this);
    }

    private void getIntentExtra() {
        data = getIntent().getParcelableExtra("data");
    }

    private void setValue() {

        if (produk.PR_CODE != null) {
            divPromo.setVisibility(View.VISIBLE);
        }

        final int min1 = 10;
        final int max1 = 35;
        final int randomTerjaul = new Random().nextInt((max1 - min1) + 1) + min1;

        final int min = 10;
        final int max = 30;
        final int random = new Random().nextInt((max - min) + 1) + min;
        int hargaAsli = Integer.valueOf(produk.BA_PRICE);
        int diskon = (int) (hargaAsli * ((double) random / 100));
        int hargaAkhir = hargaAsli - diskon;

        idBarang = ""+produk.ID_BARANG;
        tvNamaProduk.setText(produk.BA_NAME);
        tvHargaAsli.setText(new Helper().convertRupiah(hargaAsli));
        tvHargaAsli.setPaintFlags(tvHargaAsli.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvHargaProduk.setText(new Helper().convertRupiah(hargaAsli));
        tvDiscount.setText(String.valueOf(random) + "%");
        tvStok.setText(produk.BA_STOCK);
        tvTerjual.setText(String.valueOf(randomTerjaul));
        tvKondisi.setText(produk.BA_CONDITION);
        tvKeterangan.setText(produk.BA_DESCRIPTION);
        tvNamaToko.setText(produk.TOK_NAME);
        tvAsalToko.setText(produk.TOK_KOTA);

        String image = new Helper().splitText(produk.BA_IMAGE);

        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.img_kosong)
                .error(R.drawable.img_kosong)
                .noFade()
                .into(tvImgProduk);

        Picasso.with(this)
                .load(Config.url_toko + produk.TOK_FOTO)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(imgPenjual);

    }

    private void setValueD() {

        if (dilihat.kodePromo != null) {
            divPromo.setVisibility(View.VISIBLE);
        }

        final int min1 = 10;
        final int max1 = 35;
        final int randomTerjaul = new Random().nextInt((max1 - min1) + 1) + min1;

        final int min = 10;
        final int max = 30;
        final int random = new Random().nextInt((max - min) + 1) + min;
        int hargaAsli = Integer.valueOf(dilihat.harga);
        int diskon = (int) (hargaAsli * ((double) random / 100));
        int hargaAkhir = hargaAsli - diskon;

        idBarang = ""+dilihat.idProduk;
        tvNamaProduk.setText(dilihat.nama);
        tvHargaAsli.setText(new Helper().convertRupiah(hargaAsli));
        tvHargaAsli.setPaintFlags(tvHargaAsli.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        tvHargaProduk.setText(new Helper().convertRupiah(hargaAkhir));
        tvDiscount.setText(String.valueOf(random) + "%");
        tvStok.setText("" + dilihat.stok);
        tvTerjual.setText(String.valueOf(randomTerjaul));
        tvKondisi.setText(dilihat.kondisi);
        tvKeterangan.setText(dilihat.deskripsi);
        tvNamaToko.setText(dilihat.penjual);
        tvAsalToko.setText(dilihat.kotaPenjual);

        String image = new Helper().splitText(dilihat.gambar);

        Picasso.with(this)
                .load(Config.URL_produkGolden + image)
                .placeholder(R.drawable.img_kosong)
                .error(R.drawable.img_kosong)
                .noFade()
                .into(tvImgProduk);

        Picasso.with(this)
                .load(Config.url_toko + dilihat.fotoPenjual)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.ic_profil)
                .noFade()
                .into(imgPenjual);

    }

    private void mainButton() {

        divPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), DaftarPromoActivity.class);
                i.putExtra("extra", "pilihPromo");
                i.putExtra("produk", "produk");
                i.putExtra("idSub", produk.ID_SUB_KATEGORI);
                startActivity(i);
            }
        });

        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), TambahKeranjangActivity.class);
                startActivity(i);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvLihatUlasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),LihatUlasanActivity.class);
                intent.putExtra("id_barang",""+idBarang);
                startActivity(intent);
            }
        });

        btnBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (s.getStatusLogin()) {
                    Intent i = new Intent(getApplicationContext(), PesananAturActivity.class);
                    startActivity(i);
                    return;
                }

                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                divPopUpImage.setVisibility(View.GONE);
            }
        });


    }

//    private void getProduk() {
//        mProduk.addAll(DataProduct.getListData());
//        layoutManager = new GridLayoutManager(this, 2);
//        mAdapter = new AdapterProdukRecomed(mProduk, this);
//        rvRecomedPrdk.setLayoutManager(layoutManager);
//        rvRecomedPrdk.setAdapter(mAdapter);
//    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle("Toolbar");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_produk, menu);
        return true;
    }

    private void initView() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        tvImgProduk = (ImageView) findViewById(R.id.tv_imgProduk);
        btnBack = (ImageView) findViewById(R.id.btn_back);
        imgBintang = (ImageView) findViewById(R.id.img_bintang);
        tvNamaProduk = (TextView) findViewById(R.id.tv_namaProduk);
        card = (CardView) findViewById(R.id.card);
        layout = (RelativeLayout) findViewById(R.id.layout);
        tvDiscount = (TextView) findViewById(R.id.tv_discount);
        tvHargaAsli = (TextView) findViewById(R.id.tv_hargaAsli);
        tvHargaProduk = (TextView) findViewById(R.id.tv_hargaProduk);
        image = (ImageView) findViewById(R.id.image);
        tvStok = (TextView) findViewById(R.id.tv_stok);
        image2 = (ImageView) findViewById(R.id.image2);
        tvKeterangan = (TextView) findViewById(R.id.tv_keterangan);
        imgUser = (CircleImageView) findViewById(R.id.img_penjual);
        image3 = (ImageView) findViewById(R.id.image3);
        image4 = (ImageView) findViewById(R.id.image4);
        image5 = (ImageView) findViewById(R.id.image5);
        rvRecomedPrdk = (RecyclerView) findViewById(R.id.rv_recomedPrdk);
        divFooter = (LinearLayout) findViewById(R.id.div_footer);
        tvTerjual = (TextView) findViewById(R.id.tv_terjual);
        tvKondisi = (TextView) findViewById(R.id.tv_kondisi);
        btnBeli = (LinearLayout) findViewById(R.id.btn_beli);
        tvNamaToko = (TextView) findViewById(R.id.tv_namaToko);
        tvAsalToko = (TextView) findViewById(R.id.tv_asalToko);
        btnAddToCart = (LinearLayout) findViewById(R.id.btn_addToCart);
        imgPenjual = (CircleImageView) findViewById(R.id.img_penjual);
        sliderHome = (SliderLayout) findViewById(R.id.slider_home);
        divPopUpImage = (RelativeLayout) findViewById(R.id.div_popUpImage);
        btnClose = (ImageView) findViewById(R.id.btn_close);
        imgView = (PhotoView) findViewById(R.id.img_view);
        divPromo = (RelativeLayout) findViewById(R.id.div_promo);
        tvLihatUlasan = (TextView) findViewById(R.id.tv_lihat_ulasan);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
