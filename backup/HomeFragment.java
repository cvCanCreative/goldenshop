package id.co.cancreative.goldenshop;


import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.HashMap;

import id.co.cancreative.goldenshop.Activity.DaftarKategoriActivity;
import id.co.cancreative.goldenshop.Activity.DaftarProdukActivity;
import id.co.cancreative.goldenshop.Adapter.AdapterCategory;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukGS;
import id.co.cancreative.goldenshop.Adapter.AdapterProdukTrading;
import id.co.cancreative.goldenshop.App.ApiConfig;
import id.co.cancreative.goldenshop.App.ApiConfigDemo;
import id.co.cancreative.goldenshop.App.ApiService;
import id.co.cancreative.goldenshop.Helper.ChildAnimation;
import id.co.cancreative.goldenshop.Helper.Config;
import id.co.cancreative.goldenshop.Helper.SaveData;
import id.co.cancreative.goldenshop.Helper.SharedPref;
import id.co.cancreative.goldenshop.Helper.SweetAlert;
import id.co.cancreative.goldenshop.Model.ModelCategory;
import id.co.cancreative.goldenshop.Model.Produk;
import id.co.cancreative.goldenshop.Model.ResponModel;
import id.co.cancreative.goldenshop.Model.ResponModelSerializ;
import id.co.cancreative.goldenshop.SQLiteTable.TerakhirDilihat.AppDbTerakhirDilihat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ViewPagerEx.OnPageChangeListener {

    private ApiService apiDemo = ApiConfigDemo.getInstanceRetrofit();
    private ApiService api = ApiConfig.getInstanceRetrofit();
    private View view;
    private SharedPref s;
    private AppDbTerakhirDilihat db;

    // Slider
    private SliderLayout sliderHome;
    private PagerIndicator indicator;

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter mAdapter;

    private ArrayList<Produk> mItem = new ArrayList<>();
    private ArrayList<Produk> mProduk = new ArrayList<>();
    private ArrayList<ModelCategory> mKategori = new ArrayList<>();

    private RelativeLayout lySearch;
    private ImageView btnSearch;
    private ImageView btnCart;
    private RecyclerView rvFlash;
    private RecyclerView rvKategori;
    private RecyclerView rvTradingProduct;
    private RecyclerView rvMahasiswa;
    private ProgressBar pd;
    private ProgressBar pd1;
    private ProgressBar pd2;
    private SwipeRefreshLayout swipeRefrash;
    private TextView btnAllGSProduk;
    private TextView btnAllKategori;
    private TextView btnAlltradingProduk;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);

        s = new SharedPref(getActivity());
        db = Room.databaseBuilder(getActivity(), AppDbTerakhirDilihat.class, Config.db_terakhirDilihat).allowMainThreadQueries().build();


        mainButton();
        getSaveData();
        pullrefrash();
        setupSlider();

        return view;
    }

    private void mainButton() {
        btnAllGSProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DaftarProdukActivity.class);
                i.putExtra("extra", "gsproduk");
                startActivity(i);
            }
        });

        btnAllKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DaftarKategoriActivity.class);
                i.putExtra("extra", "kategori");
                startActivity(i);
            }
        });

        btnAlltradingProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DaftarProdukActivity.class);
                i.putExtra("extra", "tradingProduk");
                startActivity(i);
            }
        });

    }

    private void getSaveData() {
        mItem = SaveData.getGsProduk();
        mProduk = SaveData.getTradingProduk();
        mKategori = SaveData.getKategori();

        if (mItem.size() != 0 && mProduk.size() != 0 && mKategori.size() != 0) {
            pd2.setVisibility(View.GONE);
            pd1.setVisibility(View.GONE);
            pd.setVisibility(View.GONE);

            RecyclerView.Adapter mAdapter = new AdapterProdukGS(mItem, getActivity(), db);
            rvFlash.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            rvFlash.setAdapter(mAdapter);

            mAdapter = new AdapterProdukTrading(mProduk, getActivity(), db);
            rvTradingProduct.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            rvTradingProduct.setAdapter(mAdapter);

            mAdapter = new AdapterCategory(mKategori, getActivity());
            rvKategori.setLayoutManager(new GridLayoutManager(getActivity(), 3));
            rvKategori.setAdapter(mAdapter);

        } else {
            getKategori();
            getProduk();
        }
    }

    private void getProduk() {

        api.getProdukGS(s.getApi()).enqueue(new Callback<ResponModelSerializ>() {
            @Override
            public void onResponse(Call<ResponModelSerializ> call, Response<ResponModelSerializ> response) {
                swipeRefrash.setRefreshing(false);
                pd.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ResponModelSerializ produk = response.body();

                    mItem = (ArrayList<Produk>) produk.products;
                    RecyclerView.Adapter mAdapter = new AdapterProdukGS(mItem, getActivity(),db);
                    rvFlash.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                    rvFlash.setAdapter(mAdapter);
                }

            }

            @Override
            public void onFailure(Call<ResponModelSerializ> call, Throwable t) {
                if (getActivity() != null) {
                    SweetAlert.onFailure(getActivity(), t.getMessage());
                }
            }
        });

        api.getAllProduk(s.getApi(), "4").enqueue(new Callback<ResponModelSerializ>() {
            @Override
            public void onResponse(Call<ResponModelSerializ> call, Response<ResponModelSerializ> response) {
                swipeRefrash.setRefreshing(false);
                pd2.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ResponModelSerializ produk = response.body();

                    mProduk = (ArrayList<Produk>) produk.products;
//                    mProduk.addAll(DataProduct.getListData());
                    mAdapter = new AdapterProdukTrading(mProduk, getActivity(),db);
                    rvTradingProduct.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                    rvTradingProduct.setAdapter(mAdapter);
                }

            }

            @Override
            public void onFailure(Call<ResponModelSerializ> call, Throwable t) {
                SweetAlert.onFailure(getActivity(), t.getMessage());
            }
        });

    }

    private void getKategori() {
        api.getKategori(s.getApi()).enqueue(new Callback<ResponModel>() {
            @Override
            public void onResponse(Call<ResponModel> call, Response<ResponModel> response) {

                pd1.setVisibility(View.GONE);
                mKategori = response.body().kategori;
                mAdapter = new AdapterCategory(mKategori, getActivity());
                rvKategori.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                rvKategori.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<ResponModel> call, Throwable t) {

            }
        });

    }

    private void setupSlider() {

        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("TM 1", R.drawable.asset_slider_goldenshop_1);
        file_maps.put("TM 2", R.drawable.asset_slider_goldenshop_2);

        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            sliderHome.addSlider(textSliderView);
        }

        sliderHome.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderHome.setCustomAnimation(new ChildAnimation());
//        sliderHome.setCustomIndicator(indicator);
        sliderHome.setDuration(8000);
        sliderHome.addOnPageChangeListener(this);
    }

    private void pullrefrash() {

        // Setup refresh listener which triggers new data loading
        swipeRefrash.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getKategori();
                getProduk();
            }
        });
        // Configure the refreshing colors
        swipeRefrash.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    private void initView(View view) {
        lySearch = (RelativeLayout) view.findViewById(R.id.ly_search);
        btnSearch = (ImageView) view.findViewById(R.id.btn_search);
        btnCart = (ImageView) view.findViewById(R.id.btn_cart);
        rvFlash = (RecyclerView) view.findViewById(R.id.rv_flash);
        sliderHome = (SliderLayout) view.findViewById(R.id.slider_home);
        rvKategori = (RecyclerView) view.findViewById(R.id.rv_kategori);
        rvTradingProduct = (RecyclerView) view.findViewById(R.id.rv_tradingProduct);
        rvMahasiswa = (RecyclerView) view.findViewById(R.id.rv_mahasiswa);
        pd = (ProgressBar) view.findViewById(R.id.pd);
        pd1 = (ProgressBar) view.findViewById(R.id.pd_1);
        pd2 = (ProgressBar) view.findViewById(R.id.pd_2);
        swipeRefrash = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefrash);
        btnAllGSProduk = (TextView) view.findViewById(R.id.btn_allGSProduk);
        btnAllKategori = (TextView) view.findViewById(R.id.btn_allKategori);
        btnAlltradingProduk = (TextView) view.findViewById(R.id.btn_alltradingProduk);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SaveData.setGsProduk(mItem);
        SaveData.setKategori(mKategori);
        SaveData.setTradingProduk(mProduk);
    }
}
