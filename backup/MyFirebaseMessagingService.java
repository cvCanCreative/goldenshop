package id.co.cancreative.goldenshop.NotificationPackage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.cancreative.goldenshop.Activity.DaftarHistoryTransActivity;
import id.co.cancreative.goldenshop.Activity.DaftarTransaksiActivity;
import id.co.cancreative.goldenshop.Activity.DaftarTransaksiTokoActivity;
import id.co.cancreative.goldenshop.Activity.SplashActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }else{

        }
    }

    private void handleDataMessage(JSONObject json) {
        try {
            Intent resultIntent = new Intent();
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
            String imageUrl = data.getString("image");
            String datenotifikasi = data.getString("timestamp");

            //Convert Date To TimeStamp
            SimpleDateFormat sdfnotif = new SimpleDateFormat("yyyy-MM-DD");
            Date dateNotif = sdfnotif.parse(datenotifikasi);
            Timestamp timestamp = new Timestamp(dateNotif.getTime());


            JSONObject payload = data.getJSONObject("payload");
            String type_data = payload.getString("type");

            //Mulai  Bagian Pembeli
            if (type_data.equals("pembayaran")){
                resultIntent = new Intent(getApplicationContext(), DaftarHistoryTransActivity.class);
            }else if (type_data.equals("proses_admin")){
                resultIntent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                resultIntent.putExtra("FRAGMENT_ID", "0");
                if (NotificationUtils.isAppIsInBackground(getApplicationContext())){
                    resultIntent.putExtra("status_notif",true);
                }else {
                    resultIntent.putExtra("status_notif",false);
                }

            }else if (type_data.equals("proses_vendor_user")){
                resultIntent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                resultIntent.putExtra("FRAGMENT_ID", "2");
                if (NotificationUtils.isAppIsInBackground(getApplicationContext())){
                    resultIntent.putExtra("status_notif",true);
                }else {
                    resultIntent.putExtra("status_notif",false);
                }

            }else if (type_data.equals("batal")){
                resultIntent = new Intent(getApplicationContext(), DaftarTransaksiActivity.class);
                resultIntent.putExtra("FRAGMENT_ID", "4");
                if (NotificationUtils.isAppIsInBackground(getApplicationContext())){
                    resultIntent.putExtra("status_notif",true);
                }else {
                    resultIntent.putExtra("status_notif",false);
                }

            }
            //Selesai  Bagian Pembeli
            //Mulai Bagian Penjual
            else if (type_data.equals("proses_vendor")){
                resultIntent = new Intent(getApplicationContext(), DaftarTransaksiTokoActivity.class);
                resultIntent.putExtra("FRAGMENT_ID", "0");
                if (NotificationUtils.isAppIsInBackground(getApplicationContext())){
                    resultIntent.putExtra("status_notif",true);
                }else {
                    resultIntent.putExtra("status_notif",false);
                }

            }
            //Selesai  Bagian Penjual
            else {
                resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
            }

            resultIntent.putExtra("title", title);
            resultIntent.putExtra("timestamp", timestamp);
            resultIntent.putExtra("type_data", type_data);
            resultIntent.putExtra("image", imageUrl);

            // check for image attachment
            if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, String.valueOf(timestamp), resultIntent);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, String.valueOf(timestamp), resultIntent, imageUrl);
            }

        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}